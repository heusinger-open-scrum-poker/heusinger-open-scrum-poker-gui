import { environment } from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe("place card", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  });

  it("can place card", () => {
    cy.get(".card")
      .contains(/^1$/)
      .should("have.css", "cursor")
      .should("include", "pointer");
  });

  it("cannot place card during reveal mode", () => {
    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Take Back");
    cy.contains("Reveal").click();
    cy.contains("New Round");

    cy.get(".card").contains(/^1$/).should("not.be.visible");
    if (environment.newStatistics) {
      cy.get("app-statistics").should("be.visible");
    } else {
      cy.get("app-stats").should("be.visible");
    }
    cy.get("app-card-hand").should("not.exist");
  });

  it("cannot place card during inspection mode", () => {
    cy.contains("Inspect").click();
    cy.get("app-card-hand").should("not.be.visible");
  });

  it("can take back a card", () => {
    cy.get(".card").contains(/^1$/).click();

    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.get(".card").contains(/^1$/).should("not.be.visible");
    cy.contains("Take Back");

    cy.contains("Take Back").click();

    cy.contains("Take Back").should("not.exist");
    cy.get(".card").contains(/^1$/).should("be.visible");
  });
});
