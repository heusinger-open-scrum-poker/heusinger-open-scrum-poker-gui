import { environment } from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe("leave room", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  });

  it("can leave room during estimation", () => {
    cy.contains("Leave").click();

    cy.get(".confirmation-box-leave")
      .contains("Ok")
      .click();

    cy.location().should((loc) => {
      expect(loc.pathname).to.equal("/");
    });
  });

  it("can leave room during inspection", () => {
    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.get("app-sidebar").within(() => {
      cy.contains("Reveal").click();
      cy.contains("New Round");
      cy.contains("Leave").click();
    });

    cy.get(".confirmation-box-leave")
      .contains("Ok")
      .click();

    cy.location().should((loc) => {
      expect(loc.pathname).to.equal("/");
    });
  });
});
