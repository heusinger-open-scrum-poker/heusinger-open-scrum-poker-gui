import { openRegisterPage, randomCredentials, testCreateRoom } from "../e2e/utilities";

describe('import', () => {
  it('can not import if not signed in', () => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });

    cy.contains('Tasks').click();
    cy.contains('Game #1').parent().get('.import-button').click();

    cy.get('app-plans-page').filter(':visible').within(() => {
      cy.contains('You would like to import tasks?');
      cy.contains('Importing is so wonderfully simple and works directly from your favorite project management tool - you can get access here.');
      cy.get('button.modal-close').click();
    })
  })

  it('can import from jira when connected', () => {
    interceptJiraNetwork();
    signUp();

    cy.contains('Tasks').click();
    cy.contains('Game #1').parent().get('.import-button').click();

    cy.get('app-import-tasks-modal').within(() => {
      cy.contains('Jira').click();
      cy.get('button').contains('Connect').click();

      cy.contains('Your Jira instances');
      cy.contains('kehrwasser').click();
      cy.get('button').contains('Choose').click();

      cy.contains('KEH Board').click();
      cy.get('button').contains('Choose').click();

      cy.contains('KEH Sprint 79').click();

      cy.contains('Kontextmenüs verbessern');
      cy.contains('Separates Repository für OpenAPI Spezifikation');
      cy.contains('Blog reparieren');
      cy.get('modal-footer-v2 button').contains('Import').click();
    })

    cy.get('app-games').contains('Kontextmenüs verbessern');
    cy.get('app-games').contains('Blog reparieren');

    cy.get('app-games').contains('Separates Repository für OpenAPI Spezifikation').click();
    cy.get('.subtask-input').should('have.value', 'Untersuchen wie man das Backend auf Konformität automatisiert testen kann');
  })

  function signUp(): void {
    const { email, password } = randomCredentials();
    cy.visit('/');

    openRegisterPage(cy);

    cy.get('app-register-page').get('input[placeholder="Email"]').type(email);
    cy.get('app-register-page').get('input[placeholder="Password"]').first().type(password);
    cy.get('app-register-page').get('input[placeholder="Repeat password"]').first().type(password);
    cy.get('app-register-page').contains('Create Account').click();

    testCreateRoom(cy, {
      visit: false,
      playerName: "testuser",
    });
  }

  function interceptJiraNetwork(): void {
    cy.intercept(
      {
        method: 'GET',
        url: '/jira/resources*',
      },
      [
        {
          "id": "6b6ed08d-bb6e-4103-80a1-d7f087084a7e",
          "url": "https://kehrwasser.atlassian.net",
          "name": "kehrwasser",
          "avatarUrl": "https://site-admin-avatar-cdn.prod.public.atl-paas.net/avatars/240/globe.png"
        }
      ]
    ).as('getResources');

    cy.intercept(
      {
        method: 'GET',
        url: '/jira/isConnected*',
      },
      {
        isConnected: true,
      }
    ).as('getBoards');

    cy.intercept(
      {
        method: 'GET',
        url: '/jira/boards*',
      },
      [
        {
          "id": 3,
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/board/3",
          "name": "KEH Board",
          "type": "scrum",
          "location": {
            "projectId": 10002,
            "displayName": "Kehrwasser (KEH)",
            "projectName": "Kehrwasser",
            "projectKey": "KEH",
            "projectTypeKey": "software",
            "avatarURI": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/universal_avatar/view/type/project/avatar/10513?size=small",
            "name": "Kehrwasser (KEH)"
          }
        },
        {
          "id": 11,
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/board/11",
          "name": "AK Board",
          "type": "scrum",
          "location": {
            "projectId": 10012,
            "displayName": "aldi-sued-compdb (AK)",
            "projectName": "aldi-sued-compdb",
            "projectKey": "AK",
            "projectTypeKey": "software",
            "avatarURI": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/universal_avatar/view/type/project/avatar/10410?size=small",
            "name": "aldi-sued-compdb (AK)"
          }
        },
      ]
    ).as('getBoards');

    cy.intercept(
      {
        method: 'GET',
        url: '/jira/sprints*',
      },
      [
        {
          "id": 136,
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/sprint/136",
          "state": "active",
          "name": "KEH Sprint 77",
          "startDate": "2024-08-23T09:46:46.146Z",
          "endDate": "2024-09-13T09:46:00.000Z",
          "completeDate": null,
          "createdDate": "2024-07-26T06:45:06.048Z",
          "originBoardId": 3,
          "goal": ""
        },
        {
          "id": 137,
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/sprint/137",
          "state": "future",
          "name": "KEH Sprint 78",
          "startDate": null,
          "endDate": null,
          "completeDate": null,
          "createdDate": "2024-08-23T10:43:42.166Z",
          "originBoardId": 3,
          "goal": null
        },
        {
          "id": 138,
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/sprint/138",
          "state": "future",
          "name": "KEH Sprint 79",
          "startDate": null,
          "endDate": null,
          "completeDate": null,
          "createdDate": "2024-08-31T08:33:58.535Z",
          "originBoardId": 3,
          "goal": null
        }
      ]
    ).as('getSprints');

    cy.intercept(
      {
        method: 'GET',
        url: '/jira/issues*',
      },
      [
        {
          "id": "13782",
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/issue/13782",
          "key": "KEH-2645",
          "issuetypeId": "10001",
          "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10001",
          "timeestimate": 0,
          "description": "Als User, möchte ich ein mächtigeres Kontextmenü.\n\nAls Spieler, der einen einen anderen Spieler zum Beobachter gemacht hat, möchte ich diese Aktion im Zweifel rückgängig machen.\n\nIch möchte das Kontextmenü auch über den Tisch aufrufen können.\n\nAkzeptanzkriterien:\n\n* Der Spieler X, wenn er Spieler Y zum Beobachter gemacht hat, hat dieser Spieler im Kontaktmenü die Option “Beobachten rückgägnig machen” bzw. “Zum Spieler machen”\n* Bei Klick auf Spielername bzw. Karte eines Spieler am Tisch (nicht in der Spielerliste der Sidebar), dann soll das selbe Kontextmenü wie in der Spielerliste erscheinen",
          "rank": "0|hzzzx8:hl39i00060cvu1hzzzuixv",
          "summary": "Kontextmenüs verbessern",
          "subtasks": []
        },
        {
          "id": "13536",
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/issue/13536",
          "key": "KEH-2399",
          "issuetypeId": "10001",
          "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10001",
          "timeestimate": 0,
          "description": "Als Frontendentwickler möchte ich eine Single Source of Truth gegen welche ich entwickeln kann.\n\nAK\n\n* OpenAPI Spezifkation ausgelagert und versioniert\n* Clientlibrary in Typescript ausgelagert\n* Frontend und CLI hängen von derselben Clientlibrary ab\n* Tests im Backend sichern Konformität mit der Spezifikation",
          "rank": "0|hzzzx8:hl39i00060cvu1hzzzuiy",
          "summary": "Separates Repository für OpenAPI Spezifikation",
          "subtasks": [
            {
              "id": "13677",
              "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issue/13677",
              "key": "KEH-2540",
              "issuetypeId": "10003",
              "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10003",
              "summary": "Untersuchen wie man das Backend auf Konformität automatisiert testen kann"
            },
            {
              "id": "13678",
              "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issue/13678",
              "key": "KEH-2541",
              "issuetypeId": "10003",
              "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10003",
              "summary": "Backend-Test umsetzen"
            },
            {
              "id": "13664",
              "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issue/13664",
              "key": "KEH-2527",
              "issuetypeId": "10003",
              "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10003",
              "summary": "OpenAPI Definition vom Backend exportiert"
            },
            {
              "id": "13665",
              "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issue/13665",
              "key": "KEH-2528",
              "issuetypeId": "10003",
              "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10003",
              "summary": "Openapi Repository erstellt"
            },
            {
              "id": "13666",
              "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issue/13666",
              "key": "KEH-2529",
              "issuetypeId": "10003",
              "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10003",
              "summary": "Client Library Repository erstellt"
            },
            {
              "id": "13676",
              "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issue/13676",
              "key": "KEH-2539",
              "issuetypeId": "10003",
              "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10003",
              "summary": "Abhängigkeit zur Client Library in Frontend und CLI hinzufügen"
            }
          ]
        },
        {
          "id": "13639",
          "self": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/agile/1.0/issue/13639",
          "key": "KEH-2502",
          "issuetypeId": "10001",
          "issuetypeSelf": "https://api.atlassian.com/ex/jira/6b6ed08d-bb6e-4103-80a1-d7f087084a7e/rest/api/2/issuetype/10001",
          "timeestimate": 0,
          "description": null,
          "rank": "0|hzzzx8:hl39i00060cvu1hzzzuj",
          "summary": "Blog reparieren",
          "subtasks": []
        },
      ]
    ).as('getIssues');
  }
})
