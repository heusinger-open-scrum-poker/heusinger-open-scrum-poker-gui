import { ignoreResizeObserverErrors, testCreateRoom, testJoinRoom } from "./utilities";
import { Room } from "src/app/schemas/Room";
import {environment} from "../../src/environments/environment";

describe("Kick player", () => {
  let room2: Partial<Room> | null = null;

  beforeEach(() => {
    cy.intercept("POST", `**/room*`).as("getRoom");
  });

  it("a player joins the room", () => {
    testCreateRoom(cy, {
      playerName: "Player A",
    })

    cy.contains("Inspect");
    cy.contains("Leave");

    cy.wait("@getRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      room2 = x.response?.body;
    });
  });

  it("second player and kicks the first one", () => {
    ignoreResizeObserverErrors(cy);

    testJoinRoom(cy, {
      playerName: "Player B",
      roomId: room2?.id,
    })

    // Player A exists
    if (environment.newSidebar) {
      cy.contains('Players').click();
    }
    cy.get("app-player-list").contains("Player A");

    // click "Player A" icon
    cy.get('.table > .row').first().within(() => {
      cy.get('.name').contains("Player A").click(); // NOTE: this click helps the click below register for some reason?
      cy.get('app-context-menu-button').click()
      // kick
      cy.get(".menuitem").contains("Kick Player").click();
    });

    // confirm
    cy.get(".confirmation-box-kick").contains("Ok").click({force:true});

    // player is removed
    cy.get("app-player-list").contains("Player A").should("not.exist");
  });
});
