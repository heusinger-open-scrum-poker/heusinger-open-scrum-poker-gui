import { Room } from "src/app/schemas/Room";
import { environment } from "../../src/environments/environment";
import { testCreateRoom, testJoinRoom } from "./utilities";

let room0: Partial<Room> = { id: undefined };

describe("Create Room", () => {
  it("creates a room", () => {
    cy.intercept("POST", "**/room*").as("getRoom");

    cy.visit("/");
    cy.get('.mode-select').within(() => {
      cy.contains("Create").click();
    });
    const input = cy.get('.inputs [name="player-name"] input');
    input.should("have.attr", "placeholder", "Name of player").type("joe\n");

    cy.wait("@getRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      room0 = x.response?.body;
    });

    cy.location().should((loc) =>
      expect(loc.pathname).to.equal(`/room/${room0.id}`),
    );
    cy.contains("Inspect");
    cy.contains("Leave");
  });
});

describe("Join Room and Play", () => {
  it("joins an existing room, plays a card, reveals all, starts new round", () => {
    const roomId = room0.id;
    if (!roomId) {
      throw new Error("roomId not defined");
    }

    cy.intercept("POST", `**/room/${roomId}/players*`).as("newPlayer");

    cy.visit("/");
    cy.get('.mode-select').within(() => {
      cy.contains("Join").click();
    });
    cy.get('[name="room-name"] input').should("be.visible");
    cy.get('.inputs [name="player-name"] input').should(
      "have.attr",
      "placeholder",
      "Name of player",
    );
    cy.get('.inputs [name="player-name"] input').type("mama");
    cy.get('[name="room-name"] input').should(
      "have.attr",
      "placeholder",
      "Room ID",
    );
    cy.get('[name="room-name"] input').type(roomId.toString());

    // clicks join button
    cy.get('.join-button').within(() => {
      cy.contains('Join').click();
    });

    cy.location().should((loc) =>
      expect(loc.pathname).to.equal(`/room/${roomId}`),
    );

    cy.wait("@newPlayer").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      room0 = x.response?.body;
    });

    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Reveal").click();
    cy.contains("New Round").click();
    cy.contains("Waiting for votes");
  });
});

describe("Reveal possible even if one person hasn't chosen a card", () => {
  let room2: Partial<Room> | null = null;

  beforeEach(() => {
    cy.intercept("POST", `**/room*`).as("getRoom");
  });

  it("first player creates a room and plays no card", () => {
    testCreateRoom(cy, {
      playerName: "Player A",
    });

    cy.contains("Inspect");
    cy.contains("Leave");

    cy.wait("@getRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      room2 = x.response?.body;
    });
  });

  it("second player joins the room and plays a card", () => {
    const roomId = room2?.id;
    if (!roomId) {
      throw new Error("roomId not defined");
    }

    testJoinRoom(cy, {
      playerName: "Player B",
      roomId,
    });


    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Reveal").click();
    cy.contains("New Round").click();
    cy.contains("Waiting for votes");
  });
});

describe("Reveal not possible even if no person has chosen a card", () => {
  let room3: Partial<Room> | null = null;

  beforeEach(() => {
    cy.intercept("POST", `**/room*`).as("getRoom");
  });

  it("first player creates a room and plays no card", () => {
    testCreateRoom(cy, {
      playerName: "Player A",
    })

    cy.contains("Inspect");
    cy.contains("Leave");

    cy.wait("@getRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      room3 = x.response?.body;
    });
  });

  it("second player joins the room and plays no card", () => {
    const roomId = room3?.id;
    if (!roomId) {
      throw new Error("roomId not defined");
    }

    testJoinRoom(cy, {
      playerName: "Player B",
      roomId,
    });

    cy.contains("Reveal").should("not.exist");
  });
});
