import { v4 as uuidv4 } from 'uuid';

/** handle floating-ui errors */
export function ignoreResizeObserverErrors(_cy: typeof cy) {
  _cy.on("uncaught:exception", (err) => {
    if (err.message.includes("ResizeObserver loop limit exceeded")) {
      return false;
    }
    if (
      err.message.includes(
        "ResizeObserver loop completed with undelivered notifications.",
      )
    ) {
      return false;
    }
  });
}


export function randomCredentials() {
  const randomString = uuidv4();
  const email = `test1-${randomString}@example.com`;
  const password = `test1-${randomString}@example.com`;

  return { email, password };
}


export function testCreateRoom(_cy: typeof cy, args: {
  visit?: boolean,
  playerName: string,
}) {
  const { visit = true } = args;
  if (visit) {
    _cy.visit("/");
  }

  _cy.get('.inputs [name="player-name"] input')
  .should("have.attr", "placeholder", "Name of player")
  .type(args.playerName);
  _cy.get('app-signin-box .buttons').within(() => {
    _cy.contains('Create').click();
  });

  // check for loaded room
  _cy.contains("Leave");
}

export function testJoinRoom(_cy: typeof cy, args: {
  visit?: boolean,
  playerName: string,
  roomId: number,
}) {
  const { visit = true } = args;
  if (visit) {
    _cy.visit(`/join/${args.roomId}`);
  }

  _cy.get('.inputs [name="player-name"] input')
    .should("have.attr", "placeholder", "Name of player")
    .type(args.playerName);
  _cy.get('app-room-select input')
    .should("have.attr", "placeholder", "Room ID")
    .clear()
    .type(args.roomId.toString());
  _cy.get('app-signin-box .buttons').within(() => {
    _cy.contains('Join').click();
  });

  // check for loaded room
  _cy.contains("Leave");
}


/** opens the register page on the startpage or in a room */
export function openRegisterPage(_cy: typeof cy) {
  cy.get('app-user-menu, app-user-menu-v2').then((usermenu) => {
    if (usermenu.is("app-user-menu")) {
      cy.contains('Create Account').click();
    }
    if (usermenu.is("app-user-menu-v2")) {
      cy.get('app-user-menu-v2').click();
      cy.get('app-user-menu-v2').within(() => {
        cy.contains('Create Account').click();
      });
    }
  });
}


/** opens the login page on the startpage or in a room */
export function openLoginPage(_cy: typeof cy) {
  cy.get('app-user-menu, app-user-menu-v2').then((usermenu) => {
    if (usermenu.is("app-user-menu")) {
      cy.contains('Sign in').click();
    }
    if (usermenu.is("app-user-menu-v2")) {
      cy.get('app-user-menu-v2').click();
      cy.get('app-user-menu-v2').within(() => {
        cy.contains('Sign in').click();
      });
    }
  });
}

/** sign out on the startpage or in a room */
export function signOut(_cy: typeof cy) {
  cy.get('app-user-menu, app-user-menu-v2').then((usermenu) => {
    if (usermenu.is("app-user-menu")) {
      cy.contains('Sign out').click();
    }
    if (usermenu.is("app-user-menu-v2")) {
      cy.get('app-user-menu-v2').click();
      cy.get('app-user-menu-v2').within(() => {
        cy.contains('Sign out').click();
      });
    }
  });
}

/** checks if user is signed in on the startpage or in a room */
export function checkSignedIn(_cy: typeof cy) {
  cy.get('app-user-menu, app-user-menu-v2').then((usermenu) => {
    if (usermenu.is("app-user-menu")) {
      cy.contains('Sign out').click();
    }
    if (usermenu.is("app-user-menu-v2")) {
      cy.get('app-user-menu-v2').click();
      cy.get('app-user-menu-v2').within(() => {
        cy.contains('Sign out');
      });
      // close the menu again
      cy.get('app-user-menu-v2').click();
    }
  });
}

/** get email input in auth modal */
export function emailInput() {
  return cy.get('input[placeholder="Email"]')
}

/** get password input in auth modal */
export function passwordInput() {
  return cy.get('input[placeholder="Password"]')
}

/** get repeated password input in auth modal */
export function repeatPasswordInput() {
  return cy.get('input[placeholder="Repeat password"]')
}


/** check if browser left the room */
export function isNotInRoom(_cy: typeof cy) {
  _cy.location('pathname').should('not.match', /\/room\/.*/);
}
