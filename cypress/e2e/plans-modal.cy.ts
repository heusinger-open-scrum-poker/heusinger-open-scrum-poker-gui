import { openRegisterPage, randomCredentials, testCreateRoom } from '../e2e/utilities';
import {environment} from "../../src/environments/environment";

describe('plans modal', () => {
  const { email, password } = randomCredentials()

  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "Player",
    });
  })

  it('triggered by create subtask', () => {
    openRegisterPage(cy);

    cy.get('app-register-page').should('be.visible').within(() => {
      cy.get('.email-wrapper input').clear();
      cy.get('.email-wrapper input').type(email);
      cy.get('.password-wrapper input').clear();
      cy.get('.password-wrapper input').type(password);
      cy.get('.repeat-password-wrapper input').clear();
      cy.get('.repeat-password-wrapper input').type(password);
      cy.get('modal-footer button').contains("Create Account").click();
    })

    if (environment.newSidebar) {
      cy.contains('Tasks').click();
      cy.get('app-tasks-session-header').contains("New task").click();
    } else {
      cy.get('app-tab-bar').contains('Tasks').click();
      cy.contains('Create Task').click();
    }

    cy.get('app-games > app-create-task-modal').within(() => {
      cy.get('app-task-form').within(() => {
        cy.get('input[placeholder="Subject"]').clear();
        cy.get('input[placeholder="Subject"]').type('Test');
        cy.get('.create-subtask').click();
      })
      cy.get('app-plans-page').within(() => {
        cy.contains('Would you like to use subtasks in tasks?');
        cy.contains("Subtasks put the finishing touches to your estimates - go into depth and work out the details.");
        cy.get('.free').should('have.class', 'disabled');
        cy.get('button.modal-close').click();
      })
      cy.get('app-task-form').within(() => {
        cy.get('input[placeholder="Subject"]').should('have.value', 'Test');
      })
    })
  })
})
