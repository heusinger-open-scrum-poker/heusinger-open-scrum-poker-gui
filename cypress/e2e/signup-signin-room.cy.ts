import { checkSignedIn, emailInput, openLoginPage, openRegisterPage, passwordInput, randomCredentials, repeatPasswordInput, signOut, testCreateRoom } from '../e2e/utilities';

describe('signup-signin-room', () => {
  const { email, password } = randomCredentials()

  it('signs up', () => {
    cy.intercept("POST", "/register").as("register");

    testCreateRoom(cy, {
      playerName: "authtest",
    });

    openRegisterPage(cy);

    cy.get('app-user-menu, app-user-menu-v2').within(() => {
      emailInput().type(email)
      passwordInput().type(password)
      repeatPasswordInput().type(password);
      cy.get('modal-footer button').contains("Create Account").click();
    })

    cy.wait("@register", { timeout: 15000 });

    cy.get('modal-footer button').should("not.be.visible")
    signOut(cy);
  })

  it('catches invalid emails', () => {
    testCreateRoom(cy, {
      playerName: "authtest",
    });

    openRegisterPage(cy);

    cy.get('app-user-menu, app-user-menu-v2').within(() => {
      emailInput().type('invalidemailaddress')

      cy.get('.form-error').contains('Email is invalid');
    })
  })

  it('catches already used emails', () => {
    testCreateRoom(cy, {
      playerName: "throwaway",
    });

    openRegisterPage(cy);

    cy.get('app-user-menu, app-user-menu-v2').within(() => {
      emailInput().type(email);

      cy.get('.form-error').contains('Email address already in use.');
    })
  })

  it('catches password repeat errors', () => {
    testCreateRoom(cy, {
      playerName: "authtest",
    });

    openRegisterPage(cy);

    cy.get('app-user-menu, app-user-menu-v2').within(() => {

      emailInput().type(email)
      passwordInput().type(password)
      repeatPasswordInput().type(password + 'typo');
    })

    cy.get('.form-error').contains('The passwords don\'t match.');
  })

  it('signs in', () => {
    testCreateRoom(cy, {
      playerName: "throwaway",
    });

    openLoginPage(cy);

    cy.get('app-user-menu, app-user-menu-v2').within(() => {
      cy.get('app-auth-modal').within(() => {
        emailInput().type(email)
        passwordInput().type(password)
        cy.get('modal-footer button').contains('Sign in').click();
      })
      cy.get('app-auth-modal').should('not.be.visible');
    })

    checkSignedIn(cy);
    signOut(cy);
  })

  it('signs in, then out', () => {
    testCreateRoom(cy, {
      playerName: "throwaway",
    });

    openLoginPage(cy);

    cy.get('app-user-menu, app-user-menu-v2').within(() => {
      cy.get('app-auth-modal').within(() => {
        emailInput().type(email)
        passwordInput().type(password)
        cy.get('modal-footer button').contains('Sign in').click();
      })
      cy.get('app-auth-modal').should('not.be.visible');
    })

    signOut(cy);

    cy.get('button').contains('Sign out').click();
    cy.location('pathname').should('match', /\//)
  })
})
