import { emailInput, isNotInRoom, openLoginPage, openRegisterPage, passwordInput, randomCredentials, repeatPasswordInput, signOut, testCreateRoom } from '../e2e/utilities';

describe('signup-signin-landingpage', () => {
  const { email, password } = randomCredentials()

  beforeEach(() => {
    cy.visit("/");
  })

  it('signs up', () => {
    cy.intercept("POST", "/register").as("register");

    openRegisterPage(cy);

    cy.get('modal-footer button').contains("Create Account");
    emailInput().type(email)
    passwordInput().type(password)
    repeatPasswordInput().type(password + '{enter}');

    cy.wait("@register", { timeout: 15000 });

    cy.get('app-register-page').should("not.exist")
    signOut(cy);
  })

  it('catches invalid emails', () => {
    openRegisterPage(cy);

    emailInput().type('invalidemailaddress')

    cy.get('.form-error').contains('Email is invalid');
  })

  it('catches already used emails', () => {
    cy.visit("/");

    openRegisterPage(cy);

    emailInput().type(email)

    cy.get('.form-error').contains('Email address already in use.');
  })

  it('catches password repeat errors', () => {
    openRegisterPage(cy);

    emailInput().type(email)
    passwordInput().type(password)
    repeatPasswordInput().type(password + 'error')

    cy.get('.form-error').contains('The passwords don\'t match.');
  })

  it('shows password rules', () => {
    openRegisterPage(cy);

    passwordInput().clear();

    cy.contains('Least 8 characters').should('not.have.class', 'satisfied');
    passwordInput().type("abcdefgh");
    cy.contains('Least 8 characters').should('have.class', 'satisfied');

    cy.contains('Least one number').should('not.have.class', 'satisfied');
    passwordInput().type("1");
    cy.contains('Least one number').should('have.class', 'satisfied');

    cy.contains('Least one symbol').should('not.have.class', 'satisfied');
    passwordInput().type("!");
    cy.contains('Least one symbol').should('have.class', 'satisfied');
  })

  it('signs in', () => {
    openLoginPage(cy);

    emailInput().type(email)
    passwordInput().type(password)
    cy.get('modal-footer button').contains('Sign in').click();
    cy.get('modal-footer button').should('not.be.visible');

    signOut(cy);
  })

  it('signs in, then out', () => {
    openLoginPage(cy);

    emailInput().type(email)
    passwordInput().type(password)
    cy.get('modal-footer button').contains('Sign in').click();
    cy.get('modal-footer button').should('not.be.visible');
    signOut(cy);
  })

  it('preserves user when entering room', () => {
    openLoginPage(cy);

    emailInput().type(email)
    passwordInput().type(password)
    cy.get('modal-footer button').contains('Sign in').click();
    cy.get('modal-footer button').should('not.be.visible');

    testCreateRoom(cy, {
      visit: false,
      playerName: "throwaway",
    });

    signOut(cy);
  })

  it('preserves user when leaving room', () => {
    testCreateRoom(cy, {
      visit: false,
      playerName: "throwaway",
    });

    openLoginPage(cy);

    cy.get('app-user-menu-v2 app-auth-modal, app-user-menu app-auth-modal').within(() => {
      emailInput().type(email)
      passwordInput().type(password)
      cy.get('modal-footer button').contains('Sign in').click();
    })

    cy.get('app-user-menu-v2 app-auth-modal, app-user-menu app-auth-modal').should('not.be.visible');

    cy.contains('Leave').click();
    cy.get(".confirmation-box-leave")
    .contains("Ok")
    .click();

    isNotInRoom(cy);

    signOut(cy);
  })
})
