import { testCreateRoom } from "./utilities";

describe("copy invite to clipboard", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  })

  it("can copy the invite link", () => {
    cy.window()
      .its("navigator.clipboard")
      .then((clipboard) => {
        cy.stub(clipboard, "writeText").as("writeText");
      });

    let value = "<undefined>";

    cy.get(".invite input")
      .should((it) => {
        value = it.val()?.toString() ?? "<undefined>";
        expect(value).to.match(/\/room\/\d+\?ref=[0-9a-z]{8}$/);
      })
      .then(() => {
        cy.get(".invite > .interaction-box > .interaction-button").click();

        cy.contains("Copied to Clipboard");

        cy.get("@writeText").should("have.been.calledOnceWith", value);
      });
  });
});
