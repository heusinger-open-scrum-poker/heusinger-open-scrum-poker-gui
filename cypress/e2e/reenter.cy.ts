import { testCreateRoom } from "./utilities";

describe("reenter room", () => {
  const username = "unique user 12312341";
  let roomId: number | undefined = undefined;
  let uuid: string | undefined = undefined;

  it("joins room initially", () => {
    cy.intercept("POST", `**/user*`).as("createUser");

    cy.visit("/");
    cy.wait("@createUser").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      uuid = x.response?.body.uuid;
    });

    cy.intercept("POST", `**/room*`).as("getRoom");

    testCreateRoom(cy, {
      visit: false,
      playerName: username,
    });

    cy.wait("@getRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      roomId = x.response?.body.id;
    });

    // leave
    cy.contains("Leave").click();
  });

  it("can rejoin room", () => {
    if (!uuid) {
      throw Error("no uuid");
    }

    localStorage.setItem("uuid", uuid);

    cy.visit("/room/" + roomId);

    // is indeed the room we joined before
    cy.contains(username);
  });
});
