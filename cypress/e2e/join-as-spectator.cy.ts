import { testCreateRoom } from "./utilities";

describe("join as spectator", () => {
  it("can create a room and enter as spectator", () => {
    cy.visit("/");
    cy.get('.inputs [name="player-name"] input')
      .should("have.attr", "placeholder", "Name of player")
      .type("testuser");

    cy.get(".dropdown-button-enter").click();
    cy.contains("Enter as Spectator").click();

    cy.location().should((loc) => {
      expect(loc.pathname).to.match(/\/room\/\d+/);
    });

    cy.contains("Estimate");
  });

  it("can create a room and rejoin as spectator", () => {
    let uuid: string = "";
    cy.intercept("POST", "/user").as("createUser");

    cy.visit("/");
    cy.wait("@createUser").should((x) => {
      uuid = x.request.body.uuid;
    });
    localStorage.setItem("uuid", uuid);

    cy.intercept("POST", "**/room*").as("getRoom");

    testCreateRoom(cy, {
      visit: false,
      playerName: "testuser",
    });

    cy.location().should((loc) => {
      expect(loc.pathname).to.match(/\/room\/\d+/);
    });

    cy.visit(`/join`);
    cy.contains("Join").click();
    cy.get('.inputs [name="player-name"] input').should(
      "have.value",
      "testuser",
    );

    cy.get(".dropdown-button-enter").click();
    cy.contains("Enter as Spectator").click();

    cy.contains("Estimate");
  });
});
