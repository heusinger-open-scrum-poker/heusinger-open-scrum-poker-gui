import { environment } from "../../src/environments/environment";
import { openRegisterPage, randomCredentials, testCreateRoom } from "./utilities";

describe("scores", () => {
  it("cards are hidden in estimation mode", () => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });

    if (environment.newSidebar) {
      cy.contains('Players').click();
    }

    cy.get(".estimator .value").should("exist");
    cy.get(".estimator .value > .not-ready").should("exist");

    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }

    cy.get(".estimator .value > .ready")
      .first()
      .within(() => {
        cy.contains("1").should("not.exist");
      });
  });

  it("cards are revealed after countdown", () => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });

    if (environment.newSidebar) {
      cy.contains('Players').click();
    }

    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }

    cy.get(".estimator .value > .ready").should("exist");

    cy.contains("Reveal").click();
    // TODO unreliable check in CI/CD pipeline
    // cy.contains(/Ready in .*/);

    cy.get(".estimator .value > .ready").should("exist");
    cy.get(".estimator .value > .revealed-score").should("not.exist");

    cy.contains("New Round");

    cy.get(".estimator .value > .ready").should("not.exist");
    cy.get(".estimator .value > .revealed-score").should("exist");
    cy.get(".estimator .value")
      .first()
      .within(() => {
        cy.contains("1");
      });
  });

  it('saves consensus to current task', () => {
    signUp();

    cy.get('app-game-tab').within(() => {
      cy.contains('create a first task').click();
    });

    cy.get('app-games').within(() => {
      cy.contains('New task').click();
      cy.get('app-create-task-modal').should('be.visible').within(() => {
        cy.get('input[placeholder="Subject"]').clear();
        cy.get('input[placeholder="Subject"]').type('Test');
        cy.get('button').contains('Create Task').click();
      });
    });

    cy.get('.tab.active app-tab-header').within(() => {
      cy.contains('Back').click();
    });

    cy.get(".estimator .value").should("exist");
    cy.get(".estimator .value > .not-ready").should("exist");

    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }

    cy.contains("Reveal").click();
    cy.get('app-sidebar').contains('Tasks').click();
    cy.get('app-game .estimate').contains(1);
  });

  function signUp(): void {
    const { email, password } = randomCredentials();
    cy.visit('/');

    openRegisterPage(cy);

    cy.get('app-register-page').get('input[placeholder="Email"]').type(email);
    cy.get('app-register-page').get('input[placeholder="Password"]').first().type(password);
    cy.get('app-register-page').get('input[placeholder="Repeat password"]').first().type(password);
    cy.get('app-register-page').contains('Create Account').click();

    testCreateRoom(cy, {
      visit: false,
      playerName: "testuser",
    });
  }
});
