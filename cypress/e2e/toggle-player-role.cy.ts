import { testCreateRoom } from "./utilities";

describe("toggle player role", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  });

  it("can switch from estimator to inspector and back", () => {
    cy.intercept("PUT", "/players/*").as("putPlayer");
    cy.intercept("PUT", "/players/*").as("putPlayer");

    cy.get('.control').contains("Inspect").click();
    cy.wait("@putPlayer");

    cy.get('.control').contains("Estimate").click();
    cy.wait("@putPlayer");
  });
});
