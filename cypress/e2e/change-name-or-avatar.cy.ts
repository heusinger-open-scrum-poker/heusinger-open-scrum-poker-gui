import {environment} from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe("change name/avatar", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  })

  it("can change name", () => {
    const exampleName = "new name";

    cy.intercept("PATCH", "/players/*").as("patchPlayer");

    cy.get(".headerbar .menu").click();
    cy.get(".headerbar").within(() => {
      cy.contains("Customize player").click();
    });

    cy.get("app-customize-player-modal").within(() => {
      cy.get("input").clear().type("new name");
    });

    cy.contains("Apply Changes").click();
    cy.get(".box").filter(":visible").contains("Ok").click();
    cy.wait("@patchPlayer").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.name).to.be.eq(exampleName);
    });

    if (environment.newSidebar) {
      cy.contains('Players').click();
    }

    cy.get(".name").contains(exampleName);
  });

  it("can change avatar", () => {
    cy.intercept("PATCH", "/players/*").as("patchPlayer");

    cy.get(".headerbar .menu").click();
    cy.get(".headerbar").within(() => {
      cy.contains("Customize player").click();
    });

    cy.get(".avatar-list > :nth-child(2)").click();

    cy.contains("Apply Changes").click();
    cy.get(".box").filter(":visible").contains("Ok").click();
    cy.wait("@patchPlayer").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.avatar).to.be.eq("orange");
    });

    if (environment.newSidebar) {
      cy.contains('Players').click();
    }

    cy.get(".avatar-background img")
      .should("have.attr", "src")
      .should("include", "assets/player-orange.svg");
  });
});
