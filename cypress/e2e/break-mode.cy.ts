import { environment } from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe("break mode", () => {
  it("can take a break", () => {
    testCreateRoom(cy, {
      playerName: "testuser",
    })

    cy.get(".card-break")
      .should("have.css", "cursor")
      .should("include", "pointer");
    cy.get(".card-break").click({ force: true });
    if (!environment.directCardSelection) {
      cy.contains("Take a Break").click();
    }

    cy.contains("End Break").click();
  });

  it.skip("can take a break in inspection mode", () => {
    testCreateRoom(cy, {
      playerName: "testuser",
    })

    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Take Back");
    cy.contains("Reveal").click();
    cy.contains("New Round");

    cy.get(".card-break")
      .should("have.css", "cursor")
      .should("include", "pointer");
    cy.get(".card-break").click({ force: true });
    if (!environment.directCardSelection) {
      cy.contains("Take a Break").click();
    }
    cy.contains("End Break").click();
    cy.contains("New Round");

    /* TODO */
  });
});
