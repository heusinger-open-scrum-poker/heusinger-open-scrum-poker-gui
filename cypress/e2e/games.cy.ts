import { testCreateRoom } from "./utilities";

describe('games', () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });

    cy.get('app-sidebar').contains('Tasks').click();
  })

  it.skip('it has at least one game', () => {
    cy.get('app-sidebar').contains('Game #1');
  })

  it('can create new game', () => {
    cy.get('app-sidebar').contains('New Game').click();
    cy.get('app-sidebar').contains('Game #2');
  })
})
