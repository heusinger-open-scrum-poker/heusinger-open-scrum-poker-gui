import { ignoreResizeObserverErrors, testCreateRoom } from "./utilities";

describe("series change", () => {
  beforeEach(() => {
    ignoreResizeObserverErrors(cy);
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  })

  it("can change the cards", () => {
    const exampleCardValue = "66";

    cy.intercept("PUT", "/room/*/series*").as("putRoom");

    cy.get(".headerbar .menu").click();
    cy.contains("Adapt series").click();

    cy.get(".basic-cards > :nth-child(1) input").should("have.value", "1");
    cy.get(".basic-cards > :nth-child(1) input").clear();
    cy.get(".basic-cards > :nth-child(1) input").should("have.value", "");
    cy.get(".basic-cards > :nth-child(1) input").type(exampleCardValue);
    cy.get(".basic-cards > :nth-child(1) input").should(
      "have.value",
      exampleCardValue,
    );

    cy.contains("Apply Series").click();
    cy.get(".box").filter(":visible").contains("Ok").click();
    cy.wait("@putRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.length).to.be.eq(10);
      expect(x.request.body[0]).to.be.eq(exampleCardValue);
    });
    cy.wait(3000);
    cy.get(".card").contains(exampleCardValue);
  });

  it("detects changes in series", () => {
    const exampleCardValue = "66";

    cy.intercept("PUT", "/room/*/series*").as("putRoom");

    cy.get(".headerbar .menu").click();
    cy.contains("Adapt series").click();

    cy.get(".dropdown-button").contains("Fibonacci");
    cy.get(".basic-cards > :nth-child(1) input").clear();
    cy.get(".basic-cards > :nth-child(1) input").should("have.value", "");
    cy.get(".basic-cards > :nth-child(1) input").type(exampleCardValue);
    cy.get(".basic-cards > :nth-child(1) input").should(
      "have.value",
      exampleCardValue,
    );
    cy.get(".basic-cards > :nth-child(1) input").blur();
    cy.get(".dropdown-button").contains("Custom");
    cy.get(".basic-cards > :nth-child(1) input").clear();
    cy.get(".basic-cards > :nth-child(1) input").should("have.value", "");
    cy.get(".basic-cards > :nth-child(1) input").type("1");
    cy.get(".basic-cards > :nth-child(1) input").blur();
    cy.get(".dropdown-button").contains("Fibonacci");

    cy.get('[label="?"] > .checkbox > .switch').click();
    cy.get(".dropdown-button").contains("Custom");
    cy.get('[label="?"] > .checkbox > .switch').click();
    cy.get(".dropdown-button").contains("Fibonacci");

    cy.contains("Apply Series").click();
    cy.get(".box").filter(":visible").contains("Ok").click();
    cy.wait("@putRoom").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.length).to.be.eq(10);
      expect(x.request.body[0]).to.be.eq("1");
    });
    cy.wait(3000);
    cy.get(".card").contains("1");
  });

  // it('can change the extended cards', () => {
  //   const exampleCardValue = "66";
  //
  //   cy.intercept('PUT', '/room/*/series*').as('putRoom')
  //
  //   cy.visit('/')
  //   cy.get('.inputs [name="player-name"] input').should('have.attr', 'placeholder', 'Name of player').type('testuser\n')
  //
  //   cy.get('.headerbar .menu').click()
  //   cy.contains('Adapt series').click()
  //
  //   cy.contains('More Cards').click()
  //   cy.contains('Fewer Cards')
  //
  //   cy.get('.extended-cards > :nth-child(1)').should('have.value', "")
  //   cy.get('.extended-cards > :nth-child(1)').clear()
  //   cy.get('.extended-cards > :nth-child(1)').should('have.value', "")
  //   cy.get('.extended-cards > :nth-child(1)').type(exampleCardValue)
  //   cy.get('.extended-cards > :nth-child(1)').should('have.value', exampleCardValue)
  //
  //   cy.contains('Apply Series').click()
  //   cy.get(".confirmation-box").filter(':visible').contains('OK').click()
  //   cy.wait('@putRoom').should(x => {
  //     expect(x.response.statusCode).to.be.oneOf([200])
  //     expect(x.request.body.length).to.be.eq(18);
  //     expect(x.request.body[8]).to.be.eq(exampleCardValue);
  //   })
  //   cy.wait(3000);
  //   cy.get('.card').contains(exampleCardValue);
  // })
});
