import { environment } from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe("select card", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  });

  it("can select card", () => {
    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Take Back");
  });

  it("can choose another card", () => {
    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Take Back");
    cy.get(".card").contains(/^1$/).should("not.be.visible");

    cy.get(".card").contains(/^3$/).click({ force: true });
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.get(".card").contains(/^3$/).should("not.be.visible");
  });

  it("all cards can be selected", () => {
    cy.contains("Inspect");

    cy.get(".card").contains(/^1$/).click();

    const value = "1 2 3 5 8 13 21 34".split(" ");
    for (let i = 1; i < value.length; ++i) {
      cy.get(".card").contains(value[i]).click("topRight", { force: true });
      if (!environment.directCardSelection) {
        cy.contains("Place Card").click();
      }
      cy.contains("Take Back");
    }
  });
});
