import { environment } from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe("reveal", () => {
  const username = "unique user 12312341";
  const otherUsername = "unique user 2847389204";
  const roomId = undefined;

  it("can reveal room", () => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });

    cy.get(".card").contains(/^1$/).click();
    if (!environment.directCardSelection) {
      cy.contains("Place Card").click();
    }
    cy.contains("Reveal").click();
    cy.contains("New Round");
  });

  // TODO make this test reliable
  // it('can reveal room, but only after countdown', () => {
  //   cy.visit('/')
  //   cy.get('.inputs [name="player-name"] input').should('have.attr', 'placeholder', 'Name of player').type('testuser\n')
  //
  //   cy.get('.card').contains(/^1$/).click()
  //   if (!environment.directCardSelection) {
  //     cy.contains('Place Card').click()
  //   }
  //   cy.contains("Reveal").click();
  //   // TODO: the CI pipeline sometimes fails this test due to timing problems
  //   // cy.contains("Ready in 3...");
  //   // cy.get('.roomInfo .value').contains(/^1$/).should('not.exist');
  //   // cy.contains("Ready in 1...");
  //   cy.get('.roomInfo .value').contains(/^1$/)
  // })
  //
  // it('prepare: reveals the round as spectator', () => {
  //
  //   cy.visit('/')
  //   cy.intercept('POST', `**/room*`).as('getRoom')
  //
  //   // a player enters
  //   cy.get('.inputs [name="player-name"] input').should('have.attr', 'placeholder', 'Name of player').type(username + '\n')
  //
  //   cy.wait('@getRoom').should(x => {
  //     expect(x.response.statusCode).to.be.oneOf([200])
  //     roomId = x.response.body.id;
  //     expect(roomId ?? null).to.be.not.null;
  //   })
  //
  //   // places a card
  //   cy.get('.card').contains(/^1$/).click()
  //   if (!environment.directCardSelection) {
  //     cy.contains('Place Card').click()
  //   }
  //
  // })
  //
  // it('reveals the round as spectator', () => {
  //   // a second player enters, spectates and can still reveal the round as spectator
  //   localStorage.removeItem("uuid");
  //
  //   cy.visit('/room/' + roomId)
  //   cy.get('.inputs [name="player-name"] input').should('have.attr', 'placeholder', 'Name of player').type(otherUsername + '\n')
  //
  //   cy.contains('Inspect').click();
  //   cy.contains('Estimate');
  //   cy.contains('Reveal');
  // })
});
