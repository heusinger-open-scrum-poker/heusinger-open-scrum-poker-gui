import { testCreateRoom } from "./utilities";

describe("send feedback", () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });
  });

  it("can create and send feedback", () => {
    const exampleFeedback = "Example\nFeedback";
    const exampleEmail = "example@email.com";
    let feedbackId: number;

    cy.viewport(1000, 720);
    cy.intercept("POST", "/feedback*").as("postFeedback");
    cy.intercept("PATCH", "/feedback*").as("patchFeedback");

    cy.get('.feedback textarea')
      .should("have.attr", "placeholder", "How can we improve Agile Casino?");
    cy.get(".feedback textarea").type(exampleFeedback);
    cy.get(".feedback .interaction-button").click();
    cy.get(".feedback").contains("Feedback sent");
    cy.wait("@postFeedback").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.content).to.be.eq(exampleFeedback);

      feedbackId = x.response?.body.id;
    });

    // wait for overlay to vanish
    cy.contains('sending').should('not.exist')
    cy.contains('Feedback sent').should('not.exist')
    cy.get('.feedback textarea').should('be.visible')

    cy.get(".close-button").click();
    cy.get(".feedback textarea").type(exampleFeedback);
    cy.get(".feedback .interaction-button").click();
    cy.wait("@postFeedback").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.content).to.be.eq(exampleFeedback);

      feedbackId = x.response?.body.id;
    });

    // wait for overlay to vanish
    cy.contains('sending').should('not.exist')
    cy.contains('Feedback sent').should('not.exist')
    cy.get('.email textarea').should('be.visible')

    cy.get('.email textarea')
      .should("have.attr", "placeholder", "Email address for notifications once the feature has been implemented.");
    cy.get(".email textarea").type(exampleEmail);
    cy.get(".email .interaction-button").click();
    cy.wait("@patchFeedback").should((x) => {
      expect(x.response?.statusCode).to.be.oneOf([200]);
      expect(x.request.body.id).to.be.eq(feedbackId);
      expect(x.request.body.email).to.be.eq(exampleEmail);
    });
    cy.get(".email").contains("Thanks");
  });

  it("can not send invalid email", () => {
    const exampleFeedback = "Example\nFeedback";
    const invalidEmail = "invalid email";

    cy.viewport(1000, 720);
    cy.intercept("POST", "/feedback*").as("postFeedback");

    cy.get('.feedback textarea')
      .should("have.attr", "placeholder", "How can we improve Agile Casino?");
    cy.get(".feedback textarea").type(exampleFeedback);
    cy.get(".feedback .interaction-button").click();

    // wait for overlay to vanish
    cy.contains('sending').should('not.exist')
    cy.contains('Feedback sent').should('not.exist')
    cy.get('.email textarea').should('be.visible')

    cy.get('.email textarea')
      .should("have.attr", "placeholder", "Email address for notifications once the feature has been implemented.");
    cy.get(".email textarea").type(invalidEmail);
    cy.get(".email .interaction-button").click();
    cy.get('.notification > .message').contains('Email is invalid.');
  });
});
