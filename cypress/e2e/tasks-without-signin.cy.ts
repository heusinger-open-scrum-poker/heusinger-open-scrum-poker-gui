import {environment} from "../../src/environments/environment";
import { testCreateRoom } from "./utilities";

describe('tasks without sign-in', () => {
  beforeEach(() => {
    testCreateRoom(cy, {
      playerName: "testuser",
    });

    if (environment.newSidebar) {
      cy.contains('Tasks').click();
    } else {
      cy.get('app-tab-bar').contains('Tasks').click();
    }
  })

  it('it can open tasks tab', () => {
    cy.contains('There are no tasks defined yet. Please add tasks to vote on.')
  })
})
