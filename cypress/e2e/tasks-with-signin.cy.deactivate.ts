import { randomCredentials } from "../e2e/utilities";

describe('tasks with sign-in', () => {
  beforeEach(() => {
    const { email, password } = randomCredentials();
    cy.visit('/');

    // Create account
    cy.get('.mb-half-em').type('testuser');
    cy.get('.icon').click();
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.player-name').click();
    cy.get('.signup > a').click();
    cy.get('.email-wrapper > .input').clear();
    cy.get('.email-wrapper > .input').type(email);
    cy.get('.password-wrapper > .input').clear();
    cy.get('.password-wrapper > .input').type(password);
    cy.get('.repeat-password-wrapper > .input').clear();
    cy.get('.repeat-password-wrapper > .input').type(password);
    cy.get('modal-footer > .button').contains('Create Account').click();
  })

  it('can create a task', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.field-description > .input').type('some description');
    cy.get('.primary').contains('Create Task').click();
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.subject').contains('a new task');
    cy.get('.description').contains('some description');
    cy.get('span > img').should('be.visible');
    /* ==== End Cypress Studio ==== */
  })

  it('can delete a task', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.field-description > .input').type('some description');
    cy.get('.primary').contains('Create Task').click();
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.description').click();
    cy.get('modal-footer > .danger').click();
    cy.get('app-edit-task-modal > app-confirmation-box .box > .flex-row > .danger').click();
    cy.get('.content-column').contains('There are no tasks defined yet. Please add tasks to vote on.');
    /* ==== End Cypress Studio ==== */
  })

  it('can switch between tasks and estimate', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.primary').contains('Create Task').click();

    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a second task');
    cy.get('.primary').contains('Create Task').click();

    cy.get(':nth-child(4) > .button').contains('Vote').click();
    cy.get('app-tab-bar.content > :nth-child(1)').contains('Players').click();
    cy.get('[style="left: -110px; top: 46px; transform: rotate(-25deg);"] > .front').click();
    cy.get('.buttons-box > .button > .text').contains('Place Card').click();
    cy.contains('Reveal').click();
    cy.contains('New Round')
    cy.get('.apply > img').click();
    cy.get('.subject').contains('a second task');
    cy.get('[aria-label="Estimate previous task"] > .button-icon').click();
    cy.get('.subject').contains('a new task');
    cy.get('[aria-label="Estimate next task"] > .button-icon').click();
    cy.get('.subject').contains('a second task');
    /* ==== End Cypress Studio ==== */
    /* ==== Generated with Cypress Studio ==== */
    cy.get('app-tab-bar.content > :nth-child(2) > span').contains('Tasks').click({ force: true });
    cy.get('app-task-item > .wrapper.selected > .estimate').click();
    cy.get('.field-estimate > .input').should('have.value','1')
    /* ==== End Cypress Studio ==== */
  })

  it('can create task with subtasks', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.create-subtask').click()
    cy.get('.subtask-input').should(
      "have.attr",
      "placeholder",
      "It should...",
    );
    cy.get('.subtask-input').type('It should be blue');
    cy.get('.subtask > .checkbox').click();
    cy.get('.primary').contains('Create Task').click();
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.description').click();
    cy.get('.subtask-input').should(
      "have.value",
      "It should be blue",
    );
    cy.get('.checkbox').should('be.checked');
    /* ==== End Cypress Studio ==== */
  })

  it('can create subtask for existing task', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.primary').contains('Create Task').click();
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.description').click();
    cy.get('.create-subtask > img').click();
    cy.get('.subtask-input').clear();
    cy.get('.subtask-input').type('It should be red');
    cy.get('modal-footer').click();
    cy.get('.checkbox').check();
    cy.get('.primary').click();
    cy.get('.description').click();
    cy.get('.subtask-input').should(
      "have.value",
      "It should be red",
    );
    cy.get('.checkbox').should('be.checked');
    /* ==== End Cypress Studio ==== */
  })

  it('can rename subtask and mark it as checked', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.create-subtask').click()
    cy.get('.subtask-input').should(
      "have.attr",
      "placeholder",
      "It should...",
    );
    cy.get('.subtask-input').type('It should be blue');
    cy.get('.primary').contains('Create Task').click();

    cy.get('.description').click();
    cy.get('.subtask-input').clear();
    cy.get('.subtask-input').type('It should be red');
    cy.get('.subtask > .checkbox').click();
    cy.get('.primary').contains('Save').click();

    cy.get('.description').click();
    cy.get('.subtask-input').should(
      "have.value",
      "It should be red",
    );
    cy.get('.checkbox').should('be.checked');
  })

  it('can delete a subtask', () => {
    cy.get('app-tab-bar.content > :nth-child(2)').contains('Tasks').click({ force: true });
    cy.get('.control-wrapper > .button').contains('Create Task').click();
    cy.get('.field-subject > .input').clear();
    cy.get('.field-subject > .input').type('a new task');
    cy.get('.create-subtask').click()
    cy.get('.subtask-input').should(
      "have.attr",
      "placeholder",
      "It should...",
    );
    cy.get('.subtask-input').type('It should be blue');
    cy.get('.primary').contains('Create Task').click();
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.description').click();

    // TODO fix the hover
    // cy.get('.subtask').trigger('mouseover');
    // cy.get('.subtask > app-settings-menu > .menu > img').click();
    // cy.get('.subtask > app-settings-menu > .dropdown > .menuitem').click();
    // cy.get('.primary').click();
    // cy.get('.description').click();
    // cy.get('#subtasks').should('be.empty');
    /* ==== End Cypress Studio ==== */
  })
})
