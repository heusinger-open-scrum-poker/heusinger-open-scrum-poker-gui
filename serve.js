import express from "express";
import { join } from "path";
import bodyParser from "body-parser";
import { dirname } from "path";
import { fileURLToPath } from "url";
import console from "console";

const __dirname = dirname(fileURLToPath(import.meta.url));

const app = express();
const port = 4200;

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/assets", express.static(join(__dirname, "dist/agile-casino/assets")));

["favicon.ico", "main.js", "polyfills.js", "runtime.js", "styles.css"].forEach(
  (file) => {
    app.get(`/${file}`, (_req, res) => {
      res.sendFile(join(__dirname, `dist/agile-casino/${file}`));
    });
  },
);

// Catch all other routes and return the index file
app.get("*", (req, res) => {
  res.sendFile(join(__dirname, "dist/agile-casino/index.html"));
});

app.listen(port, () => {
  console.log(`Serving "./dist/agile-casino" on http://localhost:${port}`);
});
