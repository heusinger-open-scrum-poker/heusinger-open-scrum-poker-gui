import { defineConfig } from "cypress";

export default defineConfig({
  video: false,
  defaultCommandTimeout: 20000,

  e2e: {
    baseUrl: "http://localhost:4200",
    supportFile: false,
    experimentalStudio: true,
  },

  component: {
    devServer: {
      framework: "angular",
      bundler: "webpack",
    },
    specPattern: "**/*.cy.ts",
    excludeSpecPattern: "cypress/**/*.cy.ts",
  },
});
