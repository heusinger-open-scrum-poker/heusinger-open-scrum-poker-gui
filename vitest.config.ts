import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    environment: 'jsdom',
    includeSource: ['./src/**/*.{test,spec}.{ts,js,tsx,jsx}'],
  },
})
