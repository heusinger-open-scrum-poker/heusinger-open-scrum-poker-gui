(function (window) {
  window.env = window.env || {};

  // Environment variables TEMPLATE
  window.env.guiBaseUrl = "${GUI_BASE_URL}";
  window.env.apiBaseUrl = "${API_BASE_URL}";
  window.env.brokerBaseUrl = "${BROKER_BASE_URL}";
  window.env.jiraClientId = "${JIRA_CLIENT_ID}";
  window.env.footerVisible = "${FOOTER_VISIBLE}";
  window.env.directCardSelection = "${DIRECT_CARD_SELECTION}";
  window.env.loginFeature = "${LOGIN_FEATURE}";
  window.env.taskFeature = "${TASK_FEATURE}";
  window.env.jiraFeature = "${JIRA_FEATURE}";
  window.env.aiFeature = "${AI_FEATURE}";
  window.env.newStatistics = "${NEW_STATISTICS}";
  window.env.newTaskModal = "${NEW_TASK_MODAL}";
  window.env.newSidebar = "${NEW_SIDEBAR}";
  window.env.gameFeature = "${GAME_FEATURE}";
  window.env.taskExportFeature = "${TASK_EXPORT_FEATURE}";
  window.env.showAverage = "${SHOW_AVERAGE}";
  window.env.firebaseDocument = "${FIREBASE_DOCUMENT}";
  window.env.statisticsFeature = "${STATISTICS_FEATURE}";
  window.env.experiment203 = "${EXPERIMENT_203}";
  window.env.experiment204 = "${EXPERIMENT_204}";
})(this);
