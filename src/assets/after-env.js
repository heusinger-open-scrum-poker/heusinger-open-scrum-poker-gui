// Global site tag (gtag.js) - Google Analytics
if (window.env.guiBaseUrl === "https://agilecasino.kehrwasser.com")
{
  const script = document.createElement('script');
  script.async = true;
  script.src = "https://www.googletagmanager.com/gtag/js?id=G-C8CXRGVRP8";
  script.onerror = (err) => {
    console.error("Failed to load Google Analytics", err);
  };
  document.head.appendChild(script);
} else {
  console.debug("Google Analytics: disabled");
}

// Google Ads
if (window.env.guiBaseUrl === "https://agilecasino.kehrwasser.com")
{
  const script = document.createElement('script');
  script.async = true;
  script.src = "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6270393276549667";
  script.crossOrigin = "anonymous";
  script.onerror = (err) => {
    console.error("Failed to load Google Ads", err);
  };
  document.head.appendChild(script);
} else {
  console.debug("Google Ads: disabled");
}

