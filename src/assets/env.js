(function (window) {
  window.env = window.env || {};

  // Environment variables
  window.env.guiBaseUrl = "";
  window.env.apiBaseUrl = "";
  window.env.brokerBaseUrl = "";
  window.env.jiraClientId = "";
  window.env.footerVisible = "";
  window.env.directCardSelection = "";
  window.env.loginFeature = "";
  window.env.taskFeature = "";
  window.env.jiraFeature = "";
  window.env.aiFeature = "";
  window.env.newStatistics = "";
  window.env.newTaskModal = "";
  window.env.newSidebar = "";
  window.env.gameFeature = "";
  window.env.taskExportFeature = "";
  window.env.firebaseDocument = "";
  window.env.statisticsFeature = "";
  window.env.experiment203 = "";
  window.env.experiment204 = "";
})(this);
