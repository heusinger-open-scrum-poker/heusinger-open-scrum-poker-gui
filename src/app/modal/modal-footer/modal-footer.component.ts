import { Component, EventEmitter, Input, Output } from "@angular/core";
import { firstValueFrom, Observable, of } from "rxjs";
import { ModalAction, ModalButtonProps } from "../types";

@Component({
  selector: "modal-footer",
  templateUrl: "./modal-footer.component.html",
  styleUrls: ["./modal-footer.component.scss"],
})
export class ModalFooterComponent {
  @Input() primaryButtonProps?: ModalButtonProps;
  @Input() secondaryButtonProps?: ModalButtonProps;
  @Input() tertiaryButtonProps?: ModalButtonProps;
  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>();

  protected loadingPrimary: boolean = false;
  protected loadingSecondary: boolean = false;
  protected loadingTertiary: boolean = false;

  constructor() {}

  onPrimaryClicked(): void {
    this.loadingPrimary = true;
    const action = this.primaryButtonProps?.onClick?.() || of("close");
    this.handleModalAction(action, 1);
  }

  onSecondaryClicked(): void {
    this.loadingSecondary = true;
    const action = this.secondaryButtonProps?.onClick?.() || of("close");
    this.handleModalAction(action, 2);
  }

  onTertiaryClicked(): void {
    this.loadingTertiary = true;
    const action = this.tertiaryButtonProps?.onClick?.() || of("close");
    this.handleModalAction(action, 3);
  }

  stopAnimation(index: 1 | 2 | 3): void {
    switch (index) {
      case 1:
        this.loadingPrimary = false;
        break;
      case 2:
        this.loadingSecondary = false;
        break;
      case 3:
        this.loadingTertiary = false;
        break;
      default:
        break;
    }
  }

  private handleModalAction(
    action: Observable<ModalAction>,
    index: 1 | 2 | 3,
  ): void {
    firstValueFrom(action)
      .then((modalAction: ModalAction) => {
        this.stopAnimation(index);
        if (modalAction === "close") {
          this.onClose.emit();
        }
      })
      .finally(() => {
        this.stopAnimation(index);
      });
  }
}
