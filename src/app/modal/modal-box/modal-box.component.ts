import {
  Component, ElementRef, Input, Type,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { ModalSize } from "../types";

@Component({
  selector: "modal-box",
  templateUrl: "./modal-box.component.html",
  styleUrls: ["./modal-box.component.scss"],
})
export class ModalBoxComponent {
  @Input() index: number = 0;
  @Input() size?: ModalSize;
  @Input() hideOnOutsideClick?: boolean;
  @Input() destroyModal?: () => void;
  @Input() componentType: unknown;
  @Input() inputs?: Record<string, unknown>;

  @ViewChild("background") background!: ElementRef;
  @ViewChild("modal", { read: ViewContainerRef, static: true }) viewContainerRef: ViewContainerRef | null = null;

  protected hidden = true;

  get closeModal(): () => void {
    return (): void => {
      this.hidden = true;
      this.destroyModal?.();
    }
  }

  init(): void {
    const component = this.viewContainerRef?.createComponent(
      this.componentType as Type<unknown>,
    );

    if (component) {
      if (this.inputs) {
        this.inputs['closeModal'] = this.closeModal;
      } else {
        this.inputs = { closeModal: this.closeModal };
      }

      Object.entries(this.inputs || {}).map(([key, value]) => {
        component.setInput(key, value);
      });

      this.hidden = false;
    } else {
      throw new Error("unable to create modal");
    }
  }

  onClick(event: Event) {
    if (
      this.hideOnOutsideClick &&
      event.target === this.background?.nativeElement
    ) {
      this.closeModal();
    }
  }
}
