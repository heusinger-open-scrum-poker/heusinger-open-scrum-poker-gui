import { ContentChildren, Directive, HostListener, QueryList } from "@angular/core";
import { FormInputDirective } from "./form-input.directive";

@Directive({
  selector: '[formError]',
})
export class FormErrorDirective {
  @ContentChildren(FormInputDirective, { descendants: true }) controls?: QueryList<FormInputDirective>;

  @HostListener("keydown.enter")
  focusFirstError(): void {
    const invalidControls = this.controls?.filter((el) => el.getControl()?.invalid);
    if (invalidControls && invalidControls.length > 0) {
      const firstInvalid = invalidControls[0];

      firstInvalid.getElement().nativeElement.focus();
    }
  }
}
