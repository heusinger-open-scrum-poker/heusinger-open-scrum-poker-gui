import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ModalBodyComponent } from "./modal-body/modal-body.component";
import { ModalFooterComponent } from "./modal-footer/modal-footer.component";
import { ModalHeaderComponent } from "./modal-header/modal-header.component";
import { FormInputDirective } from "./form-input.directive";
import { ModalBoxComponent } from "./modal-box/modal-box.component";
import { ModalComponent } from "./modal/modal.component";
import { ConfirmationBoxComponent } from "./confirmation-box/confirmation-box.component";
import { AlertModalComponent } from "./alert-modal/alert-modal.component";
import { TranslateForRootModule } from "../translate-for-root.module";
import { FormErrorDirective } from "./form-error.directive";
import { ModalDialogComponent } from "./modal-dialog/modal-dialog.component";
import { KwmcModule } from "../kwmc/kwmc.module";
import { ModalFooterV2Component } from "./modal-footer-v2/modal-footer-v2.component";


const exports = [
  AlertModalComponent,
  ConfirmationBoxComponent,
  FormInputDirective,
  FormErrorDirective,
  ModalBodyComponent,
  ModalBoxComponent,
  ModalComponent,
  ModalFooterComponent,
  ModalFooterV2Component,
  ModalHeaderComponent,
  ModalDialogComponent,
];

const declarations = [...exports];

@NgModule({
  declarations,
  exports,
  imports: [CommonModule, TranslateForRootModule, KwmcModule],
})
export class ModalModule {}
