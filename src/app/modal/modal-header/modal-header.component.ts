import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "modal-header",
  templateUrl: "./modal-header.component.html",
  styleUrls: ["./modal-header.component.scss"],
})
export class ModalHeaderComponent {
  @Input() title: string = "";
  @Input() iconPath?: string;

  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>;

  constructor() {}

  get isClosable(): boolean {
    return this.onClose.observed;
  }

  close(): void {
    this.onClose.emit();
  }
}
