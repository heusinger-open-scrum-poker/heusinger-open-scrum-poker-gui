import { Component, HostListener, Input, ViewChild, booleanAttribute } from "@angular/core";
import { ModalAction, ModalButtonProps, ModalSize } from "../types";
import { ModalComponent } from "../modal/modal.component";
import { ModalFooterComponent } from "../modal-footer/modal-footer.component";
import { Action, Actions } from "../modal-footer-v2/modal-footer-v2.component";
import { Observable, firstValueFrom } from "rxjs";

@Component({
  selector: "modal-dialog",
  templateUrl: "./modal-dialog.component.html",
  styleUrls: ["./modal-dialog.component.scss"],
})
export class ModalDialogComponent {
  @ViewChild(ModalComponent) modal?: ModalComponent;
  @ViewChild(ModalFooterComponent) footer?: ModalFooterComponent;

  @Input("size") size: ModalSize = "fixed-400";
  @Input({ alias: "cancelable", transform: booleanAttribute }) cancelable: boolean = false;
  @Input({ alias: "closeOnEscape", transform: booleanAttribute }) closeOnEscape: boolean = false;
  @Input({ alias: "closeOnOutsideClick", transform: booleanAttribute }) closeOnOutsideClick: boolean = false;

  @Input({ alias: "showHeader", transform: booleanAttribute }) showHeader: boolean = true;
  @Input() title: string = "";
  @Input() iconPath?: string;
  @Input({ alias: "showFooter", transform: booleanAttribute }) showFooter: boolean = true;
  @Input() primaryButtonProps?: ModalButtonProps;
  @Input() secondaryButtonProps?: ModalButtonProps;
  @Input() tertiaryButtonProps?: ModalButtonProps;
  @Input() confirmCloseWith?: "primary" | "secondary" | "tertiary" | "close" | "ignore" = "close";

  private primaryBusy = false;
  private secondaryBusy = false;
  private tertiaryBusy = false;

  get actions(): Actions {
    return {
      primary: this.primaryButtonProps && {
        ...this.primaryButtonProps,
        busy: this.primaryBusy,
      },
      secondary: this.secondaryButtonProps && {
        ...this.secondaryButtonProps,
        busy: this.secondaryBusy,
      },
      tertiary: this.tertiaryButtonProps && {
        ...this.tertiaryButtonProps,
        busy: this.tertiaryBusy,
      },
    };
  }

  getButtonProps(action: Action): undefined | void | Observable<ModalAction> {
    switch (action) {
      case "primary":
        return this.primaryButtonProps?.onClick?.();
      case "secondary":
        return this.secondaryButtonProps?.onClick?.();
      case "tertiary":
        return this.tertiaryButtonProps?.onClick?.();
    }
  }

  setBusy(action: Action, busy: boolean) {
    switch (action) {
      case "primary":
        return this.primaryBusy = busy;
      case "secondary":
        return this.secondaryBusy = busy;
      case "tertiary":
        return this.tertiaryBusy = busy;
    }
  }

  handleAction(action: Action) {
    let modalAction = this.getButtonProps(action);

    if (modalAction) {
      this.setBusy(action, true);
      firstValueFrom(modalAction)
      .then((modalAction) => {
          this.setBusy(action, false);
          if (modalAction === "close") {
            this.close();
          }
      })
      .finally(() => {
        this.setBusy(action, false);
      })
    }
  }

  handleEscape(): void {
    if (this.closeOnEscape) {
      this.confirmClose();
    }
  }

  handleOutsideClick(): void {
    return; // BUG: KEH-2964 closes indiscriminately, even if another modal window has focus
            // Disabling this feature for the time being
    if (this.closeOnOutsideClick) {
      this.confirmClose();
    }
  }

  confirmClose(): void {
    switch (this.confirmCloseWith) {
      case "primary": {
        this.handleAction("primary");
      } break;
      case "secondary": {
        this.handleAction("secondary");
      } break;
      case "tertiary": {
        this.handleAction("tertiary");
      } break;
      case "close": {
        this.close();
      } break;
      case "ignore": {
      } break;
    }
  }

  showModal(): void {
    this.modal?.showModal();
  }

  close(): void {
    this.modal?.close();
  }
}
