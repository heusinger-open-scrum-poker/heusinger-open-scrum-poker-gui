import { MountConfig } from "cypress/angular";
import { AlertModalComponent } from "./alert-modal.component";
import { ModalModule } from "src/app/modal/modal.module";

describe("AlertModalComponent", () => {
  const config: MountConfig<AlertModalComponent> = {
    imports: [ModalModule],
    declarations: [],
    componentProperties: {
      message: "some message",
      onClose: () => {},
      primaryButtonProps: { label: "Ok", buttonFlavor: "primary" },
    },
  };

  it("mounts", () => {
    cy.mount(AlertModalComponent, config);
  });

  it("shows the message", () => {
    cy.mount(AlertModalComponent, config);
    cy.contains("some message");
  });

  it("contains an OK button", () => {
    cy.mount(AlertModalComponent, config);
    cy.get("button").contains("Ok");
  });
});
