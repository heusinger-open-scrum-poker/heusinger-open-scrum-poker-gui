import { Component, Input, ViewChild } from "@angular/core";
import { of } from "rxjs";
import { ModalButtonProps } from "src/app/modal/types";
import { ModalComponent } from "../modal/modal.component";

@Component({
  selector: "app-alert-modal",
  templateUrl: "./alert-modal.component.html",
  styleUrls: ["./alert-modal.component.scss"],
})
export class AlertModalComponent {
  @ViewChild("dialog") dialog?: ModalComponent;

  @Input() message!: string;
  @Input() onClose?: () => void;

  get closeModal() {
    return () => this.dialog?.close()
  }

  primaryButtonProps: ModalButtonProps | undefined = {
    label: "Ok",
    buttonFlavor: "primary",
    onClick: () => {
      if (this.onClose) {
        this.onClose();
      }
      return of("close");
    },
  };

  constructor() {}

  showMessage(message: string, onClose?: () => void) {
    this.onClose = onClose ?? (() => {});
    this.message = message;
    this.dialog?.showModal();
  }
}
