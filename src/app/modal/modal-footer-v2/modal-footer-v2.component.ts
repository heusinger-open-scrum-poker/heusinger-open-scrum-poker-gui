import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ButtonFlavor, ButtonHorizontalPadding, ButtonWidth } from "src/app/kwmc/button";

export interface Actions {
  primary?: ButtonConfig;
  secondary?: ButtonConfig;
  tertiary?: ButtonConfig;
}

export interface ButtonConfig {
  label: string;
  buttonClass?: string;
  buttonFlavor: ButtonFlavor;
  buttonWidth?: ButtonWidth;
  buttonPx?: ButtonHorizontalPadding;
  disabled?: boolean;
  busy?: boolean;
}

export const ACTIONS = ["primary", "secondary", "tertiary"] as const;
export type Action = (typeof ACTIONS)[number];

@Component({
  selector: "modal-footer-v2",
  templateUrl: "./modal-footer-v2.component.html",
  styleUrls: ["./modal-footer-v2.component.scss"],
})
export class ModalFooterV2Component {
  ACTION_ORDER = ["tertiary", "secondary", "primary"] as const;

  @Input("actions") actions: Actions = {};
  @Output("action") actionEvent: EventEmitter<Action> = new EventEmitter();

  constructor() {}

  get busy(): boolean {
    return this.actions.tertiary?.busy || this.actions.secondary?.busy || this.actions.primary?.busy || false;
  }

  handleClick(action: Action): void {
    this.actionEvent.emit(action);
  }
}
