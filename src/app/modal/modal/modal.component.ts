import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild, booleanAttribute } from "@angular/core";
import { ModalSize } from "../types";

export const MODAL_CLOSING_TIME_MILLIS: number = 300;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrl: './modal.component.scss',
})
export class ModalComponent implements AfterViewInit {
  @Input("size") size: ModalSize = "small";
  @Input({ alias: "cancelable", transform: booleanAttribute }) cancelable: boolean = false;

  @Output("outsideClick") outsideClickEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output("cancel") cancelEvent: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('dialog', { read: ElementRef<HTMLDialogElement>, static: true }) dialog?: ElementRef<HTMLDialogElement>;

  ngAfterViewInit() {
    this.dialog?.nativeElement?.addEventListener("cancel", this.handleCancel.bind(this));
  }

  protected hidden: boolean = true;

  public showModal() {
    this.dialog?.nativeElement?.showModal();
    this.hidden = false;
    setTimeout(() => {
      this.dialog?.nativeElement?.focus()
    }, 0);
  }

  public close() {
    this.hidden = true;
    setTimeout(() => {
      this.dialog?.nativeElement?.close();
    }, MODAL_CLOSING_TIME_MILLIS);
  }

  public isCancelable(): boolean {
    return this.cancelable;
  }

  protected handleCancel(event: Event): void {
    if (!this.cancelable) {
      event.preventDefault();
    }
  }

  @HostListener('click', ['$event'])
  handle(event: MouseEvent) {
    const dialog = this.dialog?.nativeElement;
    if (dialog) {
      const x = event.clientX;
      const y = event.clientY;
      const { top, bottom, left, right } = dialog.getBoundingClientRect();
      const isOutside = x < left || right < x || y < top || bottom < y;
      if (isOutside) {
        this.outsideClickEvent.emit();
      }
    }
  }
}
