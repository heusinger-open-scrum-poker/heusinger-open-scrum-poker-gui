import { Directive, ElementRef } from "@angular/core";
import { AbstractControl, NgControl } from "@angular/forms";

@Directive({
  selector: '[formControlName]',
})
export class FormInputDirective {
  constructor (private element: ElementRef, private control : NgControl) {}

  public getElement(): ElementRef {
    return this.element;
  }

  public getControl(): AbstractControl | null {
    return this.control.control;
  }
}
