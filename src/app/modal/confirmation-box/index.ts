export const ConfirmationResults = {
  confirm: "confirm",
  cancel: "cancel",
} as const;

export type ConfirmationResult =
  (typeof ConfirmationResults)[keyof typeof ConfirmationResults];
