import { Component, EventEmitter, HostListener, Output, ViewChild } from "@angular/core";
import { ConfirmationResult } from ".";
import { ModalComponent } from "../modal/modal.component";

@Component({
  selector: "app-confirmation-box",
  templateUrl: "./confirmation-box.component.html",
  styleUrl: "./confirmation-box.component.scss",
})
export class ConfirmationBoxComponent {
  @ViewChild("dialog") dialog?: ModalComponent;
  @Output() result: EventEmitter<ConfirmationResult> =
    new EventEmitter<ConfirmationResult>();

  public inProgress: boolean = false;

  constructor() {}

  showModal(): void {
    this.dialog?.showModal();
  }

  close(): void {
    this.dialog?.close();
  }

  confirm(): void {
    this.result.emit("confirm");
  }

  cancel(): void {
    this.result.emit("cancel");
  }

  @HostListener('keydown.esc', ['$event'])
  interceptEsc(event: KeyboardEvent): void {
    // NOTE: prevent a parent modal from closing
    event.stopPropagation();
  }

  @HostListener('click', ['$event'])
  interceptClick(event: MouseEvent): void {
    // NOTE: prevent a parent modal from closing
    event.stopPropagation();
  }
}
