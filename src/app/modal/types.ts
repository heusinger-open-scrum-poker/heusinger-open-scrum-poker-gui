import { Observable } from "rxjs";
import { ButtonFlavor, ButtonHorizontalPadding, ButtonWidth } from "../kwmc/button";

export const MODAL_CLOSING_TIME_MILLIS = 300 as const;

export type ModalButtonProps = {
  label: string;
  buttonClass?: string;
  buttonFlavor: ButtonFlavor;
  buttonWidth?: ButtonWidth;
  buttonPx?: ButtonHorizontalPadding;
  onClick?: () => void | Observable<ModalAction>;
  disabled?: boolean;
};

export type ModalOptions = {
  size?: ModalSize;
  hideOnOutsideClick?: boolean;
};

export const ModalActions = {
  close: "close",
  ignore: "ignore",
} as const;
export type ModalAction = (typeof ModalActions)[keyof typeof ModalActions];

export type ModalSize =
  | "extra-small"
  | "small"
  | "medium"
  | "large"
  | "fixed-400"
  | "fixed-440"
  | "fixed-700"
  | "fixed-865";
