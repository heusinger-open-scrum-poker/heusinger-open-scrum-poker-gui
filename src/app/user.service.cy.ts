import { TestBed } from "@angular/core/testing";

import { UserService } from "./user.service";
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";

describe("UserService", () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
    });
    service = TestBed.inject(UserService);
  });

  it("should be created", () => {
    expect(service).to.be.ok;
  });

  it("should have a user after refreshing user", () => {
    expect(service.getUser()).to.be.undefined;

    service.refreshUser().then(() => {
      expect(service.getUser()).to.be.a("User");
    });
  });
});
