import { z } from 'zod';

export type JiraResource = z.infer<typeof JiraResource>;

export const JiraResource = z.object({
  id: z.string(),
  url: z.string(),
  name: z.string(),
  avatarUrl: z.string(),
})

export type JiraProject = z.infer<typeof JiraProject>;

export const JiraProject = z.object({
  id: z.string(),
  url: z.string(),
  key: z.string(),
  avatarUrl: z.string(),
})

export type JiraSubtask = z.infer<typeof JiraSubtask>;

export const JiraSubtask = z.object({
  id: z.string(),
  self: z.string(),
  key: z.string(),
  issuetypeId: z.string(),
  issuetypeSelf: z.string(),
  summary: z.string(),
})

export type JiraIssue = z.infer<typeof JiraIssue>;

export const JiraIssue = z.object({
  id: z.string(),
  self: z.string(),
  key: z.string(),
  issuetypeId: z.string(),
  issuetypeSelf: z.string(),
  timeestimate: z.number().int(),
  description: z.any(),
  summary: z.string(),
  subtasks: z.array(JiraSubtask),
  estimate: z.string().nullish(),
})

export type JiraBoardLocation = z.infer<typeof JiraBoardLocation>;

export const JiraBoardLocation = z.object({
  projectId: z.number().int(),
  displayName: z.string(),
  projectName: z.string(),
  projectKey: z.string(),
  projectTypeKey: z.string(),
  avatarURI: z.string(),
  name: z.string(),
})


export type JiraBoard = z.infer<typeof JiraBoard>;

export const JiraBoard = z.object({
  id: z.number().int(),
  self: z.string(),
  name: z.string(),
  type: z.string(),
  location: JiraBoardLocation,
})

export type JiraSprint = z.infer<typeof JiraSprint>;

export const JiraSprint = z.object({
  id: z.number().int(),
  self: z.string(),
  state: z.string(),
  name: z.string(),
  startDate: z.string().datetime().nullable(),
  endDate: z.string().datetime().nullable(),
  completeDate: z.string().datetime().nullable(),
  createdDate: z.string().datetime(),
  originBoardId: z.number().int().nullable(),
  goal: z.string().nullable(),
})

export const JiraTaskMetadata = z.object({
  resource: z.string(),
  project: z.string(),
  issue: JiraIssue,
})

export type JiraTaskMedatata = z.infer<typeof JiraTaskMetadata>;

export const JiraSubtaskMetadata = z.object({
  resource: z.string(),
  project: z.string(),
  parentId: z.string(),
  subtask: JiraSubtask,
})

export type JiraSubtaskMedatata = z.infer<typeof JiraSubtaskMetadata>;
