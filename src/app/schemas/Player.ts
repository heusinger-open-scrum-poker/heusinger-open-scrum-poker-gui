import { z } from "zod";

export type Player = z.infer<typeof Player>;

export const Player = z.object({
  id: z.number(),
  name: z.string(),
  avatar: z.string().nullable().optional(),
  cardValue: z.string().nullable().optional(),
  type: z.enum(["inspector", "estimator"]),
});

export type PlayerRef = z.infer<typeof PlayerRef>
export const PlayerRef = Player.pick({ id: true, type: true })
