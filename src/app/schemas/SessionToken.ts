import { z } from "zod";

export type SessionToken = z.infer<typeof SessionToken>;

export const SessionToken = z.object({
  value: z.string(),
  createdAt: z.string().transform((it) => new Date(it)),
  expiresAt: z.string().transform((it) => new Date(it)),
});
