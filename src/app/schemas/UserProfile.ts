import { z } from "zod";

export type UserProfile = z.infer<typeof UserProfile>;

export const UserProfile = z.object({
  email: z.string().optional(),
});
