import { z } from "zod";

export type Room = z.infer<typeof Room>;
export type RoomStatus = z.infer<typeof RoomStatus>;

export const RoomStatus = z.enum(["estimating", "revealed"]);

export const AccessToken = z.string();
export type AccessToken = z.infer<typeof AccessToken>;

export const RoomId = z.number().int();
export type RoomId = z.infer<typeof RoomId>;

export const Room = z.object({
  id: RoomId,
  accessToken: AccessToken.nullish(),
  status: RoomStatus,
  roundsPlayed: z.number(),
  maxPlayers: z.number(),
  series: z.string(),
  created_at: z.number(),
  currentTaskId: z.number().nullish(),
  activeGameId: z.number().nullish(),
});

export type RoomReferenceLike = RoomId | AccessToken | RoomReference;

export class RoomReference {
  ref: { type: "id", id: RoomId } | { type: "accessToken", accessToken: AccessToken };

  constructor(name: RoomReferenceLike) {
    if (name instanceof RoomReference) {
      this.ref = name.ref;
      return;
    }

    if (typeof name === 'number') {
      this.ref = { type: "id", id: name };
      return;
    }

    const id = +name;
    if (isNaN(id)) {
      this.ref = { type: "accessToken", accessToken: name };
    } else {
      this.ref = { type: "id", id };
    }
  }

  matches(name: number | string | null | undefined): boolean {
    if (name === null || name === undefined) {
      return false;
    }

    const ref = new RoomReference(name);

    if (ref.ref.type === "id" && this.ref.type === "id") {
      return ref.ref.id === this.ref.id;
    } else if (ref.ref.type === "accessToken" && this.ref.type === "accessToken") {
      return ref.ref.accessToken === this.ref.accessToken;
    }

    return false;
  }

  toString() {
    if (this.ref.type === "id") {
      return this.ref.id.toString();
    } else {
      return this.ref.accessToken;
    }
  }
}
