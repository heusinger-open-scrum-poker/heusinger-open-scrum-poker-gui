import { z } from "zod";

export type RawCardSeries = z.infer<typeof RawCardSeries>;
export const RawCardSeries = z.array(z.string().min(1));

export type CardSeries = z.infer<typeof CardSeries>;
export const CardSeries = z.object({
  values: z.array(z.string().min(1)),
  break_: z.boolean(),
  inf: z.boolean(),
  unclear: z.boolean(),
})
