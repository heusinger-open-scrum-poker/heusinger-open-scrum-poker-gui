import { z } from "zod";

export type Advertisement = z.infer<typeof Advertisement>;

export const Advertisement = z.object({
  type: z.enum(["ad", "info", "tip"]),
  headline: z.string(),
  text: z.string(),
  linkText: z.string(),
  link: z.string(),
});
