import { Subtask } from "./Subtask";
import { Task } from "./Task";

export type Rankable = Task | Subtask;
