import { z } from "zod";

export type Feedback = z.infer<typeof Feedback>;

export const Feedback = z.object({
  id: z.number(),
  content: z.string(),
  email: z.string().nullable(),
  userUuid: z.string().nullable(),
});
