import { z } from "zod";

export type User = z.infer<typeof User>;

export const User = z.object({
  uuid: z.string().uuid(),
  hash: z.string(),
  language: z.string().nullable().optional(),
  role: z.enum(["anonymous", "signedup", "pro"]),
});
