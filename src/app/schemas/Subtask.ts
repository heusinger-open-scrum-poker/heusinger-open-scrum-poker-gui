import { z } from "zod";

export type Subtask = z.infer<typeof Subtask>;

export const Subtask = z.object({
  id: z.number().nullable(),
  taskId: z.number(),
  subject: z.string(),
  checked: z.boolean(),
  rank: z.optional(z.string()),
  deleted: z.boolean().optional().nullable(),
  aiGenerated: z.boolean().optional().nullable(),
  provider: z.string().nullish(),
  metadata: z.string().nullish(),
});
