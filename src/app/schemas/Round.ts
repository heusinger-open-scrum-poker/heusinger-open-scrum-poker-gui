import { z } from "zod";

export type Round = z.infer<typeof Round>;

export const Round = z.object({
  id: z.number(),
  consensus: z.string().nullish(),
});
