import { z } from "zod";

export const AiSubtaskEstimate = z.object({
  summary: z.string(),
  estimate: z.number(),
});

export type AiSubtaskEstimate = z.infer<typeof AiSubtaskEstimate>;

export const AiSubtaskEstimateQuota = z.object({
  freeAiEstimations: z.number().int().nullable(),
});

export type AiSubtaskEstimateQuota = z.infer<typeof AiSubtaskEstimateQuota>;
