import { z } from "zod";

export type SplitTest = z.infer<typeof SplitTest>;

export const SplitTest = z.object({
  feature: z.string(),
  variants: z.array(z.string()).nonempty('split test must have at least one variant'),
});
