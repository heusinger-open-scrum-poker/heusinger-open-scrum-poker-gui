import { z } from "zod";
import { Task } from "./Task";
import { Subtask } from "./Subtask";

export type Game = z.infer<typeof Game>;

export const Game = z.object({
  id: z.number(),
  roomId: z.number(),
  name: z.string(),
  created_at: z.number(),
});

export const TaskWithSubtasks = z.object({
  task: Task,
  subtasks: z.array(Subtask),
})

export type TaskWithSubtasks = z.infer<typeof TaskWithSubtasks>;

export const GameWithTasks = z.object({
  game: Game,
  tasksWithSubtasks: z.array(TaskWithSubtasks),
})

export type GameWithTasks = z.infer<typeof GameWithTasks>;

export const JiraImportParameters = z.object({
  type: z.enum(["jira"]),
  instanceId: z.string(),
  boardId: z.string(),
  sprintId: z.string(),
  importModalState: z.any(),
});

export type JiraImportParameters = z.infer<typeof JiraImportParameters>;

export const ImportParameters = JiraImportParameters;

export type ImportParameters = z.infer<typeof ImportParameters>;
