import { z } from "zod";

export type Task = z.infer<typeof Task>;

export const Task = z.object({
  id: z.number(),
  roomId: z.number(),
  subject: z.string(),
  description: z.string(),
  estimate: z.string().nullish(),
  provider: z.string().nullish(),
  metadata: z.string().nullish(),
  rank: z.string().nullish(),
  gameId: z.number().optional(),
});
