import { z } from 'zod';
import { Task } from './Task';
import { Subtask } from './Subtask';

export const OperationType = z.enum(["create", "update", "delete"]);

export type OperationType = z.infer<typeof OperationType>;

export const SubtaskOperation = z.object({
  type: OperationType,
  subtask: Subtask.omit({id: true, taskId: true}).and(Subtask.partial()),
});

export type SubtaskOperation = z.infer<typeof SubtaskOperation>;

export const TaskOperation = z.object({
  type: OperationType,
  task: Task.omit({id: true}).and(Task.partial()),
  subtaskOperations: z.array(SubtaskOperation),
});

export type TaskOperation = z.infer<typeof TaskOperation>;
