import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AgileCasinoService } from "../agile-casino.service";

@Component({
  selector: "app-jira-oauth-redirect-page",
  templateUrl: "./jira-oauth-redirect-page.component.html",
  styleUrls: ["./jira-oauth-redirect-page.component.scss"],
})
export class JiraOAuthRedirectPageComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private agileCasinoService: AgileCasinoService,

  ) {}

  ngOnInit(): void {

    this.route.queryParams.subscribe({
      next: (queryParams) => {
        const code = queryParams['code'];
        const stateWithPath = queryParams['state'].split('$');
        const state = stateWithPath[0];
        const path = stateWithPath[1];

        const newQueryParams = { ...queryParams };
        delete newQueryParams['code'];
        delete newQueryParams['state'];

        this.agileCasinoService.jiraSubmitAuthCode(code, state).subscribe({});

        if (path) {
          this.router.navigate(path.split('/'), {queryParams: newQueryParams});
        } else {
          this.router.navigate(["create"]);
        }
      },
      error: (e) => {
        console.error(e);
        this.router.navigate(["create"]);

      },
    })
  }
}
