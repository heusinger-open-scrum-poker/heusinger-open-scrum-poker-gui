import { Component, OnInit, effect } from "@angular/core";
import { Router } from "@angular/router";
import { FunnelEventService } from "./funnel-event.service";
import { TranslateService } from "@ngx-translate/core";
import { LanguageService } from "./language.service";
import { UserService } from "./user.service";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";
import {
  LOCAL_STORAGE_PERSISTENCE,
  LOGGING_EVENTS,
  KwConfiguration,
  createFeatureManager,
  mergeEventHandlers,
  SplitTestSpec,
  defaultConfiguration,
  weighedChoice,
  MaybePromise,
} from "@kehrwasser-dev/features";
import featureSpec from "./features.json";
import { environment } from "src/environments/environment";
import { FIREBASE_EVENTS } from "src/firebase";
import { RoomService } from "./room/room.service";
import { Room } from "./schemas/Room";
import { pseudoRandomFromTimestamp } from "./shared/util";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  constructor(
    funnelEventService: FunnelEventService,
    router: Router,
    private languageService: LanguageService,
    private translate: TranslateService,
    private userService: UserService,
    private featureService: KwFeatureService,
    private roomService: RoomService,
  ) {
    funnelEventService.listenToRouterEvents(router);
    translate.setDefaultLang("en");
    this.configureFeatures();
  }

  async ngOnInit() {
    const localeLanguageString = this.languageService.getLocale();
    if (
      localeLanguageString === "de" ||
      localeLanguageString.startsWith("de-")
    ) {
      this.translate.use("de");
    } else {
      this.translate.use("en");
    }

    await this.userService.refreshUser();
  }

  configureFeatures() {
    const roomSignal = this.roomService.room;

    effect(
      () => {
        roomSignal();
        this.featureService.refreshVariants();
      },
      { allowSignalWrites: true },
    );

    const config: KwConfiguration = {
      environment: (name: string): string => {
        if (name in environment) {
          type EnvVar = keyof typeof environment;
          return environment[name as EnvVar].toString();
        }

        return "false";
      },
      roles: {
        getRole: () => {
          return this.userService.getUser()?.role ?? "anonymous";
        },
      },
      persistence: LOCAL_STORAGE_PERSISTENCE,
      events: mergeEventHandlers(
        FIREBASE_EVENTS(this.userService),
        LOGGING_EVENTS,
      ),
      chooseVariant: function (
        splitTest: string,
        spec: SplitTestSpec,
      ): MaybePromise<string> {
        switch (splitTest) {
          case "plans-modal": {
            const waitForRoom = async () => {
              while (true) {
                if (roomSignal()) {
                  return roomSignal();
                } else {
                  await new Promise((ok) => setTimeout(ok, 100));
                }
              }
            };

            function pickVariant(room?: Room) {
              const choices = spec.variants.map(({ name, weight }) => ({
                value: name,
                weight,
              }));

              if (!room) {
                return choices[0].value;
              }

              const rand = pseudoRandomFromTimestamp(room.created_at);
              const pick = weighedChoice(choices, rand);
              return pick;
            }

            const room = roomSignal();
            if (room) {
              return pickVariant(room);
            } else {
              return waitForRoom().then((room) => {
                return pickVariant(room);
              });
            }
          }
          default:
            return defaultConfiguration.chooseVariant(splitTest, spec);
        }
      },
    };

    const manager = createFeatureManager(featureSpec, config);
    this.featureService.setFeatureManager(manager);
  }
}
