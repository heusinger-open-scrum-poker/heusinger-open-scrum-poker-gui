import { Component, Input } from "@angular/core";
import { PlayerView } from "../TableView";

@Component({
  selector: "app-player-label",
  templateUrl: "./player-label.component.html",
  styleUrls: ["./player-label.component.scss"],
})
export class PlayerLabelComponent {
  @Input({ required: true }) playerView!: PlayerView;

  get showIconBreak(): boolean {
    return this.getLabelType() === "break";
  }

  get showIconReady(): boolean {
    return this.getLabelType() === "ready";
  }

  get showIconMinOutlier(): boolean {
    return this.getLabelType() === 'outlier'
      && this.playerView.type === 'revealed'
      && this.playerView.stats?.minOutlier === undefined
      && this.playerView.stats?.minOutlier === this.playerView?.player.cardValue;
  }

  get showIconMaxOutlier(): boolean {
    return this.getLabelType() === 'outlier'
      && this.playerView.type === 'revealed'
      && this.playerView.stats?.maxOutlier === undefined
      && this.playerView.stats?.maxOutlier === this.playerView?.player.cardValue;
  }

  isTakingBreak(): boolean {
    return this.playerView.player.isTakingBreak;
  }

  isReady(): boolean {
    return this.playerView.player.isReady;
  }

  isOutlier(): boolean {
    switch (this.playerView.type) {
      case "revealed":
        if (!this.playerView.stats) {
          return false;
        }
        return (
          (this.playerView.stats.maxOutlier !== undefined && this.playerView.player.cardValue === this.playerView.stats.maxOutlier) ||
          (this.playerView.stats.minOutlier !== undefined && this.playerView.player.cardValue === this.playerView.stats.minOutlier)
        );
      case "concealed":
        return false;
    }
  }

  getLabelType(): "estimating" | "ready" | "outlier" | "break" {
    if (this.isTakingBreak()) {
      return "break";
    }

    if (this.playerView.type === "revealed" && this.isOutlier()) {
      return "outlier";
    }

    if (this.isReady()) {
      return "ready";
    }

    return "estimating";
  }
}
