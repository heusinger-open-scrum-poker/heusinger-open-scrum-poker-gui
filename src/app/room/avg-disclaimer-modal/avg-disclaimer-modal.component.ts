import { Component, Input, ViewChild } from "@angular/core";
import { of } from "rxjs";
import { ModalButtonProps, ModalOptions } from "../../modal/types";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";

@Component({
  selector: "app-avg-disclaimer-modal",
  templateUrl: "./avg-disclaimer-modal.component.html",
  styleUrls: ["./avg-disclaimer-modal.component.scss"],
})
export class AvgDisclaimerModalComponent {
  @ViewChild("modal") modal?: ModalDialogComponent;

  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-400",
    hideOnOutsideClick: false,
  };

  primaryButtonProps: ModalButtonProps | undefined = {
    label: "Ok",
    buttonFlavor: "primary",
    buttonPx: "12",
    onClick: () => {
      return of("close");
    },
  };

  constructor() {}

  showModal() {
    this.modal?.showModal();
  }

  close() {
    this.modal?.close();
  }
}
