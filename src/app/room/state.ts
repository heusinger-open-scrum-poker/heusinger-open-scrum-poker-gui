import { firstValueFrom } from "rxjs";
import { AgileCasinoService } from "../agile-casino.service";
import { Player } from "../schemas/Player";
import { Room, RoomReference } from "../schemas/Room";
import { Task } from "../schemas/Task";
import { User } from "../schemas/User";
import LocalStorage from "../shared/LocalStorage";
import { Stats, computeStats } from "./statistics";
import { Settings } from "../shared/Settings";
import { Game, TaskWithSubtasks } from "../schemas/Game";
import { RoomService } from "./room.service";
import { BREAK_CARD_VALUE } from ".";

export const MAX_TIME_BETWEEN_ROOM_UPDATES_MS: number = 1000 * 60;

export class RoomState {
  user?: User;
  players: Player[] = [];
  transientPlayers: undefined | Player[];
  joinedRooms: Array<{ room: Room; player: Player; numPlayers: number }> = [];
  tasks: TaskWithSubtasks[] = [];
  stats: Stats = {
    majorityCards: [],
    maxVotes: 0,
    totalVotes: 1,
    cardValues: [],
    comparator: () => 0,
    distribution: {},
  };
  hideResults: boolean = false;
  private hideResultsTimer?: ReturnType<typeof setTimeout> = undefined;

  private pollingTimeout?: ReturnType<typeof setTimeout>;
  private lastRoomUpdate?: Date;

  games: Game[] = [];

  constructor(
    private initialRoomRef: RoomReference,
    private agileCasinoService: AgileCasinoService,
    private roomService: RoomService,
    private storage: LocalStorage,
  ) {
    this.installPollingTimeout();
  }

  get room(): Room | undefined {
    return this.roomService.room();
  }

  set room(room: Room) {
    this.roomService.setRoom(room);
  }

  public getRoomId(): number | undefined {
    return this.room?.id;
  }

  get roomRef() {
    if (!this.room) {
      return;
    }

    return new RoomReference(this.room?.accessToken ?? this.room?.id);
  }

  get activeGame(): Game | undefined {
    const room = this.room;

    if (!room) {
      return;
    }

    if (!this.games) {
      return;
    }

    return this.games.find((game) => game.id === room.activeGameId);
  }

  public getPlayerId(): number | undefined {
    return this.joinedRooms.find(({ room }) => room.id == this.room?.id)?.player.id;
  }

  public getUser(): User | undefined {
    return this.user;
  }

  public getRoom(): Room | undefined {
    return this.room;
  }

  public getPlayers(): Player[] | undefined {
    return this.transientPlayers ?? this.players;
  }

  /** list of players that are estimators and are not taking a break */
  public getActivePlayers(): Player[] {
    return this.getPlayers()?.filter((player) => {
      return player.type === "estimator" && player.cardValue !== BREAK_CARD_VALUE
    }) ?? []
  }

  public getTasks(): TaskWithSubtasks[] | undefined {
    const activeGame = this.activeGame;

    if (!activeGame) {
      return;
    }

    return this.getTasksForGame(activeGame);
  }

  public async refresh() {
    await this.refreshJoinedRooms();
    await this.refreshPlayers();
    await this.refreshRoom();
    await this.refreshTasks();
    await this.refreshGames();
  }

  public async refreshJoinedRooms() {
    const userUuid = this.storage.getUuid();
    if (userUuid) {
      try {
        this.joinedRooms = await firstValueFrom(
          this.agileCasinoService.getJoinedRooms(userUuid),
        );
      } catch (error) {
        console.error("failed to refresh joined rooms list");
        throw error;
      }
    } else {
      this.joinedRooms = [];
    }

    this.storage.setHasSessions(this.joinedRooms.length > 0);
  }

  public async refreshRoom() {
    try {
      try {
        this.room = await firstValueFrom(
          this.agileCasinoService.getRoom(this.roomRef ?? this.initialRoomRef),
        );
        this.lastRoomUpdate = new Date();
      } catch (e) {
        console.warn(e);
      }

      if (this.room && this.players) {
        this.stats = computeStats(this.players, this.room);
      }

      if (this.room) {
        if (this.room.status === "estimating") {
          this.hideResults = true;
        } else if (this.room.status === "revealed" && this.hideResults && this.hideResultsTimer == undefined) {
          this.hideResultsTimer = setTimeout(() => {
            if (this.room?.status === "revealed") {
              this.hideResults = false;
            }
            this.hideResultsTimer = undefined;
          }, Settings.INITIAL_HOT * Settings.COUNTDOWN_TICK_DURATION);
        }
      }
    } catch (error) {
      console.error("failed to refresh room");
      throw error;
    }
  }

  public async refreshTasks() {
    const roomRef = this.roomRef;

    if (!roomRef) {
      return;
    }

    try {
      const gamesWithTasks = await firstValueFrom(
        this.agileCasinoService.getGamesWithTasks(this.roomRef),
      );

      const newTasks = [];
      for (const game of gamesWithTasks) {
        for (const tws of game.tasksWithSubtasks) {
          newTasks.push(tws);
        }
      }

      this.tasks = newTasks;

    } catch (error) {
      console.error("failed to refresh tasks");
      throw error;
    }
  }

  public async putRoom(update: Partial<Room> & Pick<Room, "id" | "accessToken">) {
    try {
      await firstValueFrom(this.agileCasinoService.updateRoom(update));
    } catch (error) {
      console.error("failed to patch room");
      throw error;
    }
  }

  public async refreshPlayers() {
    try {
      this.players = await firstValueFrom(
        this.agileCasinoService.getPlayersOfRoom(this.roomRef ?? this.initialRoomRef),
      );
      this.transientPlayers = undefined;
    } catch (error) {
      console.error("failed to refresh players");
      throw error;
    }
  }

  public async refreshPlayer(playerId: number) {
    const { index, transientIndex } = this.indexOfPlayerById(playerId);

    if (this.transientPlayers && transientIndex == undefined) {
      console.debug("cannot find player in local player list");
      return;
    }

    try {
      const player = await firstValueFrom(
        this.agileCasinoService.getPlayer(playerId),
      );

      if (index != undefined) {
        this.players[index] = player;
      }

      if (this.transientPlayers && transientIndex != undefined) {
        this.transientPlayers[transientIndex] = player;
      }
    } catch (error) {
      console.error("failed to refresh player");
      throw error;
    }
  }

  public async putPlayer(update: Player) {
    const { index, transientIndex } = this.indexOfPlayerById(update.id);

    if (this.transientPlayers && transientIndex == undefined) {
      throw new Error("cannot find player in local player list");
    }

    try {
      const player = await firstValueFrom(
        this.agileCasinoService.putPlayer(update),
      );

      if (index != undefined) {
        this.players[index] = player;
      }

      if (this.transientPlayers && transientIndex != undefined) {
        this.transientPlayers[transientIndex] = player;
      }
    } catch (error) {
      console.error("failed to put player");
      throw error;
    }
  }

  public getSelf(): Player | undefined {
    return this.getPlayers()?.find(
      (player) => player.id === this.getPlayerId(),
    );
  }

  public indexOfPlayerById(id: number): {
    index?: number;
    transientIndex?: number;
  } {
    const index = this.players.findIndex((p) => p.id == id);
    const transientIndex = (this.transientPlayers ?? []).findIndex(
      (p) => p.id == id,
    );
    return {
      index: index == -1 ? undefined : index,
      transientIndex: transientIndex == -1 ? undefined : transientIndex,
    };
  }

  public setPlayerOptimistically(player: Player) {
    if (this.transientPlayers) {
      this.transientPlayers = this.transientPlayers.map((p) =>
        p.id == player.id ? player : p,
      );
    } else {
      this.transientPlayers = this.players.map((p) =>
        p.id == player.id ? player : p,
      );
    }
  }

  public revertOptimisticPlayers() {
    this.transientPlayers = undefined;
  }

  public getCurrentTask(): Task | undefined {
    return this.getCurrentTaskWithSubtasks()?.task
  }

  public getCurrentTaskWithSubtasks(): TaskWithSubtasks | undefined {
    const currentTaskId = this.room?.currentTaskId;

    if (!currentTaskId) {
      return;
    }


    const task = this.tasks?.find(({task}) => task.id == currentTaskId);

    if (!task) {
      return;
    }

    return task;
  }

  installPollingTimeout(): void {
    const now = new Date();

    if (!this.lastRoomUpdate) {
      console.debug("polling room: no initial room update yet");
      this.pollingTimeout = setTimeout(() => {
        this.installPollingTimeout()
      }, MAX_TIME_BETWEEN_ROOM_UPDATES_MS)
      return;
    }

    if (this.lastRoomUpdate.getTime() + MAX_TIME_BETWEEN_ROOM_UPDATES_MS < now.getTime()) {
      console.debug("polling room: refresh");
      this.refreshRoom();
      this.pollingTimeout = setTimeout(() => {
        this.installPollingTimeout()
      }, MAX_TIME_BETWEEN_ROOM_UPDATES_MS)
      return;
    }

    console.debug("polling room: up to date");
    this.pollingTimeout = setTimeout(() => {
      this.installPollingTimeout()
    }, Math.max(1000, this.lastRoomUpdate.getTime() + MAX_TIME_BETWEEN_ROOM_UPDATES_MS - now.getTime()))
  }

  public async refreshGames(): Promise<void> {
    const roomRef = this.roomRef;

    if (!roomRef) {
      return;
    }

    return new Promise((ok, err) => this.agileCasinoService.getGames(roomRef).subscribe({
      next: (games: Game[]) => {
        this.sortGames(games);
        this.games = games;
        ok()
      },
      error: err,
    }));
  }

  private sortGames(games: Game[]): void {
    games.sort((a: Game, b: Game) => {
      const numberA = a.name.replace('Game #', '');
      const numberB = b.name.replace('Game #', '');
      return parseInt(numberA) > parseInt(numberB) ? 1 : -1
    });
  }

  public getTasksForGame(game: Game): TaskWithSubtasks[] {
    return this.tasks?.filter(({task}) => task.gameId === game?.id);
  }

  public async activateGame(game: Game): Promise<void> {
    return new Promise((ok, err) => this.agileCasinoService.activateGame(game.roomId, game.id).subscribe({
      next: () => {
        this.refreshRoom();
        ok()
      },
      error: (error) => {
        err(error)
      }
    }));
  }

  public async newGame(): Promise<void> {
    const roomRef = this.roomRef;

    if (!roomRef) {
      return;
    }

    return new Promise((ok, err) => this.agileCasinoService.createGame(roomRef).subscribe({
      next: (game) => {
        if (this.room) {
          this.room.activeGameId = game.id;
        }
        this.games.push(game);
        this.refreshRoom();
        ok()
      },
      error: (error) => {
        err(error);
      }
    }));
  }
}
