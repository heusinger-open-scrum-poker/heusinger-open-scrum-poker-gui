import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { MountConfig } from "cypress/angular";
import { TranslateTestingModule } from "ngx-translate-testing";
import { Observable, of } from "rxjs";
import { AgileCasinoService } from "../agile-casino.service";
import { CardComponent } from "../kwmc/card/card.component";
import { ConnectionStatusComponent } from "../kwmc/connection-status/connection-status.component";
import { DEFAULT_OPTIONS } from "../room/custom-cards-modal";
import { FooterComponent } from "../footer/footer.component";
import { HeaderbarComponent } from "./headerbar/headerbar.component";
import { SettingsMenuComponent } from "../kwmc/settings-menu/settings-menu.component";
import { TabBarComponent } from "../kwmc/tab-bar/tab-bar.component";
import LocalStorage from "../shared/LocalStorage";
import { BigPokerTableComponent } from "./big-poker-table/big-poker-table.component";
import { CardHandComponent } from "./bottom-area/card-hand/card-hand.component";
import { PlayerListComponent } from "./player-list/player-list.component";
import { PokerTableComponent } from "./poker-table/poker-table.component";
import { RoomComponent } from "./room.component";
import { RoomState } from "./state";
import { TasksComponent } from "./tasks/tasks.component";
import en_translation from "../../assets/i18n/en.json";
import { User } from "../schemas/User";
import { Player } from "../schemas/Player";
import { Room } from "../schemas/Room";
import { IconComponent } from "../kwmc/icon/icon.component";
import { InteractionBoxesComponent } from "./interaction-boxes/interaction-boxes.component";
import { PlayerLabelComponent } from "./player-label/player-label.component";
import { IssuePanelComponent } from "./issue-panel/issue-panel.component";
import { SnackbarComponent } from "../kwmc/snackbar/snackbar.component";
import { SmallDeviceDisclaimerComponent } from "../kwmc/small-device-disclaimer/small-device-disclaimer.component";
import { ModalModule } from "../modal/modal.module";
import { AuthModule } from "../auth/auth.module";
import { RoomModule } from "./room.module";
import { KwmcModule } from "../kwmc/kwmc.module";
import { UserService } from "../user.service";
import { FooterModule } from "../footer/footer.module";
import { environment } from "../../environments/environment";
import { AiSubtaskEstimateQuota } from "../schemas/AiSubtaskEstimate";
import { QueryClient, provideAngularQuery } from "@tanstack/angular-query-experimental";
import { Game, GameWithTasks } from "../schemas/Game";
import { z } from "zod";
import {KehrwasserFeaturesModule, KwFeatureService} from "@kehrwasser-dev/features-angular";
import { signal } from "@angular/core";
import { RoomService } from "./room.service";
import { ignoreResizeObserverErrors } from "cypress/e2e/utilities";

describe("RoomComponent", () => {
  const user: User = {
      uuid: "some-uuid",
      hash: "some-hash",
      language: "en",
      role: "anonymous"
  };
  const player: Player = Player.parse({
    id: 1,
    avatar: "green",
    name: "Me",
    cardValue: "13",
    type: "estimator",
  });
  const room: Room = Room.parse({
    id: 1,
    status: "estimating",
    roundsPlayed: 10,
    maxPlayers: 2,
    created_at: new Date().getTime(),
    series: JSON.stringify(DEFAULT_OPTIONS[0].value),
    activeGameId: 1,
  });
  const game: Game = Game.parse({
    id: 1,
    roomId: 1,
    name: "Game #1",
    created_at: new Date().getTime(),
  });
  const players: Player[] = [
    player,
    {
      id: 2,
      name: "A",
      cardValue: "@break",
      type: "estimator",
      avatar: "orange",
    },
    { id: 3, name: "B", cardValue: "1", type: "estimator", avatar: "darkred" },
  ];
  const state: Partial<RoomState> = {
    getRoomId: () => 1,
    getRoom: () => room,
    getPlayers: () => players,
    getPlayerId: () => 1,
    getSelf: () => player,
    getUser: () => user,
    refreshJoinedRooms: async () => {},
    refresh: async () => {},
    getTasks: () => [],
  };

  const room2: Room = {
    id: 1,
    status: "estimating",
    roundsPlayed: 10,
    maxPlayers: 2,
    created_at: new Date().getTime(),
    series: JSON.stringify(DEFAULT_OPTIONS[0].value),
  };
  const players2: Player[] = z.array(Player).parse([
    player,
    {
      id: 2,
      name: "A",
      cardValue: "@break",
      type: "estimator",
      avatar: "orange",
    },
    { id: 3, name: "B", cardValue: "1", type: "estimator", avatar: "darkred" },
    { id: 4, name: "C", cardValue: "1", type: "estimator", avatar: "black" },
    { id: 5, name: "D", cardValue: "1", type: "estimator", avatar: "yellow" },
    { id: 6, name: "E", cardValue: "1", type: "estimator", avatar: "rose" },
    { id: 7, name: "F", cardValue: "1", type: "estimator", avatar: "lilac" },
    {
      id: 8,
      name: "G",
      cardValue: "1",
      type: "estimator",
      avatar: "lightblue",
    },
    { id: 9, name: "H", cardValue: "1", type: "estimator", avatar: "neon" },
    { id: 10, name: "I", cardValue: "1", type: "estimator", avatar: "brown" },
    { id: 11, name: "J", cardValue: "1", type: "estimator", avatar: "blue" },
  ]);
  const state2: Partial<RoomState> = {
    getRoomId: () => 1,
    getRoom: () => room2,
    getPlayers: () => players2,
    getPlayerId: () => 1,
    getSelf: () => player,
    getUser: () => user,
    refreshJoinedRooms: async () => {},
    refresh: async () => {},
  };

  const config: MountConfig<RoomComponent> = {
    imports: [
      FormsModule,
      TranslateTestingModule.withTranslations("en", en_translation),
      AuthModule,
      ModalModule,
      RoomModule,
      ReactiveFormsModule,
      KwmcModule,
      FooterModule,
      KehrwasserFeaturesModule,
    ],
    declarations: [
      RoomComponent,
      ConnectionStatusComponent,
      FooterComponent,
      CardComponent,
      HeaderbarComponent,
      SettingsMenuComponent,
      PokerTableComponent,
      BigPokerTableComponent,
      TabBarComponent,
      PlayerListComponent,
      TasksComponent,
      CardHandComponent,
      IconComponent,
      InteractionBoxesComponent,
      PlayerLabelComponent,
      IssuePanelComponent,
      SnackbarComponent,
      SmallDeviceDisclaimerComponent,
    ],
    providers: [
      provideAngularQuery(new QueryClient()),
      {
        provide: ActivatedRoute,
        useValue: {
          snapshot: {
            routeConfig: {
              path: `/room/${room.id}`,
            },
            paramMap: {
              get() {
                return room.id.toString();
              },
            },
          },
          queryParams: of({}),
        },
      },
      {
        provide: AgileCasinoService,
        useValue: {
          getRoomAndPlayers(roomId: number) {
            expect(roomId).to.be.equal(room.id);
            return of([room as Room, players]);
          },
          getSeries(): Observable<string[]> {
            return of(DEFAULT_OPTIONS[0].value ?? []);
          },
          getAvatars(): Observable<any> {
            return of(null);
          },
          getFeatures(): Observable<Array<string>> {
            return of([]);
          },
          getJoinedRooms(): Observable<any> {
            return of([
              { room: room, player: player, numPlayers: players.length },
            ]);
          },
          getPlayersOfRoom(): Observable<any> {
            return of(players);
          },
          getRoom(): Observable<any> {
            return of(room);
          },
          getTasks(): Observable<any> {
            return of([]);
          },
          getUser(_uuid: string): Observable<any> {
            return of({ uuid: "mock-uuid" });
          },
          getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
            return of({
              freeAiEstimations: 5,
            });
          },
          getGames(): Observable<Game[]> {
            return of([game]);
          },
          getGamesWithTasks(): Observable<Array<GameWithTasks>> {
            return of([]);
          },
          getPlayer(): Observable<any> {
            return of(player);
          },
        } satisfies Partial<AgileCasinoService>,
      },
      {
        provide: LocalStorage,
        useValue: {
          getUuid() {
            return "mock-uuid";
          },
          getHideInsecureWarning() {
            return false;
          },
          setHasSessions() {},
          hasSessions() {
            return false;
          },
        } satisfies Partial<LocalStorage>,
      },
      {
        provide: UserService,
        useValue: {
          isLoggedIn: () => false,
          userFeatureEnabled: () => true,
          getUserProfile: () => undefined,
          getUser: () => ({ uuid: "mock-uuid" }),
          isPro: () => false,
        }
      },
      {
        provide: KwFeatureService,
        useValue: {
          isFeatureEnabled: (feature: string) => {
            if (feature === "experiment203") {
              return false;
            }

            return true;
          },
          isSmokeTestEnabled: () => true,
          isSplitTestEnabled: () => true,
          getVariant: () => signal("plans-modal-old"),
          getRole: () => 'anonymous',
          emitSplitTestEvent() {},
        }
      },
      {
        provide: RoomService,
        useValue: {
          setRoom: () => {},
          room: signal(room),
          isPromotionPermitted: () => false,
        }
      },
    ],
    componentProperties: {
      state: state as RoomState,
    },
  };

  const configForBigRoom: MountConfig<RoomComponent> = {
    ...config,
    providers: [
      provideAngularQuery(new QueryClient()),
      {
        provide: ActivatedRoute,
        useValue: {
          snapshot: {
            routeConfig: {
              path: `/room/${room2.id}`,
            },
            paramMap: {
              get() {
                return room2.id.toString();
              },
            },
          },
          queryParams: of({}),
        },
      },
      {
        provide: AgileCasinoService,
        useValue: {
          getRoomAndPlayers(roomId: number) {
            expect(roomId).to.be.equal(room.id);
            return of([room2 as Room, players2]);
          },
          getSeries(): Observable<string[]> {
            return of(DEFAULT_OPTIONS[0].value ?? []);
          },
          getAvatars(): Observable<null> {
            return of(null);
          },
          getFeatures(): Observable<Array<string>> {
            return of([]);
          },
          getJoinedRooms(): Observable<unknown> {
            return of([
              { room: room2, player: player, numPlayers: players2.length },
            ]);
          },
          getPlayersOfRoom(): Observable<unknown> {
            return of(players2);
          },
          getRoom(): Observable<unknown> {
            return of(room2);
          },
          getTasks(): Observable<unknown> {
            return of([]);
          },
          getUser(): Observable<unknown> {
            return of({ uuid: "mock-uuid" });
          },
          getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
            return of({
              freeAiEstimations: 5,
            });
          },
          getGames(): Observable<Game[]> {
            return of([game]);
          },
          getGamesWithTasks(): Observable<Array<GameWithTasks>> {
            return of([]);
          },
        },
      },
    ],
    componentProperties: {
      state: state2 as RoomState,
    },
  };

  beforeEach(() => {
    window.localStorage.setItem("uuid", "mock-uuid");
    cy.viewport(1280, 1024);

    cy.intercept("GET", "/room/1/players", {
      body: players,
    });

    // HACK: avoid click inconsistencies
    cy.get("body").click()

    ignoreResizeObserverErrors(cy);
  });

  it("mounts", () => {
    cy.mount(RoomComponent, config);

    cy.contains('Leave');
  });

  // it("mounts big room", () => {
  //   cy.mount(RoomComponent, configForBigRoom);
  // });
  //
  // it("should toggle panel correctly", () => {
  //   cy.mount(RoomComponent, config).should(({ component }) => {
  //     expect(component.isPanelOpen).to.be.false;
  //     component.togglePanel();
  //     expect(component.isPanelOpen).to.be.true;
  //   });
  // });
  //
  // it("should toggle panel correctly in big room", () => {
  //   cy.mount(RoomComponent, configForBigRoom).should(({ component }) => {
  //     expect(component.isPanelOpen).to.be.false;
  //     component.togglePanel();
  //     expect(component.isPanelOpen).to.be.true;
  //   });
  // });
  //
  // it("should play a card", () => {
  //   cy.mount(RoomComponent, config).should(({ component, fixture }) => {
  //     const rootElement: HTMLElement = fixture.nativeElement;
  //     const hand = rootElement.querySelector(".hand");
  //     expect(component.state.joinedRooms).to.not.be.undefined;
  //     expect(component.state.joinedRooms.length).to.equal(1);
  //     expect(component.state.players).to.not.be.undefined;
  //     expect(component.state.players.length).to.equal(players.length);
  //     expect(component.player).to.be.not.null;
  //     expect(component.player).to.be.not.undefined;
  //     expect(component.player.type).to.not.be.equal("inspector");
  //     expect(hand).to.be.not.null;
  //   });
  //   cy.get(".value").contains("13");
  // });
  //
  // it("should play a card in big room", () => {
  //   cy.mount(RoomComponent, configForBigRoom).should(
  //     ({ component, fixture }) => {
  //       const rootElement: HTMLElement = fixture.nativeElement;
  //       const hand = rootElement.querySelector(".hand");
  //       expect(component.player).to.be.not.null;
  //       expect(component.player.type).to.not.be.equal("inspector");
  //       expect(hand).to.be.not.null;
  //     },
  //   );
  //   cy.get(".value").contains("13");
  // });
  //
  // it("should have appropriate buttons", () => {
  //   cy.mount(RoomComponent, config);
  //   const TakeBackButton = cy.get("button.button.button-default");
  //   TakeBackButton.contains("Take Back").should("not.have.class", "diabled");
  //
  //   const RevealButton = cy.get("button.button.button-danger");
  //   RevealButton.should("contains.text", "Reveal").should(
  //     "not.have.class",
  //     "disabled",
  //   );
  //
  //   const NewRoundButton = cy.get("button.button.button-default");
  //   NewRoundButton.contains("New Round").should("not.be", "visible");
  // });
  //
  // it("should have appropriate buttons in big room", () => {
  //   cy.mount(RoomComponent, configForBigRoom);
  //   const TakeBackButton = cy.get("button.button.button-default");
  //   TakeBackButton.contains("Take Back").should("not.have.class", "disabled");
  //
  //   const RevealButton = cy.get("button.button.button-danger");
  //   RevealButton.should("contains.text", "Reveal").should(
  //     "not.have.class",
  //     "disabled",
  //   );
  //
  //   const NewRoundButton = cy.get("button.button.button-default");
  //   NewRoundButton.contains("New Round").should("not.be", "visible");
  // });
  //
  // it("should have kick modal", () => {
  //   cy.mount(RoomComponent, config);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Players').click();
  //   }
  //
  //   cy.get(
  //     "app-player-list > .table > :nth-child(1) > .avatar-wrapper > .avatar-background",
  //   ).should("have.class", "you");
  //   cy.get(
  //     "app-player-list > .table > :nth-child(1) > .avatar-wrapper > .avatar-background",
  //   ).click();
  //
  //   cy.get(
  //     "app-player-list > .table > :nth-child(2) > app-context-menu",
  //   ).click();
  //   cy.get(
  //     "app-player-list > .table > :nth-child(2) > app-context-menu > app-context-menu-popup > .menuitem",
  //   )
  //     .contains("Kick Player")
  //     .should("not.have.class", "nodisplay")
  //     .click();
  //   const confBox = cy.get("app-confirmation-box");
  //   confBox
  //     .contains("This will kick the selected player. Are you sure?")
  //     .should("not.have.class", "hidden");
  // });
  //
  // it("should have kick modal in big room", () => {
  //
  //   cy.mount(RoomComponent, configForBigRoom);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Players').click();
  //   }
  //
  //   cy.get(
  //     "app-player-list > .table > :nth-child(1) > .avatar-wrapper > .avatar-background",
  //   ).should("have.class", "you");
  //   cy.get(
  //     "app-player-list > .table > :nth-child(1) > .avatar-wrapper > .avatar-background",
  //   ).click();
  //
  //   cy.get(
  //     "app-player-list > .table > :nth-child(2) > app-context-menu",
  //   ).click();
  //   cy.get(
  //     "app-player-list > .table > :nth-child(2) > app-context-menu > app-context-menu-popup > .menuitem",
  //   )
  //     .contains("Kick Player")
  //     .should("not.have.class", "nodisplay")
  //     .click();
  //   const confBox = cy.get("app-confirmation-box");
  //   confBox
  //     .contains("This will kick the selected player. Are you sure?")
  //     .should("not.have.class", "hidden");
  // });
  //
  // it("should have leave confirmation box", () => {
  //
  //   cy.mount(RoomComponent, config);
  //   cy.get(".control").contains("Leave").click();
  //
  //   cy.get("app-confirmation-box")
  //     .contains("Are you sure you want to leave the room?")
  //     .should("not.have.class", "hidden");
  //   cy.get("dialog").should('be.visible');
  //   cy.get(".confirmation-box-leave button").contains("Cancel").click();
  //
  //   cy.get("app-confirmation-box").should("not.be.visible");
  //
  //   cy.get(".control > :nth-child(2)").contains("Leave").click();
  //   cy.get("app-confirmation-box")
  //     .contains("Are you sure you want to leave the room?")
  //     .should("not.have.class", "hidden");
  //   cy.get(":nth-child(3) .button-danger").contains(
  //     "Ok",
  //   );
  // });
  //
  // it("should have leave confirmation box in big room", () => {
  //
  //   cy.mount(RoomComponent, configForBigRoom);
  //   cy.get(".control > :nth-child(2)").contains("Leave").click();
  //
  //   cy.get("app-confirmation-box")
  //     .contains("Are you sure you want to leave the room?")
  //     .should("not.have.class", "hidden");
  //   cy.get("dialog").should('be.visible');
  //   cy.get(".confirmation-box-leave").contains("Cancel").click();
  //
  //   cy.get("app-confirmation-box").should("not.be.visible");
  //
  //   cy.contains("Leave").click();
  //   cy.get("app-confirmation-box")
  //     .contains("Are you sure you want to leave the room?")
  //     .should("not.have.class", "hidden");
  //   cy.get(".button-danger").contains(
  //     "Ok",
  //   );
  // });
  //
  // it("should have sideline modal", () => {
  //
  //   cy.mount(RoomComponent, config);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Players').click();
  //   }
  //
  //   cy.get(
  //     "app-player-list > .table > :nth-child(1) > .avatar-wrapper > .avatar-background",
  //   ).should("have.class", "you");
  //   cy.get(
  //     "app-player-list > .table > :nth-child(1) > .avatar-wrapper > .avatar-background",
  //   ).click();
  //
  //   cy.get(
  //     "app-player-list > .table > :nth-child(2) > app-context-menu",
  //   ).click();
  //   cy.get(
  //     "app-player-list > .table > :nth-child(2) > app-context-menu > app-context-menu-popup > .menuitem",
  //   )
  //     .contains("Make spectator")
  //     .should("not.have.class", "nodisplay")
  //     .click();
  //   const confBox = cy.get("app-confirmation-box");
  //   confBox
  //     .contains("This will turn the player into a spectator. Are you sure?")
  //     .should("not.have.class", "hidden");
  // });
  //
  // it("shows plans modal after adding tasks", () => {
  //   cy.mount(RoomComponent, config);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Edit Tasks').click();
  //     cy.contains("Game #1").parent().contains("New task").click();
  //   } else {
  //     cy.get("app-tab-bar").contains("Tasks").click();
  //     cy.contains('Create Task').click();
  //   }
  //
  //   cy.get('app-plans-page').should('be.visible').within(() => {
  //     cy.contains('Need to access Task feature?');
  //     cy.contains('You cannot use the Task Management without registration, but getting access is easy.');
  //   })
  // });
  //
  // it('shows plans modal after importing tasks', () => {
  //   cy.mount(RoomComponent, config);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Edit Tasks').click();
  //     cy.contains("Game #1").parent().get(".import-button").click();
  //   } else {
  //     cy.get("app-tab-bar").contains("Tasks").click();
  //     cy.get('app-tasks').should('be.visible').contains('Connect external source').click();
  //   }
  //
  //   cy.get('app-plans-page').should('be.visible').within(() => {
  //     cy.contains('Need to access Import feature?');
  //     cy.contains('You cannot use the Import feature without registration, but getting access is easy.');
  //   });
  // })
  //
  //
  // it('tries to create task', () => {
  //   cy.mount(RoomComponent, config);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Edit Tasks').click();
  //     cy.contains("Game #1").parent().contains("New task").click();
  //   } else {
  //     cy.get("app-tab-bar").contains("Tasks").click();
  //     cy.contains('Create Task').click();
  //   }
  //
  //   cy.get('app-plans-page').should('be.visible').within(() => {
  //     cy.contains('Plans');
  //   })
  // })
  //
  // it('tries to create task, then chooses login', () => {
  //   cy.mount(RoomComponent, config);
  //
  //   if (environment.newSidebar) {
  //     cy.contains('Edit Tasks').click();
  //     cy.contains("Game #1").parent().contains("New task").click();
  //   } else {
  //     cy.get("app-tab-bar").contains("Tasks").click();
  //     cy.contains('Create Task').click();
  //   }
  //
  //   cy.get('app-plans-page').should('be.visible').within(() => {
  //     cy.contains('Plans');
  //     cy.contains('Sign in').click();
  //   })
  //
  //   cy.contains('Login to account');
  // })

  it('tries to create task, then chooses to upgrade', () => {
    cy.mount(RoomComponent, config);

    if (environment.newSidebar) {
      cy.contains('create a first task').click();
      cy.contains('New task').click();
    } else {
      cy.get("app-tab-bar").contains("Tasks").click();
      cy.contains('Create Task').click();
    }

    cy.get('app-plans-page').should('be.visible').within(() => {
      cy.contains('Plans');
      cy.contains('Upgrade to Professional').click();
    })

    cy.get('app-checkout-page').should('be.visible').within(() => {
      cy.contains('Sorry, not yet available');
    })
  })

  it('shows payment checkout on upgrade', () => {
    cy.mount(RoomComponent, config);
    cy.get('.overflowmenu-background > .menu').click();
    cy.get('.overflowmenu-background > .dropdown').contains('Lock room').click();

    cy.get('app-plans-page').should('be.visible').within(() => {
      cy.contains('Upgrade to Professional').click();
    })

    cy.get('app-checkout-page').should('be.visible').within(() => {
      cy.contains('Professional Plan Subscription');
    })
  })

  it('triggered by ai estimations', () => {
    cy.mount(RoomComponent, config);
    cy.contains('AI').click();
    cy.get('app-ai-estimations').contains('Generate estimation').click();
    cy.get('app-plans-page').should('be.visible').within(() => {
      cy.contains('Need to access AI Support?');
      cy.contains('You cannot use AI support for free, but getting access is easy.');
    })
  })

  it('triggered by ai subtask', () => {
    cy.mount(RoomComponent, config);
    cy.contains('AI').click();
    cy.get('app-ai-subtasks').contains('Generate Concept Idea by AI').click();
    cy.get('app-plans-page').should('be.visible').within(() => {
      cy.contains('You would like to have subtasks created by AI?');
      cy.contains('AI will give you ideas, won\'t forget any essential subtasks and you can book it now.');
    })
  })

  it('triggered by lock room', () => {
    cy.mount(RoomComponent, config);
    cy.get('.overflowmenu-background > .menu > img').click();
    cy.get('.overflowmenu-background').contains('Lock room').click();
    cy.get('app-plans-page').should('be.visible').within(() => {
      cy.contains('Do you want to keep your tasks safe from third parties?');
      cy.contains('Prevent data leaks and secure your organization\'s internal information. Subscribe here for professional data security.');
    })
  })

  it('triggered by ressource allocation', () => {
    cy.mount(RoomComponent, config);
    cy.contains('Stats').click();
    cy.contains('Statistic - Game #1');
    cy.contains('Enable with Pro').click();
    cy.get('app-plans-page').should('be.visible').within(() => {
      cy.contains('Need automated reports?');
      cy.contains('Elite team management automated: RACI, resource allocation and forecasts. Always ready for the next status meeting.');
    })
  })
  // TODO: need to either mock the websocket connection (e.g. for takeCardBack())
  // TODO: or make this test an e2e test
  //
  // it('should be revealable even if one player put back his card', () => {
  //
  //   let currentState = cy.mount(RoomComponent, config);
  //   currentState.should(({ component }) => {
  //     expect(component.player.cardValue).to.be.not.null});
  //
  //   cy.get('button.button.button-default').should('contains.text', 'Take Back').click().should('not.exist')
  //
  //   currentState.then(({ component }) => {
  //     expect(component.player.cardValue).to.be.null
  //     expect(component.cardValueTemp).to.be.null});
  //
  //   cy.get('button.button.button-orange').should('contains.text', 'Reveal')
  // });
});
