import { BREAK_CARD_VALUE } from "."
import { Player } from "../schemas/Player"
import { Room } from "../schemas/Room"
import { Stats } from "./statistics"


export type TableView = {
  currentPlayer: Player
  stats: Stats
} & (RevealedTableView | ConcealedTableView)

export interface RevealedTableView {
  type: "revealed"
  players: Array<RevealedPlayer>
}

export interface ConcealedTableView {
  type: "concealed"
  players: Array<ConcealedPlayer>
}

export type PlayerExtension = {
  isTakingBreak: boolean,
  isReady: boolean,
}

export type ConcealedPlayer = Omit<Player, "cardValue"> & PlayerExtension &{
  concealedCardValue: ConcealedCardValue
}

export type RevealedPlayer = Player & PlayerExtension

export type ConcealedCardValue = "ready" | "waiting" | "break"

export type PlayerView = RevealedPlayerView | ConcealedPlayerView

export interface RevealedPlayerView {
  type: "revealed"
  stats?: Stats
  player: RevealedPlayer
}

export interface ConcealedPlayerView {
  type: "concealed"
  player: ConcealedPlayer,
}

export function concealPlayer(player: Player): ConcealedPlayer {
  const { cardValue, ...concealedPlayer} = player
  const concealedCardValue: ConcealedCardValue = cardValue === BREAK_CARD_VALUE
    ? "break"
    : cardValue == null || cardValue === ""
      ? "waiting"
      : "ready"

  return {
    ...concealedPlayer,
    concealedCardValue: concealedCardValue,
    isTakingBreak: concealedCardValue === "break",
    isReady: concealedCardValue === "ready" || concealedCardValue === "break",
  }
}

export function revealPlayer(player: Player): RevealedPlayer {
  return {
    ...player,
    isTakingBreak: player.cardValue === BREAK_CARD_VALUE,
    isReady: !!player.cardValue,
  }
}

export function getTableView(
  room: Room,
  currentPlayer: Player,
  players: Array<Player>,
  stats: Stats,
  options?: { forceConceal: boolean }
): TableView {
  const forceConceal = options?.forceConceal ?? false;

  const type = forceConceal || room.status === "estimating" ? "concealed" : "revealed"

  switch (type) {
    case "concealed": {
      return ({
        type,
        stats: stats,
        currentPlayer: currentPlayer,
        players: players.map((player) => concealPlayer(player)),
      })
    }
    case "revealed": {
      return ({
        type,
        stats: stats,
        currentPlayer: currentPlayer,
        players: players.map((player) => revealPlayer(player)),
      })
    }
  }
}

export function getPlayerView(tableView: TableView, playerId: Player["id"]): PlayerView | undefined {
  const isCurrentPlayer = playerId === tableView.currentPlayer.id;
  const player = tableView.players.find((p) => p.id === playerId);

  if (!player) {
    return;
  }

  switch (tableView.type) {
    case "revealed": {
      const player = tableView.players.find((p) => p.id === playerId);

      if (!player) {
        return;
      }

      return {
        type: tableView.type,
        player,
        stats: tableView.stats,
      };
    }
    case "concealed": {
      const player = tableView.players.find((p) => p.id === playerId);

      if (!player) {
        return;
      }

      if (isCurrentPlayer) {
        return {
          type: "revealed",
          player: revealPlayer(tableView.currentPlayer),
        }
      }

      return {
        type: tableView.type,
        player,
      };
    }
  }
}
