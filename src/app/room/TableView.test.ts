import { describe, test, expect } from 'vitest';
import { ConcealedPlayer, RevealedPlayer, TableView, getPlayerView } from './TableView';

describe("getPlayerView()", () => {
  test("revealed view", () => {
    const tableView: TableView = {
      type: "revealed",
      players: [
        {
          id: 1,
          name: "one",
          cardValue: "13",
          isReady: true,
          isTakingBreak: false,
        } , {
          id: 2,
          name: "two",
          isReady: false,
          isTakingBreak: false,
          cardValue: "",
        }
      ],
      currentPlayer: {id: 2, name: "two"},
      stats: null,

    };

    expect(getPlayerView(tableView, 1).type).toEqual("revealed");
    expect(getPlayerView(tableView, 1).player).not.toBeNull();
    expect((getPlayerView(tableView, 1).player as RevealedPlayer).cardValue).toEqual("13");
  })

  test("concealed view", () => {
    const tableView: TableView = {
      type: "concealed",
      players: [
        {
          id: 1,
          name: "one",
          concealedCardValue: "ready",
          isReady: true,
          isTakingBreak: false,
        } , {
          id: 2,
          name: "two",
          concealedCardValue: "waiting",
          isReady: false,
          isTakingBreak: false,
        } , {
          id: 3,
          name: "three",
          concealedCardValue: "break",
          isReady: true,
          isTakingBreak: true,
        }
      ],
      currentPlayer: {id: 2, name: "two", cardValue: ""},
      stats: null,

    };

    expect(getPlayerView(tableView, 1).type).toEqual("concealed");
    expect(getPlayerView(tableView, 1).player).not.toBeNull();
    expect((getPlayerView(tableView, 1).player as ConcealedPlayer).concealedCardValue).toEqual("ready");

    expect(getPlayerView(tableView, 2).type).toEqual("revealed");
    expect(getPlayerView(tableView, 2).player).not.toBeNull();
    expect((getPlayerView(tableView, 2).player as RevealedPlayer).cardValue).toEqual("");

    expect(getPlayerView(tableView, 3).type).toEqual("concealed");
    expect(getPlayerView(tableView, 3).player).not.toBeNull();
    expect((getPlayerView(tableView, 3).player as ConcealedPlayer).concealedCardValue).toEqual("break");
  })
})
