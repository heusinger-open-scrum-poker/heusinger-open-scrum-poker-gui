import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Player} from "../../schemas/Player";
import {Room} from "../../schemas/Room";
import {Stats} from "../statistics";
import {animate, style, transition, trigger} from "@angular/animations";
import {environment} from "../../../environments/environment";

@Component({
  selector: "app-bottom-area",
  templateUrl: "./bottom-area.component.html",
  styleUrls: ["./bottom-area.component.scss"],
  animations: [
    trigger(
      "blockInitialRenderAnimation",
      [
        transition( ":enter", [] )
      ]
    ),
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ position: "relative", top: '100%' }),
            animate('0.5s 0.5s ease-out',
              style({position: "relative",  top: 0 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({position: "relative",  top: 0 }),
            animate('0.5s ease-in',
              style({position: "relative",  top: '100%' }))
          ]
        )
      ]
    )
  ]
})
export class BottomAreaComponent {
  @Input() player!: Player;
  @Input() room!: Room;
  @Input() cardValueTemp!: null | string;
  @Input({ required: true }) stats!: Stats;
  @Input() roomStatus!: string;

  @Output("breakCardClicked") onBreakCardClicked: EventEmitter<void> = new EventEmitter<void>();
  @Output("cardClicked") onCardClicked: EventEmitter<string> = new EventEmitter<string>();

  bubbleUpBreakCardClicked(): void {
    this.onBreakCardClicked.emit();
  }

  bubbleUpCardClicked(event: string): void {
    this.onCardClicked.emit(event);
  }

  isRevealed(): boolean {
    return this.roomStatus === 'revealed';
  }

  get newStatisticsEnabled() {
    return environment.newStatistics;
  }

  get showAverage() {
    return environment.showAverage;
  }
}
