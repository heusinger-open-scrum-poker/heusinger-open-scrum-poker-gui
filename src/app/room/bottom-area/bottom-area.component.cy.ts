import {BottomAreaComponent} from "./bottom-area.component";
import {Room} from "../../schemas/Room";
import {DEFAULT_OPTIONS} from "../custom-cards-modal";
import {Player} from "../../schemas/Player";
import {computeStats} from "../statistics";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe("BottomAreaComponent", () => {
  const player: Player = {
    id: 1,
    avatar: "green",
    name: "Me",
    cardValue: "13",
    type: "estimator",
  };

  const roomEstimating: Room = {
    id: 1,
    status: "estimating",
    roundsPlayed: 10,
    maxPlayers: 2,
    created_at: new Date().getTime(),
    series: JSON.stringify(DEFAULT_OPTIONS[0].value),
  };

  const roomRevealed: Room = {
    id: 1,
    status: "revealed",
    roundsPlayed: 10,
    maxPlayers: 2,
    created_at: new Date().getTime(),
    series: JSON.stringify(DEFAULT_OPTIONS[0].value),
  };

  const stats = computeStats([player] as Player[], roomEstimating as Room);

  it("shows card hand while estimating", () => {
    cy.mount(BottomAreaComponent, {
      imports: [BrowserAnimationsModule],
      componentProperties: {
        player: player,
        room: roomEstimating,
        cardValueTemp: null,
        roomStatus: 'estimating',
        stats: stats,
      }
    });

    cy.get('app-card-hand').should('exist');
    cy.get('app-stats').should('not.exist');
    cy.get('app-statistics').should('not.exist');
  });

  it("shows statistics while revealed", () => {
    cy.mount(BottomAreaComponent, {
      imports: [BrowserAnimationsModule],
      componentProperties: {
        player: player,
        room: roomRevealed,
        cardValueTemp: null,
        roomStatus: 'revealed',
        stats: stats,
      }
    });

    cy.get('app-card-hand').should('not.exist');
  });
});
