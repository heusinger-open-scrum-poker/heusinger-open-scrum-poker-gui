// Math
type Vec2 = [number, number];
type Mat22 = [[number, number], [number, number]];

const rot = (ang: number): Mat22 => [
  [Math.cos(ang), -Math.sin(ang)],
  [Math.sin(ang), Math.cos(ang)],
];
const dot = (v1: Vec2, v2: Vec2): number => v1[0] * v2[0] + v1[1] * v2[1];
const mul = (m: Mat22, v: Vec2): Vec2 => [dot(m[0], v), dot(m[1], v)];

const CARD_WIDTH = 123.2;
const CARD_HEIGHT = 182.2;
const CARD_CORNER_SIZE = 38;
const v0: Vec2 = [CARD_WIDTH / 2, 0];
const w0: Vec2 = [CARD_WIDTH / 2 - CARD_CORNER_SIZE, CARD_HEIGHT];
const angOf = (deg: number): number => (deg / 180) * Math.PI;
export const degOf = (ang: number): number => (ang / Math.PI) * 180;
const v2 = dot(v0, v0);
const offsetBetween = (ang1: number, ang2: number) =>
  (v2 - dot(mul(rot(ang1), v0), mul(rot(ang2), w0))) /
  dot(mul(rot(ang2), v0), [1, 0]);
const squishAtan = (deg: number): number =>
  100 * (Math.atan((deg / 2 - 9.7) / 30) / Math.PI + 0.5) - 40;

function computeOffsets(angles: number[]): number[] {
  if (angles.length == 0) {
    return [];
  }

  let cumulativeOffset = 0;
  const offsets = [cumulativeOffset];

  for (let i = 1; i < angles.length; ++i) {
    cumulativeOffset += offsetBetween(angles[i - 1], angles[i]);
    offsets.push(cumulativeOffset);
  }

  const totalOffset = cumulativeOffset;
  return offsets.map((it) => Math.round(it - totalOffset / 2));
}

type CardStyleData = { angle: string; left: string; top: string };

export function computeCardStyleData(angles: number[]): CardStyleData[] {
  const offsets = computeOffsets(angles);
  const cardStyle = [];
  for (let i = 0; i < angles.length; ++i) {
    const deg = degOf(angles[i]);
    const offset = offsets[i];
    const top = 96 - Math.exp(-Math.pow(offset / 180, 2)) * 72;
    // const top = 11 + Math.pow(offset/30,2);
    // const top = 10 + Math.abs(Math.pow(offset/60,3));
    cardStyle.push({
      angle: `${-Math.round(deg)}deg`,
      left: `${Math.round(offset)}px`,
      top: `${Math.round(top)}px`,
    });
  }
  return cardStyle;
}

function asElementStyle(cardData: CardStyleData): string {
  return `top: ${cardData.top}; transform: rotate(${cardData.angle}); left: ${cardData.left};`;
}

function asElementStyleChosen(cardData: CardStyleData): string {
  return `top: calc(${cardData.top} - 16px); transform: rotate(${cardData.angle}); left: ${cardData.left};`;
}

function asElementStylePlaced(cardData: CardStyleData): string {
  return `top: calc(${cardData.top} - 80px); opacity: 0.0; visibility: hidden; transform: rotate(${cardData.angle}); left: ${cardData.left};`;
}

export function computeCardAngles(
  length: number,
  squish: (deg: number) => number = squishAtan,
) {
  if (length == 1) {
    return [0];
  }
  return Array.from({ length }, (_, i) => {
    return angOf(squish((i - Math.ceil(length / 2)) * -10));
  });
}

export function computeStyles(
  length: number,
  squish: (deg: number) => number = squishAtan,
): { normal: string[]; chosen: string[]; placed: string[] } {
  const angles = computeCardAngles(length, squish);
  const styleData = computeCardStyleData(angles);
  return {
    normal: styleData.map(asElementStyle),
    chosen: styleData.map(asElementStyleChosen),
    placed: styleData.map(asElementStylePlaced),
  };
}
