import { describe, it, expect } from 'vitest';
import { degOf, computeCardAngles } from "./index";

describe("card-hand", () => {
  it("correctly computes the degrees list", () => {
    const expectedDegrees = [
      [],
      [0],
      [10, 0],
      [20, 10, 0],
      [20, 10, 0, -10],
      [30, 20, 10, 0, -10],
      [30, 20, 10, 0, -10, -20],
      [40, 30, 20, 10, 0, -10, -20],
      [40, 30, 20, 10, 0, -10, -20, -30],
      [50, 40, 30, 20, 10, 0, -10, -20, -30],
      [50, 40, 30, 20, 10, 0, -10, -20, -30, -40],
      [60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40],
      [60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50],
      [70, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50],
      [70, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50, -60],
      [80, 70, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50, -60],
      [80, 70, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50, -60, -70],
      [
        90, 80, 70, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50, -60,
        -70,
      ],
      [
        90, 80, 70, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -50, -60,
        -70, -80,
      ],
    ];

    const noSquish = (x: number) => x;
    const computedDegrees = Array.from({ length: 19 }, (_, i) =>
      computeCardAngles(i, noSquish).map(degOf),
    );

    expect(computedDegrees.length).to.equal(expectedDegrees.length);
    expect(computedDegrees.map((it) => it.join(",")).join("\n")).to.equal(
      expectedDegrees.map((it) => it.join(",")).join("\n"),
    );
  });
});
