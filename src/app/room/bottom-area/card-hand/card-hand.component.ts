import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import { readSeries } from "src/app/room/custom-cards-modal";
import { Player } from "src/app/schemas/Player";
import { Room } from "src/app/schemas/Room";
import { computeStyles } from "./index";
import { BREAK_CARD_VALUE } from "../../index";

@Component({
  selector: "app-card-hand",
  templateUrl: "./card-hand.component.html",
  styleUrls: ["./card-hand.component.scss"],
})
export class CardHandComponent implements OnInit, OnChanges {
  @Input() player!: Player;
  @Input() room!: Room;
  @Input() cardValueTemp!: null | string;
  @Output("breakCardClicked") onBreakCardClicked: EventEmitter<void> =
    new EventEmitter<void>();
  @Output("cardClicked") onCardClicked: EventEmitter<string> =
    new EventEmitter<string>();
  @Output("cardValueTempChanged") onCardValueTempChanged: EventEmitter<
    null | string
  > = new EventEmitter<null | string>();

  series: string[] = [];
  seriesReversed: { value: string; cardIndex: number }[] = [];
  styles?: ReturnType<typeof computeStyles>;
  containsBreakCard: boolean = false;

  ngOnInit() {
    this.refreshCache();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["room"]) {
      this.refreshCache();
    }
  }

  refreshCache(): void {
    let config: ReturnType<typeof readSeries>; // TODO type of output of readSeries
    if (this.room.series) {
      config = readSeries(JSON.parse(this.room.series));
    } else {
      config = readSeries([
        "1",
        "2",
        "3",
        "5",
        "8",
        "13",
        "21",
        "34",
        "unclear",
        "break",
      ]);
    }
    const cards = config.values.filter((it) => !!it);
    if (config.unclear) {
      cards.push("?");
    }
    this.series = cards;
    this.containsBreakCard = config.break_;
    this.styles = computeStyles(this.numCards());
    this.seriesReversed = Array.from(
      { length: this.numCards() },
      (_, index) => {
        const cardIndex = this.series.length - 1 - index;
        return {
          value: this.series[cardIndex],
          cardIndex: cardIndex,
        };
      },
    );
  }

  isBreakCardPlaced(): boolean {
    return this.isCardPlaced(BREAK_CARD_VALUE);
  }

  isBreakCardChosen(): boolean {
    return this.isCardChosen(BREAK_CARD_VALUE);
  }

  public isCardPlaced(cardValue: string): boolean {
    return this.player?.cardValue === cardValue;
  }

  public isCardChosen(cardValue: string): boolean {
    return this.cardValueTemp === cardValue;
  }

  isCardsRevealed(): boolean {
    return this.room.status === "revealed";
  }

  numCards(): number {
    return this.series.length + (this.containsBreakCard ? 1 : 0);
  }

  getStyle(index: number): string {
    if (!this.styles) {
      return "";
    }

    if (index == this.series.length) {
      if (this.isBreakCardPlaced()) {
        return this.styles.placed[index];
      } else if (this.isBreakCardChosen()) {
        return this.styles.chosen[index];
      } else {
        return this.styles.normal[index];
      }
    }

    if (this.series[index] === this.player.cardValue) {
      return this.styles.placed[index];
    } else if (this.series[index] === this.cardValueTemp) {
      return this.styles.chosen[index];
    } else {
      return this.styles.normal[index];
    }
  }
}
