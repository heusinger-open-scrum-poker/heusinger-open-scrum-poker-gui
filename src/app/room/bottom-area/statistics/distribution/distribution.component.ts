import {Component, Input} from '@angular/core';
import {Stats} from "../../../statistics";

@Component({
  selector: 'app-distribution',
  templateUrl: './distribution.component.html',
  styleUrl: './distribution.component.scss'
})
export class DistributionComponent {
  @Input({ required: true }) stats!: Stats;
  @Input() color: string = 'var(--color-emphasis)';

  get distribution(): Stats['distribution'] {
    return this.stats.distribution;
  }

  get cardValues(): string[] {
    return Object.keys(this.distribution);
  }

  heightInPx(cardValue: string): string {
    // max height is 40px
    return (40 * (this.distribution[cardValue] ?? 0) / this.stats.maxVotes) + 'px';
  }
}
