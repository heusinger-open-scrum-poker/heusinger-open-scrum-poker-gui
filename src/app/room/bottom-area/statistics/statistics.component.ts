import {Component, Input, ViewChild} from '@angular/core';
import {getConsensus, Stats} from "../../statistics";
import {Settings} from "../../../shared/Settings";
import {AvgDisclaimerModalComponent} from "../../avg-disclaimer-modal/avg-disclaimer-modal.component";
import {FunnelEventService} from "../../../funnel-event.service";
import {Feature} from "../../../auth/auth-modal/plans-page/feature";
import {AuthModalComponent} from "../../../auth/auth-modal/auth-modal.component";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrl: './statistics.component.scss'
})
export class StatisticsComponent {
  @ViewChild('authModal') authModal?: AuthModalComponent;
  @ViewChild("avgDisclaimerModal") avgDisclaimerModal?: AvgDisclaimerModalComponent;

  @Input() stats!: Stats;

  constructor(
    private funnelEventService: FunnelEventService,
  ) {}

  get consensusInPercent(): number {
    if (this.stats.totalVotes == 0) {
      return 0;
    }
    return Math.floor((this.stats.maxVotes / this.stats.totalVotes) * 100);
  }

  get majorCandidate(): string | undefined {
    return getConsensus(this.stats);
  }

  get consensusReached(): boolean {
    return this.consensusInPercent >= Settings.MINIMUM_CONSENSUS_PERCENT;
  }

  showAvgDisclaimer() {
    this.avgDisclaimerModal?.showModal();
  }

  triggerAverage(): void {
    this.funnelEventService.smokeTestEvent(Feature.Average, 'activate average clicked');
    this.authModal?.showPlans({
      triggeredByFeature: Feature.Average,
    });
  }
}
