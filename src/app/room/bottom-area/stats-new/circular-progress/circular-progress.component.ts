import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-circular-progress',
  templateUrl: './circular-progress.component.html',
  styleUrl: './circular-progress.component.scss'
})
export class CircularProgressComponent {
  @Input() percentage: number = 0;
  @Input() value: string = "";
  @Input() backgroundColor: string = '#34343A';
  @Input() barColor: string = '#ffffff';
  @Input() innerColor: string = 'var(--color-input-border-primary)';

  get border(): string {
    return 'border: 4px solid ' + this.innerColor;
  }

  get gradient(): string {
    return 'conic-gradient(' + this.barColor + ' ' + (this.percentage * 3.6) + 'deg, ' + this.backgroundColor + ' 0deg)';
  }
}
