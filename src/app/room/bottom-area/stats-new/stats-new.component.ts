import { Component, Input, ViewChild } from "@angular/core";
import { Stats, getConsensus } from "../../statistics";
import { AvgDisclaimerModalComponent } from "../../avg-disclaimer-modal/avg-disclaimer-modal.component";
import { Settings } from "src/app/shared/Settings";

@Component({
  selector: "app-stats-new",
  templateUrl: "./stats-new.component.html",
  styleUrls: ["./stats-new.component.scss"],
})
export class StatsNewComponent {
  @ViewChild("avgDisclaimerModal") avgDisclaimerModal?: AvgDisclaimerModalComponent;
  @Input() stats!: Stats;

  constructor() {}

  get consensusInPercent(): number {
    if (this.stats.totalVotes == 0) {
      return 0;
    }
    return Math.floor((this.stats.maxVotes / this.stats.totalVotes) * 100);
  }

  get majorCandidate(): string | undefined {
    return getConsensus(this.stats);
  }

  get consensusReached(): boolean {
    return this.consensusInPercent >= Settings.MINIMUM_CONSENSUS_PERCENT;
  }

  get minCard(): string | undefined {
    return this.stats.minCard
  }

  get maxCard(): string | undefined {
    return this.stats.maxCard
  }

  showAvgDisclaimer() {
    this.avgDisclaimerModal?.showModal();
  }
}
