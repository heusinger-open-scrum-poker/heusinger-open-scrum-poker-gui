import {
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { Player, PlayerRef } from "src/app/schemas/Player";
import { PlayerView, TableView, getPlayerView } from "../TableView";
import { environment } from "../../../environments/environment";
import { filterUndefined } from "src/app/shared/util";

@Component({
  selector: "app-poker-table",
  templateUrl: "./poker-table.component.html",
  styleUrls: ["./poker-table.component.scss"],
  host: {
    revealed: "isCardsRevealed()",
  },
})
export class PokerTableComponent {
  @Input({ required: true }) tableView!: TableView;
  @Output("kick") onKick: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("sideline") onSideline: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("promote") onPromote: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();

  get self(): Player {
    return this.tableView.currentPlayer;
  }

  get newStatisticsEnabled() {
    return environment.newStatistics;
  }

  get playerLeft(): PlayerView | undefined {
    const player = this.tableView.players.filter((player) => player.type === "estimator")[0] ?? null

    if (!player) {
      return;
    }

    return getPlayerView(this.tableView, player.id);
  }

  get playersTop(): Array<PlayerView> {
    return filterUndefined(this.tableView.players
      .filter((player) => player.type === "estimator")
      .slice(1, 4)
      .map((player) => getPlayerView(this.tableView, player.id)));
  }

  getPlayersBottom(): Array<PlayerView> {
    return filterUndefined(this.tableView.players
      .filter((player) => player.type === "estimator")
      .slice(4, 7)
      .map((player) => getPlayerView(this.tableView, player.id)));
  }

  get playerRight(): PlayerView | undefined {
    const player = this.tableView.players.filter((player) => player.type === "estimator")[7] ?? null;

    if (!player) {
      return;
    }

    return getPlayerView(this.tableView, player.id);
  }

  isCardsRevealed(): boolean {
    return this.tableView.type === "revealed";
  }

  isCurrentPlayer(player: Player): boolean {
    return player.id === this.tableView.currentPlayer.id;
  }

  playerPlacedValue(player: Player): boolean {
    return player.cardValue !== null;
  }

  isPlayersCardRevealed(player: Player): boolean {
    const isCurrentPlayerWithPlacedValue =
      this.isCurrentPlayer(player) && this.playerPlacedValue(player);
    return this.isCardsRevealed() || isCurrentPlayerWithPlacedValue;
  }

  identifyPlayer(_index: number, view: PlayerView): Player["id"] {
    return view.player.id;
  }
}
