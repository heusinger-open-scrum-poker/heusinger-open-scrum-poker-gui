import { Component, EventEmitter, Input, Output } from "@angular/core";
import { PlayerRef } from "src/app/schemas/Player";

@Component({
  selector: "app-player-context-menu",
  templateUrl: "./player-context-menu.component.html",
  styleUrls: ["./player-context-menu.component.scss"],
})
export class PlayerContextMenuComponent {
  @Input({ required: true }) player!: PlayerRef;
  @Input({ required: true }) self!: PlayerRef;
  @Input() enablePromote: boolean = false;
  @Input() placement: "bottom-end" | "bottom-start" = "bottom-end";
  @Output("kick") onKick: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("sideline") onSideline: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("promote") onPromote: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();

  get isInspector() {
    return this.player?.type === "inspector";
  }

  get isEstimator() {
    return this.player?.type === "estimator";
  }
}
