import { ZodError, ZodType, z } from 'zod';
import { Client, IMessage, StompConfig, StompSubscription } from '@stomp/stompjs';
import { safeParseJSON } from '../shared/util';

export class StompManager {
  private client: Client;
  private subscriptions: Array<StompSubscription> = [];

  constructor(config: StompConfig) {
    this.client = new Client(config);
    this.client.activate();
  }

  disconnect() {
    console.debug("disconnectFromWebsocketWithStomp");

    if (this.client) {
      this.client.deactivate();
    }
  }

  unsubscribeAll() {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  subscribe(args: {
    destination: string,
    onMessage: (message: IMessage) => void,
  }) {
    const handle = this.client.subscribe(args.destination, (message) => {
      console.debug(`stream - incoming message on ${args.destination}`);
      args.onMessage(message)
    })
    this.subscriptions.push(handle);
  }

  subscribeWithValidation<SCHEMA extends ZodType, FinallyParams>(args: {
    destination: string,
    onMessage?: (body: z.infer<SCHEMA>, message: IMessage) => FinallyParams,
    onError?: (zodError: ZodError<unknown>, message: IMessage) => FinallyParams,
    finally?: (args?: FinallyParams) => void,
    messageSchema: SCHEMA,
  }) {
    const handle = this.client.subscribe(args.destination, (message) => {
      console.debug(`stream - incoming message on ${args.destination}`)
      const json = safeParseJSON(message.body)
      const parsed = args.messageSchema.safeParse(json)

      let finallyParams = undefined
      try {
        if (parsed.success) {
          finallyParams = args.onMessage?.(parsed.data, message)
        } else {
          console.error("parsing the message failed", parsed.error, message)
          finallyParams = args.onError?.(parsed.error, message)
        }
      } finally {
        args.finally?.(finallyParams)
      }
    })
    this.subscriptions.push(handle);
  }
}
