import { Injectable, Signal, WritableSignal, signal } from "@angular/core";
import { PlayerRef } from "../schemas/Player";
import { TemporaryPromotionPermission } from ".";
import { Room } from "../schemas/Room";
import { NavigationStart, Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  private currentRoom: WritableSignal<Room|undefined> = signal(undefined);
  private allowPromotionForPlayers: Array<TemporaryPromotionPermission> = []

  constructor(
    private router: Router,
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (event.url !== `/room/${this.currentRoom()?.id}`) {
          console.debug("Resetting Room");
          this.resetRoom();
        }
      }
    })
  }

  readonly room: Signal<Room|undefined> = this.currentRoom.asReadonly();

  setRoom(room?: Room) {
    this.currentRoom.set(room);
  }

  registerTemporaryPromotionPermission(player: PlayerRef) {
    this.removeExpired();
    const perm = new TemporaryPromotionPermission(player);
    this.allowPromotionForPlayers.push(perm);
  }

  isPromotionPermitted(player: PlayerRef): boolean {
    this.removeExpired();
    const perm = this.allowPromotionForPlayers.find((perm) => perm.getPlayer().id === player.id);
    return !!perm && perm.isValid();
  }

  private removeExpired(): void {
    this.allowPromotionForPlayers = this.allowPromotionForPlayers.filter((perm) => perm.isValid());
  }

  private resetRoom(): void {
    this.setRoom(undefined);
  }
}
