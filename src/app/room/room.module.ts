import { A11yModule } from "@angular/cdk/a11y";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FooterModule } from "../footer/footer.module";
import { KwmcModule } from "../kwmc/kwmc.module";
import { ModalModule } from "../modal/modal.module";
import { TranslateForRootModule } from "../translate-for-root.module";
import { AvgDisclaimerModalComponent } from "./avg-disclaimer-modal/avg-disclaimer-modal.component";
import { BigPokerTableComponent } from "./big-poker-table/big-poker-table.component";
import { CardHandComponent } from "./bottom-area/card-hand/card-hand.component";
import { CreateTaskModalComponent } from "./task-modal/create-task-modal/create-task-modal.component";
import { CustomCardsModalComponent } from "./custom-cards-modal/custom-cards-modal.component";
import { CustomizePlayerModalComponent } from "./customize-player-modal/customize-player-modal.component";
import { EditTaskModalComponent } from "./task-modal/edit-task-modal/edit-task-modal.component";
import { HeaderbarComponent } from "./headerbar/headerbar.component";
import { InteractionBoxesComponent } from "./interaction-boxes/interaction-boxes.component";
import { IssuePanelComponent } from "./issue-panel/issue-panel.component";
import { PlayerLabelComponent } from "./player-label/player-label.component";
import { PlayerListComponent } from "./player-list/player-list.component";
import { PokerTableComponent } from "./poker-table/poker-table.component";
import { RoomComponent } from "./room.component";
import { StatsComponent } from "./stats/stats.component";
import { StatsNewComponent} from "./bottom-area/stats-new/stats-new.component";
import { TaskItemComponent } from "./task-item/task-item.component";
import { TasksComponent } from "./tasks/tasks.component";
import { TasksSessionHeaderComponent } from "./tasks-session-header/tasks-session-header.component";
import { RawHtmlPipe } from "../raw-html.pipe";
import { TaskFormComponent } from "./task-modal/task-form/task-form.component";
import { AiSupportComponent } from "./ai-support/ai-support.component";
import { RessourceComponent } from "./ai-support/ressource-allocation/ressource/ressource.component";
import { CapacityComponent } from "./ai-support/capacity-forecast/capacity/capacity.component";
import { RessourceAllocationComponent } from "./ai-support/ressource-allocation/ressource-allocation.component";
import { CapacityForecastComponent } from "./ai-support/capacity-forecast/capacity-forecast.component";
import { AiEstimationsComponent } from "./ai-support/ai-estimations/ai-estimations.component";
import { AiSubtasksComponent } from "./ai-support/ai-subtasks/ai-subtasks.component";
import { AuthModule } from "../auth/auth.module";
import { SubtaskSketchComponent } from "./task-modal/subtask-sketch/subtask-sketch.component";
import { BottomAreaComponent } from "./bottom-area/bottom-area.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CircularProgressComponent } from "./bottom-area/stats-new/circular-progress/circular-progress.component";
import { NgxEditorModule } from "ngx-editor";
import { SubtaskItemComponent } from "./task-modal/subtask-item/subtask-item.component";
import { KehrwasserFeaturesModule } from "@kehrwasser-dev/features-angular";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { TabMenuComponent } from "./sidebar/tab-menu/tab-menu.component";
import { TabHeaderComponent } from "./sidebar/tab-header/tab-header.component";
import { GameTabComponent } from "./tabs/game-tab/game-tab.component";
import { ImportTasksModalComponent } from "./tasks/import-tasks-modal/import-tasks-modal.component";
import { GamesComponent } from "./tabs/games/games.component";
import { PlanBadgeComponent } from "./plan-badge/plan-badge.component";
import { GameComponent } from "./tabs/games/game/game.component";
import { ExportTasksModalComponent } from "./tasks/export-tasks-modal/export-tasks-modal.component";
import { StatisticsComponent } from "./bottom-area/statistics/statistics.component";
import { DistributionComponent } from "./bottom-area/statistics/distribution/distribution.component";
import { PlayerContextMenuComponent } from "./player-context-menu/player-context-menu.component";
import { DragDropModule } from "src/third-party/cdk/drag-drop";
import { StatsTabComponent } from "./tabs/stats-tab/stats-tab.component";
import { StatsRessourceAllocationComponent } from "./tabs/stats-tab/stats-ressource-allocation/stats-ressource-allocation.component";
import { StatsAutomaticForecastComponent } from "./tabs/stats-tab/stats-automatic-forecast/stats-automatic-forecast.component";
import { StatsForecastDetailsComponent } from "./tabs/stats-tab/stats-forecast-details/stats-forecast-details.component";
import { EnableWithProComponent } from "./tabs/stats-tab/enable-with-pro/enable-with-pro.component";
import { ProgressNavigationComponent } from "./tasks/progress-navigation/progress-navigation.component";
import { TimelineComponent } from "./tabs/stats-tab/timeline/timeline.component";
import { JiraInstanceSelectComponent } from "./tasks/jira-instance-select/jira-instance-select.component";
import { JiraErrorPageComponent } from "./tasks/jira-error-page/jira-error-page.component";
import { JiraBoardSelectComponent } from "./tasks/jira-board-select/jira-board-select.component";
import { JiraSprintSelectComponent } from "./tasks/jira-sprint-select/jira-sprint-select.component";
import { JiraIssueImportListComponent } from "./tasks/jira-issue-import-list/jira-issue-import-list.component";
import { ImportSourceSelectComponent } from "./tasks/import-source-select/import-source-select.component";
import { JiraIssueExportListComponent } from "./tasks/jira-issue-export-list/jira-issue-export-list.component";
import { GameProgressComponent } from "./tabs/game-progress/game-progress.component";
import { RoundedListComponent } from "./tasks/rounded-list.component";
import { StepModalDescriptionComponent } from "./tasks/step-modal-description.component";
import { ProjectChoiceButtonComponent } from "./tasks/project-choice-button.component";
import { ListEmptyStateComponent } from "./tasks/list-empty-state.component";
import { UnlockImportButtonComponent } from "./tasks/unlock-import-button.component";
import { IssueListItemComponent } from "./tasks/issue-list-item.component";

@NgModule({
  declarations: [
    AiSupportComponent,
    AvgDisclaimerModalComponent,
    PlanBadgeComponent,
    BigPokerTableComponent,
    BottomAreaComponent,
    CardHandComponent,
    CustomCardsModalComponent,
    CustomizePlayerModalComponent,
    CreateTaskModalComponent,
    EditTaskModalComponent,
    ExportTasksModalComponent,
    InteractionBoxesComponent,
    IssuePanelComponent,
    HeaderbarComponent,
    PlayerLabelComponent,
    PlayerListComponent,
    PokerTableComponent,
    RoomComponent,
    StatsComponent,
    StatsNewComponent,
    SubtaskItemComponent,
    TaskFormComponent,
    TaskItemComponent,
    TasksComponent,
    TasksSessionHeaderComponent,
    RawHtmlPipe,
    RessourceAllocationComponent,
    RessourceComponent,
    CapacityComponent,
    CapacityForecastComponent,
    AiEstimationsComponent,
    AiSubtasksComponent,
    SubtaskSketchComponent,
    CircularProgressComponent,
    SidebarComponent,
    TabMenuComponent,
    TabHeaderComponent,
    GameTabComponent,
    ImportTasksModalComponent,
    GamesComponent,
    GameComponent,
    GameProgressComponent,
    StatisticsComponent,
    DistributionComponent,
    PlayerContextMenuComponent,
    StatsTabComponent,
    StatsRessourceAllocationComponent,
    StatsAutomaticForecastComponent,
    StatsForecastDetailsComponent,
    EnableWithProComponent,
    ProgressNavigationComponent,
    TimelineComponent,
    ImportSourceSelectComponent,
    JiraInstanceSelectComponent,
    JiraBoardSelectComponent,
    JiraSprintSelectComponent,
    JiraIssueImportListComponent,
    JiraIssueExportListComponent,
    JiraErrorPageComponent,
    RoundedListComponent,
    StepModalDescriptionComponent,
    ProjectChoiceButtonComponent,
    ListEmptyStateComponent,
    UnlockImportButtonComponent,
    IssueListItemComponent,
  ],
  exports: [RoomComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    KwmcModule,
    FooterModule,
    TranslateForRootModule,
    DragDropModule,
    A11yModule,
    ReactiveFormsModule,
    AuthModule,
    BrowserAnimationsModule,
    NgxEditorModule,
    KehrwasserFeaturesModule,
  ],
})
export class RoomModule {}
