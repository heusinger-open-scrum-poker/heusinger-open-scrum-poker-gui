import { readSeries } from "./custom-cards-modal";
import { Player } from "src/app/schemas/Player";
import { Room } from "src/app/schemas/Room";
import { BREAK_CARD_VALUE, UNCLEAR_CARD_VALUE } from "./index";
import { Settings } from "../shared/Settings";
import { safeParseJSON } from "../shared/util";
import { RawCardSeries } from "../schemas/CardSeries";

export type Stats = {
  majorityCards: Array<string>;
  minOutlier?: string;
  maxOutlier?: string;
  minCard?: string;
  maxCard?: string;
  maxVotes: number;
  totalVotes: number;
  cardValues: Array<string>;
  comparator: (a: string, b: string) => number;
  distribution: {
    [key: string]: number,
  }
};

export function computeStats(players: Array<Player>, room: Room): Stats {
  let totalVotes = 0;
  const votes = new Map<string, number>();

  let cardValues: Array<string> = [];
  const rawSeries = RawCardSeries.safeParse(safeParseJSON(room.series));
  if (rawSeries.success) {
    cardValues = readSeries(rawSeries.data).values
  } else {
    console.error("invalid series: ", room.series);
  }
  const comparator = getComparator(cardValues);

  let minOutlier: string | undefined = undefined;
  let maxOutlier: string | undefined = undefined;
  const distribution: { [key: string]: number } = {};

  for (let i = 0; i < players.length; ++i) {
    const player = players[i];
    const cardValue = player.cardValue;
    if (
      player.type !== "estimator" ||
      player.cardValue === UNCLEAR_CARD_VALUE ||
      player.cardValue === BREAK_CARD_VALUE
    ) {
      continue;
    }

    if (cardValue) {
      if (minOutlier === undefined) {
        minOutlier = cardValue;
      } else if (comparator(minOutlier, cardValue) == 1) {
        minOutlier = cardValue;
      }

      if (maxOutlier === undefined) {
        maxOutlier = cardValue;
      } else if (comparator(cardValue, maxOutlier) == 1) {
        maxOutlier = cardValue;
      }

      if (distribution[cardValue]) {
        distribution[cardValue]++;
      } else {
        distribution[cardValue] = 1;
      }
    }

    totalVotes += 1;
    const numVotes = cardValue ? votes.get(cardValue) : undefined;

    if (cardValue && numVotes != undefined) {
      votes.set(cardValue, numVotes + 1);
    } else if (cardValue) {
      votes.set(cardValue, 1);
    }
  }

  let maxVotes = 0;
  let majorityCards: string[] = [];
  const outliers: string[] = [];

  votes.forEach((value, key) => {
    if (maxVotes < value) {
      maxVotes = value;
      for (let i = 0; i < majorityCards.length; ++i) {
        outliers.push(majorityCards[i]);
      }
      majorityCards = [key];
    } else if (maxVotes == value) {
      majorityCards.push(key);
    } else {
      outliers.push(value.toString());
    }
  });

  const minCard = minOutlier;
  if (minOutlier && majorityCards.includes(minOutlier)) {
    minOutlier = undefined;
  }

  const maxCard = maxOutlier;
  if (maxOutlier && majorityCards.includes(maxOutlier)) {
    maxOutlier = undefined;
  }

  return {
    majorityCards,
    maxVotes,
    totalVotes,
    minOutlier,
    maxOutlier,
    minCard,
    maxCard,
    cardValues,
    comparator,
    distribution
  };
}

export function getComparator(series: string[]): (a: string, b: string) => number {
  const allCardsNumeric = series.reduce((allNumeric, card) => {
    if (card && isNaN(parseFloat(card))) {
      return false;
    } else {
      return allNumeric;
    }
  }, true);

  if (allCardsNumeric) {
    return (a: string, b: string) => {
      const numA = parseFloat(a);
      if (isNaN(numA)) {
        return 0;
      }
      const numB = parseFloat(b);
      if (isNaN(numA)) {
        return 0;
      }
      return Math.sign(numA - numB);
    };
  } else {
    return (a: string, b: string) => {
      const indexA = series.indexOf(a);
      if (indexA == -1) {
        return 0;
      }
      const indexB = series.indexOf(b);
      if (indexB == -1) {
        return 0;
      }
      return Math.sign(indexA - indexB);
    };
  }
}

export function getConsensus(stats?: Stats): string | undefined {
    if (!stats || stats?.majorityCards.length == 0) {
      return;
    }

    const consensusReached = Settings.MINIMUM_CONSENSUS_PERCENT * stats.totalVotes <= 100 * stats.maxVotes;

    if (!consensusReached) {
      return;
    }

    const mCards = [...stats.majorityCards];
    mCards.sort(stats.comparator);
    return mCards[mCards.length - 1];
}
