import { PlayerRef } from "../schemas/Player";
import { Settings } from "../shared/Settings";

export const BREAK_CARD_VALUE = "@break";
export const UNCLEAR_CARD_VALUE = "?";

export interface ActionButton {
  label: string,
  icon: string,
  tooltip?: string,
  class?: string,
  click: () => void,
  busy?: () => boolean,
}

export class TemporaryPromotionPermission {
  private timeStamp: number;

  constructor(
    private player: PlayerRef,
  ) {
    this.timeStamp = Date.now();
  }

  isValid(): boolean {
    const now = Date.now();
    return now < this.timeStamp + Settings.TEMPORARY_PROMOTION_PERMISSION_TIMEOUT_MS;
  }

  getPlayer(): PlayerRef {
    return this.player;
  }
}
