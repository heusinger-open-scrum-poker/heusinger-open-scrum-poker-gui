import { FormsModule } from "@angular/forms";
import en_translation from "../../../assets/i18n/en.json";
import { MountConfig } from "cypress/angular";
import { Observable, of } from "rxjs";
import { CustomizePlayerModalComponent } from "./customize-player-modal.component";
import { User } from "src/app/schemas/User";
import { generateMock } from "@anatine/zod-mock"
import { Room } from "src/app/schemas/Room";
import { Player } from "src/app/schemas/Player";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { TranslateTestingModule } from "ngx-translate-testing";
import { ModalModule } from "src/app/modal/modal.module";
import { KwmcModule } from "src/app/kwmc/kwmc.module";
import { ignoreResizeObserverErrors } from "cypress/e2e/utilities";

describe("CustomizePlayerModalComponent", () => {
  const user: User = generateMock(User)
  const player: Player = generateMock(Player)
  player.avatar = "pink"
  const secondPlayer = generateMock(Player)
  secondPlayer.avatar = "blue"


  const room: Room = generateMock(Room)
  const players: Array<Player> = [player, secondPlayer]


  const config: MountConfig<CustomizePlayerModalComponent> = {
    imports: [
      FormsModule,
      TranslateTestingModule.withTranslations("en", en_translation),
      ModalModule,
      KwmcModule,
    ],
    declarations: [
      CustomizePlayerModalComponent,
    ],
    providers: [
      {
        provide: AgileCasinoService,
        useValue: {
          getRoomAndPlayers(roomId: number) {
            expect(roomId).to.be.equal(room.id);
            return of([room as Room, players]);
          },
          getSeries(): Observable<string[]> {
            return of(null);
          },
          getAvatars(): Observable<string[]> {
            return of([
              "pink",
              "blue",
              "orange",
            ]);
          },
          getFeatures(): Observable<Array<string>> {
            return of([]);
          },
          getJoinedRooms(): Observable<unknown> {
            return of([
              { room: room, player: player, numPlayers: players.length },
            ]);
          },
          getPlayersOfRoom(): Observable<unknown> {
            return of(players);
          },
          getRoom(): Observable<unknown> {
            return of(room);
          },
          getTasks(): Observable<unknown> {
            return of([]);
          },
          getUser(): Observable<unknown> {
            return of({ uuid: "mock-uuid" });
          },
        },
      },
    ],
    componentProperties: {
      roomId: room.id,
      player,
      players,
    },
  };


  beforeEach(() => {
    window.localStorage.setItem("uuid", "mock-uuid");
    cy.viewport(1280, 1024);
    ignoreResizeObserverErrors(cy);
  });

  it("detects change of avatars", () => {
    console.error(players)
    cy.mount(CustomizePlayerModalComponent, config).then((wrapper) => {
      wrapper.component.showModal()
      expect(wrapper.component.usedAvatars.includes("orange")).to.be.false;
      secondPlayer.avatar = "orange"
      wrapper.fixture.detectChanges()
      expect(wrapper.component.usedAvatars.includes("orange")).to.be.true;
    })
  });
});
