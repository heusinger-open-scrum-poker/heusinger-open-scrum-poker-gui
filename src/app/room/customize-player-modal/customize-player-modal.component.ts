import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { Observable, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
import { ModalActions, ModalButtonProps } from "../../modal/types";
import { AgileCasinoService } from "../../agile-casino.service";
import { ConfirmationBoxComponent } from "../../modal/confirmation-box/confirmation-box.component";
import { ConfirmationResult, ConfirmationResults } from "../../modal/confirmation-box";
import { AvatarItem, AvatarList } from ".";
import { NotificationsService } from "../../notifications.service";
import { Settings } from "../../shared/Settings";
import { TranslateService } from "@ngx-translate/core";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";

@Component({
  selector: "app-customize-player-modal",
  templateUrl: "./customize-player-modal.component.html",
  styleUrls: ["./customize-player-modal.component.scss"],
})
export class CustomizePlayerModalComponent implements OnInit, OnChanges {
  @ViewChild(ConfirmationBoxComponent) confirmationBox?: ConfirmationBoxComponent;
  @ViewChild(ModalDialogComponent) modal?: ModalDialogComponent;
  @Input() roomId!: Room["id"];
  @Input() player!: Player;
  @Input() players!: Array<Player>;

  avatar?: string;
  name: string = "";
  avatarsFromServer: Array<{ avatar: string, available: boolean }> = [];
  avatars: AvatarList = [];

  get maxNameLength() {
    return Settings.MAX_NAME_LENGTH;
  }

  primaryButtonProps: ModalButtonProps | undefined = {
    label: this.translate.instant("CustomizePlayerModal.Apply"),
    buttonFlavor: "high-impact",
    onClick: () => {
      return this.getConfirmation().pipe(
        mergeMap((confirmation) => {
          if (confirmation === "confirm") {
            return this.agileCasinoService
              .patchPlayer({
                id: this.player.id,
                avatar: this.avatar,
                name: this.name,
              })
              .pipe(
                map((player) => {
                  if (player.avatar !== this.avatar) {
                    this.notificationsService.notify({
                      message: this.translate.instant(
                        "CustomizePlayerModal.AvatarFail",
                      ),
                      title: this.translate.instant("Notification.Error"),
                    });
                  }
                  if (player.name !== this.name) {
                    this.notificationsService.notify({
                      message: this.translate.instant(
                        "CustomizePlayerModal.NameFail",
                      ),
                      title: this.translate.instant("Notification.Error"),
                    });
                  }
                  return ModalActions.close;
                }),
              );
          } else {
            return of(ModalActions.ignore);
          }
        }),
      );
    },
  };

  secondaryButtonProps: ModalButtonProps | undefined = {
    label: this.translate.instant("Cancel"),
    buttonFlavor: "secondary",
    onClick: () => {
      return of("close");
    },
  };

  get usedAvatars() {
    return this.players
      .filter(p => !!p.avatar)
      .map(p => p.avatar);
  }

  get selectedAvatar() {
    const isUnavailable = this.avatar !== this.currentAvatar && this.usedAvatars.includes(this.avatar);

    if (isUnavailable) {
      this.avatar = this.currentAvatar
    }

    return this.avatar;
  }

  itemOf(avatar: string) {
    const available = !this.usedAvatars.includes(avatar);
    const isCurrent = avatar === this.currentAvatar;
    const isSelected = avatar === this.selectedAvatar;
    const isAvailable = !isCurrent && available;
    const isUnavailable = !isCurrent && !available;

    return {
      avatar,
      isSelected,
      isCurrent,
      isAvailable,
      isUnavailable,
    };
  }

  recomputeAvatars() {
    const selectedAvatarIsUnavailable = (this.avatarsFromServer ?? [])
    .map(({avatar}) => {
      const { isSelected, isUnavailable } = this.itemOf(avatar);
      return isSelected && isUnavailable
    })
    .reduce((a,b) => a || b, false);

    if (selectedAvatarIsUnavailable) {
      this.avatar = this.player.avatar ?? undefined;
    }
  }

  get currentAvatar(): string | undefined {
    return this.player.avatar ?? undefined;
  }

  constructor(
    private agileCasinoService: AgileCasinoService,
    private notificationsService: NotificationsService,
    private translate: TranslateService,
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["players"]) {
      this.recomputeAvatars();
    }
  }

  ngOnInit(): void {
    this.resetFormValues();
    this.loadAvatars();
  }

  onChangeName(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.name = inputElement.value ?? "";
  }

  loadAvatars(): void {
    this.agileCasinoService.getAvatars(this.roomId).subscribe({
      next: (avatars) => {
        this.avatarsFromServer = avatars;
        this.recomputeAvatars();
      },
      error: (e) => {
        console.error("unable to load avatars", e);
        this.notificationsService.notify({
          message: this.translate.instant(
            "CustomizePlayerModal.AvatarLoadFail",
          ),
          title: this.translate.instant("Notification.Error"),
        });
      },
    });
  }

  setSelectedAvatar(item: AvatarItem): void {
    if (item.isUnavailable) {
      return;
    }

    this.avatar = item.avatar;
    this.recomputeAvatars();
  }

  onAvatarChanged(avatar: string): void {
    this.avatar = avatar;
  }

  getConfirmation(): Observable<ConfirmationResult> {
    if (!this.confirmationBox) {
      console.error("confirmationBox not initialized");
      return of(ConfirmationResults.cancel);
    }

    this.confirmationBox?.showModal();
    return this.confirmationBox.result.pipe(
      map((result: ConfirmationResult) => {
        this.confirmationBox?.close();
        return result;
      }),
    );
  }

  close(): void {
    this.modal?.close();
  }

  showModal(): void {
    this.resetFormValues();
    this.modal?.showModal();
  }

  resetFormValues(): void {
    this.avatar = this.player.avatar ?? undefined;
    this.name = this.player.name;
  }
}
