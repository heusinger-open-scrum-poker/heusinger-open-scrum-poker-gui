export type AvatarList = Array<AvatarItem>;

export type AvatarItem = {
  avatar: string;
  isSelected: boolean;
  isCurrent: boolean;
  isAvailable: boolean;
  isUnavailable: boolean;
}
