import {Component, Input} from '@angular/core';
import {ActionButton} from "../../index";

@Component({
  selector: 'app-tab-header',
  templateUrl: './tab-header.component.html',
  styleUrl: './tab-header.component.scss'
})
export class TabHeaderComponent {
  @Input({ required: true }) back?: () => void;
  @Input() actions: Array<ActionButton> = [];
}
