import { describe, it, expect } from 'vitest';
import { TabHandler } from "./tab.handler";

describe("TabHandler", () => {
  it("should have exactly the tabs without parent in the menu", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "players",
          parent: "ai",
        },
        {
          key: "ai",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'game'
    );

    expect(tabHandler.getMenuTabs().map(tab => tab.key)).to.contain('game');
    expect(tabHandler.getMenuTabs().map(tab => tab.key)).to.contain('ai');
    expect(tabHandler.getMenuTabs().map(tab => tab.key)).not.to.contain('players');
    expect(tabHandler.getMenuTabs().map(tab => tab.key)).not.to.contain('tasks');
  });

  it("should have the correct active tab initially", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
      ],
      'ai'
    );

    expect(tabHandler.getActiveTab()).to.equal('ai');
  });

  it("should have the correct selected tab initially", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
      ],
      'ai'
    );

    expect(tabHandler.getSelectedTab()).to.equal('ai');
  });

  it("should have the correct selected tab in depth", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'tasks'
    );

    expect(tabHandler.getSelectedTab()).to.equal('game');
    expect(tabHandler.getActiveTab()).to.equal('tasks');
  });

  it("can navigate between menu tabs", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
      ],
      'game'
    );

    tabHandler.navigate('ai');

    expect(tabHandler.getSelectedTab()).to.equal('ai');
    expect(tabHandler.getActiveTab()).to.equal('ai');
  });

  it("can navigate down", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'game'
    );

    tabHandler.navigate('tasks');

    expect(tabHandler.getSelectedTab()).to.equal('game');
    expect(tabHandler.getActiveTab()).to.equal('tasks');
  });

  it("can navigate down and up", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'game'
    );

    tabHandler.navigate('tasks');
    tabHandler.back();

    expect(tabHandler.getSelectedTab()).to.equal('game');
    expect(tabHandler.getActiveTab()).to.equal('game');
  });

  it("can not navigate up from menu tab", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "ai",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'game'
    );

    tabHandler.back();

    expect(tabHandler.getSelectedTab()).to.equal('game');
    expect(tabHandler.getActiveTab()).to.equal('game');
  });

  it("knows if menu tab is active", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'game'
    );

    tabHandler.navigate('tasks');

    expect(tabHandler.isMenuTabActive()).to.be.false;

    tabHandler.back();

    expect(tabHandler.isMenuTabActive()).to.be.true;
  });

  it("can handle greater depth", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "tasks",
          parent: "game",
        },
        {
          key: "ai",
          parent: "tasks",
        },
        {
          key: "players",
          parent: "ai",
        },
      ],
      'game'
    );

    tabHandler.navigate('tasks');
    tabHandler.navigate('ai');
    tabHandler.navigate('players');

    expect(tabHandler.getActiveTab()).to.equal('players');
    expect(tabHandler.getSelectedTab()).to.equal('game');
  });


  it("can handle circular structures", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
        },
        {
          key: "tasks",
          parent: "ai",
        },
        {
          key: "ai",
          parent: "tasks",
        },
      ],
      'game'
    );

    tabHandler.navigate('tasks');

    expect(tabHandler.getActiveTab()).to.equal('tasks');

    tabHandler.back();

    expect(tabHandler.getActiveTab()).to.equal('ai');

    tabHandler.back();

    expect(tabHandler.getActiveTab()).to.equal('tasks');

    tabHandler.navigate('game');

    expect(tabHandler.getActiveTab()).to.equal('game');
    expect(tabHandler.getSelectedTab()).to.equal('game');
  });

  it("won't fail in infinite loop", () => {
    const tabHandler = new TabHandler(
      [
        {
          key: "game",
          parent: "tasks",
        },
        {
          key: "tasks",
          parent: "game",
        },
      ],
      'game'
    );

    expect(tabHandler.getSelectedTab()).to.equal(undefined);
  });

  it("can handle empty tab list", () => {
    const tabHandler = new TabHandler(
      [],
      'game'
    );

    expect(tabHandler.getSelectedTab()).to.equal(undefined);
  });
});
