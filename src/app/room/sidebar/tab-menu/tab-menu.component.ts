import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Tab } from "../sidebar.component";

@Component({
  selector: 'app-tab-menu',
  templateUrl: './tab-menu.component.html',
  styleUrl: './tab-menu.component.scss'
})
export class TabMenuComponent {
  @Input({ required: true }) tabs!: Array<Tab>;
  @Input({ required: true }) selectedTab!: string;

  @Output() selectEmitter: EventEmitter<string> = new EventEmitter<string>();

  select(name: string) {
    this.selectEmitter.emit(name);
  }
}
