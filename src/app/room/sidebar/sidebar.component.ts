import { Component, Input, TemplateRef } from '@angular/core';
import { TabHandler } from "./tab.handler";
import { animate, style, transition, trigger } from "@angular/animations";
import {ActionButton} from "../index";

export interface Tab {
  key: string;
  label?: string;
  icon?: string;
  parent?: string;
  headerActions?: Array<ActionButton>;
}

export interface ContentTemplates {
  [key: string]: TemplateRef<unknown>;
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss',
  animations: [
    trigger(
      'slideInOut',
      [
        transition(
          ':enter',
          [
            style({ left: '100%' }),
            animate('0.4s ease-out',
              style({ left: 0 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ left: 0 }),
            animate('0.4s ease-in',
              style({ left: '100%' }))
          ]
        )
      ]
    ),
  ]
})
export class SidebarComponent {
  @Input({ required: true }) tabHandler!: TabHandler;
  @Input({ required: true }) controlAreaTemplate!: TemplateRef<unknown>;
  @Input({ required: true }) contentTemplates!: ContentTemplates;

  getContent(key: string): TemplateRef<unknown> | null {
    return this.contentTemplates[key] ?? null;
  }

  get selectedTab() {
    return this.tabHandler.getSelectedTab()
  }

  identifyTab(_index: number, tab: Tab) {
    return tab.key;
  }
}
