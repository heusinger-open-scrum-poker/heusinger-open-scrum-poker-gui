import { Tab } from "./sidebar.component";

export class TabHandler {
  private readonly maxDepth = 100;
  private readonly tabs: Array<Tab>;
  private activeTab: string;

  constructor(tabs: Array<Tab>, activeTab: string) {
    this.tabs = tabs;
    this.activeTab = activeTab;
  }

  public back: () => void = () => {
    const tab = this.getTab(this.activeTab);

    if (!tab) {
      return;
    }

    if (tab.parent) {
      this.navigate(tab.parent);
    }
  }

  public getActiveTab(): string {
    return this.activeTab;
  }

  public getMenuTabs(): Array<Tab> {
    return this.tabs.filter(tab => tab.parent === undefined);
  }

  public getChildTabs(): Array<Tab> {
    return this.tabs.filter(tab => tab.parent !== undefined);
  }

  public getSelectedTab(): string | undefined {
    let tab = this.getTab(this.activeTab);

    if (!tab) {
      return;
    }

    let selectedTab = this.activeTab;

    let i = 1;
    while (i <= this.maxDepth) {
      if (tab.parent === undefined) {
        return selectedTab;
      }

      tab = this.getTab(tab.parent)!;
      console.assert(tab !== undefined, "parent doesn't exist");
      selectedTab = tab.key;
      i++;
    }
  }

  public isMenuTabActive(): boolean {
    return this.getSelectedTab() === this.activeTab;
  }

  public navigate(key: string): void {
    const tab = this.getTab(key);

    if (tab !== undefined) {
      const parent = tab.parent;

      this.activeTab = key;
      this.back = parent ? () => this.navigate(parent) : () => {};
    }
  }

  private getTab(key: string): Tab | undefined {
    return this.tabs.find(tab => tab.key === key);
  }
}
