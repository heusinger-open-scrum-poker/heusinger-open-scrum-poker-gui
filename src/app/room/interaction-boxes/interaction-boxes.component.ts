import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { TranslateService } from "@ngx-translate/core";
import { NotificationsService } from "../../notifications.service";

@Component({
  selector: "app-interaction-boxes",
  templateUrl: "./interaction-boxes.component.html",
  styleUrls: ["./interaction-boxes.component.scss"],
})
export class InteractionBoxesComponent {
  @Input() inviteLink: string = "";
  inviteLinkCopied: boolean = false;
  @ViewChild("inviteLinkInput") inviteLinkInput!: ElementRef;
  @ViewChild("feedbackInput") feedbackInput!: ElementRef;

  feedbackControl: FormControl<string> = new FormControl("", {
    nonNullable: true,
    validators: [
      Validators.required,
    ] });

  emailControl: FormControl<string> = new FormControl("", {
    nonNullable: true,
    validators: [
      Validators.required,
      Validators.email,
    ] });

  busy: boolean = false;
  output: string = "";
  private feedbackId?: number;

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
    private notificationsService: NotificationsService,
  ) {}

  get showFeedbackForm(): boolean {
    return this.feedbackId == null;
  }

  get showEmailForm(): boolean {
    return !this.showFeedbackForm;
  }

  getInviteLinkInputElement(): HTMLInputElement {
    return this.inviteLinkInput.nativeElement;
  }

  selectAll(): void {
    const element = this.getInviteLinkInputElement();

    element.select();
    element.setSelectionRange(0, 99999);
  }

  showSuccessAndReset(): void {
    this.inviteLinkCopied = true;

    setTimeout(() => {
      this.inviteLinkCopied = false;
      this.getInviteLinkInputElement().setSelectionRange(0, 0);
    }, 800);
  }

  copyToClipboard(): void {
    if (this.inviteLinkCopied) {
      return;
    }

    this.selectAll();

    if (navigator?.clipboard?.writeText) {
      navigator.clipboard.writeText(this.inviteLink);
    } else {
      document.execCommand("copy");
    }

    this.showSuccessAndReset();
  }

  submitFeedback() {
    if (this.busy) {
      return;
    }

    this.busy = true;
    this.output = this.translate.instant("FeedbackForm.Wait");

    if (this.feedbackControl.invalid) {
      this.output = this.translate.instant("FeedbackForm.Comment");

      this.feedbackInput.nativeElement.focus();

      setTimeout(() => {
        this.busy = false;
        this.output = "";
      }, 800);

      return false;
    }

    this.agileCasinoService.sendFeedback(this.feedbackControl.value).subscribe({
      next: (res) => {
        console.debug("returned feedback object", res);

        this.output = this.translate.instant("FeedbackForm.Sent");

        setTimeout(() => {
          this.feedbackId = res.id;
          this.feedbackControl.setValue("");
          this.busy = false;
          this.output = "";
        }, 800);
      },
      error: (err) => {
        console.debug("returned error object", err);

        this.output = "error";

        setTimeout(() => {
          this.busy = false;
          this.output = "";
        }, 800);
      },
      complete: () => console.debug("HTTP request completed."),
    });
  }

  submitEmail() {
    if (this.busy) {
      return;
    }

    if (this.emailControl.invalid) {
      this.notificationsService.notify({
        message: this.translate.instant("Email.Invalid"),
        type: "warning",
      });

      return;
    }

    this.busy = true;
    this.output = this.translate.instant("FeedbackForm.Wait");

    const id = this.feedbackId;

    if (!id) {
      this.feedbackControl.setValue("");
      this.busy = false;
      this.output = "";
      this.feedbackId = undefined;
      return;
    }

    this.agileCasinoService.patchFeedback(id, undefined, this.emailControl.value).subscribe({
      next: () => {
        console.debug("updated feedback object");

        this.output = this.translate.instant(
          "FeedbackForm.Thanks",
        );

        setTimeout(() => {
          this.feedbackControl.setValue("");
          this.busy = false;
          this.output = "";
          this.feedbackId = undefined;
        }, 800);
      },
      error: (err) => {
        console.debug("returned error object", err);

        this.output = "error";

        setTimeout(() => {
          this.busy = false;
          this.output = "";
          this.feedbackId = undefined;
        }, 800);
      },
      complete: () => console.debug("HTTP request completed."),
    });
  }

  cancelEmailForm() {
    this.feedbackId = undefined;
  }
}
