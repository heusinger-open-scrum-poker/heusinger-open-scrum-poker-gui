import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { MountConfig } from "cypress/angular";
import { TranslateTestingModule } from "ngx-translate-testing";
import { Observable, of } from "rxjs";
import { AgileCasinoService } from "../../../agile-casino.service";
import { CardComponent } from "../../../kwmc/card/card.component";
import { ConnectionStatusComponent } from "../../../kwmc/connection-status/connection-status.component";
import { DEFAULT_OPTIONS } from "../../../room/custom-cards-modal";
import { FooterComponent } from "../../../footer/footer.component";
import { HeaderbarComponent } from "../../headerbar/headerbar.component";
import { SettingsMenuComponent } from "../../../kwmc/settings-menu/settings-menu.component";
import { TabBarComponent } from "../../../kwmc/tab-bar/tab-bar.component";
import LocalStorage from "../../../shared/LocalStorage";
import { BigPokerTableComponent } from "../../big-poker-table/big-poker-table.component";
import { CardHandComponent } from "../../bottom-area/card-hand/card-hand.component";
import { PlayerListComponent } from "../../player-list/player-list.component";
import { PokerTableComponent } from "../../poker-table/poker-table.component";
import { TasksComponent } from "../../tasks/tasks.component";
import en_translation from "../../../../assets/i18n/en.json";
import { User } from "../../../schemas/User";
import { Player } from "../../../schemas/Player";
import { Room } from "../../../schemas/Room";
import { IconComponent } from "../../../kwmc/icon/icon.component";
import { InteractionBoxesComponent } from "../../interaction-boxes/interaction-boxes.component";
import { PlayerLabelComponent } from "../../player-label/player-label.component";
import { IssuePanelComponent } from "../../issue-panel/issue-panel.component";
import { SnackbarComponent } from "../../../kwmc/snackbar/snackbar.component";
import { SmallDeviceDisclaimerComponent } from "../../../kwmc/small-device-disclaimer/small-device-disclaimer.component";
import { ModalModule } from "../../../modal/modal.module";
import { AuthModule } from "../../../auth/auth.module";
import { RoomModule } from "../../room.module";
import { KwmcModule } from "../../../kwmc/kwmc.module";
import { UserService } from "../../../user.service";
import { FooterModule } from "../../../footer/footer.module";
import { AiSubtaskEstimateQuota } from "../../../schemas/AiSubtaskEstimate";
import { QueryClient, provideAngularQuery } from "@tanstack/angular-query-experimental";
import { Game } from "../../../schemas/Game";
import {Stage, initialJiraStage} from "../import-tasks-modal";
import { ExportTasksModalComponent } from "./export-tasks-modal.component";
import { ignoreResizeObserverErrors } from "cypress/e2e/utilities";

describe("ImportTasksModalComponent", () => {
  const user: User = {
      uuid: "", hash: "",
      role: "anonymous"
  };
  const player: Player = {
    id: 1,
    avatar: "green",
    name: "Me",
    cardValue: "13",
    type: "estimator",
  };
  const room: Room = {
    id: 1,
    status: "estimating",
    roundsPlayed: 10,
    maxPlayers: 2,
    created_at: new Date().getTime(),
    series: JSON.stringify(DEFAULT_OPTIONS[0].value),
    activeGameId: 1,
  };
  const game: Game = {
    id: 1,
    roomId: 1,
    name: "Game #1",
    created_at: new Date().getTime(),
  };
  const players: Player[] = [
    player,
    {
      id: 2,
      name: "A",
      cardValue: "@break",
      type: "estimator",
      avatar: "orange",
    },
    { id: 3, name: "B", cardValue: "1", type: "estimator", avatar: "darkred" },
  ];

  const config: MountConfig<ExportTasksModalComponent> = {
    imports: [
      FormsModule,
      TranslateTestingModule.withTranslations("en", en_translation),
      AuthModule,
      ModalModule,
      RoomModule,
      ReactiveFormsModule,
      KwmcModule,
      FooterModule,
    ],
    declarations: [
      ExportTasksModalComponent,
      ConnectionStatusComponent,
      FooterComponent,
      CardComponent,
      HeaderbarComponent,
      SettingsMenuComponent,
      PokerTableComponent,
      BigPokerTableComponent,
      TabBarComponent,
      PlayerListComponent,
      TasksComponent,
      CardHandComponent,
      IconComponent,
      InteractionBoxesComponent,
      PlayerLabelComponent,
      IssuePanelComponent,
      SnackbarComponent,
      SmallDeviceDisclaimerComponent,
    ],
    providers: [
      provideAngularQuery(new QueryClient()),
      {
        provide: ActivatedRoute,
        useValue: {
          snapshot: {
            routeConfig: {
              path: `/room/${room.id}`,
            },
            paramMap: {
              get() {
                return room.id.toString();
              },
            },
          },
          queryParams: of({}),
        },
      },
      {
        provide: AgileCasinoService,
        useValue: {
          getRoomAndPlayers(roomId: number) {
            expect(roomId).to.be.equal(room.id);
            return of([room as Room, players]);
          },
          getSeries(): Observable<string[]> {
            return of(DEFAULT_OPTIONS[0].value);
          },
          getAvatars(): Observable<null> {
            return of(null);
          },
          getFeatures(): Observable<Array<string>> {
            return of([]);
          },
          getJoinedRooms(): Observable<unknown> {
            return of([
              { room: room, player: player, numPlayers: players.length },
            ]);
          },
          getPlayersOfRoom(): Observable<unknown> {
            return of(players);
          },
          getRoom(): Observable<unknown> {
            return of(room);
          },
          getTasks(): Observable<unknown> {
            return of([]);
          },
          getUser(): Observable<unknown> {
            return of({ uuid: "mock-uuid" });
          },
          getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
            return of({
              freeAiEstimations: 5,
            });
          },
          getGames(): Observable<Game[]> {
            return of([game]);
          },
          getImportModalState(): Observable<Stage> {
            return of(initialJiraStage());
          }
        },
      },
      {
        provide: LocalStorage,
        useValue: {
          getUuid() {
            return "mock-uuid";
          },
        },
      },
      {
        provide: UserService,
        useValue: {
          isLoggedIn: () => false,
          userFeatureEnabled: () => true,
          getUserProfile: () => undefined,
          getUser: () => ({ uuid: "mock-uuid" }),
          isPro: () => false,
        }
      },
    ],
    componentProperties: {
      roomId: room.id,
    },
  };

  beforeEach(() => {
    window.localStorage.setItem("uuid", "mock-uuid");
    cy.viewport(1280, 1024);

    cy.intercept("GET", "/room/1/players", {
      body: players,
    });

    // HACK: avoid click inconsistencies
    cy.get("body").click()

    ignoreResizeObserverErrors(cy);
  });

  it("mounts", () => {
    cy.mount(ExportTasksModalComponent, config);
  });

  it("shows title", () => {
    cy.mount(ExportTasksModalComponent, config);

    cy.get('app-modal').contains('Export Tasks');
  });

  it.skip("can select Jira", () => {
    cy.mount(ExportTasksModalComponent, config);

    cy.get('app-modal').contains('Available issue sources');
    cy.get('app-modal').contains('Jira').click();
    cy.get('app-modal').contains('Next').click();
    cy.get('app-modal').contains('Your Jira resources');
  });

  it.skip("shows progress bar after selecting Jira", () => {
    cy.mount(ExportTasksModalComponent, config);

    cy.get('app-modal').contains('Jira').click();
    cy.get('app-modal').contains('Next').click();

    cy.get('app-modal').contains('Connect');
    cy.get('app-modal').contains('Connect').parent().contains('Jira');

    cy.get('app-modal').contains('Select Project');
    cy.get('app-modal').contains('Select Project').parent().contains('Not selected');

    cy.get('app-modal').contains('Select Resource');
    cy.get('app-modal').contains('Select Resource').parent().contains('Not selected');

    cy.get('app-modal').contains('Import Sprint');
    cy.get('app-modal').contains('Import Sprint').parent().contains('To Game #1');
  });
});
