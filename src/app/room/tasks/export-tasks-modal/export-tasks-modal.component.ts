import { firstValueFrom } from "rxjs";
import { Component, Input, ViewChild, inject } from "@angular/core";
import { ISSUE_SOURCES, IssueSourceOption } from "../import-tasks-modal";
import { JiraBoard, JiraResource, JiraSprint } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ModalAction, ModalActions, ModalButtonProps } from "src/app/modal/types";
import { TranslateService } from "@ngx-translate/core";
import { Observable, from, map, of } from "rxjs";
import { CreateQueryResult, QueryClient, injectMutation, injectQuery } from "@tanstack/angular-query-experimental";
import { RoomState } from "../../state";
import { NotificationsService } from "src/app/notifications.service";
import { ImportParameters } from "src/app/schemas/Game";
import { PendingNotification, SimpleNotification } from "src/app/shared/Notification.class";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";
import { Step } from "../progress-navigation";
import { navCanContinue, connectJira, verbOfStep, IssueNav } from "../import-export";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Nav, NavData, pathKeyToPath } from "../nav-data";
import { JIRA_BOARD, JIRA_INSTANCE, JIRA_SPRINT, JiraBoardInfo, JiraInstanceInfo, JiraSprintInfo } from "../nav-data/jira";
import { SOURCE_SELECTION, SourceSelectionInfo } from "../nav-data/source-selection";


@Component({
  selector: "app-export-tasks-modal",
  templateUrl: "./export-tasks-modal.component.html",
  styleUrls: ["./export-tasks-modal.component.scss", "../import-export.scss"],
})
export class ExportTasksModalComponent {
  @ViewChild(ModalDialogComponent) modal?: ModalDialogComponent;
  @ViewChild("authModal") authModal?: AuthModalComponent;

  @Input({required: true}) roomId!: number;
  @Input({required: true}) roomState!: RoomState;

  isModalInitializing: boolean = false;
  localNav: IssueNav = new IssueNav(SOURCE_SELECTION);
  smokeTestSource: string = "";
  exportSuccess: boolean = false;

  queryClient = inject(QueryClient);

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
    private notificationsService: NotificationsService,
    private featuresService: KwFeatureService,
  ) {}

  get nav(): IssueNav {
    return this.localNav
  }

  set nav(nav: IssueNav) {
    this.localNav = nav;
  }

  get source(): SourceSelectionInfo {
    return this.nav.getTypedMetadata(SourceSelectionInfo, SOURCE_SELECTION);
  }

  get instance(): JiraInstanceInfo {
    return this.nav.getTypedMetadata(JiraInstanceInfo, JIRA_INSTANCE);
  }

  get board(): JiraBoardInfo {
    return this.nav.getTypedMetadata(JiraBoardInfo, JIRA_BOARD);
  }

  get sprint(): JiraSprintInfo {
    return this.nav.getTypedMetadata(JiraSprintInfo, JIRA_SPRINT);
  }

  modalStateQuery: CreateQueryResult<Nav> = injectQuery(() => ({
    queryKey: ['import', 'modal-state'],
    queryFn: () => new Promise((next, error) => {
      this.agileCasinoService.getImportModalState()
      .subscribe({ next: (navdata) => {
        const nav = IssueNav.fromData(navdata);
        if (nav) {
          this.localNav = nav;
        }
        next(this.localNav)
      }, error });
    }),
  }))

  jiraConnectedQuery: CreateQueryResult<boolean> = injectQuery(() => ({
    queryKey: ['import', 'jira','connection'],
    queryFn: () => new Promise((next, error) => { this.agileCasinoService.getIsJiraConnected().subscribe({
        next: ({isConnected}) => next(isConnected ?? false),
        error,
      });
    })
  }))

  disconnectJira(): void {
    this.jiraDisconnectMutation.mutate();
  }

  jiraDisconnectMutation = injectMutation(() => ({
    mutationFn: () => new Promise((next, error) => {
      this.agileCasinoService.disconnectJira().subscribe({
        next: () => {
          this.nav.setCurrentPath(pathKeyToPath(SOURCE_SELECTION));
          this.nav.evictCurrentChildren();
          next(null)
        },
        error: (e) => {
          this.nav.setCurrentPath(pathKeyToPath(SOURCE_SELECTION));
          error(e);
        },
      });
    }),
    onSuccess: () => {
      this.queryClient.invalidateQueries({queryKey: ["import", "jira", "connection"]});
    },
  }))

  get isJiraConnected(): boolean {
    return !!this.jiraConnectedQuery.data();
  }

  get issueSources() {
    return ISSUE_SOURCES;
  }

  get isInstanceSelection() {
    return this.nav.getCurrentPathKey() === JIRA_INSTANCE
  }

  get isBoardSelection() {
    return this.nav.getCurrentPathKey() === JIRA_BOARD
  }

  get isSprintSelection() {
    return this.nav.getCurrentPathKey() === JIRA_SPRINT
  }

  get isServiceSelection() {
    return this.nav.getCurrentPathKey() === SOURCE_SELECTION
  }

  get isJira() {
    return this.source.id === "jira"
  }

  get isJiraInitial() {
    return this.nav.getCurrentPathKey() === JIRA_INSTANCE
  }

  get isJiraInstanceSelected() {
    return this.nav.getCurrentPathKey().startsWith(JIRA_INSTANCE)
    && this.instance.id
  }

  get isJiraBoardSelected() {
    return this.nav.getCurrentPathKey().startsWith(JIRA_BOARD)
    && this.board.id
  }

  get isJiraSprintSelected() {
    return this.nav.getCurrentPathKey().startsWith(JIRA_SPRINT)
    && this.sprint.id
  }

  get primaryButtonProps(): ModalButtonProps {
    let pathKey = this.nav.getCurrentPathKey();
    let label: string = pathKey === JIRA_SPRINT
    ? this.translate.instant("Export")
    : verbOfStep(pathKey, this.translate);

    const onClick: () => void = () => {
      let modalAction: Observable<ModalAction> | undefined = undefined;
      this.nextStage((act) => modalAction = act);
      return modalAction ?? of(ModalActions.ignore);
    };

    let disabled = !navCanContinue(this.nav, {
      issuesLoading: false,
    });

    return {
      label,
      buttonFlavor: "primary",
      disabled,
      onClick,
    };
  }

  get secondaryButtonProps(): ModalButtonProps | undefined {
    const pathKey = this.nav.getCurrentPathKey();
    if (pathKey !== SOURCE_SELECTION) {
      return;
    }

    if (this.nav.source.id === "jira" && this.isJiraConnected) {
      return {
        label: this.translate.instant("Import.DisconnectSource"),
        buttonFlavor: "secondary",
        disabled: this.isModalInitializing,
        onClick: () => {
          this.disconnectJira();
          return of(ModalActions.close);
        }
      }
    }
  }


  get tertiaryButtonProps(): ModalButtonProps {
    return {
      label: this.translate.instant("Cancel"),
      buttonFlavor: "secondary",
      disabled: this.isModalInitializing,
      onClick: () => {
        return of(ModalActions.close);
      },
    };
  }



  reset(step: Step) {
    const pathKey = step.id;
    this.nav.setCurrentPath(pathKeyToPath(pathKey));
  }

  get steps(): Array<Step> {
    return this.nav.getExportSteps();
  }

  async showModal() {
    const state = this.roomState;
    this.exportSuccess = false;

    if (!state) {
      throw new Error("uninitialized room state");
    }

    const activeGame = state.activeGame;

    if (!activeGame) {
      throw new Error("no active game");
    }

    this.isModalInitializing = true;

    this.modal?.showModal();

    try {
      const connected = await this.checkJiraConnected();
      if (!connected) {
        this.nav.setCurrentPath(pathKeyToPath(SOURCE_SELECTION));
        this.nav.evictCurrentChildren();
        return;
      }

      let importParams: ImportParameters | undefined = undefined;
      try {
        importParams = await firstValueFrom(this.agileCasinoService.getImportParameters(this.roomId, activeGame.id)) ?? undefined
      } catch (e) {
        console.log(e);
      }

      if (importParams) {
        const checked = NavData.safeParse(importParams.importModalState);
        if (checked.success) {
          this.nav.copyMetadata(checked.data, JIRA_SPRINT)
        }
        this.queryClient.invalidateQueries({ queryKey: ["import", "jira"] });
      } else {
        const modalState = this.modalStateQuery.data();
        if (modalState) {
          Object.assign(this.nav, modalState);
        }
      }
    } finally {
      this.isModalInitializing = false;
    }
  }

  unselectSprint() {
    this.nav.setMetadata(JIRA_SPRINT, {});
  }

  close() {
    this.modal?.close();
  }

  selectIssueSource(item: IssueSourceOption) {
    switch (item.source) {
      case "jira":
        const source = this.source;
        source.id = item.source;
        source.icon = item.icon;
        source.label = item.label;
        source.enabled = `${item.enabled}`;
        this.nav.setMetadata("", source);
        break;
      default:
        this.featuresService.emitSmokeTestEvent({
            smokeTest: "task-sources",
            payload: {
              action: "export",
              source: item.source,
            },
        })
        this.smokeTestSource = item.label;
        this.authModal?.showPlans({
          triggeredByFeature: Feature.ImportTasks,
        });
        break;
    }
  }

  selectJiraResource(selection: JiraResource) {
    const instance = this.instance;
    if (instance.id !== selection.id) {
      this.nav.evictChildren(JIRA_INSTANCE);
    }
    instance.id = selection.id;
    instance.label = selection.name;
    instance.avatarUrl = selection.avatarUrl;
    this.nav.setMetadata(JIRA_INSTANCE, instance);
  }

  selectJiraBoard(selection: JiraBoard) {
    const board = this.board;
    if (selection.id.toString() !== board.id) {
      this.nav.evictChildren(JIRA_BOARD);
    }
    board.id = selection.id.toString();
    board.label = selection.name;
    this.nav.setMetadata(JIRA_BOARD, board);
  }

  selectJiraSprint(selection: JiraSprint | undefined) {
    if (!selection) {
      this.unselectSprint();
      return;
    }

    const sprint = this.sprint;
    if (selection.id.toString() !== sprint.id) {
      this.nav.evictChildren(JIRA_SPRINT);
    }
    sprint.id = selection.id.toString();
    sprint.label = selection.name;
    this.nav.setMetadata(JIRA_SPRINT, sprint);
  }

  connectJira() {
    connectJira(this.agileCasinoService, "exportTasks");
  }

  nextStage(queueModalAction: (action: Observable<ModalAction>) => void) {
    const pathKey = this.nav.getCurrentPathKey();
    switch (pathKey) {
      case SOURCE_SELECTION: {
        const source = this.source;
        switch (source.id) {
          case "jira": {
            this.queryClient.invalidateQueries({ queryKey: ["import", "jira"] });
            queueModalAction(from(this.checkJiraConnected()).pipe(map((ok) => {
              this.nav.setCurrentPath(pathKeyToPath(JIRA_INSTANCE));

              if (ok) {
                return ModalActions.ignore;
              }

              this.connectJira();
              return ModalActions.ignore;
            })))
          } break;
          default: {
          } break;
        }
      } break;
      case JIRA_INSTANCE: {
        this.nav.setCurrentPath(pathKeyToPath(JIRA_BOARD));
      } break;
      case JIRA_BOARD: {
        this.nav.setCurrentPath(pathKeyToPath(JIRA_SPRINT));
      } break;
      case JIRA_SPRINT: {
        const action = this.exportGameModalAction();
        queueModalAction(action);
      } break;
    }
  }

  async checkJiraConnected(): Promise<boolean> {
    return new Promise(ok => this.agileCasinoService.jiraResources().subscribe({
      next: () => ok(true),
      error: () => ok(false),
    }));
  }

  get tasksOfActiveGame() {
    const activeGame = this.roomState.activeGame;
    if (!activeGame) {
      return [];
    }
    return this.roomState.getTasksForGame(activeGame).map(({task}) => task);
  }

  exportGameModalAction(): Observable<ModalAction> {
    const state = this.roomState;

    if (!state) {
      throw new Error("room state uninitialized");
    }

    const activeGame = state.activeGame;

    if (!activeGame) {
      throw new Error("no active game");
    }

    if (!this.instance.id || !this.board.id || !this.sprint.id) {
      throw new Error("no sprint selected");
    }


    const pending: Promise<SimpleNotification> = firstValueFrom(this.agileCasinoService.exportGame(this.roomId, activeGame.id, this.instance.id, this.board.id, +this.sprint.id))
    .then(() => {
      this.exportSuccess = true;
      return {
        type: "ok",
        title: "",
        message: this.translate.instant("Notification.ExportTasks.Success"),
      } satisfies SimpleNotification;
    })
    .catch((e) => {
      console.error({ whileExportingTasks: e });
      return {
        type: "error",
        title: "",
        message: this.translate.instant("Notification.ExportTasks.Fail")
      };
    });

    this.notificationsService.notify({
      type: "pending",
      title: "",
      message: this.translate.instant("Notification.ExportTasks.Pending"),
      pending,
    } satisfies PendingNotification);

    return of(ModalActions.close);
  }
}
