import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "app-rounded-list",
  templateUrl: "./rounded-list.component.html",
  styleUrls: ["./rounded-list.component.scss"],
})
export class RoundedListComponent {
    @Input() title: string = "";
    @Input() backLabel?: string;
    @Output("back") backEvent: EventEmitter<string> = new EventEmitter<string>();

    constructor() {}

    handleBackClicked() {
      this.backEvent.emit()
    }
}
