import { Component, Input } from "@angular/core";
import { CreateQueryResult, injectQuery } from "@tanstack/angular-query-experimental";
import { JiraIssue, JiraTaskMedatata } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { genericIsError, genericIsLoaded, genericIsLoading } from "../query-util";
import { Task } from "src/app/schemas/Task";
import { safeParseJSON } from "src/app/shared/util";

@Component({
  selector: "app-jira-issue-export-list",
  templateUrl: "./jira-issue-export-list.component.html",
  styleUrls: ["./jira-issue-export-list.component.scss"],
})
export class JiraIssueExportListComponent {
  @Input() instanceId?: string;
  @Input() boardId?: string;
  @Input() sprintId?: string;
  @Input() tasks: Array<Task> = [];

  issuesQuery: CreateQueryResult<Array<JiraIssue>> = injectQuery(() => ({
    queryKey: ['import', 'jira', 'issues', this.sprintId],
    queryFn: () => new Promise((next, error) => {
      if (!this.instanceId || !this.boardId || !this.sprintId) {
        error(new Error("no sprint selected"));
        return;
      }

      this.agileCasinoService.jiraIssues2(this.instanceId, this.boardId, this.sprintId).subscribe({ next, error });
    })
  }))

  constructor(
    private agileCasinoService: AgileCasinoService,
  ) {}

  get isLoading(): boolean {
    return genericIsLoading(this.issuesQuery);
  }
  get isError(): boolean {
    return genericIsError(this.issuesQuery);
  }

  get isLoaded(): boolean {
    return genericIsLoaded(this.issuesQuery);
  }

  get data(): Array<JiraIssue> {
    return this.issuesQuery.data() ?? [];
  }

  isNewTask(task: Task): boolean {
    return task.provider !== "jira";
  }

  metadataFor(task: Task): string {
    try {
      const metadata: JiraTaskMedatata = safeParseJSON(task.metadata ?? "{}") as any;
      return `${metadata?.issue?.key ?? ""}`;
    } catch (e) {
      return "";
    }
  }
}
