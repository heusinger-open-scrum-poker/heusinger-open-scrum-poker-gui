import { Component, EventEmitter, Input, Output } from "@angular/core";
import { IssueSourceOption } from "../import-tasks-modal";
import { ImportSource } from "../nav-data/source-selection";

@Component({
  selector: "app-import-source-select",
  templateUrl: "./import-source-select.component.html",
  styleUrls: ["./import-source-select.component.scss", "../import-export.scss"],
  host: {
    class: "scrollable-area",
  },
})
export class ImportSourceSelectComponent {
  @Input() sources: Array<IssueSourceOption> = [];
  @Input() selected?: ImportSource;
  @Output("select") selectEmitter: EventEmitter<IssueSourceOption> = new EventEmitter<IssueSourceOption>();

  constructor() {}


  select(item: IssueSourceOption): void {
    this.selectEmitter.emit(item);
  }

  identifySource(_index: number, source: IssueSourceOption) {
    return source.source;
  }
}
