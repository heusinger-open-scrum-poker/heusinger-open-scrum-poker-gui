import { AgileCasinoService } from "src/app/agile-casino.service";
import { environment } from "src/environments/environment";
import { Nav, PathKey } from "./nav-data";
import { TranslateService } from "@ngx-translate/core";
import { JIRA_BOARD, JIRA_INSTANCE, JIRA_SPRINT, JiraBoardInfo, JiraInstanceInfo, JiraSprintInfo } from "./nav-data/jira";
import { SOURCE_SELECTION, SourceSelectionInfo } from "./nav-data/source-selection";
import { Step } from "./progress-navigation";
import { JiraBoard, JiraResource, JiraSprint } from "src/app/schemas/jira";

export function connectJira(agileCasinoService: AgileCasinoService, action: "exportTasks" | "importTasks") {
  agileCasinoService.jiraGenerateState().subscribe({
    next: (state) => {
      const scope = [
        // grant refresh token
        "offline_access",
        // granular scopes
        "read:jira-work",
        "write:jira-work",
        // fine-grained scopes
        "read:issue:jira",
        "read:issue-details:jira",
        "read:project:jira",
        "read:board-scope.admin:jira-software",
        "read:board-scope:jira-software",
        "read:issue:jira-software",
        "read:sprint:jira-software",
        "write:sprint:jira-software",
        "read:jql:jira",
        "write:issue:jira-software",
        "write:issue:jira",
        "read:issue-meta:jira",
        "read:avatar:jira",
        "read:field-configuration:jira",
      ];
      const oauthUrl = `https://auth.atlassian.com/authorize?audience=api.atlassian.com&client_id=${
        environment.jiraClientId
      }&scope=${
        encodeURIComponent(scope.join(" "))
      }&redirect_uri=${
        encodeURIComponent(environment.guiBaseUrl + `/submit_jira_auth_code?action=${action}`)
      }&state=${
        encodeURIComponent(state + '$' + location.pathname)
      }&response_type=code&prompt=consent`;
      window.location.href = oauthUrl;
    },
    error: (e) => {
      console.error(e);
    },
  })
}

export function verbOfStep(pathKey: PathKey, t: TranslateService): string {
  switch (pathKey) {
    case SOURCE_SELECTION: {
      return t.instant("Connect");
    }
    case JIRA_INSTANCE:
    case JIRA_BOARD: {
      return t.instant("Choose");
    }
    case JIRA_SPRINT: {
      return t.instant("Import");
    }
    default: {
      return t.instant("Next");
    }
  }
}

export function navCanContinue(nav: Nav, context: {
  issuesLoading: boolean
}) {
    let pathKey = nav.getCurrentPathKey();
    switch (pathKey) {
      case SOURCE_SELECTION: {
        return true;
      }
      case JIRA_INSTANCE: {
        return !!nav.getTypedMetadata(JiraInstanceInfo, JIRA_INSTANCE).id;
      }
      case JIRA_BOARD: {
        return !!nav.getTypedMetadata(JiraBoardInfo, JIRA_BOARD).id;
      }
      case JIRA_SPRINT: {
        return !!nav.getTypedMetadata(JiraSprintInfo, JIRA_SPRINT).id
        && !context.issuesLoading
      }
      default: {
        return false;
      }
    }
}

export class IssueNav extends Nav {
  constructor(pathKey: PathKey) {
    super(pathKey);
  }

  get source(): SourceSelectionInfo {
    return this.getTypedMetadata(SourceSelectionInfo, SOURCE_SELECTION) ?? {};
  }

  get instance(): JiraInstanceInfo {
    return this.getTypedMetadata(JiraInstanceInfo, JIRA_INSTANCE);
  }

  get board(): JiraBoardInfo {
    return this.getTypedMetadata(JiraBoardInfo, JIRA_BOARD);
  }

  get sprint(): JiraSprintInfo {
    return this.getTypedMetadata(JiraSprintInfo, JIRA_SPRINT);
  }

  public static override fromData(data: unknown): IssueNav | undefined {
    const nav = new IssueNav("");
    Object.assign(nav, super.fromData(data));
    return nav;
  }

  public static override fromJSON(serialized: string): IssueNav | undefined {
    const nav = new IssueNav("");
    Object.assign(nav, super.fromJSON(serialized));
    return nav;
  }

  public selectJiraResource(selection: JiraResource) {
    const instance = this.getTypedMetadata(JiraInstanceInfo, JIRA_INSTANCE);
    if (instance.id !== selection.id) {
      this.evictChildren(JIRA_INSTANCE);
    }
    instance.id = selection.id;
    instance.label = selection.name;
    instance.avatarUrl = selection.avatarUrl;
    this.setMetadata(JIRA_INSTANCE, instance);
  }

  public selectJiraBoard(selection: JiraBoard) {
    const board = this.getTypedMetadata(JiraBoardInfo, JIRA_BOARD);
    if (selection.id.toString() !== board.id) {
      this.evictChildren(JIRA_BOARD);
    }
    board.id = selection.id.toString();
    board.label = selection.name;
    this.setMetadata(JIRA_BOARD, board);
  }

  public deselectJiraSprint() {
    this.setMetadata(JIRA_SPRINT, {});
  }

  public selectJiraSprint(selection: undefined | JiraSprint) {
    if (!selection) {
      this.deselectJiraSprint();
      return;
    }

    const sprint = this.getTypedMetadata(JiraSprintInfo, JIRA_SPRINT);
    if (selection.id.toString() !== sprint.id) {
      this.evictChildren(JIRA_SPRINT);
    }
    sprint.id = selection.id.toString();
    sprint.label = selection.name;
    this.setMetadata(JIRA_SPRINT, sprint);
  }

  public getImportSteps(): Array<Step> {
    const pathKey = this.getCurrentPathKey();
    // NOTE: might want to replace this with a translation key, e.g. "Import.Unnamed" to better inform the user
    const UNNAMED = "";

    if (!this.source.id) {
      return [{
        id: SOURCE_SELECTION,
        heading: "Import.Connect",
        subheading: this.source.id ? (this.source.label ?? UNNAMED) : "Import.NotSelected",
        completed: !!this.source.id,
        active: pathKey === SOURCE_SELECTION,
        done: true,
      }];
    }

    switch (this.source.id) {
      case "jira": {
        return [
          {
            id: SOURCE_SELECTION,
            heading: "Import.Connect",
            subheading: this.source.id ? (this.source.label ?? UNNAMED) : "Import.NotSelected",
            completed: true,
            active: pathKey === SOURCE_SELECTION,
            done: true,
          },
          {
            id: JIRA_INSTANCE,
            heading: "Import.SelectResource",
            subheading: this.instance.id ? (this.instance.label ?? UNNAMED) : "Import.NotSelected",
            completed: pathKey.startsWith(JIRA_INSTANCE),
            active: pathKey === JIRA_INSTANCE,
            done: !!this.instance.id,
          },
          {
            id: JIRA_BOARD,
            heading: "Import.SelectProject",
            subheading: this.board.id ? (this.board.label ?? UNNAMED) : "Import.NotSelected",
            completed: pathKey.startsWith(JIRA_BOARD),
            active: pathKey === JIRA_BOARD,
            done: !!this.board.id,
          },
          {
            id: JIRA_SPRINT,
            heading: this.sprint.id  ? "Import" : "Import.SelectSprint",
            subheading: this.sprint.id ? (this.sprint.label ?? UNNAMED) : "Import.NotSelected",
            completed: pathKey.startsWith(JIRA_SPRINT),
            active: pathKey === JIRA_SPRINT,
            done: !!this.sprint.id,
          },
        ];

      };
      default: {
        console.warn(`steps() not implemented for source=${this.source.id}`);
        return [];
      };
    }
  }

  public getExportSteps(): Array<Step> {
    const pathKey = this.getCurrentPathKey();
    // NOTE: might want to replace this with a translation key, e.g. "Import.Unnamed" to better inform the user
    const UNNAMED = "";

    if (!this.source.id) {
      return [{
        id: SOURCE_SELECTION,
        heading: "Import.Connect",
        subheading: this.source.id ? (this.source.label ?? UNNAMED) : "Import.NoSource",
        completed: !!this.source.id,
        active: false,
        done: true,
      }];
    }

    switch (this.source.id) {
      case "jira": {
        return [
          {
            id: SOURCE_SELECTION,
            heading: "Import.Connect",
            subheading: this.source.id ? (this.source.label ?? UNNAMED) : "Import.NoSource",
            completed: true,
            active: pathKey === SOURCE_SELECTION,
            done: true,
          },
          {
            id: JIRA_INSTANCE,
            heading: "Import.SelectResource",
            subheading: this.instance.id ? (this.instance.label ?? UNNAMED) : "Import.NotSelected",
            completed: pathKey.startsWith(JIRA_INSTANCE),
            active: pathKey === JIRA_INSTANCE,
            done: !!this.instance.id,
          },
          {
            id: JIRA_BOARD,
            heading: "Import.SelectProject",
            subheading: this.board.id ? (this.board.label ?? UNNAMED) : "Import.NotSelected",
            completed: pathKey.startsWith(JIRA_BOARD),
            active: pathKey === JIRA_BOARD,
            done: !!this.board.id,
          },
          {
            id: JIRA_SPRINT,
            heading: this.sprint.id  ? "Export" : "Import.SelectSprint",
            subheading: this.sprint.id ? (this.sprint.label ?? UNNAMED) : "Import.NotSelected",
            completed: pathKey.startsWith(JIRA_SPRINT),
            active: pathKey === JIRA_SPRINT,
            done: !!this.sprint.id,
          },
        ];

      };
      default: {
        console.warn(`steps() not implemented for source=${this.source.id}`);
        return [];
      };
    }
  }

}
