import { Component, Input, booleanAttribute } from "@angular/core";

@Component({
  selector: "app-issue-list-item",
  templateUrl: "./issue-list-item.component.html",
  styleUrls: ["./issue-list-item.component.scss"],
  host: {
    "[class.disabled]": "disabled",
  },
})
export class IssueListItemComponent {
  @Input({ transform: booleanAttribute }) disabled: boolean = false;
}
