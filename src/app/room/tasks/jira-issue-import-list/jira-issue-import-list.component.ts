import { Component, Input, inject } from "@angular/core";
import { CreateQueryResult, QueryClient, injectQuery } from "@tanstack/angular-query-experimental";
import { JiraIssue } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { genericIsError, genericIsLoaded, genericIsLoading } from "../query-util";

@Component({
  selector: "app-jira-issue-import-list",
  templateUrl: "./jira-issue-import-list.component.html",
  styleUrls: ["./jira-issue-import-list.component.scss"],
})
export class JiraIssueImportListComponent {
  @Input() instanceId?: string;
  @Input() boardId?: string;
  @Input() sprintId?: string;

  @Input() isImportRestricted: boolean = false;
  @Input() maxRestrictedImport: number = Infinity;

  constructor(
    private agileCasinoService: AgileCasinoService,
  ) {}

  queryClient = inject(QueryClient);

  issuesQuery: CreateQueryResult<Array<JiraIssue>> = injectQuery(() => ({
    queryKey: ['import', 'jira', 'issues', this.sprintId],
    queryFn: () => new Promise((next, error) => {
      if (!this.instanceId || !this.boardId || !this.sprintId) {
        error(new Error("no sprint selected"));
        return;
      }

      this.agileCasinoService.jiraIssues2(this.instanceId, this.boardId, this.sprintId).subscribe({ next, error });
    }),
    enabled: !!this.sprintId,
  }))

  get isLoading(): boolean {
    return genericIsLoading(this.issuesQuery);
  }
  get isError(): boolean {
    return genericIsError(this.issuesQuery);
  }

  get error(): Error | null {
    return this.issuesQuery.error();
  }

  get isLoaded(): boolean {
    return genericIsLoaded(this.issuesQuery);
  }

  get issuesData(): Array<JiraIssue> {
    return this.issuesQuery.data() ?? [];
  }
}
