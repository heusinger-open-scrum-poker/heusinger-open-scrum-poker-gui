import { z } from "zod";
import { PathKey } from "./path";

export const IMPORT_SOURCES = [
  "jira",
  "github",
  "asana",
  "gitlab",
  "wrike",
  "csv",
] as const;

export type ImportSource = z.infer<typeof ImportSource>
export const ImportSource = z.enum(IMPORT_SOURCES)


export const SOURCE_SELECTION: PathKey = ""


export type SourceSelectionInfo = z.infer<typeof SourceSelectionInfo>
export const SourceSelectionInfo = z.object({
  id: ImportSource.optional(),
  label: z.string().optional(),
  icon: z.string().optional(),
  enabled: z.enum(["true", "false"] as const).optional(),
})
