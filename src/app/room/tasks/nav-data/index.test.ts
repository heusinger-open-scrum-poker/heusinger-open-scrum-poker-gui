import { expect, describe, it } from "vitest";
import { z } from "zod";
import { Nav, NavData } from ".";

describe("Navigation", () => {
  it("can be deserialized and then serialized", () => {
    const data: NavData = {
        currentPath: ["foo", "bar"],
        metadata: {
          "foo::bar": {modifiedAt: new Date().toISOString(), store: {"url": "https://example.com"}},
        },
    };
    const json = Nav.fromData(data)?.toJSON()!;
    expect(json).toBeTruthy();
    const parsed = Nav.fromJSON(json)!;
    expect(parsed.toJSON()).toStrictEqual(JSON.stringify(data));
  });

  it("can clear old entries", () => {
    const data: NavData = {
        currentPath: ["foo", "bar"],
        metadata: {
          "foo::bar": {modifiedAt: new Date(2024, 0, 1).toISOString(), store: {"url": "https://example.com"}},
          "foo::baz": {modifiedAt: new Date(2023, 0, 1).toISOString(), store: {"url": "https://example.com"}},
        },
    };
    const nav = Nav.fromData(data)!;
    expect(nav).toBeDefined();

    nav.clearEntriesOlderThan(new Date(2023, 5, 6));

    nav.setCurrentPath(['foo','bar']);
    expect(nav.getCurrentMetadata()).toBeDefined();

    nav.setCurrentPath(['foo','baz']);
    expect(nav.getCurrentMetadata()).toStrictEqual({});
  });

  it("can evict children", () => {
    const data: NavData = {
        currentPath: ["foo"],
        metadata: {
          "foo": {modifiedAt: new Date(2023, 0, 1).toISOString(), store: {}},
          "foo::bar": {modifiedAt: new Date(2024, 0, 1).toISOString(), store: {}},
          "foo::baz": {modifiedAt: new Date(2025, 0, 1).toISOString(), store: {}},
        },
    };
    const nav = Nav.fromData(data)!;
    expect(nav).toBeDefined();

    expect(Object.keys(nav.metadata)).toStrictEqual(["foo", "foo::bar", "foo::baz"]);
    nav.evictCurrentChildren();

    expect(Object.keys(nav.metadata)).toStrictEqual(["foo"]);
  });

  it("can access metadata in a typesafe fashion", () => {
    const data: NavData = {
        currentPath: ["foo"],
        metadata: {
          "foo": {modifiedAt: new Date(2023, 0, 1).toISOString(), store: {
            "foo": "1",
            "bar": "string",
          }},
        },
    };
    const nav = Nav.fromData(data)!;
    expect(nav).toBeDefined();

    const m = nav.getTypedCurrentMetadata(z.object({"foo": z.string(), bar: z.string()}));
    expect(m.foo).toEqual("1");
    expect(m.bar).toEqual("string");
  });
})
