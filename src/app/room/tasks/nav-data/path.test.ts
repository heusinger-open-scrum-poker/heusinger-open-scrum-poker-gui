import { expect, describe, it } from "vitest";
import { PathSegment, pathKeyToPath, pathToPathKey } from "./path";

describe("PathSegment::validate", () => {
  it("can validate path segments", () => {
    expect(PathSegment.safeParse("").success).toEqual(false);
    expect(PathSegment.safeParse("bad path segment containing spaces").success).toEqual(false);
    expect(PathSegment.safeParse("bad_path_segment::_the_path_separator").success).toEqual(false);
    expect(PathSegment.safeParse("a-good-path_segment").success).toEqual(true);
  })
})

describe("pathToPathKey", () => {
  it("can turn a path to a path key", () => {
    expect(pathToPathKey(["a", "b", "c"])).toStrictEqual("a::b::c")
    expect(pathToPathKey(["a"])).toStrictEqual("a")
    expect(pathToPathKey([])).toStrictEqual("")
  });

  it("it is the inverse of pathKeyToPath", () => {
    for (const x of [
      [],
      ["a"],
      ["a","b"],
      ["a","b", "c"],
    ]) {
      expect(pathKeyToPath(pathToPathKey(x))).toStrictEqual(x)
    }

    for (const x of [
      "",
      "a",
      "a::b",
      "a::b::c",
    ]) {
      expect(pathToPathKey(pathKeyToPath(x))).toStrictEqual(x)
    }
  });
})

describe("pathKeyToPath", () => {
  it("can turn a path key to a path", () => {
    expect(pathKeyToPath("a")).toStrictEqual(["a"])
    expect(pathKeyToPath("a::b::c")).toStrictEqual(["a","b","c"])
    expect(pathKeyToPath("")).toStrictEqual([])
  });
})
