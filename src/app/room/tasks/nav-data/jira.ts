import { z } from 'zod';
import { PathKey } from "./path";

export const JIRA_INSTANCE: PathKey = "instance"
export const JIRA_BOARD: PathKey = "instance::board"
export const JIRA_SPRINT: PathKey = "instance::board::sprint"

export type JiraInstanceInfo = z.infer<typeof JiraInstanceInfo>
export const JiraInstanceInfo = z.object({
  id: z.string().optional(),
  label: z.string().optional(),
  avatarUrl: z.string().optional(),
})

export type JiraBoardInfo = z.infer<typeof JiraInstanceInfo>
export const JiraBoardInfo = z.object({
  id: z.string().optional(),
  label: z.string().optional(),
})

export type JiraSprintInfo = z.infer<typeof JiraInstanceInfo>
export const JiraSprintInfo = z.object({
  id: z.string().optional(),
  label: z.string().optional(),
})
