import { z } from 'zod';

export const PATH_SEP = "::";

export type PathSegment = z.infer<typeof PathSegment>
export const PathSegment = z.string().min(1).refine(
  (segment) => !segment.includes(PATH_SEP),
  { message: "a path segment may not include '" + PATH_SEP + "'" }
).refine(
  (segment) => !/\s/.test(segment),
  { message: "a path segment may not spaces '" + PATH_SEP + "'" }
)

export type Path = z.infer<typeof Path>
export const Path = z.array(PathSegment)

export type PathKey = z.infer<typeof PathKey>
export const PathKey = z.string().refine(
  (pathkey) => !/\s/.test(pathkey),
  { message: "a path segment may not spaces '" + PATH_SEP + "'" }
)

export function pathToPathKey(path: Path): PathKey {
  return path.join(PATH_SEP)
}

export function pathKeyToPath(key: PathKey): Path {
  if (key === "") {
    return [];
  }
  return key.split(PATH_SEP)
}
