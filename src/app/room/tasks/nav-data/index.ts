import { ZodType, z } from 'zod';
import { PATH_SEP, Path, PathKey, PathSegment, pathKeyToPath, pathToPathKey } from './path';
import { safeParseJSON } from '../../../shared/util';

export * from './path';

export type KeyValueStore = z.infer<typeof KeyValueStore>
export const KeyValueStore = z.record(z.string(), z.string())

export type Metadata = z.infer<typeof Metadata>
export const Metadata = z.object({
  modifiedAt: z.string().datetime(),
  store: KeyValueStore,
})

export type NavData = z.infer<typeof NavData>
export const NavData = z.object({
  currentPath: Path,
  metadata: z.record(PathKey, Metadata),
});


export class Nav implements NavData {
  currentPath: Path = []
  metadata: Record<PathKey, Metadata> = {}

  constructor(pathKey: string) {
    this.currentPath = pathKeyToPath(pathKey);
  }

  public static fromData(data: unknown): Nav | undefined {
    try{
      const navData = NavData.parse(data);
      const nav = new Nav("");
      nav.currentPath = navData.currentPath;
      nav.metadata = navData.metadata;
      return nav;
    } catch (e) {
      console.debug("incompatible nav data");
      return;
    }
  }

  public toData(): NavData {
    return structuredClone(
      NavData.parse({
        currentPath: this.currentPath,
        metadata: this.metadata,
      } satisfies NavData)
    )
  }

  public static fromJSON(serialized: string): Nav | undefined {
    try {
      const json = safeParseJSON(serialized);
      const navData = NavData.parse(json);
      const nav = new Nav("");
      nav.currentPath = navData.currentPath;
      nav.metadata = navData.metadata;
      return nav;
    } catch (e) {
      console.error(e);
      return;
    }
  }

  public toJSON(): string {
    const data = NavData.parse({
      currentPath: this.currentPath,
      metadata: this.metadata,
    });

    return JSON.stringify(data);
  }

  public getCurrentPath(): Path {
    return structuredClone(this.currentPath);
  }

  public setCurrentPath(path: Path): void {
    Path.parse(path);
    this.currentPath = path;
  }

  public getCurrentPathKey(): PathKey {
    return pathToPathKey(this.currentPath);
  }

  public push(segment: string): void {
    const seg: PathSegment = PathSegment.parse(segment);
    this.currentPath.push(seg);
  }

  public pop(): void {
    this.currentPath.pop();
  }

  public clearEntriesOlderThan(cutoff: Date): void {
    this.metadata = Object.fromEntries(
      Object
      .entries(this.metadata)
      .filter(([_, m]) => new Date(m.modifiedAt) >= cutoff)
    );
  }

  public evictCurrentChildren(): void {
    this.evictChildren(this.getCurrentPathKey());
  }

  public evictChildren(pathKey: PathKey): void {
    if (pathKey === "") {
      this.metadata = {};
      return;
    }

    const evictPrefix = `${pathKey}${PATH_SEP}`;

    this.metadata = Object.fromEntries(
      Object
      .entries(this.metadata)
      .filter(([k, _]) => !k.startsWith(evictPrefix))
    );
  }

  public getCurrentMetadata(): KeyValueStore {
    return this.getMetadata(this.getCurrentPathKey());
  }

  public getMetadata(pathKey: PathKey): KeyValueStore {
    return structuredClone(this.metadata[pathKey]?.store ?? {});
  }

  public getTypedCurrentMetadata<SCHEMA extends ZodType>(schema: SCHEMA): z.infer<SCHEMA> extends KeyValueStore ? z.infer<SCHEMA> : never {
    return this.getTypedMetadata(schema, this.getCurrentPathKey());
  }

  public getTypedMetadata<SCHEMA extends ZodType>(schema: SCHEMA, pathKey: PathKey): z.infer<SCHEMA> extends KeyValueStore ? z.infer<SCHEMA> : never {
    return schema.parse(this.getMetadata(pathKey));
  }

  public setMetadata(pathKey: PathKey, metadata: KeyValueStore) {
    PathKey.parse(pathKey);
    this.metadata[pathKey] = {
      modifiedAt: new Date().toISOString(),
      store: metadata,
    }
  }

  public copyMetadata(other: NavData, pathKey: PathKey) {
    for (const [otherPathKey, entry] of Object.entries(other.metadata)) {
      if (pathKey.startsWith(otherPathKey)) {
        this.metadata[otherPathKey] = entry;
      }
    }
    this.currentPath = other.currentPath;
  }

  public setCurrentMetadata(metadata: KeyValueStore) {
    this.setMetadata(this.getCurrentPathKey(), metadata);
  }

  public getMetadataTimestamp(): Date | undefined {
    const modifiedAt = this.metadata[this.getCurrentPathKey()].modifiedAt;
    if (modifiedAt) {
      return new Date(modifiedAt);
    }
  }
}
