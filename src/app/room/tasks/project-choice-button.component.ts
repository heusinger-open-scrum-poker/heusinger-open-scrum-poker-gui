import { Component, Input, booleanAttribute } from "@angular/core";

@Component({
  selector: "app-project-choice-button",
  templateUrl: "./project-choice-button.component.html",
  styleUrls: ["./project-choice-button.component.scss"],
})
export class ProjectChoiceButtonComponent {
    @Input({ transform: booleanAttribute }) selected: boolean = false;
    @Input() headline: string = "";
    @Input() status: string = "";
    @Input() icon?: string;
    @Input() img?: string;
}
