import { Component, EventEmitter, Input, Output, ViewChild, inject } from "@angular/core";
import { CreateQueryResult, QueryClient, injectQuery } from "@tanstack/angular-query-experimental";
import { JiraSprint } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { genericIsError, genericIsLoaded, genericIsLoading } from "../query-util";
import { LanguageService } from "src/app/language.service";
import { Task } from "src/app/schemas/Task";
import { JiraIssueImportListComponent } from "../jira-issue-import-list/jira-issue-import-list.component";

@Component({
  selector: "app-jira-sprint-select",
  templateUrl: "./jira-sprint-select.component.html",
  styleUrls: ["./jira-sprint-select.component.scss", "../import-export.scss"],
  host: {
    class: "scrollable-area",
  },
})
export class JiraSprintSelectComponent {
  @ViewChild("importList") importList?: JiraIssueImportListComponent;

  @Input() mode: "import" | "export" = "import";
  @Input() instanceId?: string;
  @Input() boardId?: string;
  @Input() selected?: string;
  //
  // Import Mode Params
  @Input() isImportRestricted: boolean = false;
  @Input() maxRestrictedImport: number = Infinity;
  //
  // Export Mode Params
  @Input() exportTasks: Array<Task> = [];

  @Output("reconnect") reconnectEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("select") selectEmitter: EventEmitter<JiraSprint|undefined> = new EventEmitter<JiraSprint|undefined>();

  queryClient = inject(QueryClient);

  sprintsQuery: CreateQueryResult<Array<JiraSprint>> = injectQuery(() => ({
    queryKey: ['import', 'jira', 'sprints', this.boardId],
    queryFn: () => new Promise((next, error) => {
      if (!this.instanceId || !this.boardId) {
        error(new Error("no board selected"));
        return;
      }

      this.agileCasinoService.jiraSprints(this.instanceId, this.boardId).subscribe({ next, error });
    }),
  }))



  constructor(
    private agileCasinoService: AgileCasinoService,
    private languageService: LanguageService,
  ) {}


  get isLoading(): boolean {
    return genericIsLoading(this.sprintsQuery);
  }

  get isError(): boolean {
    return genericIsError(this.sprintsQuery)
    || (this.importList && genericIsError(this.importList.issuesQuery)) || false;
  }

  get error(): null | Error {
    return this.sprintsQuery.error() ?? this.importList?.issuesQuery.error?.() ?? null;
  }

  get isLoaded(): boolean {
    return genericIsLoaded(this.sprintsQuery);
  }

  get data(): Array<JiraSprint> {
    return this.sprintsQuery.data() ?? [];
  }

  get sprintName(): string {
    const sprints = this.sprintsQuery.data() ?? [];
    for (const sprint of sprints) {
      if (sprint.id.toString() == this.selected) {
        return sprint.name;
      }
    }
    return "";
  }

  select(item: JiraSprint): void {
    this.selectEmitter.emit(item);
  }

  formatDate(dateString: string) {
    return Intl.DateTimeFormat(this.languageService.getLocale(), {
      dateStyle: 'full',
    }).format(new Date(dateString));
  }

  unselect(): void {
    this.selectEmitter.emit(undefined);
  }
}
