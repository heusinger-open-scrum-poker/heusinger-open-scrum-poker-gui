import { Component, ViewChild } from "@angular/core";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";

@Component({
  selector: "app-unlock-import-button",
  templateUrl: "./unlock-import-button.component.html",
  styleUrls: ["./unlock-import-button.component.scss"],
})
export class UnlockImportButtonComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  unlockImportRestriction(): void {
    this.authModal?.showPlans({
      triggeredByFeature: Feature.ImportMoreTasks,
    });
  }
}
