import { Component, Input } from "@angular/core";

@Component({
  selector: "app-step-modal-description",
  templateUrl: "./step-modal-description.component.html",
  styleUrls: ["./step-modal-description.component.scss"],
})
export class StepModalDescriptionComponent {
    @Input() headline?: string = "";
    @Input() text?: string = "";

    get paragraphs(): Array<string> {
      if (!this.text) {
        return [];
      }

      return this.text.split("\n\n");
    }
}
