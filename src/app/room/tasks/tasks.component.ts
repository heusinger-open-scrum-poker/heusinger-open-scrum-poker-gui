import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { environment } from "src/environments/environment";
import { RoomState } from "../state";
import { EditTaskModalComponent } from "../task-modal/edit-task-modal/edit-task-modal.component";
import { UserService } from "src/app/user.service";
import { TasksService } from "./tasks.service";
import { Settings } from "src/app/shared/Settings";
import { CdkDragDrop } from "src/third-party/cdk/drag-drop";
import { TaskWithSubtasks } from "src/app/schemas/Game";
import { CreateTaskModalComponent } from "../task-modal/create-task-modal/create-task-modal.component";

@Component({
  selector: "app-tasks",
  templateUrl: "./tasks.component.html",
  styleUrls: ["./tasks.component.scss"],
})
export class TasksComponent {
  @ViewChild("createTaskModal") createTaskModal?: CreateTaskModalComponent;
  @ViewChild("editTaskModal") editTaskModal?: EditTaskModalComponent;

  @Input("roomState") roomState!: RoomState;
  @Input() showControlButtons: boolean = true;

  @Output("import") onImport: EventEmitter<void> = new EventEmitter<void>();

  selectedTask?: TaskWithSubtasks;

  get roomId() {
    return this.roomState.getRoomId();
  }

  get tasks() {
    return this.roomState.getTasks();
  }

  get selectedId(): number | undefined {
    return this.roomState.room?.currentTaskId ?? undefined;
  }

  get taskFeatureEnabled(): boolean {
    return environment.taskFeature;
  }

  get isSignedIn() {
    return this.userService.isLoggedIn();
  }

  get isReorderingAllowed() {
    return this.isSignedIn;
  }

  constructor(
    private userService: UserService,
    private tasksService: TasksService,
  ) {}

  edit(task: TaskWithSubtasks) {
    this.selectedTask = JSON.parse(JSON.stringify(task));

    this.editTaskModal?.showModal();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (!this.roomId) {
      console.error("missing roomId");
      return;
    }

    if (!this.tasks) {
      console.error("no tasks");
      return
    }

    this.tasksService.drop(this.tasks.map(({task}) => task), event);
  }

  get dragStartDelay() {
    return Settings.DRAG_START_DELAY;
  }
}
