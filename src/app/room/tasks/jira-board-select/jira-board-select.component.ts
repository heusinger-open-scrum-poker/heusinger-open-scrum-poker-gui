import { Component, EventEmitter, Input, Output } from "@angular/core";
import { CreateQueryResult, injectQuery } from "@tanstack/angular-query-experimental";
import { JiraBoard } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { genericIsError, genericIsLoaded, genericIsLoading } from "../query-util";

@Component({
  selector: "app-jira-board-select",
  templateUrl: "./jira-board-select.component.html",
  styleUrls: ["./jira-board-select.component.scss"],
  host: {
    class: "scrollable-area",
  },
})
export class JiraBoardSelectComponent {
  @Input() instanceId?: string;
  @Input() selected?: string;
  @Output("reconnect") reconnectEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("select") selectEmitter: EventEmitter<JiraBoard> = new EventEmitter<JiraBoard>();

  boardsQuery: CreateQueryResult<Array<JiraBoard>> = injectQuery(() => ({
    queryKey: ['import', 'jira', 'boards', this.instanceId],
    queryFn: () => new Promise((next, error) => {
      if (!this.instanceId) {
        console.error("trying to query jira boards without instance id");
        return;
      }

      this.agileCasinoService.jiraBoards(this.instanceId).subscribe({ next, error });
    }),
  }))

  constructor(
    private agileCasinoService: AgileCasinoService,
  ) {}


  get isLoading(): boolean {
    return genericIsLoading(this.boardsQuery);
  }

  get isError(): boolean {
    return genericIsError(this.boardsQuery);
  }

  get error(): Error | null {
    return this.boardsQuery.error();
  }

  get isLoaded(): boolean {
    return genericIsLoaded(this.boardsQuery);
  }

  get data(): Array<JiraBoard> {
    return this.boardsQuery.data() ?? [];
  }

  select(item: JiraBoard): void {
    this.selectEmitter.emit(item);
  }

  identifyBoard(_index: number, board: JiraBoard): number {
    return board.id;
  }
}
