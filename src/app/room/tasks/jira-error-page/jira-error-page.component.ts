import { Component, EventEmitter, Input, Output, Signal } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

export type HttpError = Error & {
  error?: {
    status?: number,
    error?: string,
    message?: string,
  }
}

export type InlineAlertData = {
  title: string,
  description: string,
  offerReconnect: boolean,
};


@Component({
  selector: "app-jira-error-page",
  templateUrl: "./jira-error-page.component.html",
  styleUrls: ["./jira-error-page.component.scss"],
})
export class JiraErrorPageComponent {
  @Input() error?: null | HttpError;
  @Output("reconnect") reconnectEmitter: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private translate: TranslateService,
  ) {}

  get inlineAlertData(): InlineAlertData {
    const status = this.error?.error?.status ?? 0;
    switch (status) {
      case 0: { // CONNECTION ERROR
        return {
          title: this.translate.instant("Import.Error.ConnectionError.Title"),
          description: this.translate.instant("Import.Error.ConnectionError.Description"),
          offerReconnect: false,
        };
      }
      case 400: { // BAD REQUEST
        return {
          title: this.translate.instant("Import.Error.BadRequest.Title"),
          description: this.translate.instant("Import.Error.BadRequest.Description"),
          offerReconnect: false,
        };
      }
      case 401: { // UNAUTHORIZED
        return {
          title: this.translate.instant("Import.Error.Unauthorized.Title"),
          description: this.translate.instant("Import.Error.Unauthorized.Description"),
          offerReconnect: true,
        };
      }
      case 403: { // FORBIDDEN
        return {
          title: this.translate.instant("Import.Error.Forbidden.Title"),
          description: this.translate.instant("Import.Error.Forbidden.Description"),
          offerReconnect: true,
        };
      }
      default: { // INTERNAL SERVER ERROR
        return {
          title: this.translate.instant("Import.Error.Generic.Title"),
          description: this.translate.instant("Import.Error.Generic.Description"),
          offerReconnect: false
        };
      }
    }
  }
}
