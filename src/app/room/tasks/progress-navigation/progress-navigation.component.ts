import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Step } from ".";

@Component({
  selector: "app-progress-navigation",
  templateUrl: "./progress-navigation.component.html",
  styleUrls: ["./progress-navigation.component.scss"],
})
export class ProgressNavigationComponent {
  @Input() steps: Array<Step> = [];
  @Output("reset") resetEmitter: EventEmitter<Step> = new EventEmitter<Step>();


  identifyStep(_index: number, step: Step): string {
    return step.id;
  }

  handleClick(step: Step) {
    if (step.done || step.completed) {
      this.resetEmitter.emit(step);
    }
  }
}
