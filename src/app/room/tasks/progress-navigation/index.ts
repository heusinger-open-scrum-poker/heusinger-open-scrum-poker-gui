export interface Step {
  id: string,
  heading: string,
  subheading: string,
  completed: boolean,
  active: boolean,
  done: boolean,
}
