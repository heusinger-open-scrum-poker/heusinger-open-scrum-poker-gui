import { MountConfig } from "cypress/angular";
import { ProgressNavigationComponent } from "./progress-navigation.component";

describe("ProgressNavigationComponent", () => {
  const config: MountConfig<ProgressNavigationComponent> = {}

  it("mounts", () => {
    cy.mount(ProgressNavigationComponent, config);
  });
})
