import { Injectable } from "@angular/core";
import { Task } from "../../schemas/Task";
import { RoomState } from "../state";
import { AgileCasinoService } from "../../agile-casino.service";
import { getConsensus } from "../statistics";
import { NotificationsService } from "../../notifications.service";
import { TranslateService } from "@ngx-translate/core";
import { LexorankService } from "../../lexorank.service";
import { CdkDragDrop } from "src/third-party/cdk/drag-drop";

@Injectable({
  providedIn: "root",
})
export class TasksService {
  constructor(
    private agileCasinoService: AgileCasinoService,
    private notificationsService: NotificationsService,
    private translate: TranslateService,
    private lexorankService: LexorankService,
  ) {
  }

  public async selectTask(task: Task): Promise<void> {
    return new Promise((next, error) => {
      this.agileCasinoService.setCurrentTask(task.roomId, task.id).subscribe({
        next, error,
      })
    })
  }

  public async selectNext(roomState: RoomState): Promise<void> {
    const task = this.getNext(roomState);
    if (task) {
      await this.selectTask(task);
    }
  }

  public getNext(roomState: RoomState): Task | undefined {
    const tasks: Array<Task> = (roomState.getTasks() ?? []).map(({task}) => task);

    const currentTaskIndex = this.getCurrentTaskIndex(roomState, tasks);
    const tasksAfterCurrent: Array<Task> = tasks.slice(currentTaskIndex + 1);
    const tasksBeforeCurrent: Array<Task> = tasks.slice(0, currentTaskIndex);

    for (const list of [tasksAfterCurrent, tasksBeforeCurrent]) {
      const candidate = list.find((task) => {
        return !task.estimate || task.estimate === ""
      });
      if (candidate) {
        return candidate;
      }
    }
  }

  public async selectPrevious(roomState: RoomState): Promise<void> {
    const task = this.getPrevious(roomState);
    if (task) {
      await this.selectTask(task);
    }
  }

  public getPrevious(roomState: RoomState): Task | undefined {
    const tasks = (roomState.getTasks() ?? []).map(({task}) => task);
    const currentTaskIndex = this.getCurrentTaskIndex(roomState, tasks);

    const previousIndex = (currentTaskIndex - 1 + tasks.length) % tasks.length;

    return tasks[previousIndex];
  }

  public isFinalConsensus(roomState: RoomState): boolean {
    const cardValues = new Set(roomState.getActivePlayers().map((player) => player.cardValue));
    return cardValues.size == 1;
  }

  public async saveConsensusToCurrentTask(roomState: RoomState): Promise<void> {
    const roomRef = roomState.roomRef;

    if (!roomRef) {
      return
    }

    const task = roomState.getCurrentTask();

    if (!task) {
      return;
    }

    const consensus = getConsensus(roomState.stats);
    task.estimate = consensus;

    return new Promise((next, error) => this.agileCasinoService.updateTaskWithSubtasks(roomRef, task, []).subscribe({
      error,
      next: () => {
        const message = consensus
          ? this.translate.instant("Estimate.Saved") + " " + consensus
          : this.translate.instant("Estimate.Saved") + " " + this.translate.instant("Stats.NoConsensus");

        this.notificationsService.notify({
          type: "info",
          title: task.subject,
          message: message
        })

        next()
      }
    }));
  }

  public async drop(tasks: Task[], event: CdkDragDrop<string[]>) {
    const task: Task = this.lexorankService.drop(event, tasks) as Task;

    if (!task) {
      return;
    }

    return new Promise((next, _error) => this.agileCasinoService
      .updateTaskWithSubtasks(task.roomId, task, [])
      .subscribe({
        next,
        error: (error) => {
          console.error("failed to rank the item", error);
        },
      }));
  }

  private getCurrentTaskIndex(roomState: RoomState, tasks: Task[]): number {
    return tasks.findIndex((task) => task.id == roomState.getRoom()?.currentTaskId);
  }
}
