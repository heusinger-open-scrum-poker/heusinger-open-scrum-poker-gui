import { Component, EventEmitter, Input, Output } from "@angular/core";
import { CreateQueryResult, injectQuery } from "@tanstack/angular-query-experimental";
import { JiraResource } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { genericIsError, genericIsLoaded, genericIsLoading } from "../query-util";

@Component({
  selector: "app-jira-instance-select",
  templateUrl: "./jira-instance-select.component.html",
  styleUrls: ["./jira-instance-select.component.scss"],
  host: {
    class: "scrollable-area",
  },
})
export class JiraInstanceSelectComponent {
  @Input() selected?: string;

  @Output("reconnect") reconnectEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("select") selectEmitter: EventEmitter<JiraResource> = new EventEmitter<JiraResource>();

  resourcesQuery: CreateQueryResult<Array<JiraResource>> = injectQuery(() => ({
    queryKey: ['import', 'jira', 'resources'],
    queryFn: () => new Promise((next, error) => {
      this.agileCasinoService.jiraResources().subscribe({ next, error });
    }),
  }))

  constructor(
    private agileCasinoService: AgileCasinoService,
  ) {}


  get isLoading(): boolean {
    return genericIsLoading(this.resourcesQuery);
  }

  get isError(): boolean {
    return genericIsError(this.resourcesQuery);
  }

  get error(): Error | null {
    return this.resourcesQuery.error();
  }

  get isLoaded(): boolean {
    return genericIsLoaded(this.resourcesQuery);
  }

  get data(): Array<JiraResource> {
    return this.resourcesQuery.data() ?? [];
  }

  select(item: JiraResource): void {
    this.selectEmitter.emit(item);
  }

  identifyInstance(_index: number, instance: JiraResource): string {
    return instance.id;
  }
}
