import { CreateQueryResult } from "@tanstack/angular-query-experimental";

export function genericIsLoaded<T>(query: CreateQueryResult<T>): boolean {
  return query.isSuccess() && !query.isFetching()
}

export function genericIsLoading<T>(query: CreateQueryResult<T>): boolean {
  return query.isLoading() || query.isFetching();
}

export function genericIsError<T>(query: CreateQueryResult<T>) {
  return !query.isFetching() && query.isError();
}
