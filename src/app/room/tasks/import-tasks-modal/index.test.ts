import { expect, describe, it } from "vitest";
import { PROVIDER, normalizeIntegers, tagTasksWithJiraId } from ".";
import { Task } from "src/app/schemas/Task";

describe("tagTasksWithJiraId()", () => {
  it("should correctly tag tasks", () => {
    const invalidProvider = "not-jira";
    const provider = PROVIDER.JIRA;
    const invalidMetadata0 = "{}";
    const invalidMetadata1 = "{";
    const invalidMetadata2 = "";
    const invalidMetadata3 = null;
    const invalidMetadata4 = undefined;
    const metadata = JSON.stringify({issue:{id:"12345" }})
    const tasks = [
      { provider, metadata },
      { provider: invalidProvider, metadata },
      { provider, metadata: invalidMetadata0 },
      { provider, metadata: invalidMetadata1 },
      { provider, metadata: invalidMetadata2 },
      { provider, metadata: invalidMetadata3 },
      { provider, metadata: invalidMetadata4 },
    ].map((task: Partial<Task>) => ({task: task as Task, subtasks: []}));
    expect(tagTasksWithJiraId(tasks)).toMatchSnapshot();
  })
})

describe("normalizeIntegers()", () => {
  it("ignores non-integer strings", () => {
    const examples = [
      "",
      "XL",
      "1.1",
      "-10.999",
    ];
    expect(examples.map(it => ({ before: it, after: normalizeIntegers(it) }))).toMatchSnapshot();
  })

  it("removes decimals for integer valued floats", () => {
    const examples = [
      "0.000",
      "3.0",
      "-10.0000000",
    ];
    expect(examples.map(it => ({ before: it, after: normalizeIntegers(it) }))).toMatchSnapshot();
  })
})
