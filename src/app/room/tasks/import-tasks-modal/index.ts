import { Task } from "src/app/schemas/Task";
import { filterUndefined, safeParseJSON } from "../../../shared/util";
import { z } from "zod";
import { TaskWithSubtasks } from "src/app/schemas/Game";

//
export enum PROVIDER {
  JIRA = "jira",
}

export const IMPORT_SOURCES = [
  "jira",
  "github",
  "asana",
  "gitlab",
  "wrike",
  "csv",
] as const;

export const ImportSource = z.enum(IMPORT_SOURCES);
export type ImportSource = z.infer<typeof ImportSource>;

export const ISSUE_SOURCES = [
  {
    source: "jira",
    label: "Jira",
    icon: "svgrepo-jira",
    enabled: true,
  },
  {
    source: "github",
    label: "Github",
    icon: "github-fill",
    enabled: false,
  },
  {
    source: "asana",
    label: "Asana",
    icon: "svgrepo-asana",
    enabled: false,
  },
  {
    source: "gitlab",
    label: "Gitlab",
    icon: "gitlab-fill",
    enabled: false,
  },
  {
    source: "wrike",
    label: "Wrike",
    icon: "figma-wrike",
    enabled: false,
  },
  {
    source: "csv",
    label: "CSV",
    icon: "figma-csv",
    enabled: false,
  },
] as const satisfies Array<IssueSourceOption>;

export interface IssueSourceOption {
  source: ImportSource,
  label: string,
  icon: string,
  enabled: boolean,
}

export function tagTasksWithJiraId(tasks: Array<TaskWithSubtasks>): Array<{ task: Task, jiraId?: string }> {
  return filterUndefined(tasks.map(({task}) => {
    let jiraId: string | undefined = undefined;

    if (task.provider === PROVIDER.JIRA) {
      try {
        const metadata = safeParseJSON(task.metadata ?? "") as any;
        const { issue: { id } } = metadata;
        if (id && typeof id === "string") {
          jiraId = id;
        }
      } catch (e) {
        // id doesnt exist
      }
    }

    return { task, jiraId };
  }));
}

/** Removes decimals in case of integers
  */
export function normalizeIntegers(estimate: string): string {
  const testFloat = parseFloat(estimate);
  const testInt = parseInt(estimate);
  if (isNaN(testFloat) || isNaN(testInt)) {
    return estimate;
  }
  if (testInt == testFloat) {
    return `${testInt}`;
  }
  return estimate;
}
