import { JiraIssue, JiraSubtaskMedatata, JiraTaskMedatata } from "src/app/schemas/jira";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ModalAction, ModalActions, ModalButtonProps } from "src/app/modal/types";
import { TranslateService } from "@ngx-translate/core";
import { Observable, catchError, firstValueFrom, from, map, mergeMap, of } from "rxjs";
import { CreateQueryResult, QueryClient, injectMutation, injectQuery } from "@tanstack/angular-query-experimental";
import { Task } from "src/app/schemas/Task";
import j2m from "jira2md";
import { SubtaskOperation, TaskOperation } from "src/app/schemas/TaskOperation";
import { RoomState } from "../../state";
import { ImportParameters, JiraImportParameters, TaskWithSubtasks } from "src/app/schemas/Game";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";
import { UserService } from "src/app/user.service";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";
import { genericIsLoading } from "../query-util";
import { navCanContinue, connectJira, verbOfStep, IssueNav } from "../import-export";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { Nav, NavData, pathKeyToPath } from "../nav-data";
import { JIRA_BOARD, JIRA_INSTANCE, JIRA_SPRINT } from "../nav-data/jira";
import { NotificationsService } from "src/app/notifications.service";
import { SOURCE_SELECTION } from "../nav-data/source-selection";
import { Step } from "../progress-navigation";
import { Component, Input, Signal, ViewChild, inject } from "@angular/core";
import { ISSUE_SOURCES, IssueSourceOption, normalizeIntegers, tagTasksWithJiraId } from ".";


@Component({
  selector: "app-import-tasks-modal",
  templateUrl: "./import-tasks-modal.component.html",
  styleUrls: ["./import-tasks-modal.component.scss", "../import-export.scss"],
})
export class ImportTasksModalComponent {
  readonly MAX_RESTRICTED_IMPORT = 10;

  @ViewChild(ModalDialogComponent) modal?: ModalDialogComponent;
  @ViewChild("authModal") authModal?: AuthModalComponent;

  @Input({required: true}) roomId!: number;
  @Input({required: true}) roomState!: RoomState;

  isModalInitializing: boolean = false;
  nav: IssueNav = new IssueNav(SOURCE_SELECTION);
  smokeTestSource: string = "";
  importSuccess: boolean = false;

  queryClient = inject(QueryClient);

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
    private notificationsService: NotificationsService,
    private featuresService: KwFeatureService,
    private userService: UserService,
  ) {}

  get isImportRestricted(): boolean {
    const variant: Signal<string> | undefined = this.featuresService.getVariant("plans-modal");
    if (!variant) {
      return false;
    }
    const user = this.userService.getUser();
    if (!user) {
      return false;
    }
    const role = user.role;
    return role !== "pro" && (variant() === "plans-modal-new" || variant() === "plans-modal-new-free-trial");
  }

  private updateNav(callback: (nav: IssueNav) => void) {
    callback(this.nav);
    this.modalStateMutation.mutate();
  }

  modalStateQuery: CreateQueryResult<Nav> = injectQuery(() => ({
    queryKey: ['import', 'modal-state'],
    queryFn: () => new Promise((next, error) => {
      this.agileCasinoService.getImportModalState()
      .subscribe({ next: (navdata) => {
        const nav = IssueNav.fromData(navdata);
        if (nav) {
          this.nav = nav;
        }
        next(this.nav)
      }, error });
    }),
  }))

  modalStateMutation = injectMutation(() => ({
    mutationFn: () => new Promise((next, error) => {
      this.agileCasinoService.putImportModalState(this.nav.toData()).subscribe({ next, error });
    }),
    onSuccess: () => {
      this.queryClient.invalidateQueries({ queryKey: ["import", "modal-state"] });
    },
  }))

  jiraConnectedQuery: CreateQueryResult<boolean> = injectQuery(() => ({
    queryKey: ['import', 'jira','connection'],
    queryFn: () => new Promise((next, error) => { this.agileCasinoService.getIsJiraConnected().subscribe({
        next: ({isConnected}) => next(isConnected ?? false),
        error,
      });
    })
  }))

  disconnectJira(): void {
    this.jiraDisconnectMutation.mutate();
  }

  jiraDisconnectMutation = injectMutation(() => ({
    mutationFn: () => new Promise((next, error) => {
      this.agileCasinoService.disconnectJira().subscribe({
        next: () => {
          this.nav.setCurrentPath(pathKeyToPath(SOURCE_SELECTION));
          this.nav.evictCurrentChildren();
          next(null)
        },
        error: (e) => {
          this.nav.setCurrentPath(pathKeyToPath(SOURCE_SELECTION));
          error(e);
        },
      });
    }),
    onSuccess: () => {
      this.queryClient.invalidateQueries({queryKey: ["import", "jira", "connection"]});
    },
  }))

  issuesQuery: CreateQueryResult<Array<JiraIssue>> = injectQuery(() => ({
    queryKey: ['import', 'jira', 'issues', this.nav.sprint.id],
    queryFn: () => new Promise((next, error) => {
      if (!this.nav.instance.id || !this.nav.board.id || !this.nav.sprint.id) {
        error(new Error("no sprint selected"));
        return;
      }

      this.agileCasinoService
      .jiraIssues2(this.nav.instance.id, this.nav.board.id, this.nav.sprint.id)
      .subscribe({ next, error });
    })
  }))

  get isJiraConnected(): boolean {
    return !!this.jiraConnectedQuery.data();
  }

  get issueSources() {
    return ISSUE_SOURCES;
  }

  get isServiceSelection() {
    return this.nav.getCurrentPathKey() === SOURCE_SELECTION
  }

  get isInstanceSelection() {
    return this.nav.getCurrentPathKey() === JIRA_INSTANCE
  }

  get isBoardSelection() {
    return this.nav.getCurrentPathKey() === JIRA_BOARD
  }

  get isSprintSelection() {
    return this.nav.getCurrentPathKey() === JIRA_SPRINT
  }

  get isImportOverview() {
    return this.nav.getCurrentPathKey() === JIRA_SPRINT && this.isJiraSprintSelected
  }

  get isJira() {
    return this.nav.source.id === "jira"
  }

  get isJiraInitial() {
    return this.nav.getCurrentPathKey() === JIRA_INSTANCE
  }

  get isJiraInstanceSelected() {
    return this.nav.getCurrentPathKey().startsWith(JIRA_INSTANCE)
    && this.nav.instance.id
  }

  get isJiraBoardSelected() {
    return this.nav.getCurrentPathKey().startsWith(JIRA_BOARD)
    && this.nav.board.id
  }

  get isJiraSprintSelected() {
    return this.nav.getCurrentPathKey().startsWith(JIRA_SPRINT)
    && this.nav.sprint.id
  }

  get primaryButtonProps(): ModalButtonProps {
    let pathKey = this.nav.getCurrentPathKey();
    let label: string = verbOfStep(pathKey, this.translate);

    const onClick: () => void = () => {
      let modalAction: Observable<ModalAction> | undefined = undefined;
      this.nextStage((act) => modalAction = act);
      return modalAction ?? of(ModalActions.ignore);
    };

    let disabled = this.isModalInitializing || !navCanContinue(this.nav, {
      issuesLoading: genericIsLoading(this.issuesQuery)
    });

    return {
      label,
      buttonFlavor: "primary",
      disabled,
      onClick,
    };
  }

  get secondaryButtonProps(): ModalButtonProps | undefined {
    const pathKey = this.nav.getCurrentPathKey();
    if (pathKey !== SOURCE_SELECTION) {
      return;
    }

    if (this.nav.source.id === "jira" && this.isJiraConnected) {
      return {
        label: this.translate.instant("Import.DisconnectSource"),
        buttonFlavor: "secondary",
        disabled: this.isModalInitializing,
        onClick: () => {
          this.disconnectJira();
          return of(ModalActions.close);
        }
      }
    }
  }


  get tertiaryButtonProps(): ModalButtonProps {
    return {
      label: this.translate.instant("Cancel"),
      buttonFlavor: "secondary",
      disabled: this.isModalInitializing,
      onClick: () => {
        return of(ModalActions.close);
      },
    };
  }

  reset(step: Step) {
    const pathKey = step.id;
    this.updateNav((nav) => {
      nav.setCurrentPath(pathKeyToPath(pathKey));
    });
  }

  get steps(): Array<Step> {
    return this.nav.getImportSteps();
  }

  async showModal() {
    const state = this.roomState;
    this.importSuccess = false;

    if (!state) {
      throw new Error("uninitialized room state");
    }

    const activeGame = state.activeGame;

    if (!activeGame) {
      throw new Error("no active game");
    }

    this.isModalInitializing = true;

    this.modal?.showModal();

    try {
      const connected = await this.checkJiraConnected();
      if (!connected) {
        this.nav.setCurrentPath(pathKeyToPath(SOURCE_SELECTION));
        return;
      }

      let importParams: ImportParameters | undefined = undefined;
      try {
        importParams = await firstValueFrom(this.agileCasinoService.getImportParameters(this.roomId, activeGame.id)) ?? undefined
      } catch (e) {
        console.log("invalid import parameters", e);
      }

      if (importParams) {
        const checked = NavData.safeParse(importParams.importModalState);
        if (checked.success) {
          this.nav.copyMetadata(checked.data, JIRA_SPRINT)
        }
        this.queryClient.invalidateQueries({ queryKey: ["import", "jira"] });
      } else {
        this.navigateToRecommendedStage();
      }
    } finally {
      this.isModalInitializing = false;
    }
  }

  navigateToRecommendedStage() {
    if (this.nav.getCurrentPathKey() === JIRA_SPRINT && this.nav.sprint.id) {
      this.updateNav((nav) => {
        nav.deselectJiraSprint()
      })
    }
  }

  close() {
    this.modal?.close();
  }

  selectIssueSource(item: IssueSourceOption) {
    console.debug("selected issue source", item);
    switch (item.source) {
      case "jira": {
        const source = this.nav.source;
        source.id = item.source;
        source.icon = item.icon;
        source.label = item.label;
        source.enabled = `${item.enabled}`;
        this.updateNav((nav) => {
          nav.setMetadata(SOURCE_SELECTION, source);
        })
      } break;
      default:
        this.featuresService.emitSmokeTestEvent({
            smokeTest: "task-sources",
            payload: {
              action: "import",
              source: item.source,
            },
        })
        this.smokeTestSource = item.label;
        this.authModal?.showPlans({
          triggeredByFeature: Feature.ImportTasks,
        });
        break;
    }
  }


  connectJira() {
    connectJira(this.agileCasinoService, "importTasks");
  }

  nextStage(queueModalAction: (action: Observable<ModalAction>) => void) {
    const pathKey = this.nav.getCurrentPathKey();
    switch (pathKey) {
      case SOURCE_SELECTION: {
        const source = this.nav.source;
        switch (source.id) {
          case "jira": {
            this.queryClient.invalidateQueries({ queryKey: ["import", "jira"] });
            queueModalAction(from(this.checkJiraConnected()).pipe(map((ok) => {
              this.updateNav((nav) => {
                nav.setCurrentPath(pathKeyToPath(JIRA_INSTANCE));
              })

              if (ok) {
                return ModalActions.ignore;
              }

              this.connectJira();
              return ModalActions.ignore;
            })))
          } break;
          default: {
          } break;
        }
      } break;
      case JIRA_INSTANCE: {
        this.updateNav((nav) => {
          nav.setCurrentPath(pathKeyToPath(JIRA_BOARD));
        })
      } break;
      case JIRA_BOARD: {
        this.updateNav((nav) => {
          nav.setCurrentPath(pathKeyToPath(JIRA_SPRINT));
        })
      } break;
      case JIRA_SPRINT: {
        if (this.nav.sprint.id) {
          const action = this.importSprintModalAction();
          queueModalAction(action);
        }
      } break;
    }
  }

  async checkJiraConnected(): Promise<boolean> {
    return new Promise(ok => this.agileCasinoService.getIsJiraConnected().subscribe({
      next: ({isConnected}) => ok(isConnected ?? false),
      error: () => ok(false),
    }));
  }

  importSprintModalAction(): Observable<ModalAction> {
    const room = this.roomState.getRoom();

    if (!room) {
      throw new Error("room uninitialized");
    }

    const state = this.roomState;

    if (!state) {
      throw new Error("room state uninitialized");
    }

    const activeGame = state.activeGame;

    if (!activeGame) {
      throw new Error("no active game");
    }

    const sprintId = this.nav.sprint.id;
    const boardId = this.nav.board.id;
    const instanceId = this.nav.instance.id;

    if (!sprintId || !boardId || !instanceId) {
      throw new Error("no sprint selected");
    }

    const activeTasks: Array<TaskWithSubtasks> = this.roomState.tasks.filter(({task}) => task.gameId == activeGame.id);
    const activeTasksWithJiraId: Array<{ task: Task, jiraId?: string }> = tagTasksWithJiraId(activeTasks);

    const taskBatchOperations: Array<TaskOperation> = [];

    const issueList = this.isImportRestricted
      ? (this.issuesQuery.data() ?? []).slice(0, this.MAX_RESTRICTED_IMPORT)
      : this.issuesQuery.data() ?? [];

    for (const issue of issueList) {

      const taskWithJiraId = activeTasksWithJiraId.find(({jiraId}) => issue.id === jiraId);
      if (taskWithJiraId) {
        taskBatchOperations.push({
          type: "delete",
          task: {
              id: taskWithJiraId.task.id,
              description: "",
              roomId: room.id,
              subject: ""
          },
          subtaskOperations: [],
        } satisfies TaskOperation)
      }

      const subtaskOperations: Array<SubtaskOperation> = issue.subtasks.map((subtask) => ({
        type: "create",
        subtask: {
            subject: subtask.summary,
            checked: false,
            provider: "jira",
            metadata: JSON.stringify({ resource: instanceId, project: boardId, parentId: issue.id, subtask } satisfies JiraSubtaskMedatata),
        }
      }) satisfies SubtaskOperation);

      const task: Omit<Task, "id"> = {
        roomId: this.roomId,
        subject: issue.summary,
        description: issue.description ? j2m.jira_to_html(issue.description) : "",
        metadata: JSON.stringify({ resource: instanceId, project: boardId, issue } satisfies JiraTaskMedatata),
        provider: "jira",
        estimate: normalizeIntegers(issue.estimate ?? ""),
        rank: taskWithJiraId?.task.rank,
      };

      taskBatchOperations.push({
        type: "create",
        task,
        subtaskOperations,
      } satisfies TaskOperation);
    }

    const importParameters: JiraImportParameters = {
      type: "jira",
      instanceId,
      boardId,
      sprintId,
      importModalState: this.nav.toData(),
    };

      return this.agileCasinoService.putImportParameters(this.roomId, activeGame.id, importParameters).pipe(
        mergeMap(() =>
            this.agileCasinoService.batchUpdateTasks(this.roomId, taskBatchOperations)
            .pipe(map(() => {
              this.importSuccess = true;
              return ModalActions.close;
            }), catchError((e) => {
              console.debug(e);
              this.notificationsService.notify({type: "error", title: this.translate.instant("Notification.Error"), message: this.translate.instant("Notification.FailImportTasks")});
              return of(ModalActions.close);
            }))));
  }
}
