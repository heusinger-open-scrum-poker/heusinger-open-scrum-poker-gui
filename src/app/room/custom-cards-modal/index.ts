import { CardSeries, RawCardSeries } from "src/app/schemas/CardSeries";
import { Option } from "../../kwmc/dropdown";

export const CUSTOM_SERIES = {
  label: "Custom",
  value: undefined,
};

export const DEFAULT_OPTIONS: Option<string[] | undefined>[] = [
  {
    label: "Fibonacci (1, 2, 3, 5, 8, ...)",
    value: ["1", "2", "3", "5", "8", "13", "21", "34", "unclear", "break"],
  },
  {
    label: "Powers of Two (1, 2, 4, 8, 16, ...)",
    value: ["1", "2", "4", "8", "16", "32", "64", "128", "unclear", "break"],
  },
  {
    label: "Shirt Sizes",
    value: ["XS", "S", "M", "L", "XL", "2XL", "3XL", "4XL", "unclear", "break"],
  },
  CUSTOM_SERIES,
];

export const RESERVED_CARDS: string[] = ["break", "inf", "unclear"];

export function padArray<T>(
  array: Array<T>,
  length: number,
  generator: () => T,
): Array<T> {
  if (length >= array.length) {
    return [
      ...array,
      ...Array.from({ length: length - array.length }, generator),
    ];
  }
  return array;
}

export function readSeries(cards: RawCardSeries): CardSeries {
  const values = cards.filter((value) => !RESERVED_CARDS.includes(value));
  const unclear = cards.includes("unclear");
  const break_ = cards.includes("break");
  const inf = cards.includes("inf");
  return { values, break_, unclear, inf };
}

export function arrayEq(a: string[], b: string[]): boolean {
  return a.length === b.length && a.every((value, index) => value === b[index]);
}

export function truncate(input: string, maxLength: number): string {
  return input.slice(0, maxLength);
}
