import { Component, Input, ViewChild } from "@angular/core";
import { Observable, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
import {
  arrayEq,
  CUSTOM_SERIES,
  DEFAULT_OPTIONS,
  padArray,
  readSeries,
  RESERVED_CARDS,
  truncate,
} from ".";
import { ModalActions, ModalButtonProps } from "../../modal/types";
import { Option } from "../../kwmc/dropdown";
import { AgileCasinoService } from "../../agile-casino.service";
import { ConfirmationBoxComponent } from "../../modal/confirmation-box/confirmation-box.component";
import { ConfirmationResult, ConfirmationResults } from "../../modal/confirmation-box";
import { Settings } from "../../shared/Settings";
import { TranslateService } from "@ngx-translate/core";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";
import { RawCardSeries } from "src/app/schemas/CardSeries";

@Component({
  selector: "app-custom-cards-modal",
  templateUrl: "./custom-cards-modal.component.html",
  styleUrls: ["./custom-cards-modal.component.scss"],
})
export class CustomCardsModalComponent {
  @ViewChild(ModalDialogComponent) modal?: ModalDialogComponent;
  @ViewChild("confirmationBox") confirmationBox?: ConfirmationBoxComponent;
  @Input() room!: Room;
  @Input() player!: Player;

  public options: Option<string[] | undefined>[] = DEFAULT_OPTIONS;
  selected: Option<string[]> | Option<undefined> | null = null;
  values: string[] = [];
  unclear: boolean = false;
  break_: boolean = false;
  inf: boolean = false;
  extendedCardSlots: boolean = false;

  private numCards: 8 | 16 = 8;

  primaryButtonProps: ModalButtonProps | undefined = {
    label: this.translate.instant("CustomCardsModal.Apply"),
    buttonFlavor: "high-impact",
    onClick: () => {
      return this.getConfirmation().pipe(
        mergeMap((confirmation) => {
          if (confirmation === "confirm") {
            return this.agileCasinoService
              .setSeries(
                this.room.id,
                JSON.stringify(this.getSeries()),
                this.player.id,
              )
              .pipe(map(() => ModalActions.close));
          } else {
            return of(ModalActions.ignore);
          }
        }),
      );
    },
  };

  secondaryButtonProps: ModalButtonProps | undefined = {
    label: this.translate.instant("Cancel"),
    buttonFlavor: "secondary",
    onClick: () => {
      return of("close");
    },
  };

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
  ) {
    this.values = padArray(this.values, this.numCards, () => "");
  }

  get maxCardValueLength(): number {
    return Settings.MAX_CARD_VALUE_LENGTH;
  }

  getValue(index: number): string {
    return this.values[index];
  }

  onValueChanged(
    index: number,
    event: Event,
  ): void {
    this.selected = CUSTOM_SERIES;
    const target = event.target as HTMLInputElement;
    const newValue = target.value;
    if (RESERVED_CARDS.includes(newValue)) {
      // TODO signal to user that this card value is reserved
      return;
    }
    this.values[index] = truncate(
      newValue,
      this.maxCardValueLength
    );

    this.updateSelectedSeries();
  }

  onBreakChanged(): void {
    this.updateSelectedSeries();
  }

  onUnclearChanged(): void {
    this.updateSelectedSeries();
  }

  onSelectedChanged(): void {
    if (this.selected?.value) {
      const unreserved: Array<string> = padArray(
        this.selected.value.filter((value) => !RESERVED_CARDS.includes(value)),
        this.selected.value.length > 8 ? 16 : 8,
        () => "",
      );
      this.values = unreserved;
      this.numCards = unreserved.length > 8 ? 16 : 8;
      this.unclear = this.selected.value.includes("unclear");
      this.break_ = this.selected.value.includes("break");
    }
  }

  updateSelectedSeries(): void {
    for (let i = 0; i < this.options.length; ++i) {
      const option = this.options[i];
      if (option.value !== undefined) {
        const optionConfig = readSeries(option.value);
        if (
          optionConfig.break_ === this.break_ &&
          optionConfig.unclear === this.unclear &&
          optionConfig.inf === this.inf &&
          arrayEq(optionConfig.values, this.values)
        ) {
          this.selected = option;
          return;
        }
      }
    }
    this.selected = CUSTOM_SERIES;
  }

  generateIndices(start: number, length: number): number[] {
    return Array.from({ length }, (_, i) => start + i);
  }

  getSeries(): string[] {
    const unclear = this.unclear ? ["unclear"] : [];
    const break_ = this.break_ ? ["break"] : [];
    const hasEmptyExtraSlots = this.values.slice(8, 16).join("") === "";
    let values: string[];
    if (hasEmptyExtraSlots) {
      values = this.values.slice(0, 8);
    } else {
      values = this.values;
    }
    return [...values, ...unclear, ...break_];
  }

  getConfirmation(): Observable<ConfirmationResult> {
    if (!this.confirmationBox) {
      console.error("confirmationBox not initialized");
      return of(ConfirmationResults.cancel);
    }

    this.confirmationBox?.showModal();
    return this.confirmationBox.result.pipe(
      map((result: ConfirmationResult) => {
        this.confirmationBox?.close();
        return result;
      }),
    );
  }

  onToggleExpandCards() {
    this.extendedCardSlots = !this.extendedCardSlots;
    if (this.extendedCardSlots) {
      this.numCards = 16;
      this.values = padArray(this.values, this.numCards, () => "");
    } else {
      this.numCards = 8;
      this.values = this.values.slice(0, 8);
    }
  }

  loadSeries() {
    const series = RawCardSeries.parse(JSON.parse(this.room.series));
    const config = readSeries(series);
    if (config.values.length > 8) {
      this.numCards = 16;
      this.extendedCardSlots = true;
    } else {
      this.numCards = 8;
      this.extendedCardSlots = false;
    }
    this.values = padArray(config.values, this.numCards, () => "");
    this.unclear = config.unclear;
    this.break_ = config.break_;
    this.selected = CUSTOM_SERIES;
    this.updateSelectedSeries();
  }

  close() {
    this.modal?.close();
  }

  showModal() {
    this.loadSeries();
    this.modal?.showModal();
  }
}
