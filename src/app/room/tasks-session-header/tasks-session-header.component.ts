import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ActionButton } from "../index";

@Component({
  selector: "app-tasks-session-header",
  templateUrl: "./tasks-session-header.component.html",
  styleUrls: ["./tasks-session-header.component.scss"],
})
export class TasksSessionHeaderComponent {
  @Input() title: string = '';
  @Input() actions: ActionButton[] = [];
  @Output("titleClicked") titleClickedEmitter: EventEmitter<void> = new EventEmitter<void>();

  isBusy(action: ActionButton): boolean {
    if (!action.busy) {
      return false;
    }
    return action.busy();
  }

  identifyAction(_index: number, action: ActionButton) {
    return `${action.label}:${action.icon}`;
  }
}
