import { z } from "zod";
import { Game } from "../schemas/Game";

export const gameActivatedInfoSchema = z.object({
  game: Game,
});

export const gamesUpdatedInfoSchema = z.object({});

const partialPlayerSchema = z.object({
  id: z.number().int(),
  name: z.string(),
});

export const kickInfoSchema = z
  .object({
    player: partialPlayerSchema,
    kickingPlayer: partialPlayerSchema,
  })
  .partial();

export const sideliningInfoSchema = z
  .object({
    player: partialPlayerSchema,
    sideliningPlayer: partialPlayerSchema,
  })
  .partial();


export const promotionInfoSchema = z
  .object({
    player: partialPlayerSchema,
    initiatingPlayer: partialPlayerSchema,
  })
  .partial();

export const leaveInfoSchema = z
  .object({
    player: partialPlayerSchema,
  })
  .partial();

export type RoomChange = z.infer<typeof roomChangeSchema>;

export const roomChangeSchema = z.discriminatedUnion("type", [
  z.object({
    type: z.literal("SERIES_CHANGE"),
    player: partialPlayerSchema.optional(),
  }),
  z.object({
    type: z.literal("NAME_CHANGE"),
    player: partialPlayerSchema,
  }),
  z.object({
    type: z.literal("PLAYER_CHANGE"),
    player: partialPlayerSchema,
  }),
  z.object({
    type: z.literal("LOCKED"),
    player: partialPlayerSchema,
  }),
  z.object({
    type: z.literal("UNLOCKED"),
    player: partialPlayerSchema,
  }),
  z.object({
    type: z.literal("EMPTY"),
  }),
]);

export const revealInfoSchema = z.object({
  player: partialPlayerSchema.nullable(),
});

