import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Player, PlayerRef } from "src/app/schemas/Player";
import { PlayerView, TableView, getPlayerView } from "../TableView";
import { filterUndefined } from "src/app/shared/util";

@Component({
  selector: "app-big-poker-table",
  templateUrl: "./big-poker-table.component.html",
  styleUrls: ["./big-poker-table.component.scss"],
  host: {
    revealed: "isCardsRevealed()",
  },
})
export class BigPokerTableComponent {
  @Input({ required: true }) tableView!: TableView;
  @Output("kick") onKick: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("sideline") onSideline: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("promote") onPromote: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();

  isCardsRevealed(): boolean {
    return this.tableView.type === "revealed";
  }

  get self(): Player {
    return this.tableView.currentPlayer;
  }

  get estimators(): Array<PlayerView> {
    return filterUndefined(this.tableView.players
      .filter((player) => player.type === "estimator")
      .map((player) => getPlayerView(this.tableView, player.id)));
  }

  identifyPlayer(_index: number, view: PlayerView): Player["id"] {
    return view.player.id;
  }
}
