import { Component, Input } from "@angular/core";
import { Task } from "src/app/schemas/Task";
import { TasksService } from "../tasks/tasks.service";

@Component({
  selector: "app-task-item",
  templateUrl: "./task-item.component.html",
  styleUrls: ["./task-item.component.scss"],
})
export class TaskItemComponent {
  @Input() task!: Task;
  @Input() selected: boolean = false;
  @Input() selectable: boolean = true;
  @Input() draggable: boolean = false;

  busy: boolean = false

  constructor(
    private tasksService: TasksService
  ) {
  }

  select(event: Event): void {
    event.stopPropagation();
    this.busy = true;
    this.tasksService.selectTask(this.task)
    .then(() => this.busy = false)
    .catch((e) => {
      console.error(e);
      this.busy = false;
    })
  }

  get plainTaskDescription(): string {
    // removes all html tags except <p>
    return this.task.description.replace(/(<\/?(?!p)\w*\b[^>]*>)/gi, "");
  }
}
