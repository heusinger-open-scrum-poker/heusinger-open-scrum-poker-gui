import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../assets/i18n/en.json";
import { RoomModule } from "../room.module";
import { CdkDrag, CdkDragHandle, DragDropModule } from "src/third-party/cdk/drag-drop";
import { TaskItemComponent } from "./task-item.component";
import { MountConfig } from "cypress/angular";
import { TasksService } from "../tasks/tasks.service";

describe("TaskItemComponent", () => {
  let config: MountConfig<TaskItemComponent>;

  beforeEach(() => {
    config = {
      imports: [
        TranslateTestingModule.withTranslations("en", en_translation),
        RoomModule,
        DragDropModule,
        CdkDragHandle,
        CdkDrag,
      ],
      declarations: [],
      providers: [
        {
          provide: TasksService,
          useValue: {}
        }
      ],
      componentProperties: {
        task: {
          id: 1, roomId: 2, subject: "", description: ""

        }
      },
    };
  });

  it("mounts", () => {
    cy.mount(TaskItemComponent, config);
  });
});
