import { describe, it, expect } from 'vitest';
import { Player } from "src/app/schemas/Player";
import { playersSorted } from "./player-list.component";

describe("PlayerListComponent", () => {
  const players: Array<Player> = [
    { name: "D", type: "inspector", cardValue: undefined },
    { name: "A", type: "estimator", cardValue: undefined },
    { name: "B", type: "estimator", cardValue: undefined },
    { name: "C", type: "estimator", cardValue: undefined },
    { name: "E", type: "inspector", cardValue: undefined },
  ] as Player[];

  const players2: Array<Player> = [
    { name: "D", type: "inspector", cardValue: undefined },
    { name: "A", type: "estimator", cardValue: "xl" },
    { name: "B", type: "estimator", cardValue: "s" },
    { name: "C", type: "estimator", cardValue: undefined },
    { name: "E", type: "inspector", cardValue: undefined },
  ] as Player[];

  it("does not change the length of the list", () => {
    expect(playersSorted(players).length).to.be.equal(players.length);
    expect(playersSorted(players2).length).to.be.equal(players2.length);
  });

  it("reorders the player list as expected", () => {
    expect(
      playersSorted(players)
        .map((p) => p.name)
        .join(""),
    ).to.be.equal("ABCDE");
    expect(
      playersSorted(players2)
        .map((p) => p.name)
        .join(""),
    ).to.be.equal("CABDE");
  });
});
