import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Player, PlayerRef } from "src/app/schemas/Player";
import { isNullish } from "../../shared/util";
import { RoomService } from "../room.service";

@Component({
  selector: "app-player-list",
  templateUrl: "./player-list.component.html",
  styleUrls: ["./player-list.component.scss"],
})
export class PlayerListComponent {
  @Input() players: Array<Player> = [];
  @Input() player!: Player;
  @Input() revealCards: boolean = false;

  @Output("kick") onKick: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("sideline") onSideline: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();
  @Output("promote") onPromote: EventEmitter<PlayerRef> = new EventEmitter<PlayerRef>();

  constructor(
    private roomService: RoomService,
  ) {}

  get playersSorted() {
    return playersSorted(this.players ?? []);
  }

  canBePromoted(player: Player): boolean {
    return this.roomService.isPromotionPermitted(player);
  }
}

export function playersSorted(players: Array<Player>): Array<Player> {
  const ready = players.filter(
    (player) => player.type === "estimator" && !isNullish(player.cardValue),
  );
  const waiting = players.filter(
    (player) => player.type === "estimator" && isNullish(player.cardValue),
  );
  const spectating = players.filter((player) => player.type === "inspector");
  return [...waiting, ...ready, ...spectating];
}
