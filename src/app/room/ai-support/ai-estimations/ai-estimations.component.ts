import { Component, ViewChild } from '@angular/core';
import { FunnelEventService } from "src/app/funnel-event.service";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { AuthModalComponent } from 'src/app/auth/auth-modal/auth-modal.component';

@Component({
  selector: 'app-ai-estimations',
  templateUrl: './ai-estimations.component.html',
  styleUrl: './ai-estimations.component.scss'
})
export class AiEstimationsComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  constructor(
    private funnelEventService: FunnelEventService,
  ) {}

  generateEstimations(): void {
    this.funnelEventService.smokeTestEvent(Feature.AiEstimations, 'generate estimations clicked');

    this.authModal?.showPlans({
      triggeredByFeature: Feature.AiEstimations
    });
  }
}
