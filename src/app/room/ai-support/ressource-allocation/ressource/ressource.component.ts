import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-ressource',
  templateUrl: './ressource.component.html',
  styleUrl: './ressource.component.scss'
})
export class RessourceComponent {
  @Input({ required: true }) name!: string;
  @Input({ required: true }) percentage!: number;
  @Input({ required: true }) color!: "green" | "blue" | "pink" | "lime" | "orange";
}
