import { Component, ViewChild } from '@angular/core';
import { FunnelEventService } from "src/app/funnel-event.service";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { AuthModalComponent } from 'src/app/auth/auth-modal/auth-modal.component';

@Component({
  selector: 'app-ressource-allocation',
  templateUrl: './ressource-allocation.component.html',
  styleUrl: './ressource-allocation.component.scss'
})
export class RessourceAllocationComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  constructor(
    private funnelEventService: FunnelEventService,
  ) {}

  improve(): void {
    this.funnelEventService.smokeTestEvent(Feature.RessourceAllocation, 'improve button clicked');
    this.authModal?.showPlans({
      triggeredByFeature: Feature.RessourceAllocation,
    });
  }
}
