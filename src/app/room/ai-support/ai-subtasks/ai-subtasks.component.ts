import { Component, ViewChild } from '@angular/core';
import { FunnelEventService } from "src/app/funnel-event.service";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { AuthModalComponent } from 'src/app/auth/auth-modal/auth-modal.component';

@Component({
  selector: 'app-ai-subtasks',
  templateUrl: './ai-subtasks.component.html',
  styleUrl: './ai-subtasks.component.scss'
})
export class AiSubtasksComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  constructor(
    private funnelEventService: FunnelEventService,
  ) {}

  generateSubtasks(): void {
    this.funnelEventService.smokeTestEvent(Feature.AiSubtasks, 'generate concept idea clicked');

    this.authModal?.showPlans({
      triggeredByFeature: Feature.AiSubtasks,
    });
  }
}
