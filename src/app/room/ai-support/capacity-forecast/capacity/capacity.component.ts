import { Component, Input } from '@angular/core';
import { util } from "zod";
import isInteger = util.isInteger;

@Component({
  selector: 'app-capacity',
  templateUrl: './capacity.component.html',
  styleUrl: './capacity.component.scss'
})
export class CapacityComponent {
  @Input() name: string = "";
  @Input() percentage: number = 0;

  get percentageString(): string {
    if (!isInteger(this.percentage)) {
      console.log('Invalid percentage: not an integer');
      return '0%';
    }

    if (this.percentage < 0 || this.percentage > 100) {
      console.log('Invalid percentage: not between 0 and 100');
      return '0%';
    }

    return this.percentage.toString() + '%';
  }
}
