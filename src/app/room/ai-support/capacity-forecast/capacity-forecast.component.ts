import { Component, Input, ViewChild } from '@angular/core';
import { Player } from "src/app/schemas/Player";
import { FunnelEventService } from "src/app/funnel-event.service";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { AuthModalComponent } from 'src/app/auth/auth-modal/auth-modal.component';

@Component({
  selector: 'app-capacity-forecast',
  templateUrl: './capacity-forecast.component.html',
  styleUrl: './capacity-forecast.component.scss'
})
export class CapacityForecastComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  @Input() players: Player[] = [];

  constructor(
    private funnelEventService: FunnelEventService,
  ) {}

  improve(): void {
    this.funnelEventService.smokeTestEvent(Feature.CapacityForecast, 'improve button clicked');

    this.authModal?.showPlans({
      triggeredByFeature: Feature.CapacityForecast,
    });
  }
}
