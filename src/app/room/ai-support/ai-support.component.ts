import { Component, Input } from "@angular/core";
import { Player } from "../../schemas/Player";

@Component({
  selector: "app-ai-support",
  templateUrl: "./ai-support.component.html",
  styleUrls: ["./ai-support.component.scss"],
})
export class AiSupportComponent {
  @Input({ required: true }) players!: Player[];

  protected expanded: boolean = true;

  protected toggleExpansion(): void {
    this.expanded = !this.expanded;
  }
}
