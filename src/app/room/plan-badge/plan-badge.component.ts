import { Component, ViewChild } from "@angular/core";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { FunnelEventService } from "src/app/funnel-event.service";

@Component({
  selector: "app-plan-badge",
  templateUrl: "./plan-badge.component.html",
  styleUrl: "./plan-badge.component.scss",
})
export class PlanBadgeComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  constructor(
    private funnelEventService: FunnelEventService,
  ) {}

  openPlansModal() {
    this.funnelEventService.smokeTestEvent(Feature.FreeBadge, 'free badge in headerbar clicked');
    this.authModal?.showPlans({
      triggeredByFeature: Feature.FreeBadge,
    });
  }
}
