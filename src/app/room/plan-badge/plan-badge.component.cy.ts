import { MountConfig } from "cypress/angular";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../assets/i18n/en.json";
import { PlanBadgeComponent } from "./plan-badge.component";
import { KehrwasserFeaturesModule } from "@kehrwasser-dev/features-angular";
import { ActivatedRoute } from "@angular/router";

describe("PlanBadgeComponent", () => {
  const config: MountConfig<PlanBadgeComponent> = {
    providers: [
      {
        provide: ActivatedRoute,
        useValue: {},
      },

    ],
    imports: [TranslateTestingModule.withTranslations("en", en_translation), KehrwasserFeaturesModule],
  };

  it("mounts", () => {
    cy.mount(PlanBadgeComponent, config);
  });
});

