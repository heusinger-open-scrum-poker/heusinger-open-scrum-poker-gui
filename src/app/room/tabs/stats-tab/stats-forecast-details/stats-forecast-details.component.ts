import { Component } from '@angular/core';
import { UserService } from "../../../../user.service";

@Component({
  selector: 'app-stats-forecast-details',
  templateUrl: './stats-forecast-details.component.html',
  styleUrl: './stats-forecast-details.component.scss'
})
export class StatsForecastDetailsComponent {
  get disabled(): boolean {
    return !this.userService.isPro();
  }

  constructor(
    private userService: UserService,
  ) {}
}
