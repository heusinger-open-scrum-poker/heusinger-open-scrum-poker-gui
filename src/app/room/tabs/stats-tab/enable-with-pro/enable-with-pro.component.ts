import {Component} from '@angular/core';
import {UserService} from "../../../../user.service";

@Component({
  selector: 'app-enable-with-pro',
  templateUrl: './enable-with-pro.component.html',
  styleUrl: './enable-with-pro.component.scss'
})
export class EnableWithProComponent {
  get show() {
    return !this.userService.isPro();
  }

  constructor(private userService: UserService) {
  }
}
