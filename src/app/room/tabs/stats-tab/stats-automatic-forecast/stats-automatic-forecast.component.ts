import { Component } from '@angular/core';
import { UserService } from "../../../../user.service";

@Component({
  selector: 'app-stats-automatic-forecast',
  templateUrl: './stats-automatic-forecast.component.html',
  styleUrl: './stats-automatic-forecast.component.scss'
})
export class StatsAutomaticForecastComponent {
  get disabled(): boolean {
    return !this.userService.isPro();
  }

  constructor(
    private userService: UserService,
  ) {}
}
