import {Component, Input, ViewChild} from '@angular/core';
import {Game} from "../../../schemas/Game";
import {AuthModalComponent} from "../../../auth/auth-modal/auth-modal.component";
import {FunnelEventService} from "../../../funnel-event.service";
import {Feature} from "../../../auth/auth-modal/plans-page/feature";
import {UserService} from "../../../user.service";
import {AlertModalComponent} from "../../../modal/alert-modal/alert-modal.component";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-stats-tab',
  templateUrl: './stats-tab.component.html',
  styleUrl: './stats-tab.component.scss'
})
export class StatsTabComponent {
  @Input() game?: Game;

  @ViewChild('authModal') authModal?: AuthModalComponent;
  @ViewChild('alertModal') alertModal?: AlertModalComponent;

  constructor(
    private funnelEventService: FunnelEventService,
    private userService: UserService,
    private translate: TranslateService,
  ) {}

  showPlans(feature: Feature, action: string): void {
    if (this.userService.isPro()) {
      this.alertModal?.showMessage(
        this.translate.instant("Notification.FeatureNotExistent"),
      );
    } else {
      this.funnelEventService.smokeTestEvent(feature, action);
      this.authModal?.showPlans({
        triggeredByFeature: feature,
      });
    }
  }

  protected readonly Feature = Feature;
}
