import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrl: './timeline.component.scss'
})
export class TimelineComponent {
  @Input() duration: number = 15;
  @Input() startDate: Date = new Date();
  @Input() disabled: boolean = true;

  get endDate(): Date {
    const endDate = new Date();
    endDate.setDate(this.startDate.getDate() + this.duration);

    return endDate;
  }
}
