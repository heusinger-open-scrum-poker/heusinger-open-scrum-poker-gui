import { Injectable } from "@angular/core";

export interface RessourceAllocationStats {
  utilization: number,
  planning: number,
  support: number,
  maintenance: number,
  meetings: number,
}

@Injectable({
  providedIn: "root",
})
export class RessourceAllocationService {
  constructor() {}

  public calculate(): RessourceAllocationStats {
    // When implemented, write test to check whether values always sum up to 100
    return {
      utilization: 22,
      planning: 15,
      support: 32,
      maintenance: 8,
      meetings: 23,
    };
  }
}
