import {Component} from '@angular/core';
import {RessourceAllocationStats, RessourceAllocationService} from "./ressource-allocation.service";
import {UserService} from "../../../../user.service";
import {Feature} from "../../../../auth/auth-modal/plans-page/feature";

@Component({
  selector: 'app-stats-ressource-allocation',
  templateUrl: './stats-ressource-allocation.component.html',
  styleUrl: './stats-ressource-allocation.component.scss'
})
export class StatsRessourceAllocationComponent {
  stats: RessourceAllocationStats = this.ressourceAllocationService.calculate();

  colors = {
    utilization: '#ADD7F6',
    planning: '#87BFFF',
    support: '#3F8EFC',
    maintenance: '#2667FF',
    meetings: '#3B28CC',
  }

  colorsDisabled = {
    utilization: '#6D707C',
    planning: '#595C66',
    support: '#4D5059',
    maintenance: '#41444C',
    meetings: '#33353B',
  }

  translationKeys = {
    utilization: 'Utilization',
    planning: 'Planning',
    support: 'Support',
    maintenance: 'Maintenance',
    meetings: 'Meetings',
  }

  fullHeight = 176;

  get keys(): (keyof RessourceAllocationStats)[] {
    return this.stats ? Object.keys(this.stats) as Array<keyof RessourceAllocationStats> : [];
  }

  get disabled(): boolean {
    return !this.userService.isPro();
  }

  constructor(
    private ressourceAllocationService: RessourceAllocationService,
    private userService: UserService,
  ) {}

  getHeight(key: string): string {
    const stat = this.stats[key as keyof RessourceAllocationStats];
    if (stat == undefined) {
      return '0px';
    }
    return (stat * this.fullHeight / 100) + 'px';
  }

  protected readonly Feature = Feature;
}
