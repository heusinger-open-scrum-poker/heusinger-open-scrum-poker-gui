import { MountConfig } from "cypress/angular";
import { GameTabComponent } from "./game-tab.component";
import { User } from "../../../schemas/User";
import { Player } from "../../../schemas/Player";
import { Room, RoomReferenceLike } from "../../../schemas/Room";
import { DEFAULT_OPTIONS } from "../../custom-cards-modal";
import { Game, TaskWithSubtasks } from "../../../schemas/Game";
import { RoomState } from "../../state";
import { RoomModule } from "../../room.module";
import { ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { AgileCasinoService } from "../../../agile-casino.service";
import { UserService } from "../../../user.service";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";
import { AuthModule } from "../../../auth/auth.module";
import { ModalModule } from "../../../modal/modal.module";
import { Task } from "src/app/schemas/Task";
import { AiSubtaskEstimateQuota } from "../../../schemas/AiSubtaskEstimate";
import { DragDropModule } from "src/third-party/cdk/drag-drop";

describe("GameComponent", () => {
  let user: User;
  let player: Player;
  let room: Room;
  let games: Game[];
  let tasks: Partial<TaskWithSubtasks>[];
  let players: Player[];
  let state: Partial<RoomState>;
  let config: MountConfig<GameTabComponent>;

  beforeEach(() => {
    window.localStorage.setItem("uuid", "mock-uuid");

    user = {
        uuid: "", hash: "",
        role: "anonymous"
    };
    player = {
      id: 1,
      avatar: "green",
      name: "Me",
      cardValue: "13",
      type: "estimator",
    };
    room = {
      id: 1,
      status: "estimating",
      roundsPlayed: 10,
      maxPlayers: 2,
      created_at: new Date().getTime(),
      series: JSON.stringify(DEFAULT_OPTIONS[0].value),
      activeGameId: 1,
      currentTaskId: 1,
    };
    games = [
      {
        id: 1,
        roomId: 1,
        name: "Game #1",
        created_at: new Date().getTime(),
      },
      {
        id: 2,
        roomId: 1,
        name: "Game #2",
        created_at: new Date().getTime(),
      },
      {
        id: 3,
        roomId: 1,
        name: "Game #3",
        created_at: new Date().getTime(),
      },
    ];
    tasks = [
      {
        id: 1,
        subject: "Task #1",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 2,
        subject: "Task #2",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 3,
        subject: "Task #3",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 4,
        subject: "Task #4",
        description: "Task description",
        roomId: 1,
        gameId: 3,
      },
      {
        id: 5,
        subject: "Task #5",
        description: "Task description",
        roomId: 1,
        gameId: 3,
      },
      {
        id: 6,
        subject: "Task #6",
        description: "Task description",
        roomId: 1,
        gameId: 3,
      }
    ].map((task) => ({ task, subtasks: [] }));
    players = [
      player,
      {
        id: 2,
        name: "A",
        cardValue: "@break",
        type: "estimator",
        avatar: "orange",
      },
      { id: 3, name: "B", cardValue: "1", type: "estimator", avatar: "darkred" },
    ];
    state = {
      getRoomId: () => 1,
      getRoom: () => room,
      getPlayers: () => players,
      getPlayerId: () => 1,
      getSelf: () => player,
      getUser: () => user,
      refreshJoinedRooms: async () => {},
      refresh: async () => {},
      games: games,
      activeGame: games[0],
      tasks: tasks,
      getTasks(): TaskWithSubtasks[] | undefined {
        return this.getTasksForGame(this.activeGame);
      },
      getTasksForGame(game: Game): TaskWithSubtasks[] {
        return this.tasks.filter(({task}) => task.gameId === game.id);
      },
      getCurrentTaskWithSubtasks(): TaskWithSubtasks | undefined {
        return this.tasks?.find(({task}) => task.id == this.getRoom().currentTaskId);
      },
      async activateGame(game: Game): Promise<void> {
        this.activeGame = game;
      }
    };

    config = {
      imports: [
        TranslateTestingModule.withTranslations("en", en_translation),
        RoomModule,
        AuthModule,
        ModalModule,
        DragDropModule,
      ],
      declarations: [
        GameTabComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              routeConfig: {
                path: `/room/${room.id}`,
              },
              paramMap: {
                get() {
                  return room.id.toString();
                },
              },
            },
            queryParams: of({}),
          },
        },
        {
          provide: AgileCasinoService,
          useValue: {
            getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
              return of({
                freeAiEstimations: 5,
              });
            },
            setCurrentTask(_roomId: RoomReferenceLike, taskId: Task["id"]): Observable<void> {
              room.currentTaskId = taskId;
              return of();
            }
          },
        },
        {
          provide: UserService,
          useValue: {
            isLoggedIn: () => false,
            userFeatureEnabled: () => true,
            getUserProfile: () => undefined,
            getUser: () => ({ uuid: "mock-uuid" }),
            isPro: () => false,
          }
        },
      ],
      componentProperties: {
        roomState: state as RoomState,
      },
    };
  });

  it("mounts", () => {
    cy.mount(GameTabComponent, config);
  });

  it("has edit tasks button", () => {
    cy.mount(GameTabComponent, config);
    cy.contains("Tasks");
  });

  it("has next tasks button", () => {
    cy.mount(GameTabComponent, config);
    cy.contains("Next");
  });

  it("shows current task", () => {
    cy.mount(GameTabComponent, config);
    cy.contains("Estimating Task #1");
  });

  it("can select next task", () => {
    cy.mount(GameTabComponent, config);
    cy.contains("Estimating Task #1");
    cy.contains("Next").click();
    cy.contains("Estimating Task #2");
  });

  it("next task is always within active game", () => {
    cy.mount(GameTabComponent, config);
    cy.contains("Estimating Task #1");
    cy.contains("Next").click();
    cy.contains("Estimating Task #2");
    cy.contains("Next").click();
    cy.contains("Estimating Task #3");
    cy.contains("Next").click();
    cy.contains("Estimating Task #1");
  });
});
