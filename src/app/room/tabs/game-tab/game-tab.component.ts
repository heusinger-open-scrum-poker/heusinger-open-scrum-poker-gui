import { Component, Input, ViewChild } from '@angular/core';
import { TasksService } from "../../tasks/tasks.service";
import { RoomState } from "../../state";
import { UserService } from "../../../user.service";
import { environment } from "../../../../environments/environment";
import { TaskWithSubtasks } from 'src/app/schemas/Game';
import { Room } from 'src/app/schemas/Room';
import { EditTaskModalComponent } from '../../task-modal/edit-task-modal/edit-task-modal.component';
import { CreateTaskModalComponent } from '../../task-modal/create-task-modal/create-task-modal.component';
import { getConsensus } from '../../statistics';

@Component({
  selector: 'app-game-tab',
  templateUrl: './game-tab.component.html',
  styleUrls: ['./game-tab.component.scss', './game-tab.component.css'],
})
export class GameTabComponent {
  readonly GAME_PROGRESS_ENABLED = true;

  @ViewChild("editTaskModal") editTaskModal?: EditTaskModalComponent;
  @ViewChild("createTaskModal") createTaskModal?: CreateTaskModalComponent;

  @Input({required: true}) roomState!: RoomState;
  @Input() navigateToTasks: () => void = () => {};
  @Input() navigateToStats: () => void = () => {};
  @Input() newRound: () => void = () => {};

  savingConsensus: boolean = false;

  get statisticsFeatureEnabled(): boolean {
    return environment.statisticsFeature;
  }

  get room(): Room | undefined {
    return this.roomState?.getRoom();
  }

  get task(): TaskWithSubtasks | undefined {
    return this.roomState?.getCurrentTaskWithSubtasks();
  }

  get subject(): string {
    return this.task?.task?.subject ?? "";
  }

  get estimate(): undefined | string {
    return getConsensus(this.roomState.stats)
  }

  get description(): string {
    return this.task?.task?.description ?? "";
  }

  get isLoggedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  get numTasks(): number {
    const activeGame = this.roomState?.activeGame;
    if (!activeGame) {
      return 0;
    }
    const tasks = this.roomState?.getTasksForGame(activeGame) ?? [];
    return tasks.length;
  }

  get progress(): number {
    if (this.numTasks == 0) {
      return 0;
    }

    return this.numEstimatedTasks / this.numTasks * 100
  }

  get numEstimatedTasks(): number {
    const activeGame = this.roomState?.activeGame;
    if (!activeGame) {
      return 0;
    }
    const tasks = this.roomState?.getTasksForGame(activeGame) ?? [];
    return tasks
      .filter(({task}) => !!task.estimate)
      .length
  }

  constructor(
    private tasksService: TasksService,
    private userService: UserService,
  ) {
  }

  hasNext(): boolean {
    return this.tasksService.getNext(this.roomState) !== undefined;
  }

  next(): void {
    this.tasksService.selectNext(this.roomState);
    this.newRound();
  }

  editTask(): void {
    this.editTaskModal?.showModal();
  }

  saveConsensus(): void {
    this.savingConsensus = true;
    this.tasksService.saveConsensusToCurrentTask(this.roomState)
    .finally(() => this.savingConsensus = false);
    this.next();
  }
}
