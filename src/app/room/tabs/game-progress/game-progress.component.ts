import { Component, Input } from "@angular/core";

@Component({
  selector: "app-game-progress",
  templateUrl: "./game-progress.component.html",
  styleUrls: ["./game-progress.component.scss"],
})
export class GameProgressComponent {
    /** a number from 0 - 100 */
    @Input() progress: number = 0;
    @Input() disabled: boolean = false;

    get fillPath() {
      const fillFraction = this.disabled ? 1.0 : this.progress / 100
      const LOWER_BOUND = 3;
      const UPPER_BOUND = 78.5;
      const a = LOWER_BOUND + fillFraction * (UPPER_BOUND - LOWER_BOUND)
      const b = a + 2.5;
      return `M 5.5 3.5 L 3 11.5 H ${a} L ${b} 3.5 H 5.5 Z`
    }

    get fillColor() {
      if (this.disabled) {
        return "#D9D9D9";
      }

      return "#CC00FF";
    }

    get strokeColor() {
      if (this.disabled) {
        return "#787878";
      }

      return "#CC00FF";
    }
}
