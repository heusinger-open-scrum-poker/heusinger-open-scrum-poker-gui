import { MountConfig } from "cypress/angular";
import { GameComponent } from "./game.component";
import { User } from "../../../../schemas/User";
import { Player } from "../../../../schemas/Player";
import { Room } from "../../../../schemas/Room";
import { DEFAULT_OPTIONS } from "../../../custom-cards-modal";
import { Game, TaskWithSubtasks } from "../../../../schemas/Game";
import { RoomState } from "../../../state";
import { RoomModule } from "../../../room.module";
import { ActivatedRoute } from "@angular/router";
import {Observable, of} from "rxjs";
import { AgileCasinoService } from "../../../../agile-casino.service";
import { UserService } from "../../../../user.service";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../../assets/i18n/en.json";
import { AuthModule } from "../../../../auth/auth.module";
import { ModalModule } from "../../../../modal/modal.module";
import { Task } from "src/app/schemas/Task";
import {AiSubtaskEstimateQuota} from "../../../../schemas/AiSubtaskEstimate";
import { DragDropModule } from "src/third-party/cdk/drag-drop";

describe("GameElementComponent", () => {
  let user: User;
  let player: Player;
  let room: Room;
  let game: Game;
  let tasks: TaskWithSubtasks[];
  let players: Player[];
  let state: Partial<RoomState>;
  let expanded: boolean;
  let config: MountConfig<GameComponent>;

  beforeEach(() => {
    window.localStorage.setItem("uuid", "mock-uuid");

    user = {
        uuid: "", hash: "",
        role: "anonymous"
    };
    player = {
      id: 1,
      avatar: "green",
      name: "Me",
      cardValue: "13",
      type: "estimator",
    };
    room = {
      id: 1,
      status: "estimating",
      roundsPlayed: 10,
      maxPlayers: 2,
      created_at: new Date().getTime(),
      series: JSON.stringify(DEFAULT_OPTIONS[0].value),
      activeGameId: 1,
    };
    game = {
      id: 1,
      roomId: 1,
      name: "Game #1",
      created_at: new Date().getTime(),
    };
    tasks = [
      {
        id: 1,
        subject: "Task #1",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 2,
        subject: "Task #2",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 3,
        subject: "Task #3",
        description: "Task description",
        roomId: 1,
        gameId: 2,
      }
    ].map((task: Task) => ({ task, subtasks: [] }));
    players = [
      player,
      {
        id: 2,
        name: "A",
        cardValue: "@break",
        type: "estimator",
        avatar: "orange",
      },
      { id: 3, name: "B", cardValue: "1", type: "estimator", avatar: "darkred" },
    ];
    state = {
      room: room,
      games: [game],
      tasks: tasks,
      getRoomId: () => 1,
      getRoom(): Room | undefined { return this.room },
      getPlayers: () => players,
      getPlayerId: () => 1,
      getSelf: () => player,
      getUser: () => user,
      refreshJoinedRooms: async () => {},
      refresh: async () => {},
      getTasksForGame(game: Game): TaskWithSubtasks[] {
        return (this.tasks ?? []).filter(({task}) => {
          return task.gameId === game?.id;
        })
      },
      getCurrentTaskWithSubtasks(): TaskWithSubtasks | undefined {
        return (this.tasks ?? [])[0];
      },
      getCurrentTask(): Task | undefined {
        return (this.tasks ?? [])[0].task;
      },
      get activeGame(): Game | undefined { return this.games?.find((game) => game.id === this.room?.activeGameId); },
    };
    expanded = true;

    config = {
      imports: [
        TranslateTestingModule.withTranslations("en", en_translation),
        RoomModule,
        AuthModule,
        ModalModule,
        DragDropModule,
      ],
      declarations: [
        GameComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              routeConfig: {
                path: `/room/${room.id}`,
              },
              paramMap: {
                get() {
                  return room.id.toString();
                },
              },
            },
            queryParams: of({}),
          },
        },
        {
          provide: AgileCasinoService,
          useValue: {
            getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
              return of({
                freeAiEstimations: 5,
              });
            },
          },
        },
        {
          provide: UserService,
          useValue: {
            isLoggedIn: () => false,
            userFeatureEnabled: () => true,
            getUserProfile: () => undefined,
            getUser: () => ({ uuid: "mock-uuid" }),
            isPro: () => false,
          }
        },
      ],
      componentProperties: {
        game: game,
        roomState: state as RoomState,
        expanded: expanded,
      },
    };
  });

  it("mounts", () => {
    cy.mount(GameComponent, config);
  });

  it("shows the game title", () => {
    cy.mount(GameComponent, config);
    cy.contains("Game #1");
  });

  it("shows info when no tasks defined", () => {
    state.tasks = [];
    cy.mount(GameComponent, config);
    cy.contains("There are no tasks defined yet. Please add tasks to vote on.");
  });

  it("shows tasks if tasks are defined", () => {
    cy.mount(GameComponent, config);
    cy.contains("Task #1");
    cy.contains("Task #2");
  });

  it("shows only tasks belonging to game", () => {
    cy.mount(GameComponent, config);
    cy.contains("Task #3").should("not.exist");
  });

  it("shows correct buttons for active game", () => {
    cy.mount(GameComponent, config);
    cy.get(".import-button");
    cy.get(".new-task-button");
    cy.contains("New task");
  });

  it("shows correct buttons for inactive game", () => {
    cy.mount(GameComponent, config);

    const activeGameId = 2;

    state.games = [{
      id: activeGameId,
      name: "Game #2",
      roomId: 1,
      created_at: new Date().getTime(),
    }]

    if (state.room) {
      state.room.activeGameId = activeGameId;
    }

    cy.contains("Activate");
  });

  it("only shows tasks if expanded is true", () => {
    if (config.componentProperties) {
      config.componentProperties.expanded = false;
    }
    cy.mount(GameComponent, config);
    cy.get("app-task-item").should("not.exist");
    cy.contains("There are no tasks defined yet. Please add tasks to vote on.").should("not.exist");
  });
});
