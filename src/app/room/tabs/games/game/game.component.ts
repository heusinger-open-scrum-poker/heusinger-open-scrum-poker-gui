import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Game, TaskWithSubtasks } from "../../../../schemas/Game";
import { ActionButton } from "../../../index";
import { UserService } from "../../../../user.service";
import { EditTaskModalComponent } from "../../../task-modal/edit-task-modal/edit-task-modal.component";
import { CreateTaskModalComponent } from "../../../task-modal/create-task-modal/create-task-modal.component";
import { TasksService } from "../../../tasks/tasks.service";
import { RoomState } from "../../../state";
import { environment } from 'src/environments/environment';
import { Settings } from 'src/app/shared/Settings';
import { CdkDragDrop } from 'src/third-party/cdk/drag-drop';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrl: './game.component.scss',
})
export class GameComponent {
  @ViewChild("editTaskModal") editTaskModal?: EditTaskModalComponent;
  @ViewChild("createTaskModal") createTaskModal?: CreateTaskModalComponent;

  @Input({ required: true}) game!: Game;
  @Input({ required: true}) roomState!: RoomState;
  @Input() expanded: boolean = false;
  @Input() isLoading: boolean = false;

  @Output("select") selectEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("connectExternalSource") connectExternalSourceEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("createTask") createTaskEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("editTask") editTaskEmitter: EventEmitter<TaskWithSubtasks> = new EventEmitter<TaskWithSubtasks>();
  @Output("exportTasks") exportTasksEmitter: EventEmitter<Game> = new EventEmitter<Game>();

  activeGameActionsWithExport = [
    {
      icon: 'download-2-line',
      label: '',
      class: 'import-button',
      tooltip: 'Import',
      click: () => this.connectExternalSourceEmitter.emit(),
    },
    {
      icon: 'upload-2-line',
      label: '',
      class: 'export-button',
      tooltip: 'Export',
      click: () => this.exportTasksEmitter.emit(),
    },
    {
      icon: "task-add-line",
      label: "New.Task",
      class: 'new-task-button',
      click: () => this.createTaskEmitter.emit(),
    },
  ];

  activeGameActionsWithoutExport = [
    {
      icon: 'import-line',
      label: 'Import',
      class: 'import-button',
      click: () => this.connectExternalSourceEmitter.emit(),
    },
    {
      icon: "task-add-line",
      label: "New.Task",
      class: 'new-task-button',
      click: () => this.createTaskEmitter.emit(),
    },
  ]

  get activeGameActions(): ActionButton[] {
    if (environment.taskExportFeature) {
      return this.activeGameActionsWithExport;
    } else {
      return this.activeGameActionsWithoutExport;
    }
  }

  defaultActions: ActionButton[] = [
    {
      icon: 'game',
      label: 'Activate',
      click: () => this.activate(),
      busy: () => this.isLoading,
    }
  ];

  get isSignedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  get currentTaskId(): number | undefined {
    return this.roomState.getCurrentTask()?.id;
  }

  get active(): boolean {
    if (!this.roomState.room) {
      return false;
    }
    return this.roomState.room.activeGameId === this.game.id;
  }

  get tasks(): TaskWithSubtasks[] {
    return this.roomState.getTasksForGame(this.game);
  }

  constructor(
    private userService: UserService,
    private tasksService: TasksService,
  ) {
  }

  activate(): void {
    this.selectEmitter.emit()
  }

  editTask(task: TaskWithSubtasks) {
    this.editTaskEmitter.emit(task);
  }

  drop(event: CdkDragDrop<string[]>) {
    this.tasksService.drop(this.tasks.map(({task}) => task), event);
  }

  get dragStartDelay() {
    return Settings.DRAG_START_DELAY;
  }

  get dragEnabled() {
    return this.isSignedIn;
  }
}
