import { MountConfig } from "cypress/angular";
import { GamesComponent } from "./games.component";
import { User } from "../../../schemas/User";
import { Player } from "../../../schemas/Player";
import { Room } from "../../../schemas/Room";
import { DEFAULT_OPTIONS } from "../../custom-cards-modal";
import { Game, TaskWithSubtasks } from "../../../schemas/Game";
import { RoomState } from "../../state";
import { RoomModule } from "../../room.module";
import { ActivatedRoute } from "@angular/router";
import {Observable, of} from "rxjs";
import { AgileCasinoService } from "../../../agile-casino.service";
import { UserService } from "../../../user.service";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";
import { AuthModule } from "../../../auth/auth.module";
import { ModalModule } from "../../../modal/modal.module";
import { Task } from "src/app/schemas/Task";
import {AiSubtaskEstimateQuota} from "../../../schemas/AiSubtaskEstimate";
import { DragDropModule } from "src/third-party/cdk/drag-drop";

describe("GamesComponent", () => {
  let user: User;
  let player: Player;
  let room: Room;
  let games: Game[];
  let tasks: Partial<TaskWithSubtasks>[];
  let players: Player[];
  let state: Partial<RoomState>;
  let selectedGameId: number;
  let config: MountConfig<GamesComponent>;

  beforeEach(() => {
    window.localStorage.setItem("uuid", "mock-uuid");

    user = {
        uuid: "", hash: "",
        role: "anonymous"
    };
    player = {
      id: 1,
      avatar: "green",
      name: "Me",
      cardValue: "13",
      type: "estimator",
    };
    room = {
      id: 1,
      status: "estimating",
      roundsPlayed: 10,
      maxPlayers: 2,
      created_at: new Date().getTime(),
      series: JSON.stringify(DEFAULT_OPTIONS[0].value),
      activeGameId: 1,
    };
    games = [
      {
        id: 1,
        roomId: 1,
        name: "Game #1",
        created_at: new Date().getTime(),
      },
      {
        id: 2,
        roomId: 1,
        name: "Game #2",
        created_at: new Date().getTime(),
      },
      {
        id: 3,
        roomId: 1,
        name: "Game #3",
        created_at: new Date().getTime(),
      },
    ];
    tasks = [
      {
        id: 1,
        subject: "Task #1",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 2,
        subject: "Task #2",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 3,
        subject: "Task #3",
        description: "Task description",
        roomId: 1,
        gameId: 1,
      },
      {
        id: 4,
        subject: "Task #4",
        description: "Task description",
        roomId: 1,
        gameId: 3,
      },
      {
        id: 5,
        subject: "Task #5",
        description: "Task description",
        roomId: 1,
        gameId: 3,
      },
      {
        id: 6,
        subject: "Task #6",
        description: "Task description",
        roomId: 1,
        gameId: 3,
      }
    ].map((task) => ({ task, subtasks: [] }));
    players = [
      player,
      {
        id: 2,
        name: "A",
        cardValue: "@break",
        type: "estimator",
        avatar: "orange",
      },
      { id: 3, name: "B", cardValue: "1", type: "estimator", avatar: "darkred" },
    ];
    state = {
      getRoomId: () => 1,
      getRoom: () => room,
      getPlayers: () => players,
      getPlayerId: () => 1,
      getSelf: () => player,
      getUser: () => user,
      refreshJoinedRooms: async () => {},
      refresh: async () => {},
      games: games,
      activeGame: games[0],
      tasks: tasks,
      room: room,
      getTasksForGame(game: Game): TaskWithSubtasks[] {
        return this.tasks.filter(({task}) => task.gameId === game.id);
      },
      getCurrentTask(): Task | undefined {
        return this.tasks[0];
      },
      async activateGame(game: Game): Promise<void> {
        this.activeGame = game;
        this.room.activeGameId = game.id;
      }
    };
    selectedGameId = games[0].id;

    config = {
      imports: [
        TranslateTestingModule.withTranslations("en", en_translation),
        RoomModule,
        AuthModule,
        ModalModule,
        DragDropModule,
      ],
      declarations: [
        GamesComponent,
      ],
      providers: [
        {
          useValue: {
            createAlertModal() {},
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              routeConfig: {
                path: `/room/${room.id}`,
              },
              paramMap: {
                get() {
                  return room.id.toString();
                },
              },
            },
            queryParams: of({}),
          },
        },
        {
          provide: AgileCasinoService,
          useValue: {
            getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
              return of({
                freeAiEstimations: 5,
              });
            },
          },
        },
        {
          provide: UserService,
          useValue: {
            isLoggedIn: () => false,
            userFeatureEnabled: () => true,
            getUserProfile: () => undefined,
            getUser: () => ({ uuid: "mock-uuid" }),
            isPro: () => false,
          }
        },
      ],
      componentProperties: {
        roomState: state as RoomState,
        selectedGameId: selectedGameId,
        connectExternalSource(): void {
          games[0].name = "Imported Tasks";
        }
      },
    };
  });

  it("mounts", () => {
    cy.mount(GamesComponent, config);
  });

  it("shows list of games", () => {
    cy.mount(GamesComponent, config);
    cy.contains("Game #1");
    cy.contains("Game #2");
    cy.contains("Game #3");
  });

  it("shows only active game initially", () => {
    cy.mount(GamesComponent, config);

    cy.contains("Game #1").parent().get(".import-button");
    cy.contains("Game #2").parent().parent().contains("Activate");
    cy.contains("Game #3").parent().parent().contains("Activate");
    cy.contains("Task #1");
    cy.contains("Task #2");
    cy.contains("Task #3");
    cy.contains("There are no tasks defined yet. Please add tasks to vote on.").should("not.exist");
    cy.contains("Task #4").should("not.exist");
    cy.contains("Task #5").should("not.exist");
    cy.contains("Task #6").should("not.exist");
  });

  it("can expand games", () => {
    cy.mount(GamesComponent, config);

    cy.contains("Game #2").parent().click();

    cy.contains("Task #1").should("not.exist");
    cy.contains("There are no tasks defined yet. Please add tasks to vote on.");
    cy.contains("Task #4").should("not.exist");

    cy.contains("Game #3").parent().click();

    cy.contains("Task #1").should("not.exist");
    cy.contains("There are no tasks defined yet. Please add tasks to vote on.").should("not.exist");
    cy.contains("Task #4");
  });

  it("shows correct buttons for each game", () => {
    cy.mount(GamesComponent, config);

    cy.contains("Game #1").parent().get(".import-button");
    cy.contains("Game #1").parent().parent().contains("New task");
    cy.contains("Game #2").parent().parent().contains("Activate");
    cy.contains("Game #3").parent().parent().contains("Activate");
  });

  it("can import tasks for active game", () => {
    cy.mount(GamesComponent, config);

    cy.contains("Game #1").parent().get(".import-button").click();
    cy.contains("Imported Tasks");
  });

  it("can activate game and buttons get updated", () => {
    cy.mount(GamesComponent, config);
    cy.contains("Game #2").parent().parent().contains("Activate").click();

    cy.contains("Game #1").parent().parent().contains("Activate");
    cy.contains("Game #2").parent().get(".import-button");
    cy.contains("Game #2").parent().parent().contains("New task");
    cy.contains("Game #3").parent().parent().contains("Activate");
  });
});
