import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { EditTaskModalComponent } from "../../task-modal/edit-task-modal/edit-task-modal.component";
import { RoomState } from "../../state";
import { UserService } from "../../../user.service";
import { Game, TaskWithSubtasks } from "../../../schemas/Game";
import { CreateTaskModalComponent } from "../../task-modal/create-task-modal/create-task-modal.component";
import LocalStorage from 'src/app/shared/LocalStorage';
import { AuthModalComponent } from 'src/app/auth/auth-modal/auth-modal.component';
import { Feature } from 'src/app/auth/auth-modal/plans-page/feature';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrl: './games.component.scss'
})
export class GamesComponent {
  @ViewChild("editTaskModal") editTaskModal?: EditTaskModalComponent;
  @ViewChild("createTaskModal") createTaskModal?: CreateTaskModalComponent;
  @ViewChild("authModal") authModal?: AuthModalComponent;

  @Input() roomState!: RoomState;
  @Input() selectedGameId?: number;
  @Input() connectExternalSource!: () => void;
  @Input() exportTasks!: () => void;

  @Output("import") onImport: EventEmitter<void> = new EventEmitter<void>(); // TODO!!!

  selectedTask?: TaskWithSubtasks;
  isLoading: boolean = false

  get games(): Game[] {
    return this.roomState.games;
  }

  get isSignedIn() {
    return this.userService.isLoggedIn();
  }

  get roomId(): number | undefined {
    return this.roomState.getRoom()?.id;
  }

  constructor(
    private localStorage: LocalStorage,
    private userService: UserService,
  ) {
  }

  isSelected(game: Game): boolean {
    return this.selectedGameId === game.id;
  }

  select(game: Game): void {
    this.selectedGameId = game.id;

    this.isLoading = true;
    this.roomState.activateGame(game)
    .then(() => {
      this.isLoading = false;
    })
    .catch((err) => {
      this.isLoading = false;
      console.error(err);
    })
  }

  editTask(task: TaskWithSubtasks) {
    this.selectedTask = JSON.parse(JSON.stringify(task));

    this.editTaskModal?.showModal();
  }

  createTask(): void {
    this.createTaskModal?.showModal()
  }

  identifyGame(_index: number, game: Game) {
    return game.id;
  }

  get openInlineAlert(): boolean {
    return !this.localStorage.getHideInsecureWarning();
  }

  closeInlineAlert() {
    this.localStorage.setHideInsecureWarning(true);
  }

  setPassword() {
    this.authModal?.showPlans({
      triggeredByFeature: Feature.InsecureWarning,
    })
  }
}
