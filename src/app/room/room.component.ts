import { Component, EventEmitter, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AgileCasinoService } from "../agile-casino.service";
import { environment } from "../../environments/environment";
import { interval, Observable, of } from "rxjs";
import { NotificationsService } from "../notifications.service";
import LocalStorage from "../shared/LocalStorage";
import { ConfirmationBoxComponent } from "../modal/confirmation-box/confirmation-box.component";
import { ConfirmationResult } from "../modal/confirmation-box";
import { map, catchError } from "rxjs/operators";
import { BREAK_CARD_VALUE } from "./index";
import { StompManager } from "./stomp-manager";
import {
  kickInfoSchema,
  leaveInfoSchema,
  promotionInfoSchema,
  revealInfoSchema,
  roomChangeSchema,
  sideliningInfoSchema,
  gameActivatedInfoSchema,
  gamesUpdatedInfoSchema,
} from "./stomp-schemas";
import { RoomState } from "./state";
import { TabBarState, TabSpec } from "../kwmc/tab-bar";
import { FunnelEventService } from "../funnel-event.service";
import { TranslateService } from "@ngx-translate/core";
import { Player, PlayerRef } from "../schemas/Player";
import { Room, RoomReference } from "../schemas/Room";
import { Settings } from "../shared/Settings";
import { Task } from "../schemas/Task";
import { TabChangeEvent } from "../kwmc/tab-bar/tab-bar.component";
import { ExtendedContextMenuEvent, handleContextMenu } from "../shared/util";
import { UserService } from "../user.service";
import { AlertModalComponent } from "../modal/alert-modal/alert-modal.component";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { TableView, getTableView } from "./TableView";
import { TabHandler } from "./sidebar/tab.handler";
import { AuthModalComponent } from "../auth/auth-modal/auth-modal.component";
import { CreateTaskModalComponent } from "./task-modal/create-task-modal/create-task-modal.component";
import { ImportTasksModalComponent } from "./tasks/import-tasks-modal/import-tasks-modal.component";
import { TasksService } from "./tasks/tasks.service";
import { ExportTasksModalComponent } from "./tasks/export-tasks-modal/export-tasks-modal.component";
import { RoomService } from "./room.service";
import { getConsensus } from "./statistics";

@Component({
  selector: "app-room",
  templateUrl: "./room.component.html",
  styleUrls: ["./room.component.scss"],
})
export class RoomComponent implements OnInit {
  @ViewChild("alertModal") alertModal!: AlertModalComponent;
  @ViewChild("confirmationBox") confirmationBox!: ConfirmationBoxComponent;
  @ViewChild("confirmationBoxLeave") confirmationBoxLeave!: ConfirmationBoxComponent;
  @ViewChild("confirmationBoxSideline") confirmationBoxSideline!: ConfirmationBoxComponent;
  @ViewChild("confirmationBoxPromote") confirmationBoxPromote!: ConfirmationBoxComponent;
  @ViewChild("authModal") authModal!: AuthModalComponent;
  @ViewChild("createTaskModal") createTaskModal!: CreateTaskModalComponent;
  @ViewChild("importTasksModal") importTasksModal!: ImportTasksModalComponent;
  @ViewChild("exportTasksModal") exportTasksModal!: ExportTasksModalComponent;

  tabHandler: TabHandler = new TabHandler(
    [
      {
        key: "game",
        label: "Game",
        icon: "game",
      },
      {
        key: "players",
        label: "Players",
        icon: "group-list",
      },
      {
        key: "ai",
        label: "AI",
        icon: "robot",
      },
      {
        key: "tasks",
        parent: "game",
        headerActions: [
          {
            icon: "import-line",
            label: "Import.Noun",
            click: () => this.connectExternalSource()
          },
          {
            icon: "task-add-line",
            label: "Create",
            click: () => this.createTaskModal?.showModal(),
          },
        ]
      },
      {
        key: "games",
        parent: "game",
        headerActions: [
          {
            icon: "gamepad-line-plus",
            label: "New.Game",
            busy: () => this.isNewGameLoading,
            click: () => {
              this.isNewGameLoading = true;
              this.state?.newGame()
                .catch((e) => console.error(e))
                .then(() => this.isNewGameLoading = false)
            }
          },
        ]
      },
      {
        key: "stats",
        parent: "game",
        headerActions: [],
      },
    ],
    'game');

  get navigateToTasks(): () => void {
    const key = this.gameFeatureEnabled ? 'games' : 'tasks';
    return () => this.tabHandler.navigate(key);
  }

  get navigateToStats(): () => void {
    return () => this.tabHandler.navigate('stats');
  }

  state?: RoomState;
  tabBarState: TabBarState = { selectedIndex: 0 };
  changeAd: EventEmitter<void> = new EventEmitter<void>();

  stompManager?: StompManager;

  isPanelOpen = false;
  isConnecting = false;
  isRevealing = false;
  isNewGameLoading = false;

  cardValueTemp: string | null = null;

  togglingPlayerType = false;

  hot = 0;
  cooldownShownForRoundsPlayed = 0;

  tabs: Array<TabSpec> = [
    {
      label: "Players",
      icon: "group-line",
    },
    {
      label: "Tasks",
      icon: "list-check-3",
    },
  ];

  constructor(
    private storage: LocalStorage,
    private route: ActivatedRoute,
    private agileCasinoService: AgileCasinoService,
    private router: Router,
    private notificationsService: NotificationsService,
    private funnelEventService: FunnelEventService,
    private translate: TranslateService,
    private userService: UserService,
    private tasksService: TasksService,
    private roomService: RoomService,
  ) {
    if (this.aiFeatureEnabled) {
      this.tabs.push(    {
        label: "AI",
        icon: "robot",
      });
    }
  }

  get player() {
    const player = this.state?.getSelf();
    return player;
  }

  get room() {
    return this.state?.getRoom();
  }

  get roomRef() {
    return this.state?.roomRef;
  }

  get players() {
    return this.state?.getPlayers() ?? [];
  }

  get taskFeatureEnabled() {
    return environment.taskFeature;
  }

  get aiFeatureEnabled() {
    return environment.aiFeature;
  }

  get newSidebarEnabled(): boolean {
    return environment.newSidebar;
  }

  get roomStatus() {
    return this.hot > 0 ? 'estimating' : this.room?.status ?? 'estimating';
  }

  get gameFeatureEnabled(): boolean {
    return environment.gameFeature;
  }


  confirm<T>(args: {
    confirmationBox: ConfirmationBoxComponent,
    action: () => Observable<T>,
    next: (value: T) => void,
    error?: (error: unknown) => void,
  }) {
    const { confirmationBox } = args;

    confirmationBox.showModal();
    const handle1 = confirmationBox.result.subscribe(
      (result: ConfirmationResult) => {
        handle1.unsubscribe();
        if (result === "confirm") {
          confirmationBox.inProgress = true;
          args.action().subscribe({
            next: (result) => {
              confirmationBox.inProgress = false;
              confirmationBox.close();
              args.next(result)
            },
            error: (e: unknown) => {
              confirmationBox.inProgress = false;
              confirmationBox.close();
              args.error?.(e);
            },
          });
        }
        if (result === "cancel") {
          confirmationBox.close();
        }
      }
    )
  }

  confirmKick(player: PlayerRef): void {
    const currentPlayer = this.player;

    if (!currentPlayer) {
      return;
    }

    this.confirm({
      confirmationBox: this.confirmationBox,
      action: () => {
      if (player.id === currentPlayer.id) {
          this.leaveRoom();
          return of(true);
        } else {
          return this.agileCasinoService.deletePlayer(player.id).pipe(
            map(() => {
              this.state?.refreshPlayers();
              return true;
            }),
            catchError(() => {
              this.state?.refresh();
              return of(false);
            }),
          );
        }
      },
      next: (success: boolean) => {
        if (!success) {
          this.notificationsService.notify({
            type: "error",
            title: this.translate.instant("Notification.Error"),
            message: this.translate.instant("Notification.FailKick"),
          });
        }
      },
    })
  }

  confirmSideline(player: PlayerRef): void {
    const currentPlayer = this.player;

    if (!currentPlayer) {
      return;
    }

    this.confirm({
      confirmationBox: this.confirmationBoxSideline,
      action: () => {
        if (player.id === currentPlayer.id) {
          this.leaveRoom();
          return of(true);
        } else {
          return this.agileCasinoService.sidelinePlayer(player.id).pipe(
            map(() => {
              this.state?.refreshPlayers();
              return true;
            }),
            catchError(() => {
              this.state?.refresh();
              return of(false);
            }),
          );
        }
      },
      next: (success: boolean) => {
        if (!success) {
          this.notificationsService.notify({
            type: "error",
            title: this.translate.instant("Notification.Error"),
            message: this.translate.instant("Notification.FailSideline"),
          });
        } else {
          this.roomService.registerTemporaryPromotionPermission(player);
        }
      },
    })
  }

  confirmPromote(player: PlayerRef): void {
    const currentPlayer = this.player;

    if (!currentPlayer) {
      return;
    }

    this.confirm({
      confirmationBox: this.confirmationBoxPromote,
      action: () => {
        if (player.id === currentPlayer.id) {
          this.leaveRoom();
          return of(true);
        } else {
          return this.agileCasinoService.promotePlayer(player.id).pipe(
            map(() => {
              this.state?.refreshPlayers();
              return true;
            }),
            catchError(() => {
              this.state?.refresh();
              return of(false);
            }),
          );
        }
      },
      next: (success: boolean) => {
        if (!success) {
          this.notificationsService.notify({
            type: "error",
            title: this.translate.instant("Notification.Error"),
            message: this.translate.instant("Notification.FailPromote"),
          });
        }
      },
    })
  }

  subscribeToStomp(): void {
    const roomRef = this.roomRef;

    if (!roomRef) {
      throw new Error("subscribeToStomp: roomRef not initialized");
    }

    this.stompManager?.subscribe({
      destination: "/room/" + roomRef.toString() + "/join",
      onMessage: () => {
        this.state?.refreshPlayers();
      }
    })

    this.stompManager?.subscribeWithValidation({
      messageSchema: revealInfoSchema,
      destination: "/room/" + roomRef.toString() + "/reveal",
      onMessage: (body) => {
        this.state?.refreshRoom()
        .catch()
        .then(() => {
          this.isRevealing = false;
          if (
            !this.isCooldownAlreadyShown() &&
            this.room?.status === "revealed"
          ) {
            this.startCoolDown();
          }
          this.notificationsService.notify({
            type: "info",
            title: this.translate.instant("Notification.RoundFinished"),
            message:
              `${body.player?.name}` + this.translate.instant("Notification.Revealed"),
          });
        });
      },
    });

    this.stompManager?.subscribe({
      destination: "/room/" + roomRef.toString() + "/reset",
      onMessage: () => {
        this.state?.refresh().then(() => {
          this.isConnecting = false;
          this.cardValueTemp = null;
        });

        this.changeAd.emit();
      },
    });

    this.stompManager?.subscribeWithValidation({
      messageSchema: leaveInfoSchema,
      destination: "/room/" + roomRef.toString() + "/left",
      onMessage: (body) => {
        this.notificationsService.notify({
          type: "info",
          title: "",
          message:
            `${body.player?.name}` +
            this.translate.instant("Notification.Left"),
        });
      },
      onError: () => {
        this.notificationsService.notify({
          type: "info",
          title: "",
          message: this.translate.instant("Notification.SomeoneLeft"),
        });
      },
      finally: () => {
        this.state?.refreshPlayers()
      },
    })

    this.stompManager?.subscribeWithValidation({
      messageSchema: kickInfoSchema,
      destination: "/room/" + roomRef.toString() + "/kicked",
      onMessage: (body) => {
        this.notificationsService.notify({
          type: "info",
          title: this.translate.instant("Notification.Removed"),
          message:
            `${body.player?.name}` +
            this.translate.instant("Notification.Kicked") +
            `${body.kickingPlayer?.name}`,
        });
      },
      onError: () => {
        this.notificationsService.notify({
          message: this.translate.instant("Notification.SomeoneKicked"),
          title: "",
        });
      },
      finally: () => {
        this.state?.refreshPlayers();
      },
    })

    this.stompManager?.subscribeWithValidation({
      messageSchema: sideliningInfoSchema,
      destination: "/room/" + roomRef.toString() + "/sidelined",
      onMessage: (body) => {
        this.notificationsService.notify({
          type: "info",
          title: "",
          message:
            `${body.player?.name}` +
            this.translate.instant("Notification.Sidelined") +
            `${body.sideliningPlayer?.name}`,
        });
      },
      onError: () => {
        this.notificationsService.notify({
          message: this.translate.instant("Notification.SomeoneSidelined"),
          title: "",
        });
      },
      finally: () => {
        this.state?.refreshPlayers()
      },
    })

    this.stompManager?.subscribeWithValidation({
      messageSchema: promotionInfoSchema,
      destination:  "/room/" + roomRef.toString() + "/promoted",
      onMessage: (body) => {
        this.notificationsService.notify({
          type: "info",
          title: "",
          message: this.translate.instant("Notification.Promoted", {
            promoted: body.player?.name,
            promoter: body.initiatingPlayer?.name,
          }),
        });
      },
      onError: () => {
        this.notificationsService.notify({
          message: this.translate.instant("Notification.SomeonePromoted"),
          title: "",
        });
      },
      finally: () => {
        this.state?.refreshPlayers();
      },
    });

    this.stompManager?.subscribeWithValidation({
      messageSchema: roomChangeSchema,
      destination: "/room/" + roomRef.toString() + "/changed",
      onMessage: (change) => {
        switch (change["type"]) {
          case "SERIES_CHANGE":
            if (change.player?.name) {
              this.notificationsService.notify({
                type: "info",
                title: this.translate.instant(
                  "Notification.CardsChanged",
                ),
                message:
                  `${change.player.name}` +
                  this.translate.instant("Notification.UpdatedCards"),
              });
            } else {
              this.notificationsService.notify({
                type: "info",
                title: this.translate.instant(
                  "Notification.CardsChanged",
                ),
                message: this.translate.instant(
                  "Notification.SomeoneUpdatedCards",
                ),
              });
            }
            this.state?.refreshRoom();
            break;
          case "NAME_CHANGE":
            this.state?.refreshPlayer(change.player.id);

          break;
          case "PLAYER_CHANGE":
            this.state?.refreshPlayer(change.player.id);

          break;
          case "LOCKED":
            this.notificationsService.notify({
              type: "info",
              title: this.translate.instant(
                "Notification.CardsChanged",
              ),
              message:
                `${change.player.name} ` +
                this.translate.instant("Notification.LockedRoom"),
            });

            this.state?.refreshRoom().then(() => {
              this.router.navigate(["room", roomRef.toString()])
              this.resubscribeToStomp();
            });
            break;
          case "UNLOCKED":
            this.notificationsService.notify({
              type: "info",
              title: this.translate.instant(
                "Notification.CardsChanged",
              ),
              message:
                `${change.player.name} ` +
                this.translate.instant("Notification.UnlockedRoom"),
            });

            this.state?.refreshRoom().then(() => {
              this.router.navigate(["room", roomRef.toString()])
              this.resubscribeToStomp();
            });
            break;
          default:
            this.state?.refresh();
            break;
        }
      },
    });

    this.stompManager?.subscribe({
      destination: "/room/" + roomRef.toString() + "/taskCreated",
      onMessage: () => {
        this.state?.refreshTasks();
      },
    });

    this.stompManager?.subscribe({
      destination: "/room/" + roomRef.toString() + "/taskDeleted",
      onMessage: () => {
          this.state?.refreshTasks();
      },
    });

    this.stompManager?.subscribe({
      destination: "/room/" + roomRef.toString() + "/taskUpdated",
      onMessage: () => {
        this.state?.refreshTasks();
      },
    });

    this.stompManager?.subscribeWithValidation({
      messageSchema: Task,
      destination: "/room/" + roomRef.toString() + "/currentTaskChanged",
      onMessage: (body) => {
        this.notificationsService.notify({ type: "info", message: `Estimating Task "${body.subject}"` });
      },
      finally: () => {
        this.state?.refreshRoom();
      },
    });

    this.stompManager?.subscribeWithValidation({
        messageSchema: gameActivatedInfoSchema,
        destination: "/room/" + roomRef.toString() + "/gameActivated",
        onMessage: (body) => {
          let needToRefreshGames = false;

          const gameIsKnown = this.state?.games.find((game) => game.id === body.game.id);
          needToRefreshGames = !gameIsKnown;
          this.notificationsService.notify({
            type: "info",
            title: this.translate.instant("Notification.GameActivated"),
            message: this.translate.instant("Notification.GameActivated.Message", { name: body.game.name }),
          });
          return needToRefreshGames;
        },
        finally: (needToRefreshGames?: boolean) => {
          if (needToRefreshGames) {
            this.state?.refreshGames().then(() => console.debug({games: this.state?.games}));
          }
          this.state?.refreshRoom();
        },
      });

    this.stompManager?.subscribeWithValidation({
      messageSchema: gamesUpdatedInfoSchema,
      destination: "/room/" + roomRef.toString() + "/gamesUpdated",
      finally: () => {
          this.state?.refreshGames();
          this.state?.refreshTasks();
          this.state?.refreshRoom();
      },
    });
  }

  unsubscribeFromStomp(): void {
    this.stompManager?.unsubscribeAll();
  }

  resubscribeToStomp(): void {
    this.unsubscribeFromStomp();
    this.subscribeToStomp();
  }

  connectToWebsocketWithStomp(): void {
    this.stompManager = new StompManager({
      brokerURL: `${environment.brokerBaseUrl}/ac-broker`,
      debug: function (str) {
        console.info("STOMP-LOG", str);
      },
      reconnectDelay: 1000,
      heartbeatIncoming: 1000,
      heartbeatOutgoing: 1000,
      onConnect: (frame) => {
        console.debug("Connected:", frame);
        this.state?.refresh();
        this.subscribeToStomp();
      },
      onStompError: (frame) => {
        console.error("Stomp Error: ", frame);
        console.debug("Broker reported error: " + frame.headers["message"]);
        console.debug("Additional details: " + frame.body);
      },
      onDisconnect: (frame) => {
        console.error("Stomp disconnected: ", frame);
        this.unsubscribeFromStomp();
      },
      onWebSocketError: (event) => {
        console.error("WebSocket Error: ", event);
      },
      onWebSocketClose: (event) => {
        console.error("WebSocket Error: ", event);
      },
    });
  }

  disconnectFromWebsocketWithStomp(): void {
    this.stompManager?.disconnect();
  }

  async ngOnInit() {
    try {
      const roomRef = this.getRoomRefByRoute();

      if (!roomRef.toString()) {
        this.redirectToCreatePage();
        return;
      }

      this.state = new RoomState(
        roomRef,
        this.agileCasinoService,
        this.roomService,
        this.storage,
      );

      await this.state.refreshJoinedRooms();

      const playerId = this.getPlayerIdByRoute();

      if (!playerId) {
        this.redirectToJoinPage(roomRef);
        return;
      }

      await this.state.refresh();

      this.afterRoomStateInit();

      this.useCanonicalRoomUrl();

      this.connectToWebsocketWithStomp();
    } catch (error: any) {
      console.error(error);
      if (!("status" in error)) {
        return;
      }
      const statusCode = error.status;
      let errorMessage = "";

      if (statusCode == 404) {
        errorMessage =
          "Sorry! The room " +
          this.state?.getRoomId() +
          " does not exist. Feel free to create one or join a different one.";
      } else {
        errorMessage =
          "Sorry! An error occured loading room " +
          this.state?.getRoomId() +
          ".";
      }

      this.alertModal.showMessage(errorMessage, () => {
        this.router.navigate(["create"]);
      });
    }
  }

  afterRoomStateInit() {
    this.route.queryParams.subscribe(
      (queryParams) => {
        if (queryParams["action"] === "importTasks") {
          this.navigateToTasks();
          this.connectExternalSource();
        }

        if (queryParams["action"] === "exportTasks") {
          this.navigateToTasks();
          this.exportTasks();
        }

        this.removeQueryParam("action");
      }
    )
  }

  removeQueryParam(paramName: string) {
    this.route.queryParams.subscribe(queryParams => {
      const newQueryParams = { ...queryParams };
      delete newQueryParams[paramName];
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: newQueryParams,
        replaceUrl: true,
      });
    });
  }

  useCanonicalRoomUrl(): void {
    const roomRef = this.roomRef;
    if (!roomRef) {
      throw new Error("uninitialized roomRef");
    }
    if (this.getRoomRefByRoute().toString() !== roomRef.toString()) {
      this.route.queryParams.subscribe(
        (queryParams) => this.router.navigate(["room", roomRef.toString()], { queryParams })
      );
    }
  }

  getRoomRefByRoute(): RoomReference {
    const id = this.route.snapshot.paramMap.get("id");
    if (!id) {
      throw new Error("invalid route");
    }
    const roomRef = new RoomReference(id);
    return roomRef;
  }

  getPlayerIdByRoute(): number | undefined {
    const roomRef = this.getRoomRefByRoute();

    const playerId: number | undefined = this.state?.joinedRooms.find(
      ({ room }) => roomRef.matches(room.id) || roomRef.matches(room.accessToken)
    )?.player.id;

    if (!playerId) {
      console.error("no player id has been stored for the current room");
    }

    return playerId;
  }

  redirectToCreatePage() {
    this.disconnectFromWebsocketWithStomp();
    this.router.navigate(["create"]);
  }

  redirectToJoinPage(roomRef: RoomReference) {
    this.disconnectFromWebsocketWithStomp();
    this.route.queryParams.subscribe((queryParams) =>
      this.router.navigate(["join", roomRef.toString()], { queryParams }),
    );
  }

  get inviteLink() {
    let inviteLink = window.location.origin + this.router.url;
    const user = this.userService.getUser();

    if (user) {
      inviteLink += "?ref=" + user.hash;
    }

    return inviteLink;
  }

  startCoolDown() {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    this.hot = Settings.INITIAL_HOT;
    this.cooldownShownForRoundsPlayed = this.room.roundsPlayed;

    const subscription = interval(Settings.COUNTDOWN_TICK_DURATION).subscribe((value) => {
      this.hot = Settings.INITIAL_HOT - 1 - value;
      if (!this.hot) {
        subscription.unsubscribe();
        const consensus = getConsensus(this.state?.stats) ?? "";
        if (!this.roomRef) {
          return;
        }
        this.agileCasinoService.endRound(this.roomRef, consensus).subscribe({
          next: () => {},
          error: (error) => {
            console.log('Failed to end round with error:', error);
          },
        });

        if (this.gameFeatureEnabled && this.state && this.tasksService.isFinalConsensus(this.state)) {
          this.tasksService.saveConsensusToCurrentTask(this.state!)
          .then(() => this.tasksService.selectNext(this.state!))
          .then(() => this.newRound());
        }
      }
    });
  }

  isCooldownAlreadyShown(): boolean {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    return this.cooldownShownForRoundsPlayed === this.room.roundsPlayed;
  }

  isCardsRevealed(): boolean {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    return this.room.status === "revealed";
  }

  findPlayer(players: Player[], id: number) {
    let result: Player | null = null;
    players.forEach((player) => {
      if (player.id === id) result = player;
    });
    return result;
  }

  isRoomFilled() {
    const numEstimators = this.players?.filter(
      (p) => p?.type === "estimator",
    ).length ?? 0;
    return numEstimators >= Settings.MAX_ESTIMATORS;
  }

  togglePanel(): void {
    this.isPanelOpen = !this.isPanelOpen;
  }

  leaveRoom(): void {
    const handleLeave = () => {
      console.group("== LEAVING ROOM ==");

      this.disconnectFromWebsocketWithStomp();

      console.debug("disconnected from messaging broker");
      console.groupEnd();

      this.router.navigate([""]);
    };

    this.confirm({
      confirmationBox: this.confirmationBoxLeave,
      action: () => {
        if (!this.player) {
          this.router.navigate([""]);
          return of(undefined);
        }
        return this.agileCasinoService.deletePlayer(this.player.id);
      },
      next: () => {
        handleLeave();
      },
      error: () => {
        handleLeave();
      }
    })
  }

  togglePlayerType(): void {
    if (!this.player) {
      throw new Error("player uninitialized");
    }

    this.togglingPlayerType = true;

    const player: Player = {
      ...this.player,
      type:
        this.player.type === "inspector"
          ? ("estimator" as const)
          : ("inspector" as const),
    };

    this.state?.putPlayer(player)
      .then(() => {
        this.togglingPlayerType = false;
      })
      .catch((error) => {
        this.togglingPlayerType = false;
        console.error("unable to update player");

        if (error.status === 423) {
          this.notificationsService.notify({
            type: "error",
            title: this.translate.instant("Notification.Error"),
            message: this.translate.instant("Notification.Filled"),
          });
        } else {
          this.notificationsService.notify({
            type: "error",
            title: this.translate.instant("Notification.Error"),
            message: this.translate.instant("Notification.Toggle"),
          });
        }
      });
  }

  /* at least on player placed a card already */
  atLeastOneCardSubmitted(): boolean {
    if (!this.players) {
      throw new Error("players uninitialized");
    }

    const values = this.players.map((player) => player.cardValue);
    const placedValues = values.filter(
      (value) => value && value !== BREAK_CARD_VALUE,
    );
    return placedValues.length > 0;
  }

  isBreakMode(): boolean {
    return this.player?.cardValue === BREAK_CARD_VALUE;
  }

  isBreakCardSelected(): boolean {
    return this.cardValueTemp === BREAK_CARD_VALUE;
  }

  public onCardClicked(cardValue: string): void {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    if (cardValue === BREAK_CARD_VALUE || this.room.status !== "estimating")
      return;

    if (environment.directCardSelection) {
      if (this.isBreakMode() && cardValue !== BREAK_CARD_VALUE) {
        this.endBreak();
      }

      this.placeCard(cardValue);
    } else {
      this.privateChoice(cardValue);
    }
  }

  onBreakCardClicked() {
    if (environment.directCardSelection) {
      if (!this.isBreakMode()) {
        this.takeBreak();
      }
    } else {
      this.privateChoice(BREAK_CARD_VALUE);
    }
  }

  public privateChoice(cardValue: string): void {
    if (this.cardValueTemp === cardValue) {
      this.cardValueTemp = null;
    } else {
      this.cardValueTemp = cardValue;
    }
    console.log("private Wahl der Karte erfolgt");
  }

  public takeCardBack(): void {
    this.cardValueTemp = null;
    this.genericPlaceCard("");
  }

  public placeCard(cardValue: string): void {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    if (this.room.status !== "estimating") {
      return;
    }
    this.genericPlaceCard(cardValue);
  }

  public takeBreak(): void {
    this.genericPlaceCard(BREAK_CARD_VALUE);
  }

  public endBreak() {
    if (this.cardValueTemp === BREAK_CARD_VALUE) {
      this.cardValueTemp = null;
    }
    this.genericPlaceCard("");
  }

  private genericPlaceCard(cardValue: string): void {
    if (!this.player) {
      throw new Error("player uninitialized");
    }

    const playerUpdate = { ...this.player, cardValue: cardValue };

    this.isConnecting = true;
    this.state?.setPlayerOptimistically(playerUpdate);

    this.state?.putPlayer(playerUpdate)
      .then(() => {
        this.isConnecting = false;
        this.notificationsService.notify({
          type: "ok",
          title: "",
          message: cardValue
            ? cardValue === BREAK_CARD_VALUE
              ? this.translate.instant("Notification.YouBreak")
              : `${cardValue}` +
                this.translate.instant("Notification.HasBeenPlaced")
            : this.translate.instant("Notification.CardTaken"),
        });
        console.debug(
          "Player successfully updated with card value",
          this.player?.cardValue,
        );
      })
      .catch((error) => {
        this.isConnecting = false;
        console.error("Player update failed", error);
        this.state?.revertOptimisticPlayers();
        this.notificationsService.notify({
          type: "error",
          title: this.translate.instant("Notification.Error"),
          message: this.translate.instant("Notification.FailPlayCard"),
        });
        this.state?.refresh();
      });
  }

  public revealCards(): void {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    if (this.room.status === "revealed") return;

    if (this.isRevealing) return;

    this.isRevealing = true;

    const room = this.room;

    const updatedRoom: Partial<Room> & Pick<Room, "id" | "accessToken"> = {
      id: room.id,
      accessToken: room.accessToken,
      roundsPlayed: room.roundsPlayed,
      status: "revealed",
      series: room.series,
    };

    this.state?.putRoom(updatedRoom)
      .then(() => {
        this.isRevealing = false;
        if (!this.isCooldownAlreadyShown() && this.room?.status === "revealed") {
          this.startCoolDown();
        }
      })
      .catch(() => {
        this.isRevealing = false;
        console.error("Reveal failed");
        this.notificationsService.notify({
          type: "error",
          title: this.translate.instant("Notification.Error"),
          message: this.translate.instant("Notification.FailReveal"),
        });
        this.state?.refresh();
      });
  }

  public newRound(): void {
    if (!this.room) {
      throw new Error("room uninitialized");
    }

    const roomRef = this.roomRef;

    if (!roomRef) {
      throw new Error("roomRef uninitialized");
    }

    if (this.room.status === "estimating") {
      return;
    }

    if (this.isConnecting) return;

    this.isConnecting = true;

    const room = this.room;

    const updatedRoom: Partial<Room> & Pick<Room, "id" | "accessToken"> = {
      id: room.id,
      accessToken: room.accessToken,
      roundsPlayed: room.roundsPlayed,
      status: "estimating",
      series: room.series,
    };

    this.state?.putRoom(updatedRoom)
      .then(() => {
        console.debug("Room update successfull", this.room);
        this.cardValueTemp = null;
        this.agileCasinoService.startRound(roomRef).subscribe({
          next: () => {},
          error: (error) => {
            console.log('Failed to start round with error:', error);
          },
        });
      })
      .catch((error) => {
        console.error("Room update failed", error);
        this.isConnecting = false;
        this.notificationsService.notify({
          type: "error",
          title: this.translate.instant("Notification.Error"),
          message: this.translate.instant("Notification.FailNewRound"),
        });
        this.state?.refreshRoom();
      });
  }

  triggerNewRound() {
    this.newRound();
  }

  importTasks(): void {
    this.funnelEventService.smokeTestEvent(Feature.ImportTasks, 'import tasks clicked');
    this.alertModal.showMessage(
      this.translate.instant("Notification.FeatureNotExistent"),
    );
  }

  onTabChange(event: TabChangeEvent): void {
    const tab = event.spec;

    if (tab.label === "Tasks" && (!this.taskFeatureEnabled || !this.userService.isLoggedIn())) {
      this.funnelEventService.smokeTestEvent(Feature.Tasks, 'tasks tab clicked');
    }
  }

  get gameTabSelected(): boolean {
    return this.tabBarState.selectedIndex == 0;
  }

  get taskTabSelected(): boolean {
    return this.tabBarState.selectedIndex == 1;
  }

  get aiTabSelected(): boolean {
    return this.tabBarState.selectedIndex == 2;
  }

  get tableView(): TableView {
    if (!this.room) {
      throw new Error("uninitialized room");
    }

    if (!this.player) {
      throw new Error("uninitialized player");
    }

    if (!this.players) {
      throw new Error("uninitialized player list");
    }

    if (!this.state) {
      throw new Error("uninitialized state");
    }

    return getTableView(this.room, this.player, this.players, this.state.stats, {forceConceal: this.hot > 0})
  }

  connectExternalSource() {
    if (!environment.jiraFeature) {
      this.funnelEventService.smokeTestEvent(Feature.ImportTasks, 'connect external source clicked');
      this.alertModal?.showMessage(
        this.translate.instant("Notification.FeatureNotExistent"),
      );
      return;
    }

    if (!this.userService.isLoggedIn()) {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.ImportTasks,
      });
      return;
    }

    this.importTasksModal.showModal();
  }

  exportTasks() {
    if (!environment.jiraFeature) {
      this.funnelEventService.smokeTestEvent(Feature.ExportTasks, 'export tasks clicked');
      this.alertModal?.showMessage(
        this.translate.instant("Notification.FeatureNotExistent"),
      );
      return;
    }

    if (!this.userService.isLoggedIn()) {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.ExportTasks,
      });
      return;
    }

    this.exportTasksModal.showModal();
  }

  triggerConnectExternalSource() {
    this.connectExternalSource();
  }

  handleContextMenu(event: ExtendedContextMenuEvent): boolean {
    return handleContextMenu(event);
  }

  triggerExportTasks() {
    this.exportTasks();
  }
}
