import { describe, test, expect } from 'vitest';
import { computeStats, getComparator } from './statistics';
import { Room } from '../schemas/Room';
import { Player } from '../schemas/Player';

describe("statistics", () => {
  test("ranking pure numbers", () => {
    const cards = "3 2 17 1 2 10".split(" ");
    cards.sort(getComparator(cards));
    expect(cards).toMatchSnapshot();
  })

  test("ranking non-numbers", () => {
    const cards = "s m l xl 2xl 3xl".split(" ");
    cards.sort(getComparator(cards));
    expect(cards).toMatchSnapshot();
  })

  test("ranking mixed", () => {
    const cards = "s m 1 10 2xl 17".split(" ");
    cards.sort(getComparator(cards));
    expect(cards).toMatchSnapshot();
  })

  test("example", () => {
    const room: Room = {
      id: 1,
      series: '["1", "2", "3", "5", "8", "13", "21", "34"]',
      status: "revealed",
      created_at: new Date().getTime(),
      maxPlayers: 2,
      roundsPlayed: 17,
      currentTaskId: undefined,
    };
    const players: Player[] = [
      {id: 1, name: "A", type: "estimator", avatar: undefined, cardValue: "1" },
      {id: 2, name: "B", type: "estimator", avatar: undefined, cardValue: "2" },
      {id: 3, name: "C", type: "estimator", avatar: undefined, cardValue: "3" },
      {id: 4, name: "D", type: "estimator", avatar: undefined, cardValue: "3" },
      {id: 5, name: "E", type: "inspector", avatar: undefined, cardValue: "34" },
      {id: 6, name: "F", type: "inspector", avatar: undefined, cardValue: "34" },
    ];
    const stats = computeStats(players, room);
    expect(stats).toMatchSnapshot();
  })
})
