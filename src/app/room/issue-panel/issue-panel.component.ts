import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { SettingsMenuComponent } from "../../kwmc/settings-menu/settings-menu.component";
import { AgileCasinoService } from "../../agile-casino.service";
import { RoomState } from "../../room/state";
import { environment } from "src/environments/environment";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";
import { Task } from "src/app/schemas/Task";
import { TranslateService } from "@ngx-translate/core";
import { getConsensus } from "../statistics";
import sanitizeHtml from "sanitize-html";
import { TasksService } from "../tasks/tasks.service";

@Component({
  selector: "app-issue-panel",
  templateUrl: "./issue-panel.component.html",
  styleUrls: ["./issue-panel.component.scss"],
})
export class IssuePanelComponent implements OnInit {
  @ViewChild(SettingsMenuComponent) overflowMenu?: SettingsMenuComponent;
  @ViewChild("overrideInput") overrideInput?: ElementRef;
  @Input() room!: Room;
  @Input() player!: Player;
  @Input() roomState!: RoomState;

  features: string[] = [];
  customEstimate?: string = undefined;

  seriesFeatureExists: boolean = false;
  customizePlayerFeatureExists: boolean = false;
  visitBlogLinkExists: boolean = true;
  reportIssueLinkExists: boolean = true;

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
    private tasksService: TasksService,
  ) {}

  get taskFeatureEnabled(): boolean {
    return environment.taskFeature;
  }

  get currentTask(): Task | undefined {
    return this.roomState.getCurrentTask();
  }

  get previousTaskLabel() {
    return this.translate.instant('Task.Previous');
  }

  get nextTaskLabel() {
    return this.translate.instant('Task.Next');
  }

  get confirmEstimateLabel() {
    return this.translate.instant('Task.ConfirmEstimate');
  }

  get resetEstimateLabel() {
    return this.translate.instant('Task.ResetEstimate');
  }

  get cancelOverrideLabel() {
    return this.translate.instant("Cancel");
  }

  get previousTask(): Task | undefined {
    return this.tasksService.getPrevious(this.roomState);
  }

  get nextTask(): Task | undefined {
    return this.tasksService.getNext(this.roomState);
  }

  get majority(): string | undefined {
    if (this.hideResults) {
      return;
    }

    return getConsensus(this.roomState.stats);
  }

  get locked() {
    return !!this.currentTask?.estimate;
  }

  get hideResults() {
    return this.roomState.hideResults;
  }

  get overrideActive() {
    return this.customEstimate !== undefined;
  }

  ngOnInit(): void {
    this.agileCasinoService.getFeatures().subscribe({
      next: (features) => {
        this.features = features;
      },
    });
  }

  selectNextTask() {
    this.tasksService.selectNext(this.roomState);
  }

  selectPreviousTask() {
    this.tasksService.selectPrevious(this.roomState);
  }

  confirmEstimate() {
    if (!this.currentTask) {
      throw new Error("currentTask not set");
    }

    this.agileCasinoService.updateTaskWithSubtasks(this.room.id, { ...this.currentTask, estimate: this.customEstimate ?? this.majority}, []).subscribe({
      next: () => {
        this.resetOverride();
      },
      error: (error) => {
        console.error(error);
        this.resetOverride();
      },
    });
  }

  resetEstimate() {
    if (!this.currentTask) {
      throw new Error("currentTask not set");
    }

    this.resetOverride();
    this.agileCasinoService.updateTaskWithSubtasks(this.room.id, { ...this.currentTask, estimate: ""}, []).subscribe({
      error: (error) => {
        console.error(error);
      }
    });
  }

  overrideEstimate() {
    this.customEstimate = this.majority ?? '';
  }

  resetOverride() {
    this.customEstimate = undefined;
  }

  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.confirmEstimate();
    } else if (event.key === 'Escape') {
      this.resetOverride();
    }
  }

  safe(html: string): string {
    return sanitizeHtml(html);
  }
}
