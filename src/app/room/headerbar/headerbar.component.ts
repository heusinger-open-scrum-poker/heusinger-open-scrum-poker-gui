import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { CustomizePlayerModalComponent } from "src/app/room/customize-player-modal/customize-player-modal.component";
import { CustomCardsModalComponent } from "src/app/room/custom-cards-modal/custom-cards-modal.component";
import { SettingsMenuComponent } from "../../kwmc/settings-menu/settings-menu.component";
import { AgileCasinoService } from "../../agile-casino.service";
import { RoomState } from "../../room/state";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";
import { FunnelEventService } from "src/app/funnel-event.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationBoxComponent } from "src/app/modal/confirmation-box/confirmation-box.component";
import { LogoutOptionsModalComponent, SignOutMode } from "../../auth/logout-options-modal/logout-options-modal.component";
import { UserService } from "../../user.service";
import { AlertModalComponent } from "src/app/modal/alert-modal/alert-modal.component";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Router } from "@angular/router";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";

@Component({
  selector: "app-headerbar",
  templateUrl: "./headerbar.component.html",
  styleUrls: ["./headerbar.component.scss"],
})
export class HeaderbarComponent implements OnChanges, OnInit {
  @ViewChild("alertModal") alertModal?: AlertModalComponent;
  @ViewChild(SettingsMenuComponent) overflowMenu!: SettingsMenuComponent;
  @ViewChild("lockConfirmationBox") lockConfirmationBox!: ConfirmationBoxComponent;
  @ViewChild("unlockConfirmationBox") unlockConfirmationBox!: ConfirmationBoxComponent;
  @ViewChild("returnToStartPageConfirmationBox") returnToStartPageConfirmationBox!: ConfirmationBoxComponent;
  @ViewChild("signoutModal") signoutModal?: LogoutOptionsModalComponent;
  @ViewChild("authModal") authModal?: AuthModalComponent;
  @ViewChild(CustomCardsModalComponent) customCardsModal?: CustomCardsModalComponent;
  @ViewChild(CustomizePlayerModalComponent) customizePlayerModal?: CustomizePlayerModalComponent;

  @Input({ required: true }) room!: Room;
  @Input({ required: true }) player!: Player;
  @Input({ required: true }) roomState!: RoomState;
  @Output("logout") logoutEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output("importTasks") importTasksEmitter: EventEmitter<void> = new EventEmitter<void>();

  features: string[] = [];
  signOutMode: SignOutMode = "default";

  protected seriesFeatureExists: boolean = false;
  protected customizePlayerFeatureExists: boolean = false;
  protected visitBlogLinkExists: boolean = true;
  protected reportIssueLinkExists: boolean = true;

  constructor(
    private agileCasinoService: AgileCasinoService,
    private funnelEventService: FunnelEventService,
    private translate: TranslateService,
    private userService: UserService,
    private router: Router,
    private featureService: KwFeatureService,
  ) {}

  get userFeatureEnabled(): boolean {
    return this.userService.userFeatureEnabled();
  }

  get roomIsLocked(): boolean {
    return !!this.room.accessToken;
  }

  get players(): Array<Player> {
    return this.roomState.players;
  }

  showLogoutOptions(options: {leaveRoom: boolean}): void {
    if (options.leaveRoom) {
      this.signOutMode = "leave";
      this.signoutModal?.showModal()
    } else {
      this.signOutMode = "stay";
      this.signoutModal?.showModal()
    }
  }

  get roomIdentifier(): string {
    return this.room.accessToken ?? this.room.id.toString()
  }

  get experiment203(): boolean {
    return this.featureService.isFeatureEnabled("experiment203") ?? false;
  }

  get experiment205(): boolean {
    return false;
  }

  ngOnInit(): void {
    this.agileCasinoService.getFeatures().subscribe({
      next: (features) => {
        this.features = features;
      },
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.seriesFeatureExists && changes["room"] && this.room?.id) {
      this.agileCasinoService.getSeries(this.room.id).subscribe(() => {
        this.seriesFeatureExists = true;
      });
    }

    if (
      !this.customizePlayerFeatureExists &&
      changes["room"] &&
      this.room?.id
    ) {
      this.agileCasinoService.getAvatars(this.room.id).subscribe(() => {
        this.customizePlayerFeatureExists = true;
      });
    }
  }

  openEditSeriesModal(): void {
    this.overflowMenu.close();

    if (this.seriesFeatureExists) {
      this.customCardsModal?.showModal();
    }
  }

  protected openCustomizePlayerModal(): void {
    this.overflowMenu.close();

    this.customizePlayerModal?.showModal();
  }

  showPlansFor(feature: Feature) {
    this.authModal?.showPlans({
      triggeredByFeature: feature,
    });
    this.overflowMenu?.close();
  }

  importTasks() {
    this.importTasksEmitter.emit();
    this.overflowMenu?.close();
  }

  showPlansForModerateRoom() {
    this.showPlansFor(Feature.ModerateRoom);
  }

  showPlansForLockRoom() {
    this.showPlansFor(Feature.LockRoom);
  }

  showPlansForReadableLink() {
    this.showPlansFor(Feature.ReadableLink);
  }

  showPlansForRemoveAds() {
    this.showPlansFor(Feature.RemoveAds);
  }

  openLockRoomModal(): void {
    const roomRef = this.roomState.roomRef;
    if (!roomRef) {
      throw new Error("roomRef undefined");
    }

    this.overflowMenu.close();
    this.funnelEventService.smokeTestEvent(Feature.LockRoom, 'lock room button clicked');

    if (this.userService.isPro()) {
      if (this.roomIsLocked) {
        this.unlockConfirmationBox.showModal();
        const subscription = this.unlockConfirmationBox.result.subscribe((result) => {
          subscription.unsubscribe();
          this.unlockConfirmationBox.inProgress = true;
          if (result === "confirm") {
            this.agileCasinoService.unlockRoom(roomRef).subscribe({
              next: () => {
                this.unlockConfirmationBox.inProgress = false;
                this.unlockConfirmationBox.close();
              },
              error: (e) => {
                console.error("error while unlocking room", e);
                this.unlockConfirmationBox.inProgress = false;
                this.unlockConfirmationBox.close();
                this.alertModal?.showMessage(this.translate.instant("Room.CouldNotUnlock"));
              }
            })
          } else {
            this.unlockConfirmationBox.inProgress = false;
            this.unlockConfirmationBox.close();
          }
        })
      } else {
        this.lockConfirmationBox.showModal();
        const subscription = this.lockConfirmationBox.result.subscribe((result) => {
          subscription.unsubscribe();
          this.lockConfirmationBox.inProgress = true;
          if (result === "confirm") {
            this.agileCasinoService.lockRoom(roomRef).subscribe({
              next: () => {
                this.lockConfirmationBox.inProgress = false;
                this.lockConfirmationBox.close();
              },
              error: (e) => {
                console.error("error while locking room", e);
                this.lockConfirmationBox.inProgress = false;
                this.lockConfirmationBox.close();
                this.alertModal?.showMessage(this.translate.instant("Room.CouldNotLock"));
              }
            })
          } else {
            this.lockConfirmationBox.inProgress = false;
            this.lockConfirmationBox.close();
          }
        })
      }
    } else {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.LockRoom,
      });
    }
  }

  confirmReturnToStartpage() {
    this.returnToStartPageConfirmationBox.showModal();
    const subscription = this.returnToStartPageConfirmationBox.result.subscribe((result) => {
      subscription.unsubscribe()

      if (result === "cancel") {
        this.returnToStartPageConfirmationBox.close();
        return;
      }

      if (result === "confirm") {
        this.router.navigate(["join", this.roomIdentifier])
      }
    })
  }

  onLogout(): void {
    this.logoutEvent.emit();
  }
}
