import { Component, Input, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { catchError, map, of } from "rxjs";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ModalActions, ModalButtonProps } from "src/app/modal/types";
import { NotificationsService } from "src/app/notifications.service";
import { Room } from "src/app/schemas/Room";
import { createEmptyTask } from "../task-modal";
import { AuthModalComponent } from "../../../auth/auth-modal/auth-modal.component";
import { UserService } from "../../../user.service";
import { Feature } from "../../../auth/auth-modal/plans-page/feature";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";
import { TaskWithSubtasks } from "src/app/schemas/Game";

@Component({
  selector: "app-create-task-modal",
  templateUrl: "./create-task-modal.component.html",
  styleUrls: ["./create-task-modal.component.scss"],
})
export class CreateTaskModalComponent {
  @ViewChild("modal") modal?: ModalDialogComponent;
  @ViewChild("authModal") authModal?: AuthModalComponent;

  @Input() roomId!: Room["id"];

  task: TaskWithSubtasks = createEmptyTask(this.roomId);

  updateTask(task: TaskWithSubtasks): void {
    this.task = task;
  }

  get primaryButtonProps(): ModalButtonProps | undefined {
    return {
      label: this.translate.instant("CreateTask"),
      buttonFlavor: "primary",
      onClick: () => {
        return this.agileCasinoService.createTaskWithSubtasks(this.roomId, this.task.task, this.task.subtasks).pipe(
          map(() => {
            this.resetTask();
            return ModalActions.close;
          }),
            catchError((error) => {
            console.error(error);
            this.notificationsService.notify({
              message: "Failed to create the task.",
            });
            return of(ModalActions.ignore);
          }),
        );
      },
    };
  }

  get tertiaryButtonProps(): ModalButtonProps | undefined {
    return {
      label: this.translate.instant("Cancel"),
      buttonFlavor: "secondary",
      onClick: () => {
        this.resetTask();
        return of(ModalActions.close)
      },
    };
  }

  constructor(
    private agileCasinoService: AgileCasinoService,
    private notificationsService: NotificationsService,
    private translate: TranslateService,
    private userService: UserService,
  ) {}

  showModal() {
    if (!this.userService.isLoggedIn()) {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.AddTasks,
      });
      return;
    }

    this.modal?.showModal();
  }

  closeModal() {
    this.modal?.close();
    this.resetTask();
  }

  resetTask() {
    this.task = createEmptyTask(this.roomId);
  }
}
