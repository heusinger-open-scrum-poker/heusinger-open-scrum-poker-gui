import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Subtask } from "src/app/schemas/Subtask";
import { UserService } from "src/app/user.service";

@Component({
  selector: "app-subtask-item",
  templateUrl: "./subtask-item.component.html",
  styleUrls: ["./subtask-item.component.scss"],
})
export class SubtaskItemComponent {
  @Input({ required: true }) subtask!: Subtask;
  @Output() subtaskChanged: EventEmitter<Subtask> = new EventEmitter<Subtask>();
  @Output() subtaskDeleted: EventEmitter<Subtask> = new EventEmitter<Subtask>();

  constructor(
    private userService: UserService,
  ) {}

  get isSignedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  onSubjectChanged() {
    this.subtaskChanged.emit(this.subtask);
  }

  typeOfSubtask(subtask: Subtask): "ai-generated" | "pending" | "created" {
    if (subtask.aiGenerated) {
      return "ai-generated";
    } else if (subtask.id) {
      return "created";
    } else {
      return "pending";
    }
  }

  deleteSubtask() {
    this.subtaskDeleted.emit(this.subtask);
  }
}
