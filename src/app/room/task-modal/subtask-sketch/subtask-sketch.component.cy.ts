import { MountConfig } from "cypress/angular"
import en_translation from "../../../../assets/i18n/en.json"
import { SubtaskSketchComponent } from "./subtask-sketch.component"
import { KehrwasserFeaturesModule, KwFeatureService } from "@kehrwasser-dev/features-angular"
import { TranslateTestingModule } from "ngx-translate-testing"
import { AgileCasinoService } from "src/app/agile-casino.service"
import { LexorankService } from "src/app/lexorank.service"
import { UserService } from "src/app/user.service"
import { NotificationsService } from "src/app/notifications.service"
import { FunnelEventService } from "src/app/funnel-event.service"
import { KwmcModule } from "src/app/kwmc/kwmc.module"
import { RoomModule } from "../../room.module"
import { MountConfigBuilder, loremIpsum } from "src/app/shared/testutil"
import { Task } from "src/app/schemas/Task"
import LocalStorage from "src/app/shared/LocalStorage"
import { SplitTestService } from "src/app/splittest/split-test.service"
import { LanguageService } from "src/app/language.service"
import { HttpClientModule } from "@angular/common/http"
import { ActivatedRoute } from "@angular/router"
import { of } from "rxjs"
import { User } from "src/app/schemas/User"

const BASE_CONFIG: MountConfig<SubtaskSketchComponent> = {
  imports: [
    TranslateTestingModule.withTranslations("en", en_translation),
    RoomModule,
    KehrwasserFeaturesModule,
    KwmcModule,
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    {
      provide: ActivatedRoute,
      useValue: {},
    },
    {
      provide: LexorankService,
      useValue: {}
    },
    {
      provide: UserService,
      useValue: {}
    },
    {
      provide: NotificationsService,
      useValue: {}
    },
    {
      provide: FunnelEventService,
      useValue: {}
    },
    {
      provide: LocalStorage,
      useValue: {}
    },
    {
      provide: LanguageService,
      useValue: {}
    },
    {
      provide: SplitTestService,
      useValue: {}
    },
    {
      provide: KwFeatureService,
      useValue: new KwFeatureService(),
    },
  ],
  componentProperties: {
    task: {
      task: {
        description: loremIpsum(1),
      } satisfies Partial<Task> as Task,
      subtasks: []
    },
  },
}

describe("SubtaskSketchComponent", () => {
  it("mounts", () => {
    const config = new MountConfigBuilder(BASE_CONFIG)
    cy.mount(SubtaskSketchComponent, config.build())
  })

  it("shows the correct labels when pro", () => {
    const config = new MountConfigBuilder(BASE_CONFIG)
    config.addService({
      provide: AgileCasinoService,
      useValue: {
        getAiSubtasksQuota: () => of({freeAiEstimations: 5})
      }
    })
    config.addService({
      provide: UserService,
      useValue: {
        isLoggedIn: () => true,
        isPro: () => true,
      }
    })
    config.addService({
      provide: KwFeatureService,
      useValue: {
        getRole: () => "pro",
        isFeatureEnabled: () => true,
        isSmokeTestEnabled: () => true,
        isSplitTestEnabled: () => true,
      }
    })
    cy.mount(SubtaskSketchComponent, config.build())
    cy.get(".license-badge").get("app-feature-lock.unlocked")
  })

  it("shows the correct labels when registered", () => {
    const config = new MountConfigBuilder(BASE_CONFIG)
    config.addService({
      provide: AgileCasinoService,
      useValue: {
        getAiSubtasksQuota: () => of({freeAiEstimations: 5})
      }
    })
    config.addService({
      provide: UserService,
      useValue: {
        isLoggedIn: () => true,
        isPro: () => false,
      }
    })
    config.addService({
      provide: KwFeatureService,
      useValue: {
        getRole: () => "signedup",
        isFeatureEnabled: () => true,
        isSmokeTestEnabled: () => true,
        isSplitTestEnabled: () => true,
      }
    })
    cy.mount(SubtaskSketchComponent, config.build())
    cy.get(".license-badge").get("app-feature-lock").contains("5x left")
  })

  it("shows the correct labels when registered, but out of free estimations", () => {
    const config = new MountConfigBuilder(BASE_CONFIG)
    config.addService({
      provide: AgileCasinoService,
      useValue: {
        getAiSubtasksQuota: () => of({freeAiEstimations: 0})
      }
    })
    config.addService({
      provide: UserService,
      useValue: {
        isLoggedIn: () => true,
        isPro: () => false,
      }
    })
    config.addService({
      provide: KwFeatureService,
      useValue: {
        getRole: () => "signedup",
        isFeatureEnabled: () => true,
        isSmokeTestEnabled: () => true,
        isSplitTestEnabled: () => true,
      }
    })
    cy.mount(SubtaskSketchComponent, config.build())
    cy.get(".license-badge").get("app-feature-lock.locked")
  })

  it("shows the correct labels when anonymous", () => {
    const config = new MountConfigBuilder(BASE_CONFIG)
    config.addService({
      provide: AgileCasinoService,
      useValue: {
        getAiSubtasksQuota: () => of({freeAiEstimations: 5})
      }
    })
    config.addService({
      provide: UserService,
      useValue: {
        isLoggedIn: () => false,
        isPro: () => false,
        getUser: () => ({ role: "anonymous" }) satisfies Partial<User> as User
      }
    })
    config.addService({
      provide: KwFeatureService,
      useValue: {
        getRole: () => "anonymous",
        isFeatureEnabled: () => true,
        isSmokeTestEnabled: () => true,
        isSplitTestEnabled: () => true,
      }
    })
    cy.mount(SubtaskSketchComponent, config.build())
    cy.get(".license-badge").get("app-feature-lock.locked")
  })
})

