import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { FunnelEventService } from "src/app/funnel-event.service";
import { LexorankService } from "src/app/lexorank.service";
import { Subtask } from "src/app/schemas/Subtask";
import { UserService } from "src/app/user.service";
import { NotificationsService } from "src/app/notifications.service";
import { LexoRank } from "lexorank";
import { Settings } from "src/app/shared/Settings";
import { CdkDragDrop } from "src/third-party/cdk/drag-drop";
import { TaskWithSubtasks } from "src/app/schemas/Game";

@Component({
  selector: "app-subtask-sketch",
  templateUrl: "./subtask-sketch.component.html",
  styleUrls: ["./subtask-sketch.component.scss"],
})
export class SubtaskSketchComponent implements OnInit {
  @Output() taskChanged = new EventEmitter<TaskWithSubtasks>();
  @Input({ required: true }) task!: TaskWithSubtasks;
  @ViewChild("authModal") authModal?: AuthModalComponent;

  pendingAiGeneration: boolean = false;
  freeAiEstimations: number = 0;

  constructor(
    private agileCasinoService: AgileCasinoService,
    private lexorankService: LexorankService,
    private userService: UserService,
    private notificationsService: NotificationsService,
    private funnelEventService: FunnelEventService,
  ) {}

  get subtasks(): Array<Subtask> {
    return this.task.subtasks ?? [];
  }

  get isSignedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  get subtasksExist() {
    return this.subtasks && this.subtasks.filter((s) => !s.deleted).length > 0;
  }

  get hasFreeAiEstimations() {
    return this.userService.isLoggedIn() && this.freeAiEstimations > 0;
  }

  get requiresPro() {
    return !this.hasFreeAiEstimations && !this.userService.isPro();
  }

  get isPro() {
    return this.userService.isPro();
  }

  get isNotPro() {
    return !this.userService.isPro();
  }

  ngOnInit() {
    this.updateQuota();
  }

  onTaskChanged(): void {
    this.taskChanged.emit(this.task);
  }

  addSubtask(): void {
    this.funnelEventService.smokeTestEvent(Feature.Subtasks, 'create subtask clicked');

    const task = this.task;
    if (!task) {
      console.error("cannot add a subtask because no task is given");
      return;
    }

    if (this.userService.isPro()) {
      this.pushSubtask(task, {
          id: null,
          subject: "",
          taskId: task.task.id,
          checked: false
      });
      this.onTaskChanged();
      // TODO: focus newly created subtask
    } else {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.Subtasks,
      });
    }
  }

  pushSubtask(task: TaskWithSubtasks, subtask: Subtask) {
      let rank: LexoRank;
      if (this.task.subtasks.length > 0) {
        const lastRank = this.task.subtasks[this.task.subtasks.length - 1].rank;
        rank = lastRank ? LexoRank.parse(lastRank).genNext() : LexoRank.middle();
      } else {
        rank = LexoRank.middle();
      }

      task.subtasks.push({
        rank: rank.toString(),
        ...subtask,
      })
  }

  generateAiSubtasks(): void {
    this.funnelEventService.smokeTestEvent(Feature.AiSubtasks, 'create ai subtasks clicked');

    const task = this.task;
    if (!task) {
      console.error("cannot generate AI subtasks because no task is given");
      return;
    }

    if (this.userService.isPro() || this.hasFreeAiEstimations) {
      const summary = `# ${task.task.subject} \n\n${task.task.description}`;
      this.pendingAiGeneration = true;
      this.agileCasinoService.generateAiSubtasks(summary).subscribe({
        next: (aiSubtasks) => {
          this.pendingAiGeneration = false;
          for (const { summary } of aiSubtasks) {
            this.pushSubtask(task, {
                aiGenerated: true,
                subject: summary,
                id: null,
                taskId: task.task.id,
                checked: false
            })
          }
          this.onTaskChanged();
          this.updateQuota();
        },
        error: (error) => {
          this.pendingAiGeneration = false;
          this.notificationsService.notify({
            type: "error",
            title: "AI subtasks",
            message: "Could not generate subtasks",
          });
          console.log(error);
          this.updateQuota();
        },
      })
    } else {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.AiSubtasks,
      });
    }
  }

  dropSubtask(event: CdkDragDrop<string[]>) {
    this.lexorankService.drop(event, this.task.subtasks);

    this.onTaskChanged();
  }

  deleteSubtask(subtask: Subtask): void {
    if (!this.isSignedIn) {
      console.error("not signed in");
      return;
    }

    subtask.deleted = true;

    this.onTaskChanged();
  }

  updateQuota(): void {
    this.agileCasinoService.getAiSubtasksQuota().subscribe((quota) => {
      if (typeof quota.freeAiEstimations === 'number' && quota.freeAiEstimations > 0) {
        this.freeAiEstimations = quota.freeAiEstimations;
      } else {
        this.freeAiEstimations = 0;
      }
    });
  }

  onTryoutLinkClicked() {
    this.funnelEventService.smokeTestEvent(Feature.AiSubtasks, 'empty state tryout link clicked');

    this.generateAiSubtasks();
  }

  onSubjectChanged() {
    this.onTaskChanged();
  }

  get dragStartDelay() {
    return Settings.DRAG_START_DELAY;
  }
}
