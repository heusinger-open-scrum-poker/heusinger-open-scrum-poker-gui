import { TaskWithSubtasks } from "src/app/schemas/Game";
import { Room } from "src/app/schemas/Room";


export function createEmptyTask(roomId: Room["id"]): TaskWithSubtasks {
  return {
    task: {
        roomId: roomId,
        id: 0,
        subject: "",
        description: ""
    },
    subtasks: [],
  }
}
