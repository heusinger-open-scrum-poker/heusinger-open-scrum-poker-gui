import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild, SimpleChanges } from "@angular/core";
import { UserService } from "src/app/user.service";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { Editor, toDoc, toHTML } from "ngx-editor";
import { environment } from "src/environments/environment";
import { LexorankService } from "src/app/lexorank.service";
import { FunnelEventService } from "src/app/funnel-event.service";
import { Feature } from "src/app/auth/auth-modal/plans-page/feature";
import { Subtask } from "src/app/schemas/Subtask";
import { Settings } from "src/app/shared/Settings";
import { CdkDragDrop } from "src/third-party/cdk/drag-drop";
import { TaskWithSubtasks } from "src/app/schemas/Game";

@Component({
  selector: "app-task-form",
  templateUrl: "./task-form.component.html",
  styleUrls: ["./task-form.component.scss"],
})
export class TaskFormComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild("authModal") authModal?: AuthModalComponent;
  @Input({ required: true }) task!: TaskWithSubtasks;
  @Output() taskChanged = new EventEmitter<TaskWithSubtasks>();

  protected editor?: Editor;
  descriptionDoc?: object;

  constructor(
    private userService: UserService,
    // NOTE: remove once we switch to new subtask sketch component
    private lexorankService: LexorankService,
    // NOTE: remove once we switch to new subtask sketch component
    private funnelEventService: FunnelEventService,
  ) {}

  ngOnInit(): void {
    this.editor = new Editor();
    this.onTaskChanged()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["task"]) {
      this.onTaskChanged()
    }
  }

  onTaskChanged() {
    this.descriptionDoc = toDoc(this.task?.task?.description ?? "");
  }

  ngOnDestroy(): void {
    this.editor?.destroy();
  }

  get newTaskModal(): boolean {
    return environment.newTaskModal;
  }

  get isSignedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  get subject(): string {
    return this.task?.task.subject ?? "";
  }

  get estimate(): string {
    return this.task?.task.estimate ?? "";
  }

  changeTask(value: TaskWithSubtasks): void {
    this.taskChanged.emit(value);
  }

  onChange(change: object) {
    this.descriptionDoc = change;
    if (this.task?.task) {
      this.task.task.description = toHTML(change);
    }
  }

  // NOTE: remove once we switch to new subtask sketch component
  addSubtask(): void {
    this.funnelEventService.smokeTestEvent(Feature.Subtasks, 'create subtask clicked');

    const task = this.task;
    if (!task) {
      console.error("cannot add a subtask because no task is given");
      return;
    }

    if (this.userService.isPro()) {
      this.task.subtasks.push({
          taskId: task.task.id,
          id: null,
          subject: "",
          checked: false
      });
    } else {
      this.authModal?.showPlans({
        triggeredByFeature: Feature.Subtasks,
      });
    }
  }

  // NOTE: remove once we switch to new subtask sketch component
  dropSubtask(event: CdkDragDrop<string[]>) {
    this.lexorankService.drop(event, this.task.subtasks);
  }

  // NOTE: remove once we switch to new subtask sketch component
  deleteSubtask(subtask: Subtask): void {
    subtask.deleted = true;
  }

  handleSubtasksChanged(): void {
    this.taskChanged.emit(this.task);
  }

  allowContextMenu(event: Event): void {
    (event as any)["overrideContextMenuBlock"] = true;
  }

  get dragStartDelay() {
    return Settings.DRAG_START_DELAY;
  }

  onSubjectChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.task.task.subject = inputElement.value;
  }


  onEstimateChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.task.task.estimate = inputElement.value;
  }
}
