import { Component, Input, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { catchError, map, mergeMap, Observable, of } from "rxjs";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ConfirmationResult } from "src/app/modal/confirmation-box";
import { ConfirmationBoxComponent } from "src/app/modal/confirmation-box/confirmation-box.component";
import { ModalDialogComponent } from "src/app/modal/modal-dialog/modal-dialog.component";
import {
  ModalActions,
  ModalButtonProps,
} from "src/app/modal/types";
import { NotificationsService } from "src/app/notifications.service";
import { TaskWithSubtasks } from "src/app/schemas/Game";
import { Room } from "src/app/schemas/Room";

@Component({
  selector: "app-edit-task-modal",
  templateUrl: "./edit-task-modal.component.html",
  styleUrls: [ "./edit-task-modal.component.scss"],
})
export class EditTaskModalComponent {

  @ViewChild("modal") modal?: ModalDialogComponent;
  @ViewChild(ConfirmationBoxComponent) confirmationBox!: ConfirmationBoxComponent;

  @Input() roomId!: Room["id"];
  @Input({ required: true }) task?: TaskWithSubtasks;
  @Input() isSignedIn: boolean = false;

  private hasTaskChanged = false;

  confirmationMessage: string = "";

  updateTask(task: TaskWithSubtasks): void {
    this.task = task;
    this.hasTaskChanged = true;
  }

  get primaryButtonProps(): ModalButtonProps | undefined {
    if (!this.isSignedIn) {
      return;
    }

    return {
      label: this.translate.instant("Save"),
      buttonFlavor: "primary",
      onClick: () => {
        if (!this.task) {
          throw new Error("task is undefined");
        }
        return this.agileCasinoService.updateTaskWithSubtasks(this.roomId, this.task.task, this.task.subtasks).pipe(
          map(() => {
            return ModalActions.close;
          }),
          catchError((error) => {
            console.error(error);
            this.notificationsService.notify({
              message: "Failed to update the task.",
            });
            return of(ModalActions.ignore);
          }),
        );
      },
    }
  }

  get tertiaryButtonProps(): ModalButtonProps | undefined {
    return {
      label: this.translate.instant(this.isSignedIn ? "Cancel" : "Close"),
      buttonFlavor: "secondary",
      onClick: () => {
        if (this.hasTaskChanged) {
          return this.getDiscardChangesConfirmation().pipe(
            mergeMap((confirmation) => {
              if (confirmation === "confirm") {
                return of(ModalActions.close);
              } else {
                return of(ModalActions.ignore);
              }
            }),
          );
        } else {
          return of(ModalActions.close);
        }
      },
    }
  }

  get secondaryButtonProps(): ModalButtonProps | undefined {
    if (!this.isSignedIn) {
      return;
    }

    return {
      label: this.translate.instant("Delete"),
      buttonFlavor: "high-impact",
      onClick: () => {
        return this.getDeletionConfirmation().pipe(
          mergeMap((confirmation) => {
            if (!this.task) {
              throw new Error("task is undefined");
            }
            if (confirmation === "confirm") {
              return this.agileCasinoService
                .deleteTask(this.roomId, this.task.task.id)
                .pipe(
                  map(() => {
                    return ModalActions.close;
                  }),
                  catchError((error) => {
                    console.error(error);
                    this.notificationsService.notify({
                      message: "Failed to delete the task.",
                    });
                    return of(ModalActions.ignore);
                  }),
                );
            } else {
              return of(ModalActions.ignore);
            }
          }),
        );
      },
    }
  }

  constructor(
    private agileCasinoService: AgileCasinoService,
    private notificationsService: NotificationsService,
    private translate: TranslateService,
  ) {}

  getDeletionConfirmation(): Observable<ConfirmationResult> {
    this.confirmationBox.showModal();
    this.confirmationMessage = this.translate.instant(
      "Task.Delete.ConfirmationMessage",
    );
    return this.confirmationBox.result.pipe(
      map((result: ConfirmationResult) => {
        this.confirmationBox.close();
        return result;
      }),
    );
  }

  getDiscardChangesConfirmation(): Observable<ConfirmationResult> {
    this.confirmationBox.showModal();
    this.confirmationMessage = this.translate.instant(
      "Task.DiscardChanges.ConfirmationMessage",
    );
    return this.confirmationBox.result.pipe(
      map((result: ConfirmationResult) => {
        this.confirmationBox.close();
        return result;
      }),
    );
  }

  showModal() {
    this.modal?.showModal();
  }

  closeModal() {
    this.modal?.close();
    this.hasTaskChanged = false;
  }
}
