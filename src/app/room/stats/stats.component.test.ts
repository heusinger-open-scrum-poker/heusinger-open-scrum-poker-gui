import { describe, it, expect } from 'vitest';
import { Player } from "src/app/schemas/Player";
import { Room } from "src/app/schemas/Room";
import { computeStats } from "../statistics";

describe("Stats", () => {
  const room1: Partial<Room> = {
    series: JSON.stringify("1 2 3 4 5 6 7 8".split(" ")),
  };

  const players1: Array<Partial<Player>> = [
    { type: "estimator", cardValue: "1" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "3" },
    { type: "inspector", cardValue: "3" },
    { type: "inspector", cardValue: "3" },
    { type: "inspector", cardValue: "3" },
    { type: "inspector", cardValue: "7" },
  ];

  it("should ignore inspectors", () => {
    const stats = computeStats(players1 as Player[], room1 as Room);
    expect(stats.maxVotes).to.equal(2);
    expect(stats.totalVotes).to.equal(4);
    expect(stats.minOutlier).to.equal("1");
    expect(stats.maxOutlier).to.equal("3");
    expect(stats.majorityCards.length).to.equal(1);
    expect(stats.majorityCards[0]).to.equal("2");
  });

  const players2: Array<Partial<Player>> = [
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "3" },
  ];

  it("should have no min outlier which is also a majority", () => {
    const stats = computeStats(players2 as Player[], room1 as Room);
    expect(stats.maxVotes).to.equal(3);
    expect(stats.totalVotes).to.equal(4);
    expect(stats.minOutlier).to.equal(undefined);
    expect(stats.maxOutlier).to.equal("3");
    expect(stats.majorityCards.length).to.equal(1);
    expect(stats.majorityCards[0]).to.equal("2");
  });

  const players3: Array<Partial<Player>> = [
    { type: "estimator", cardValue: "1" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "2" },
  ];

  it("should have no max outlier which is also a majority", () => {
    const stats = computeStats(players3 as Player[], room1 as Room);
    expect(stats.maxVotes).to.equal(3);
    expect(stats.totalVotes).to.equal(4);
    expect(stats.minOutlier).to.equal("1");
    expect(stats.maxOutlier).to.equal(undefined);
    expect(stats.majorityCards.length).to.equal(1);
    expect(stats.majorityCards[0]).to.equal("2");
  });

  const players4: Array<Partial<Player>> = [
    { type: "estimator", cardValue: "1" },
    { type: "estimator", cardValue: "1" },
    { type: "estimator", cardValue: "2" },
    { type: "estimator", cardValue: "2" },
  ];

  it("should have multiple majorities", () => {
    const stats = computeStats(players4 as Player[], room1 as Room);
    expect(stats.maxVotes).to.equal(2);
    expect(stats.totalVotes).to.equal(4);
    expect(stats.minOutlier).to.equal(undefined);
    expect(stats.maxOutlier).to.equal(undefined);
    expect(stats.majorityCards.length).to.equal(2);
    expect(stats.majorityCards).to.contain("1");
    expect(stats.majorityCards).to.contain("2");
  });

  const room2: Partial<Room> = {
    series: JSON.stringify("XXS XS S M L XL XXL".split(" ")),
  };

  const players5: Array<Partial<Player>> = [
    { type: "estimator", cardValue: "S" },
    { type: "estimator", cardValue: "M" },
    { type: "estimator", cardValue: "L" },
    { type: "estimator", cardValue: "L" },
    { type: "estimator", cardValue: "L" },
    { type: "estimator", cardValue: "XL" },
    { type: "estimator", cardValue: "XXL" },
    { type: "estimator", cardValue: "XXL" },
  ];

  it("can deal with non-numeric hands", () => {
    const stats = computeStats(players5 as Player[], room2 as Room);
    expect(stats.maxVotes).to.equal(3);
    expect(stats.totalVotes).to.equal(8);
    expect(stats.minOutlier).to.equal("S");
    expect(stats.maxOutlier).to.equal("XXL");
    expect(stats.majorityCards.length).to.equal(1);
    expect(stats.majorityCards).to.contain("L");
  });
});
