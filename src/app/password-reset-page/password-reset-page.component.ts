import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-password-reset-page",
  templateUrl: "./password-reset-page.component.html",
  styleUrls: ["./password-reset-page.component.scss"],
})
export class PasswordResetPageComponent implements OnInit {
  constructor(
    private router: Router,

  ) {}

  ngOnInit(): void {
    this.router.navigate(["create"], {
      queryParamsHandling: "preserve",
    });
  }
}
