import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { KwmcModule } from "../kwmc/kwmc.module";
import { TranslateForRootModule } from "../translate-for-root.module";
import { FooterComponent } from "./footer.component";
import { KehrwasserFeaturesModule } from "@kehrwasser-dev/features-angular";
import { AuthModule } from "../auth/auth.module";

@NgModule({
  declarations: [FooterComponent],
  exports: [FooterComponent],
  imports: [CommonModule, KwmcModule, TranslateForRootModule, KehrwasserFeaturesModule, AuthModule],
  providers: [],
})
export class FooterModule {}
