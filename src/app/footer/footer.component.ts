import { AfterViewInit, Component, Input, OnInit, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { LanguageService } from "../language.service";
import { AuthModalComponent } from "../auth/auth-modal/auth-modal.component";
import { Feature } from "../auth/auth-modal/plans-page/feature";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit, AfterViewInit {
  @ViewChild("authModal") authModal!: AuthModalComponent;
  @Input() adEnabled: boolean = false;
  @Input() changeAd!: Observable<unknown>;

  constructor(private languageService: LanguageService) {}

  visible: boolean = environment.footerVisible;

  ngOnInit(): void {
    console.log("footer visible:", this.visible);
  }

  get isGermanLocale(): boolean {
    // https://www.rfc-editor.org/rfc/rfc5646.html
    const localeString = this.languageService.getLocale();
    return localeString === "de" || localeString.startsWith("de-");
  }

  ngAfterViewInit(): void {
    window.adsbygoogle?.push({});
  }

  hideFooterClicked(): void {
    this.authModal?.showPlans({
      triggeredByFeature: Feature.RemoveAds,
    });
  }
}
