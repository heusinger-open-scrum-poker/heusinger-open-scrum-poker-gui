import { MountConfig } from "cypress/angular";
import { FooterComponent } from "./footer.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../assets/i18n/en.json";

describe("FooterComponent", () => {
  const config: MountConfig<FooterComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
  };

  it("mounts", () => {
    cy.mount(FooterComponent, config);
  });
});
