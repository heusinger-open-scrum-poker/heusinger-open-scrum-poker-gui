import { describe, it, expect } from "vitest";
import { KwSpec } from "@kehrwasser-dev/features";
import spec from "./features.json";


describe("features.json", () => {
  it("is a well-formed spec", () => {
    expect(KwSpec.safeParse(spec).success).to.be.true;
  })
})
