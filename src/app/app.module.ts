import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { TranslateForRootModule } from "./translate-for-root.module";
import { RoomModule } from "./room/room.module";
import { PasswordResetPageComponent } from "./password-reset-page/password-reset-page.component";
import { JiraOAuthRedirectPageComponent } from "./jira-oauth-redirect-page/jira-oauth-redirect-page.component";
import { QueryClient, provideAngularQuery } from "@tanstack/angular-query-experimental";
import { StartpageModule } from "./startpage/startpage.module";

@NgModule({
  declarations: [
    AppComponent,
    PasswordResetPageComponent,
    JiraOAuthRedirectPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientModule,
    RoomModule,
    TranslateForRootModule,
    StartpageModule,
  ],
  providers: [
    provideAngularQuery(new QueryClient()),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
