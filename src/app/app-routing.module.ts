import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RoomComponent } from "./room/room.component";
import { PasswordResetPageComponent } from "./password-reset-page/password-reset-page.component";
import { JiraOAuthRedirectPageComponent } from "./jira-oauth-redirect-page/jira-oauth-redirect-page.component";
import { StartpageComponent } from "./startpage/startpage.component";
import { DetailsPageComponent } from "./startpage/details-page/details-page.component";

const routes: Routes = [
  { path: "", component: StartpageComponent, pathMatch: "full" },
  { path: "create", component: StartpageComponent },
  { path: "join", component: StartpageComponent },
  { path: "join/:id", component: StartpageComponent },
  {
    path: "room/:id",
    component: RoomComponent,
    data: {
      title: "Planning Poker - Agile Casino - Room",
    },
  },
  { path: "detailed-info", component: DetailsPageComponent },
  { path: "", redirectTo: "/create", pathMatch: "full" },
  { path: "reset_password", component: PasswordResetPageComponent },
  { path: "submit_jira_auth_code", component: JiraOAuthRedirectPageComponent },
  { path: "", redirectTo: "/create", pathMatch: "prefix" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
