import { RouterTestingModule } from "@angular/router/testing";
import { MountConfig } from "cypress/angular";
import { TranslateTestingModule } from "ngx-translate-testing";
import { AppComponent } from "./app.component";
import { ModalModule } from "./modal/modal.module";
import en_translation from "../assets/i18n/en.json";
import { HttpClientModule } from "@angular/common/http";

describe("AppComponent", () => {
  const config: MountConfig<AppComponent> = {
    imports: [
      RouterTestingModule,
      ModalModule,
      TranslateTestingModule.withTranslations("en", en_translation),
      HttpClientModule,
    ],
  };

  it("should create the app", () => {
    cy.mount(AppComponent, config).should(({ component, fixture }) => {
      expect(component).to.be.ok;
      expect(fixture.nativeElement).to.be.ok;
    });
  });

  it("should have a user object", () => {
    cy.mount(AppComponent, config).should(({ component, fixture }) => {
      expect(component).to.be.ok;
      expect(localStorage.getItem("uuid")).not.to.be.undefined;
    });
  });

  // it(`should have as title 'agile-casino'`, () => {
  //   cy.mount(AppComponent, config).should(({ component, fixture }) => {
  //     expect(component).to.be.ok
  //     expect(fixture.nativeElement).to.be.ok
  //     expect(component.title).to.equal('agile-casino');
  //   })
  // });
  //
  // it('should render title', () => {
  //   cy.mount(AppComponent, config).should(({ component, fixture }) => {
  //     expect(component).to.be.ok
  //     expect(fixture.nativeElement).to.be.ok
  //   })
  //   cy.get('.content span').contains('agile-casino app is running!');
  // });
});
