import { LexoRank } from "lexorank";
import { Injectable } from "@angular/core";
import { Rankable } from "./schemas/Rankable";
import { CdkDragDrop, moveItemInArray } from "src/third-party/cdk/drag-drop";

@Injectable({
  providedIn: 'root',
})
export class LexorankService {
  drop(event: CdkDragDrop<string[]>, all: Rankable[]): Rankable | undefined {
    const entity = all[event.previousIndex];

    let newRank: LexoRank;
    if (event.currentIndex === event.previousIndex || all.length < 2) {
      return;
    } else if (event.currentIndex == 0) {
      newRank = LexoRank.parse(all[0].rank ?? "").genPrev();
    } else if (event.currentIndex == all.length - 1) {
      newRank = LexoRank.parse(
        all[all.length - 1].rank ?? "",
      ).genNext();
    } else if (event.previousIndex > event.currentIndex) {
      newRank = LexoRank.parse(all[event.currentIndex - 1].rank ?? "").between(
        LexoRank.parse(all[event.currentIndex].rank ?? ""),
      );
    } else if (event.previousIndex < event.currentIndex) {
      newRank = LexoRank.parse(all[event.currentIndex].rank ?? "").between(
        LexoRank.parse(all[event.currentIndex + 1].rank ?? ""),
      );
    } else {
      return;
    }

    entity.rank = newRank.toString();

    moveItemInArray(
      all,
      event.previousIndex,
      event.currentIndex,
    );

    return entity;
  }
}
