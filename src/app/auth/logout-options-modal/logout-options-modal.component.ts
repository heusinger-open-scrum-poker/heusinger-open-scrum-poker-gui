import {
  Component, EventEmitter, Input, Output, ViewChild,
} from "@angular/core";
import { AgileCasinoService } from "../../agile-casino.service";
import { ModalOptions } from "../../modal/types";
import { Router } from "@angular/router";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";
import { NotificationsService } from "../../notifications.service";
import { UserService } from "../../user.service";
import { ModalComponent } from "src/app/modal/modal/modal.component";

export type SignOutMode = "default" | "stay" | "leave";

@Component({
  selector: "app-logout-options-modal",
  templateUrl: "./logout-options-modal.component.html",
  styleUrls: ["./logout-options-modal.component.scss"],
})
export class LogoutOptionsModalComponent {
  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-400",
    hideOnOutsideClick: false,
  };

  @ViewChild("modal") modal?: ModalComponent;

  @Input() playerId?: Player["id"];
  @Input() roomId?: Room["id"];
  @Input() mode: SignOutMode = "default";

  @Output("logout") onLogout: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private agileCasinoService: AgileCasinoService,
    private router: Router,
    private notificationsService: NotificationsService,
    private userService: UserService,
  ) {}

  get signOutLabel(): string {
    switch (this.mode) {
      case "leave": {
        return "SignOutAndLeave";
      }
      case "stay": {
        return "SignOutAndStay";
      }
      case "default": {
        return "SignOut"
      }
    }
  }

  onSubmit(): void {
    switch (this.mode) {
      case "leave": {
        this.signOutAndLeave();
      } break;
      case "stay": {
        this.signOutAndStay();
      } break;
      case "default": {
        this.signOut();
      } break;
    }
  }

  signOutAndLeave(): void {
    const id = this.playerId;

    if (id) {
      this.agileCasinoService.deletePlayer(id).subscribe({
        error: (error) => {
          console.error(error);
          this.notificationsService.notify({ type: "error", message: "could not leave room" })
        },
      });
    }

    this.signOut();
  }

  signOutAndStay(): void {
    this.signOut();
  }

  signOut(): void {
    this.userService.logout();
    this.modal?.close();
    this.onLogout.emit();
    this.router.navigate([""]);
  }

  showModal(): void {
    this.modal?.showModal();
  }

  close(): void {
    this.modal?.close();
  }
}
