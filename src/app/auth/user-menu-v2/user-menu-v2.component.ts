import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { UserService } from "../../user.service";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";
import { ContextMenuComponent } from "src/app/kwmc/context-menu/context-menu.component";

@Component({
  selector: "app-user-menu-v2",
  templateUrl: "./user-menu-v2.component.html",
  styleUrls: ["./user-menu-v2.component.scss"],
})
export class UserMenuComponentV2 {
  @ViewChild("authModal") authModal?: AuthModalComponent;
  @ViewChild(ContextMenuComponent) contextMenu?: ContextMenuComponent;

  @Input() size: "default" | "larger" = "default"
  @Input() placement: "bottom-start" | "bottom-end" = "bottom-start"
  @Input() context: "room" | "homepage" = "room";

  @Output("logout") logoutEvent: EventEmitter<{leaveRoom: boolean}> = new EventEmitter<{leaveRoom: boolean}>();
  @Output("login") loginEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private userService: UserService,
  ) {}

  get isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  get userProfile() {
    return this.userService.getUserProfile();
  }

  get arrowSize() {
    switch (this.size) {
      case "default": return "12px";
      case "larger": return "18px";
    }
  }

  get isRoom(): boolean {
    return this.context === "room";
  }

  get isHomepage(): boolean {
    return this.context === "homepage";
  }

  openLoginModal(): void {
    this.closeMenu();
    this.authModal?.showSignin();
  }

  openRegisterModal(): void {
    this.closeMenu();
    this.authModal?.showSignup();
  }

  handleLogin(): void {
    this.closeMenu();
    this.loginEvent.emit();
  }

  handleLogout(): void {
    this.closeMenu();
    this.logoutEvent.emit();
  }

  handleSignOutAndStay(): void {
    this.closeMenu();
    this.logoutEvent.emit({leaveRoom: false});
  }

  handleSignOutAndLeave(): void {
    this.closeMenu();
    this.logoutEvent.emit({leaveRoom: true});
  }

  closeMenu(): void {
    this.contextMenu?.close();
  }
}
