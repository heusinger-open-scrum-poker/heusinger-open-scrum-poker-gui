import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslateForRootModule } from "../translate-for-root.module";
import { ModalModule } from "src/app/modal/modal.module";
import { LogoutOptionsModalComponent } from "./logout-options-modal/logout-options-modal.component";
import { PasswordResetModalComponent } from "./password-reset-modal/password-reset-modal.component";
import { ReactiveFormsModule } from "@angular/forms";
import { AuthModalComponent } from "./auth-modal/auth-modal.component";
import { LoginPageComponent } from "./auth-modal/login-page/login-page.component";
import { RegisterPageComponent } from "./auth-modal/register-page/register-page.component";
import { ResetPageComponent } from "./auth-modal/reset-page/reset-page.component";
import { PlansPageComponent } from "./auth-modal/plans-page/plans-page.component";
import { KwmcModule } from "../kwmc/kwmc.module";
import { UserMenuComponent } from "./user-menu/user-menu.component";
import { CheckoutPageComponent } from "./auth-modal/checkout-page/checkout-page.component";
import { KehrwasserFeaturesModule } from "@kehrwasser-dev/features-angular";
import { UserMenuComponentV2 } from "./user-menu-v2/user-menu-v2.component";

const exports = [
  AuthModalComponent,
  LogoutOptionsModalComponent,
  PasswordResetModalComponent,
  UserMenuComponent,
  UserMenuComponentV2,
];

const declarations = [
  ...exports,
  LoginPageComponent,
  RegisterPageComponent,
  ResetPageComponent,
  PlansPageComponent,
  CheckoutPageComponent,
];

@NgModule({
  declarations,
  exports,
  imports: [CommonModule, TranslateForRootModule, ModalModule, ReactiveFormsModule, KwmcModule, KehrwasserFeaturesModule],
})
export class AuthModule {}
