import {
  Component,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable, map, of, catchError } from "rxjs";
import { AgileCasinoService } from "../../agile-casino.service";
import { ModalAction, ModalActions, ModalButtonProps, ModalOptions } from "../../modal/types";
import { TranslateService } from "@ngx-translate/core";
import { containsDigitValidator, containsSymbolValidator, passwordsMatchValidator } from "../auth-modal/register-page";
import { AlertModalComponent } from "../../modal/alert-modal/alert-modal.component";
import { ModalComponent } from "src/app/modal/modal/modal.component";
import { ModalFooterComponent } from "src/app/modal/modal-footer/modal-footer.component";
import { FormErrorDirective } from "src/app/modal/form-error.directive";

@Component({
  selector: "app-password-reset-modal",
  templateUrl: "./password-reset-modal.component.html",
  styleUrls: ["./password-reset-modal.component.scss"],
})
export class PasswordResetModalComponent {
  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-440",
    hideOnOutsideClick: false,
  };

  @ViewChild(ModalFooterComponent) modalFooter?: ModalFooterComponent;
  @ViewChild(FormErrorDirective) errorDirective?: FormErrorDirective;
  @ViewChild("alertModal") alertModal?: AlertModalComponent;
  @ViewChild("modal") modal?: ModalComponent;

  token: string = "";
  email: string = "";
  isOpen: boolean = false;

  ENABLE_TOKEN_INPUT: boolean = false;

  errorMessage?: string;

  formGroups: {
    "ask-for-new-password": FormGroup<{
      password: FormControl<string>;
      repeated_password: FormControl<string>;
    }>
  } = {
    "ask-for-new-password": new FormGroup(
      {
        password: new FormControl("", {
          nonNullable: true,
          validators: [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(1024),
            containsDigitValidator,
            containsSymbolValidator,
          ] }),
        repeated_password: new FormControl("", { nonNullable: true }),
      },
      [passwordsMatchValidator],
    )
  } as const;

  get passwordControl(): FormControl<string> {
    return this.formGroups['ask-for-new-password'].controls.password;
  }

  get repeatedPasswordControl(): FormControl<string> {
    return this.formGroups['ask-for-new-password'].controls.repeated_password;
  }

  get primaryButtonProps(): ModalButtonProps {
    return {
      label: this.translate.instant("ResetPassword"),
      buttonFlavor: "high-impact",
      buttonWidth: "medium",
      onClick: () => this.onSubmit(),
    };
  }

  get secondaryButtonProps(): ModalButtonProps | undefined {
    return {
      label: this.translate.instant("Cancel"),
      buttonFlavor: "secondary",
      buttonWidth: "medium",
      onClick: () => this.modal?.close(),
    };
  }

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
  ) {}

  onSubmit(): Observable<ModalAction> {
    return this.onSetNewPassword();
  }

  onSetNewPassword(): Observable<ModalAction> {
    if (this.formGroups['ask-for-new-password'].invalid) {
      this.errorDirective?.focusFirstError();
      return of(ModalActions.ignore);
    }

    return this.agileCasinoService.resetPassword({
      email: this.email,
      resetToken: this.token,
      newPassword: this.formGroups['ask-for-new-password'].value.password ?? "",
    })
    .pipe(
      map(() => {
        return ModalActions.close;
      }),
      catchError((error) => {
        if (error.status == 401) {
          if (error?.error?.message?.match('^reset token expired')) {

            this.alertModal?.showMessage("Reset token expired. Please try again.");
            return of(ModalActions.ignore);

          }

          if (error?.error?.message?.match('^invalid reset token')) {

            this.alertModal?.showMessage("The reset token is invalid. Please try again.");
            return of(ModalActions.ignore);

          }

        }

        if (error.status == 404) {
          if (error?.error?.message?.match('^user not found:')) {

            this.alertModal?.showMessage("The email address is not associated to a user.");
            return of(ModalActions.ignore);

          }
        }

        this.alertModal?.showMessage("An unexpected error occured.");
        return of(ModalActions.ignore);
      }),
    )
  }

  showModal(params: {
    email: string,
    token: string,
  }): void {
    this.isOpen = true;
    this.email = params.email;
    this.token = params.token;
    this.modal?.showModal();
  }

  close(): void {
    this.isOpen = false;
    this.modal?.close();
  }
}
