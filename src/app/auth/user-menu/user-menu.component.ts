import { Component, Input, ViewChild } from "@angular/core";
import { UserService } from "../../user.service";
import { AuthModalComponent } from "src/app/auth/auth-modal/auth-modal.component";

@Component({
  selector: "app-user-menu",
  templateUrl: "./user-menu.component.html",
  styleUrls: ["./user-menu.component.scss"],
})
export class UserMenuComponent {
  @ViewChild("authModal") authModal?: AuthModalComponent;

  @Input() handleLogin: () => void = () => {}
  @Input() handleLogout: () => void = () => {
    this.userService.logout();
  };

  constructor(
    private userService: UserService,
  ) {}

  get isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  get userProfile() {
    return this.userService.getUserProfile();
  }

  openLoginModal(): void {
    this.authModal?.showSignin();
  }

  openRegisterModal(): void {
    this.authModal?.showSignup();
  }
}
