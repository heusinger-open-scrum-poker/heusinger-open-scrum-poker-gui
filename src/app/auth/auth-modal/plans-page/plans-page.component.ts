import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ModalOptions } from "src/app/modal/types";
import { FunnelEventService } from "src/app/funnel-event.service";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "src/environments/environment";
import { UserService } from "src/app/user.service";
import { AlertModalComponent } from "src/app/modal/alert-modal/alert-modal.component";
import { SplitTestService } from "src/app/splittest/split-test.service";
import { Feature } from "./feature";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";
import { CheckoutTarget } from "../checkout-page/checkout-page.component";

@Component({
  selector: "app-plans-page",
  templateUrl: "./plans-page.component.html",
  styleUrls: ["./plans-page.component.scss"],
})
export class PlansPageComponent {
  @ViewChild("alertModal") alertModal?: AlertModalComponent;

  @Input("triggeredByFeature") triggeredByFeature: Feature = Feature.Other;

  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>;
  @Output("open-signin") openSignin: EventEmitter<void> = new EventEmitter<void>;
  @Output("open-checkout") openCheckout: EventEmitter<CheckoutTarget> = new EventEmitter<CheckoutTarget>;

  constructor(
    private funnelEventService: FunnelEventService,
    private translate: TranslateService,
    private userService: UserService,
    private splitTestService: SplitTestService,
    private featuresService: KwFeatureService,
  ) {}

  ngOnInit() {
    this.emitSplitTestEvents("view");
  }

  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-700",
  };

  get loginEnabled() {
    return environment.loginFeature;
  }

  get title(): string {
    if (this.isLoggedIn()) {
      return this.isNewPlansModal
        ? this.translate.instant("Pricing")
        : this.translate.instant("ProfessionalFeatures");
    }

    return this.translate.instant("Plans");
  }

  get proPrice() {
    return this.renderPrice(15.99);
  }

  get oneDayTicketPrice() {
    return this.renderPrice(1.50);
  }

  get headline(): string {
    switch (this.triggeredByFeature) {
      case Feature.Tasks:
      case Feature.AddTasks:
        return this.translate.instant("Plans.Headline.Tasks");
      case Feature.ImportTasks:
      case Feature.ImportMoreTasks:
        return this.translate.instant("Plans.Headline.Import");
      case Feature.ExportTasks:
        return this.translate.instant("Plans.Headline.Export");
      case Feature.Subtasks:
        return this.translate.instant("Plans.Headline.Subtasks");
      case Feature.AiSubtasks:
        return this.translate.instant("Plans.Headline.AiSubtasks");
      case Feature.LockRoom:
      case Feature.InsecureWarning:
        return this.translate.instant("Plans.Headline.Lock.Room");
      case Feature.AiEstimations:
      case Feature.CapacityForecast:
        return this.translate.instant("Plans.Headline.Ai.Support");
      case Feature.Average:
        return this.translate.instant("Plans.Headline.Average");
      case Feature.AutomaticForecast:
      case Feature.ForecastDetails:
      case Feature.RessourceAllocation:
        return this.translate.instant("Plans.Headline.Automatic.Reports");
      case Feature.ModerateRoom:
        return this.translate.instant("Plans.Headline.ModerateRoom");
      case Feature.ReadableLink:
        return this.translate.instant("Plans.Headline.ReadableLink");
      case Feature.RemoveAds:
        return this.translate.instant("Plans.Headline.RemoveAds");
      case Feature.FreeBadge:
      case Feature.Other:
      default:
        return this.translate.instant("Plans.Headline");
    }
  }

  get description(): string {
    switch (this.triggeredByFeature) {
      case Feature.Tasks:
      case Feature.AddTasks:
        return this.translate.instant("Plans.Description.Tasks");
      case Feature.ImportTasks:
      case Feature.ImportMoreTasks:
        return this.translate.instant("Plans.Description.Import");
      case Feature.ExportTasks:
        return this.translate.instant("Plans.Description.Export");
      case Feature.Subtasks:
        return this.translate.instant("Plans.Description.Subtasks");
      case Feature.AiSubtasks:
        return this.translate.instant("Plans.Description.AiSubtasks");
      case Feature.LockRoom:
      case Feature.InsecureWarning:
        return this.translate.instant("Plans.Description.Lock.Room");
      case Feature.AiEstimations:
      case Feature.CapacityForecast:
        return this.translate.instant("Plans.Description.Ai.Support");
      case Feature.Average:
        return this.translate.instant("Plans.Description.Average");
      case Feature.AutomaticForecast:
      case Feature.ForecastDetails:
      case Feature.RessourceAllocation:
        return this.translate.instant("Plans.Description.Automatic.Reports");
      case Feature.ModerateRoom:
        return this.translate.instant("Plans.Description.ModerateRoom");
      case Feature.ReadableLink:
        return this.translate.instant("Plans.Description.ReadableLink");
      case Feature.RemoveAds:
        return this.translate.instant("Plans.Description.RemoveAds");
      case Feature.FreeBadge:
      case Feature.Other:
      default:
        return this.translate.instant("Plans.Description");
    }
  }

  get freeFeatures(): string[] {
    return [
      this.translate.instant("Tasks.Without.Subtasks"),
      this.translate.instant("Unlimited.Rounds"),
      this.translate.instant("Connect with Jira"),
    ];
  }

  get proFeatures(): string[] {
    const features = [
      this.translate.instant("Everything.In.Starter"),
      this.translate.instant("Private.Rooms"),
      this.translate.instant("Moderated.Rooms"),
      this.translate.instant("Subtasks.Management"),
      this.translate.instant("Show.Average"),
    ];

    if (this.splitTestService.getProVariant() == 'even-id') {
      features.push(this.translate.instant("Ressource.Allocation"));
      features.push(this.translate.instant("Forecast.Capacity"));
    } else {
      features.push(this.translate.instant("Ai.Subtasks"));
      features.push(this.translate.instant("Ai.Estimations"));
    }

    return features;
  }

  get oneDayTicketFeatures(): string[] {
    const features = ([
      "SameAsProfessional",
      "OnlyOneGamePlayableForADay",
    ]).map((it) => this.translate.instant(it));

    return features;
  }

  onSignIn() {
    this.funnelEventService.smokeTestEvent(this.triggeredByFeature, 'plan modal login');

    if (!this.loginEnabled) {
      this.alertModal?.showMessage(this.translate.instant("Notification.FeatureNotExistent"));
      return;
    }

    this.openSignin.emit();
  }

  onUpgrade() {
    this.splitTestService.triggerFunnelEventPro();

    this.emitSplitTestEvents("click: upgrade to pro");
    this.funnelEventService.smokeTestEvent(this.triggeredByFeature, 'plan modal upgrade');

    this.openCheckout.emit("pro-subscription");
  }

  onDayTicket() {
    this.splitTestService.triggerFunnelEventPro();

    this.emitSplitTestEvents("click: one day ticket");
    this.funnelEventService.smokeTestEvent(this.triggeredByFeature, 'buy one day ticket');

    this.openCheckout.emit("one-day-ticket");
  }

  isLoggedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  get isNewPlansModal() {
    const variantSignal = this.featuresService.getVariant("plans-modal");
    const variant = variantSignal ? variantSignal() : "plans-modal-old";
    return variant !== "plans-modal-old";
  }

  private emitSplitTestEvents(cause: string): void {
    const feature = "plans-modal";

    this.featuresService.emitSplitTestEvent({
      splitTest: feature,
      payload: { cause: cause, triggeredByFeature: this.triggeredByFeature }
    });
  }

  private renderPrice(priceEuro: number): string {
    return Intl.NumberFormat(
      this.translate.getBrowserLang(),
      { style: 'currency', currency: 'EUR' }
    ).format(priceEuro);
  }
}
