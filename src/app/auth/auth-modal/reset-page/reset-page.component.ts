import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable, map, of, catchError } from "rxjs";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ModalAction, ModalActions, ModalButtonProps, ModalOptions } from "src/app/modal/types";
import { TranslateService } from "@ngx-translate/core";
import { containsDigitValidator, containsSymbolValidator, passwordsMatchValidator } from "../register-page";
import { AlertModalComponent } from "src/app/modal/alert-modal/alert-modal.component";
import { ModalFooterComponent } from "src/app/modal/modal-footer/modal-footer.component";
import { FormErrorDirective } from "src/app/modal/form-error.directive";
import { ButtonFlavor } from "src/app/kwmc/button";

@Component({
  selector: "app-reset-page",
  templateUrl: "./reset-page.component.html",
  styleUrls: ["./reset-page.component.scss"],
})
export class ResetPageComponent {
  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-440",
    hideOnOutsideClick: false,
  };

  @ViewChild(ModalFooterComponent) modalFooter?: ModalFooterComponent;
  @ViewChild(FormErrorDirective) errorDirective?: FormErrorDirective;
  @ViewChild("alertModal") alertModal?: AlertModalComponent;

  @Input() initialToken?: string;
  @Input() initialEmail?: string;

  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>;
  @Output("open-signin") openSignin: EventEmitter<void> = new EventEmitter<void>;

  ENABLE_TOKEN_INPUT: boolean = false;

  page: "ask-for-email" | "ask-to-check-inbox" = "ask-for-email";

  errorMessage?: string;

  formGroups: {
    "ask-for-email": FormGroup<{
      email: FormControl<string>;
    }>,
    "ask-for-token": FormGroup<{
      token: FormControl<string>;
    }>,
    "ask-for-new-password": FormGroup<{
      password: FormControl<string>;
      repeated_password: FormControl<string>;
    }>
  } = {
    "ask-for-email": new FormGroup<{email: FormControl<string>}>({
      email: new FormControl("", { nonNullable: true, validators: [Validators.required] }),
    }),
    "ask-for-token": new FormGroup({
      token: new FormControl("", { nonNullable: true, validators: [Validators.required] }),
    }),
    "ask-for-new-password": new FormGroup(
      {
        password: new FormControl("", {
          nonNullable: true,
          validators: [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(1024),
            containsDigitValidator,
            containsSymbolValidator,
          ] }),
        repeated_password: new FormControl("", { nonNullable: true }),
      },
      [passwordsMatchValidator],
    )
  } as const;

  get emailControl(): FormControl<string> {
    return this.formGroups['ask-for-email'].controls.email;
  }

  get tokenControl(): FormControl<string> {
    return this.formGroups['ask-for-token'].controls.token;
  }

  get passwordControl(): FormControl<string> {
    return this.formGroups['ask-for-new-password'].controls.password;
  }

  get repeatedPasswordControl(): FormControl<string> {
    return this.formGroups['ask-for-new-password'].controls.repeated_password;
  }

  get primaryButtonProps(): ModalButtonProps {
    return {
      label: this.primaryButtonLabel,
      buttonFlavor: this.primaryButtonFlavor,
      buttonWidth: "medium",
      onClick: () => this.onSubmit(),
    };
  }

  get secondaryButtonProps(): ModalButtonProps | undefined {
    if (this.page === "ask-to-check-inbox") {
      return;
    }

    return {
      label: this.translate.instant("Cancel"),
      buttonFlavor: "secondary",
      buttonWidth: "medium",
      onClick: () => this.onClose.emit(),
    };
  }

  get primaryButtonLabel(): string {
    switch (this.page) {
      case "ask-for-email":
        if (this.ENABLE_TOKEN_INPUT) {
          return this.translate.instant("Continue");
        } else {
          return this.translate.instant("ResetPassword");
        }
      case "ask-to-check-inbox":
        return this.translate.instant("Close");
    }
  }

  get primaryButtonFlavor(): ButtonFlavor {
    switch (this.page) {
      case "ask-for-email":
        return "primary";
      case "ask-to-check-inbox":
        return "primary";
    }
  }

  get modalTitle(): string {
    switch (this.page) {
      case "ask-for-email":
        return this.translate.instant("ResetPassword");
      case "ask-to-check-inbox":
        return this.translate.instant("ResetPassword");
    }
  }

  get isAskingForEmail(): boolean {
    return this.page === 'ask-for-email';
  }

  get isAskToCheckInbox(): boolean {
    return this.page === 'ask-to-check-inbox';
  }

  constructor(
    private agileCasinoService: AgileCasinoService,
    private translate: TranslateService,
  ) {}

  openSignInModal() {
    this.openSignin.emit();
  }

  onSubmit(): Observable<ModalAction> {
    switch (this.page) {
      case "ask-for-email":
        return this.onRequestNewPassword();
      case "ask-to-check-inbox":
        return this.onCheckForResetmail();
    }
  }

  onRequestNewPassword(): Observable<ModalAction> {
    if (this.formGroups['ask-for-email'].invalid) {
      this.errorDirective?.focusFirstError();
      return of(ModalActions.ignore);
    }

    return this.agileCasinoService.requestPasswordReset({
      email: this.emailControl.value,
    })
    .pipe(
      map(() => {
        this.page = "ask-to-check-inbox";
        return ModalActions.ignore;
      }),
      catchError((error) => {
        if (error.status == 404) {
          if (error?.error?.message?.match('^user not found:')) {

            this.alertModal?.showMessage("The email address is not associated to a user.");
            return of(ModalActions.ignore);

          }
        }

        this.alertModal?.showMessage("An unexpected error occured.");
        return of(ModalActions.ignore);
      }),
    )
  }

  onCheckForResetmail(): Observable<ModalAction> {
    return of(ModalActions.close);
  }
}
