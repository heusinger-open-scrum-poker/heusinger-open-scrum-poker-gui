import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";
import {
  containsDigitValidator,
  containsSymbolValidator,
  passwordsMatchValidator,
} from ".";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ModalAction, ModalActions, ModalButtonProps, ModalOptions } from "src/app/modal/types";
import { NotificationsService } from "src/app/notifications.service";
import LocalStorage from "src/app/shared/LocalStorage";
import { TranslateService } from "@ngx-translate/core";
import { UserService } from "src/app/user.service";
import { EmailValidator } from "./email.validator";
import { ModalFooterComponent } from "src/app/modal/modal-footer/modal-footer.component";
import { FormErrorDirective } from "src/app/modal/form-error.directive";
import { InputComponent } from "src/app/kwmc/input/input.component";

@Component({
  selector: "app-register-page",
  templateUrl: "./register-page.component.html",
  styleUrls: ["./register-page.component.scss"],
})
export class RegisterPageComponent implements AfterViewInit {
  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-440",
    hideOnOutsideClick: false,
  };

  @ViewChild("emailInput") emailInput!: InputComponent;
  @ViewChild(ModalFooterComponent) modalFooter?: ModalFooterComponent;
  @ViewChild(FormErrorDirective) errorDirective?: FormErrorDirective;

  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>;
  @Output("signin") onSignin: EventEmitter<void> = new EventEmitter<void>;
  @Output("open-signin") openSignin: EventEmitter<void> = new EventEmitter<void>;


  errorMessage?: string;

  get emailControl() {
    return this.formGroup.controls.email;
  }

  get passwordControl() {
    return this.formGroup.controls.password;
  }

  get repeatedPasswordControl() {
    return this.formGroup.controls.repeated_password;
  }

  formGroup: FormGroup<{
    email: FormControl<string | null>;
    password: FormControl<string | null>;
    repeated_password: FormControl<string | null>;
  }> = new FormGroup(
    {
      email: new FormControl("", [
        Validators.required
      ], [
        EmailValidator.createValidator(this.agileCasinoService)
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(1024),
        containsDigitValidator,
        containsSymbolValidator,
      ]),
      repeated_password: new FormControl(""),
    },
    [passwordsMatchValidator],
  );

  primaryButtonProps: ModalButtonProps = {
    label: this.translate.instant("CreateAccount"),
    buttonFlavor: "primary",
    buttonWidth: "medium",
    onClick: () => this.onSubmit(),
  };

  constructor(
    private agileCasinoService: AgileCasinoService,
    private storage: LocalStorage,
    private notificationsService: NotificationsService,
    private translate: TranslateService,
    private userService: UserService,
  ) {}

  ngAfterViewInit(): void {
    setTimeout(() => this.emailInput.focus(), 50);
  }

  openLoginModal() {
    this.openSignin.emit();
  }

  onSubmit(): Observable<ModalAction> {
    this.formGroup.markAllAsTouched();
    this.errorMessage = undefined;

    if (this.formGroup.invalid) {
      this.errorDirective?.focusFirstError();
      return of(ModalActions.ignore);
    }

    return this.agileCasinoService
      .registerWithEmail({
        email: this.emailControl.value ?? '',
        password: this.passwordControl.value ?? '',
        userUuid: this.userService.getUser()?.uuid,
      })
      .pipe(
        map((response) => {
          const { uuid, token } = response;
          this.storage.setSessionToken(token);
          this.storage.setUuid(uuid);
          this.userService.refreshUser();
          this.onSignin.emit();
          return ModalActions.close;
        }),
        catchError((error) => {
          if (error.status == 403) {
            if (error?.error?.message?.match('^user already signed-up:')) {

              this.notificationsService.notify({message: this.translate.instant("Already.SignedUp")});
              this.userService.refreshUser();
              return of(ModalActions.close);
            }

            if (error?.error?.message?.match('^invalid email address:')) {
              this.emailControl.setErrors({notValid: true});
              return of(ModalActions.ignore);
            }

            if (error?.error?.message?.match('^email already in use:')) {
              this.errorMessage = this.translate.instant("Email.Not.Available");
              return of(ModalActions.ignore);
            }

            if (error?.error?.message?.match('^password too short$')) {
              this.passwordControl.setErrors({minlength: true});
              return of(ModalActions.ignore);
            }
          }

          console.error(error);

          this.errorMessage = this.translate.instant("Error.General");
          this.notificationsService.notify({
            message: this.translate.instant("SignUp.Error"),
            type: "error",
          });

          return of(ModalActions.ignore);
        }),
      );
  }
}
