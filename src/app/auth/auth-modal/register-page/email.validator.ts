import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { concatMap, Observable, of, timer } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AgileCasinoService } from "src/app/agile-casino.service";

export class EmailValidator {
  static createValidator(agileCasinoService: AgileCasinoService): AsyncValidatorFn {

    return (control: AbstractControl): Observable<ValidationErrors> => {
      const source = agileCasinoService
        .validateEmail(control.value)
        .pipe(
          map((isAvailable: boolean) => isAvailable ? {} : { notAvailable: true }),
          catchError((error) => {
            if (error.status == 403 && error?.error?.message?.match('^invalid email address:')) {
              return of({ notValid: true });
            }

            console.error(error);
            return of({});
          })
        );

      return timer(250)
        .pipe(concatMap(() => source))
    }
  }
}
