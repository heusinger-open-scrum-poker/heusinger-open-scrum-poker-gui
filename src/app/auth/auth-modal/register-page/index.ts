import { AbstractControl, ValidationErrors } from "@angular/forms";

export function containsSymbolValidator(
  control: AbstractControl,
): ValidationErrors | null {
  const containsSymbol = /[^a-zA-Z0-9]/.test(control.value);
  return containsSymbol ? null : { containsNoSymbol: true };
}

export function containsDigitValidator(
  control: AbstractControl,
): ValidationErrors | null {
  const containsDigit = /[0-9]/.test(control.value);
  return containsDigit ? null : { containsNoDigit: true };
}

export function passwordsMatchValidator(
  control: AbstractControl,
): ValidationErrors | null {
  const match = control.value.password === control.value.repeated_password;

  return match
    ? null
    : {
        passwordsDontMatch: true,
      };
}
