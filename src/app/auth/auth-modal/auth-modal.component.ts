import {
  Component,
  EventEmitter,
  Output,
  ViewChild,
} from "@angular/core";
import { ModalComponent } from "src/app/modal/modal/modal.component";
import { MODAL_CLOSING_TIME_MILLIS, ModalSize } from "src/app/modal/types";
import { Feature } from "./plans-page/feature";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";
import { CheckoutTarget } from "./checkout-page/checkout-page.component";

@Component({
  selector: "app-auth-modal",
  templateUrl: "./auth-modal.component.html",
  styleUrls: ["./auth-modal.component.scss"],
})
export class AuthModalComponent {
  @ViewChild("modal") modal?: ModalComponent;

  @Output("signin") onSignin: EventEmitter<void> = new EventEmitter<void>();

  page: "signin" | "signup" | "reset" | "plans" | "checkout" = "signin";
  size: ModalSize = "small";
  triggeredByFeature: Feature = Feature.Other;
  isOpen: boolean = false;
  checkoutTarget: CheckoutTarget = "pro-subscription"

  constructor(
    private featuresService: KwFeatureService,
  ) {}

  get isSignin(): boolean {
    return this.page === "signin";
  }

  get isSignup(): boolean {
    return this.page === "signup";
  }

  get isReset(): boolean {
    return this.page === "reset";
  }

  get isPlans(): boolean {
    return this.page === "plans";
  }

  get isCheckout(): boolean {
    return this.page === "checkout";
  }

  openSignin(): void {
    this.size = "fixed-440";
    this.page = "signin";
  }

  showSignin(): void {
    this.openSignin();
    this.showModal();
  }

  openSignup(): void {
    this.size = "fixed-440";
    this.page = "signup";
  }

  showSignup(): void {
    this.openSignup();
    this.showModal();
  }

  openReset(): void {
    this.size = "fixed-440";
    this.page = "reset";
  }

  showReset(): void {
    this.openReset();
    this.showModal();
  }

  isThreePanels(): boolean {
    const feature = "plans-modal";
    const variant = this.featuresService.getVariant(feature);

    if (!variant) {
      return false;
    }

    return variant() !== "plans-modal-old";
  }

  openPlans(params?: {
    triggeredByFeature?: Feature,
  }): void {
    if (this.isThreePanels()) {
      this.size = "fixed-865";
    } else {
      this.size = "fixed-700";
    }
    this.page = "plans";
    if (params?.triggeredByFeature) {
      this.triggeredByFeature = params.triggeredByFeature;
    }
  }

  showPlans(params?: {
    triggeredByFeature: Feature,
  }): void {
    this.openPlans(params);
    this.showModal();
  }

  openCheckout(checkoutTarget: CheckoutTarget): void {
    this.size = "fixed-700";
    this.page = "checkout";
    this.checkoutTarget = checkoutTarget;
  }

  showCheckout(): void {
    this.openCheckout(this.checkoutTarget);
    this.showModal();
  }

  showModal(): void {
    this.isOpen = true;
    this.modal?.showModal();
  }

  close(): void {
    this.modal?.close();
    setTimeout(() => this.isOpen = false, MODAL_CLOSING_TIME_MILLIS);
  }
}
