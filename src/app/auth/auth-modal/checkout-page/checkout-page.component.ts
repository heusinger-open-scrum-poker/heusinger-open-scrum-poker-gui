import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalButtonProps } from "src/app/modal/types";
import { TranslateService } from "@ngx-translate/core";

export type CheckoutTarget = "pro-subscription" | "one-day-ticket"

@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrl: './checkout-page.component.scss'
})
export class CheckoutPageComponent {
  @Input("target") target?: CheckoutTarget = "pro-subscription";
  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>;

  constructor(
    private translate: TranslateService,
  ) {}

  primaryButtonProps: ModalButtonProps = {
    label: this.translate.instant("Next"),
    buttonFlavor: "primary",
    buttonWidth: "medium",
    onClick: () => {},
    disabled: true,
  };

  get isProSubscription(): boolean {
    return !this.target || this.target === "pro-subscription";
  }

  get isOneDayTicket(): boolean {
    return this.target === "one-day-ticket";
  }
}
