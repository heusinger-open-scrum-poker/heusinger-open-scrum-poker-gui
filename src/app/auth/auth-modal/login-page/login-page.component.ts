import {
  AfterViewInit,
  Component,
  EventEmitter,
  Output,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { ModalAction, ModalActions, ModalButtonProps, ModalOptions } from "src/app/modal/types";
import { NotificationsService } from "src/app/notifications.service";
import LocalStorage from "src/app/shared/LocalStorage";
import { TranslateService } from "@ngx-translate/core";
import { UserService } from "src/app/user.service";
import { FormErrorDirective } from "src/app/modal/form-error.directive";
import { ModalFooterComponent } from "src/app/modal/modal-footer/modal-footer.component";
import { InputComponent } from "src/app/kwmc/input/input.component";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.scss"],
})
export class LoginPageComponent implements AfterViewInit {
  static DEFAULT_MODAL_OPTIONS: ModalOptions = {
    size: "fixed-440",
    hideOnOutsideClick: false,
  };

  @ViewChild("emailInput") emailInput!: InputComponent;
  @ViewChild(FormErrorDirective) errorDirective?: FormErrorDirective;
  @ViewChild(ModalFooterComponent) modalFooter?: ModalFooterComponent;

  @Output("close") onClose: EventEmitter<void> = new EventEmitter<void>;
  @Output("signin") onSignin: EventEmitter<void> = new EventEmitter<void>;
  @Output("open-signup") openSignup: EventEmitter<void> = new EventEmitter<void>;
  @Output("open-reset") openReset: EventEmitter<void> = new EventEmitter<void>;


  errorMessage?: string;

  get emailControl() {
    return this.formGroup.controls.email;
  }

  get passwordControl() {
    return this.formGroup.controls.password;
  }

  get showEmailError() {
    return this.emailControl.invalid && (this.emailControl.dirty || this.emailControl.touched);
  }

  get showPasswordError() {
    return this.passwordControl.invalid && (this.passwordControl.dirty || this.passwordControl.touched)
  }

  formGroup: FormGroup<{
    email: FormControl<string | null>;
    password: FormControl<string | null>;
  }> = new FormGroup({
    email: new FormControl("", [Validators.required]),
    password: new FormControl("", [Validators.required]),
  });

  get primaryButtonProps(): ModalButtonProps {
    return {
      label: this.translate.instant("SignIn"),
      buttonFlavor: "primary",
      buttonWidth: "medium",
      onClick: () => this.onSubmit(),
    };
  }

  constructor(
    private agileCasinoService: AgileCasinoService,
    private storage: LocalStorage,
    private notificationsService: NotificationsService,
    private translate: TranslateService,
    private userService: UserService,
  ) {}

  ngAfterViewInit(): void {
    setTimeout(() => this.emailInput.focus(), 50);
  }

  openRegisterModal() {
    this.openSignup.emit();
  }

  openPasswordResetModal() {
    this.openReset.emit();
  }

  onSubmit(): Observable<ModalAction> {
    this.formGroup.markAllAsTouched();
    this.errorMessage = undefined;

    if (this.formGroup.invalid) {
      this.errorDirective?.focusFirstError();
      return of(ModalActions.ignore);
    }

    return this.agileCasinoService
      .loginWithEmail({
        email: this.emailControl.value ?? "",
        password: this.passwordControl.value ?? "",
        userUuid: this.userService.getUser()?.uuid,
      })
      .pipe(
        map((response) => {
          const { token, uuid } = response;
          this.storage.setSessionToken(token);
          this.storage.setUuid(uuid);
          this.userService.refreshUser();
          this.onSignin.emit();
          return ModalActions.close;
        }),
        catchError((error) => {

          if (error.status == 403) {
            if (error?.error?.message?.match('^no account associated with this email:')) {

              // TODO: i18n
              this.errorMessage = "No account associated with this email address.";

            } else if (error?.error?.message?.match('^wrong authentication method:')) {

              // TODO: i18n
              this.errorMessage = "An unexpected error occured.";

            } else if (error?.error?.message?.match('^wrong password')) {

              // TODO: i18n
              this.errorMessage = "Wrong credentials.";

            } else {

              console.error(error);
              // TODO: i18n
              this.errorMessage = "An unexpected error occured, please try again";
              // TODO: i18n
              this.notificationsService.notify({
                message: "Failed to sign in.",
              });

            }
          } else {

            console.error(error);
            // TODO: i18n
            this.errorMessage = "An unexpected error occured, please try again";
            // TODO: i18n
            this.notificationsService.notify({
              message: "Failed to sign in.",
            });

          }

          return of(ModalActions.ignore);
        }),
      );
  }
}
