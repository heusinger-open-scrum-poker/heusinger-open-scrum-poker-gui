import { Injectable } from '@angular/core';
import { AgileCasinoService } from "./agile-casino.service";
import LocalStorage from "./shared/LocalStorage";
import { LanguageService } from "./language.service";
import { User } from "./schemas/User";
import { firstValueFrom } from "rxjs";
import { UserProfile } from "./schemas/UserProfile";
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private user?: User;
  private userProfile?: UserProfile;
  private features: string[] = []

  constructor(
    private agileCasinoService: AgileCasinoService,
    private storage: LocalStorage,
    private languageService: LanguageService,
  ) {
    this.agileCasinoService.getFeatures().subscribe({
      next: (features) => {
        this.features = features;
      },
    });
  }

  public userFeatureEnabled(): boolean {
    return this.features.includes('email_password_registration') && environment.loginFeature;
  }

  public getUserUuid(): string | undefined {
    return this.storage.getUuid() ?? undefined;
  }

  public getUser(): User | undefined {
    return this.user;
  }

  public getUserProfile(): UserProfile | undefined {
    return this.userProfile;
  }

  public isLoggedIn(): boolean {
    return !!this.user && this.user.role && this.user.role !== 'anonymous'
  }

  public isPro(): boolean {
    return this.user?.role === "pro"
  }

  public logout(): void {
    this.storage.logout();
    this.refreshUser();
  }

  public async refreshUser(inviterHash?: string) {
    if (!this.storage.getUuid()) {
      console.warn("no user uuid set, creating a new user");

      try {
        const user = await firstValueFrom(this.agileCasinoService.createUser(inviterHash));
        this.storage.setUuid(user.uuid);
        this.user = user;

      } catch (error) {
        console.error("failed to create user");
        throw error;
      }
    }

    try {
      const uuid = this.storage.getUuid();
      if (!uuid) {
        throw new Error("no user uuid set");
      }
      this.user = await firstValueFrom(
        this.agileCasinoService.getUser(uuid),
      );
    } catch (error) {
      console.error("failed to retrieve user");
      throw error;
    }

    try {
      if (this.user) {
        const user = await this.synchronizeUserLocale();
        if (user) {
          this.user = user;
        }
      }
    } catch (error) {
      console.error("failed to synchronize user locale");
      throw error;
    }

    this.agileCasinoService.getUserProfile(this.user.uuid).subscribe({
      next: (profile) => {
        this.userProfile = profile;
      },
      error: (error) => {
        console.log("user is not registered", error);
        this.userProfile = undefined;
      },
    });
  }

  private async synchronizeUserLocale(): Promise<User | undefined> {
    try {
      const locale = this.languageService.getLocale();
      if (
        locale &&
        this.user &&
        this.user.uuid &&
        locale !== this.user.language
      ) {
        return await this.agileCasinoService.patchUser(this.user.uuid, {
          language: locale,
        });
      }

      return;
    } catch (error) {
      console.error(error);
      return;
    }
  }
}
