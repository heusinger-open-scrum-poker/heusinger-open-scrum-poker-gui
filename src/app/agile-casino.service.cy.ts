import { HttpClientModule } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";

import { AgileCasinoService } from "./agile-casino.service";

describe("AgileCasinoService", () => {
  let service: AgileCasinoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(AgileCasinoService);
  });

  it("should be created", () => {
    expect(service).to.be.ok;
  });
});
