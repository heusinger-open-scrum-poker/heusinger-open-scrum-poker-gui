export type SimpleNotification = {
  message: string;
  title: string;
  type: "ok" | "info" | "error" | "warning";
};

export type PendingNotification = {
  message: string,
  title: string,
  type: "pending",
  pending: Promise<SimpleNotification>,
};

export type Notification = SimpleNotification | PendingNotification;
