export const Settings = {
  MAX_NAME_LENGTH: 30 as const,
  MAX_CARD_VALUE_LENGTH: 3 as const,
  MAX_ESTIMATORS: 15 as const,
  INITIAL_HOT: 3 as const,
  COUNTDOWN_TICK_DURATION: 500 as const,
  MINIMUM_CONSENSUS_PERCENT: 50 as const,
  DRAG_START_DELAY: { touch: 300, mouse: 0 } as const,
  TEMPORARY_PROMOTION_PERMISSION_TIMEOUT_MS: 300_000 as const,
};
