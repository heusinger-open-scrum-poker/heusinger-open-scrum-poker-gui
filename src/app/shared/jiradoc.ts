export type JiraDoc = {
  version: 1,
  type: "doc",
  content: Content,
}

type Content = Array<Para|Text|BulletList>;

type Para = {
  type: "paragraph",
  content: Content,
}

type Text = {
  type: "text",
  text: string,
}

type BulletList = {
  type: "bulletList",
  content: Array<ListItem>,
}

type OrderedList = {
  type: "orderedList",
  content: Array<ListItem>,
}

type ListItem = {
  type: "listItem",
  content: Content,
}

type HardBreak = {
  type: "hardBreak",
}

type CodeBlock = {
  type: "codeBlock",
  attrs: {
    language?: string
  },
  content: Content,
}

type Unknown = {
  type: "",
}

export function extractFromItem(item: Para | Text | HardBreak | BulletList | OrderedList | ListItem | CodeBlock | Unknown): string {
  switch (item.type) {
    case "paragraph":
      return item.content.map(extractFromItem).join("\n\n");
    case "text":
      return item.text;
    case "bulletList":
    case "orderedList":
    case "listItem":
      return item.content.map(extractFromItem).join("\n\n");
    case "codeBlock":
      return `
\`\`\`${item.attrs.language ?? ""}
${item.content.map(extractFromItem).join("\n\n")}
\`\`\`
`;
    case "hardBreak":
      return "\n";
  }
  console.warn(`unknown item type ${item.type}`);
  return "";
}


export function extractFromDoc(doc: JiraDoc): string {
  let buffer = "";
  doc.content.map((c) => {
    buffer += "\n" + extractFromItem(c)
  })

  return buffer;
}

export function jiraDocumentToText(doc: string | undefined | null | JiraDoc): string {
  if (!doc) {
    return "";
  }

  if (typeof(doc) === 'string') {
    return doc;
  }

  return extractFromDoc(doc);
}

export function jiraDocumentToRichText(doc: string | undefined | null | JiraDoc): string {
  if (!doc) {
    return "";
  }

  if (typeof(doc) === 'string') {
    return doc;
  }

  // TODO: rich text
  return extractFromDoc(doc);
}
