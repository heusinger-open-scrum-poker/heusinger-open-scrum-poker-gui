import { describe, test, expect } from 'vitest'
import LocalStorage from './LocalStorage';
import { SplitTest } from "../schemas/SplitTest";

describe("LocalStorage", () => {
  test('split test', () => {
    const exampleSplitTest: SplitTest = {
      feature: "someFeature",
      variants: [ "A", "B", "C" ],
    }

    const local = new LocalStorage();
    let exn = undefined;
    try {
      local.setSplitTestVariant(exampleSplitTest, "D");
    } catch (e) {
      exn = e
    }

    expect(exn).toBeDefined()
    expect(local.getSplitTestVariant(exampleSplitTest)).toEqual(null);


    local.setSplitTestVariant(exampleSplitTest, "A");
    expect(local.getSplitTestVariant(exampleSplitTest)).toEqual("A");
  })

  test('different split tests', () => {
    const exampleSplitTest: SplitTest = {
      feature: "someFeature",
      variants: [ "A", "B", "C" ],
    }

    const exampleSplitTest2: SplitTest = {
      feature: "someOtherFeature",
      variants: [ "D", "E" ],
    }

    const local = new LocalStorage();

    local.setSplitTestVariant(exampleSplitTest, "A");
    expect(local.getSplitTestVariant(exampleSplitTest)).toEqual("A");

    expect(local.getSplitTestVariant(exampleSplitTest2)).toEqual(null);
  })

  test('persistent insecure warning option', () => {
    const local = new LocalStorage();
    local.setHideInsecureWarning(true);
    expect(local.getHideInsecureWarning()).toEqual(true);
    local.setHideInsecureWarning(false);
    expect(local.getHideInsecureWarning()).toEqual(false);
  })
});
