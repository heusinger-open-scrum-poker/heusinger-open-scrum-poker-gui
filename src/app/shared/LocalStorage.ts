import { Injectable } from "@angular/core";
import { SessionToken } from "../schemas/SessionToken";
import { SplitTest } from "../schemas/SplitTest";
import { safeParseJSON } from "./util";

const ITEM_UUID = "uuid";
const ITEM_TOKEN = "sessionToken";
const SPLIT_TEST_KEY = "splitTest-";
const ITEM_HIDE_INSECURE_WARNING = "hideInsecureWarning";
const ITEM_HAS_SESSIONS = 'has-sessions';

@Injectable({
  providedIn: "root",
})
export default class LocalStorage {
  private storage = window.localStorage;

  getUuid(): string | null {
    return this.storage.getItem(ITEM_UUID);
  }

  setUuid(uuid: string): void {
    return this.storage.setItem(ITEM_UUID, uuid);
  }

  clearUuid(): void {
    this.storage.removeItem(ITEM_UUID);
  }

  getSessionToken(): SessionToken | undefined {
    const itemString = this.storage.getItem(ITEM_TOKEN);

    const item = safeParseJSON(itemString);

    if (!item) {
      return;
    }

    const validationResult = SessionToken.safeParse(item);

    if (validationResult.success) {
      return validationResult.data;
    } else {
      console.error(
        `LocalStorage: invalid \`${ITEM_TOKEN}\` object: `,
        validationResult,
        itemString,
      );
      return;
    }
  }

  setSessionToken(token: SessionToken) {
    this.storage.setItem(ITEM_TOKEN, JSON.stringify(token));
  }

  clearSessionToken() {
    this.storage.removeItem(ITEM_TOKEN);
  }

  logout() {
    this.clearUuid();
    this.clearSessionToken();
  }

  getSplitTestVariant(splitTest: SplitTest): string | null {
    const variant = this.storage.getItem(SPLIT_TEST_KEY + splitTest.feature);

    if (variant == null) {
      return null;
    }

    if (!splitTest.variants.includes(variant)) {
      return null;
    }

    return variant;
  }

  setSplitTestVariant(splitTest: SplitTest, variant: string): void {
    if (!splitTest.variants.includes(variant)) {
      throw new Error("unknown split test variant");
    }
    this.storage.setItem(SPLIT_TEST_KEY + splitTest.feature, variant);
  }

  getHideInsecureWarning(): boolean {
    return this.storage.getItem(ITEM_HIDE_INSECURE_WARNING) === "true";
  }

  setHideInsecureWarning(hide: boolean): void {
    this.storage.setItem(ITEM_HIDE_INSECURE_WARNING, hide.toString());
  }

  hasSessions(): boolean {
    return this.storage.getItem(ITEM_HAS_SESSIONS) === "true";
  }

  setHasSessions(hasSessions: boolean): void {
    this.storage.setItem(ITEM_HAS_SESSIONS, hasSessions.toString());
  }
}
