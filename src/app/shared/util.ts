import { Observable } from "rxjs";

export const toObservable = <T>(p: Promise<T>): Observable<T> => {
  return new Observable((subscriber) => {
    p.then((value) => subscriber.next(value)).catch((error) =>
      subscriber.error(error),
    );
  });
};

export function safeParseJSON(input: string | null): unknown | undefined {
  if (input == null) {
    return null;
  }

  try {
    return JSON.parse(input);
  } catch (e) {
    console.error(e);
    return;
  }
}

export function renderUrl(baseUrl: string, pathFragments?: string[], queryParams?: Record<string, string | number>): string {
  const path = baseUrl + "/" + (pathFragments ?? []).join("/");

  if (!queryParams) {
    return path;
  }

  const queryString = Object.entries(queryParams).map(([key, value]) => `${key}=${encodeURIComponent(value)}`).join("&");

  return path + "?" + queryString;
}

export function isNullish(value: unknown): boolean {
  return value == undefined || value == null;
}


export type ExtendedContextMenuEvent = Event & { overrideContextMenuBlock?: boolean };

export function handleContextMenu(event: ExtendedContextMenuEvent): boolean {
    if (event.overrideContextMenuBlock) {
      return true;
    }

    return false;
}

export function filterUndefined<T>(list: Array<T|undefined>): Array<T> {
  return list.filter((item) => item !== undefined) as Array<T>;
}

export function concat<T>(list: Array<Array<T>>): Array<T> {
  const result = [];
  for (const items of list) {
    result.push(...items);
  }
  return result;
}

export function isTouchDevice(): boolean {
  return (('ontouchstart' in window) ||
     (navigator["maxTouchPoints"] > 0))
}

export function pseudoRandomFromTimestamp(timestamp: number): number {
  const seconds_mod_60 = Math.floor(timestamp / 1000) % 60;
  const permutation_mod_60 = (seconds_mod_60 * 23) % 60;
  return permutation_mod_60/(60 - 1);
}

export async function sleep(timeMillis: number) {
  return new Promise(ok => setTimeout(ok, timeMillis));
}
