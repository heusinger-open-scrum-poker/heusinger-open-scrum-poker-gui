import { describe, it, expect } from 'vitest'
import { concat, isNullish, pseudoRandomFromTimestamp, renderUrl } from './util'

describe('renderUrl', () => {
  it('renders the base url', () => {
    expect(renderUrl("https://example.com")).toMatchSnapshot()
  })

  it('renders only path segments', () => {
    expect(renderUrl("https://example.com", ['a','b','c'])).toMatchSnapshot()
  })

  it('renders path segments and query params', () => {
    expect(renderUrl("https://example.com", ['a','b','c'], { d: 'e', f: 123 })).toMatchSnapshot()
    expect(renderUrl("https://example.com", ['a','b','c'], { d: '*&)#@)(^$!&*(@$^!@', f: 123 })).toMatchSnapshot()
  })

  it('renders only query params', () => {
    expect(renderUrl("https://example.com", [], { d: 'e', f: 123 })).toMatchSnapshot()
    expect(renderUrl("https://example.com", undefined, { d: 'e', f: 123 })).toMatchSnapshot()
  })

  it('detects null or undefined', () => {
    expect(isNullish("")).to.be.false
    expect(isNullish([])).to.be.false
    expect(isNullish(0)).to.be.false
    expect(isNullish(NaN)).to.be.false
    expect(isNullish(null)).to.be.true
    expect(isNullish(undefined)).to.be.true
  })
})

describe('concat', () => {
  it('concats the empty list', () => {
    expect(concat([])).toEqual([]);
  }),
  it('concats multiple lists into single list', () => {
    expect(concat([[0],[1,2],[],[3]])).toEqual([0,1,2,3]);
  })
})

describe('pseudoRandomFromTimestamp', () => {
  it('produces unique values within a minute', () => {
    const sixty = Array.from({length: 60}, (_, i) => i);
    const expectedValues = sixty.map(it => it / 59);
    const producedValues = [];

    for (let i = 0; i < 60; ++i) {
      const time = (new Date(`2024-10-25T10:16:${i.toString().padStart(2, '0')}.0Z`)).getTime();
      const rand = pseudoRandomFromTimestamp(time);
      producedValues.push(rand);
    }

    for (const produced of producedValues) {
      expect(0 <= produced).toEqual(true);
      expect(produced <= 1).toEqual(true);
      expect(expectedValues).toContain(produced);
    }

    for (const valid of expectedValues) {
      expect(producedValues).toContain(valid);
    }
  })
})
