import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Notification, PendingNotification } from "./shared/Notification.class";

@Injectable({
  providedIn: "root",
})
export class NotificationsService {
  public subject$: Subject<Notification>;
  public subscribe: typeof this.subject$.subscribe;

  constructor() {
    this.subject$ = new Subject<Notification>();
    this.subscribe = this.subject$.subscribe;
  }

  public notify(options: Partial<Notification>) {
    const notification = this.makeNotification(options);

    this.subject$.next(notification);
  }

  private makeNotification(options: Partial<Notification>): Notification {
    const { message = "", title = "", type = "info"} = options;

    if (!message) {
      console.warn("No message defined");
    }

    let notification: Notification;

    if (type === "pending") {
      const pending = (options as PendingNotification).pending;
      if (!pending) {
        throw new Error("pending notification requires the `pending` field to be set");
      }
      notification = {
        message,
        title,
        type,
        pending,
      };
    } else {
      notification = {
        message,
        title,
        type,
      };
    }

    return notification;
  }
}
