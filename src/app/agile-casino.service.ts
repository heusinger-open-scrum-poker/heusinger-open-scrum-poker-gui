import { Injectable } from "@angular/core";
import { firstValueFrom, Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../environments/environment";
import { catchError, map, mergeMap } from "rxjs/operators";
import LocalStorage from "./shared/LocalStorage";
import { SessionToken } from "./schemas/SessionToken";
import { UserProfile } from "./schemas/UserProfile";
import { User } from "./schemas/User";
import { Room, RoomReference, RoomReferenceLike } from "./schemas/Room";
import { Player } from "./schemas/Player";
import { Feedback } from "./schemas/Feedback";
import { Task } from "./schemas/Task";
import { renderUrl } from "./shared/util";
import { z, ZodType } from "zod";
import { JiraBoard, JiraIssue, JiraProject, JiraResource, JiraSprint } from "./schemas/jira";
import { Subtask } from "./schemas/Subtask";
import { AiSubtaskEstimate, AiSubtaskEstimateQuota } from "./schemas/AiSubtaskEstimate";
import { Game, GameWithTasks, ImportParameters } from "./schemas/Game";
import { SubtaskOperation, TaskOperation } from "./schemas/TaskOperation";
import { Round } from "./schemas/Round";
import { Nav, NavData } from "./room/tasks/nav-data";

@Injectable({
  providedIn: "root",
})
export class AgileCasinoService {
  constructor(
    private http: HttpClient,
    private storage: LocalStorage,
  ) {}

  private baseUrl = environment.apiBaseUrl;

  private httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };

  createUser(hash?: string): Observable<User> {
    const queryParams = hash ? { invited_by_hash: hash } : undefined;
    return this.http.post<User>(
      this.url(["user"], queryParams),
      {},
      this.httpOptions,
    );
  }

  getUser(uuid: string): Observable<User> {
    return this.http.get<User>(this.url(["user", uuid]));
  }

  postRoom(): Observable<Room> {
    return this.http.post<Room>(
      this.url(["room"], this.queryParamsWithUserUuid()),
      {},
      this.httpOptions,
    );
  }

  getRoom(roomId: RoomReferenceLike): Observable<Room> {
    const roomRef = new RoomReference(roomId);
    return this.getWithValidation({
      schema: Room,
      path: ["room", roomRef.toString()],
      withUserUuid: true,
    });
  }

  getPlayer(playerId: number): Observable<Player> {
    return this.http.get<Player>(
      this.url(
        ["players", playerId.toString()],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  putPlayer(player: Player): Observable<Player> {
    return this.http.put<Player>(
      this.url(
        ["players", player.id.toString()],
        this.queryParamsWithUserUuid(),
      ),
      player,
      this.httpOptions
    );
  }

  patchPlayer(player: Partial<Player> & { id: Player["id"] }): Observable<Player> {
    return this.http.patch<Player>(
      this.url(
        ["players", player.id.toString()],
        this.queryParamsWithUserUuid(),
      ),
      player,
      this.httpOptions
    );
  }

  deletePlayer(playerId: Player["id"]): Observable<void> {
    return this.http.delete<void>(
      this.url(
        ["players", playerId.toString()],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  sidelinePlayer(playerId: Player["id"]): Observable<void> {
    return this.postWithValidation({
      path: ["players", playerId.toString(), "sideline"],
      schema: z.any(),
      withUserUuid: true,
    });
  }

  promotePlayer(playerId: Player["id"]): Observable<null> {
    return this.postWithValidation({
      path: ["players", playerId.toString(), "promote"],
      schema: z.null(),
      withUserUuid: true,
    });
  }

  postPlayerOfRoom(player: Player | Omit<Player, "id">, roomId: RoomReferenceLike): Observable<Player> {
    const roomRef = new RoomReference(roomId);

    return this.http.post<Player>(
      this.url(
        ["room", roomRef.toString(), "players"],
        this.queryParamsWithUserUuid(),
      ),
      player,
      this.httpOptions
    );
  }

  getPlayersOfRoom(roomId: RoomReferenceLike): Observable<Player[]> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Player[]>(
      this.url(
        ["room", roomRef.toString(), "players"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  getRoomAndPlayers(roomId: RoomReferenceLike): Observable<readonly [Room, Player[]]> {
    return this.getRoom(roomId).pipe(
      mergeMap((room) =>
        this.getPlayersOfRoom(roomId).pipe(
          map((players) => {
            if (!room) {
              throw new Error("room does not exist");
            }
            return [room, players] as const;
          }),
        ),
      ),
    );
  }

  createRoomAndPlayer(player: Player | Omit<Player, "id">): Observable<readonly [Room, Player]> {
    return this.postRoom()
      .pipe(
        mergeMap((room) =>
          this.postPlayerOfRoom(player, room.id).pipe(
            map((player) => [room.id, player] as const),
          ),
        ),
      )
      .pipe(
        mergeMap(([roomId, player]) =>
          this.getRoom(roomId).pipe(
            map((room) => [room, player] as [Room, Player]),
          ),
        ),
      );
  }

  joinRoomAndCreatePlayer(
    roomId: RoomReferenceLike,
    player: Player | Omit<Player, "id">,
  ): Observable<readonly [Room, Player]> {
    const roomRef = new RoomReference(roomId);

    return this.postPlayerOfRoom(player, roomRef).pipe(
      mergeMap((player) =>
        this.getRoom(roomRef).pipe(map((room) => [room, player] as const)),
      ),
    );
  }

  joinRoomAndPutPlayer(
    roomId: RoomReferenceLike,
    player: Player,
  ): Observable<readonly [Room, Player]> {
    return this.putPlayer(player).pipe(
      mergeMap((player) =>
        this.getRoom(roomId).pipe(map((room) => [room, player] as const)),
      ),
    );
  }

  sendFeedback(content: string, email?: string): Observable<Feedback> {
    const userUuid = this.storage.getUuid();
    const body = { content, userUuid, email };

    return this.http.post<Feedback>(
      this.url(
        ["feedback"],
      ),
      body,
      this.httpOptions
    );
  }

  patchFeedback(
    id: Feedback["id"],
    content?: string,
    email?: string,
  ): Observable<Feedback> {
    const body = { id, content, email };

    return this.http.patch<Feedback>(
      this.url(
        ["feedback"],
        this.queryParamsWithUserUuid(),
      ),
      body,
      this.httpOptions
    );
  }

  updateRoom(room: Pick<Room, "id" | "accessToken">): Observable<Room> {
    const roomRef = new RoomReference(room.accessToken ?? room.id);

    return this.http.put<Room>(
      this.url(
        ["room", roomRef.toString()],
        this.queryParamsWithUserUuid(),
      ),
      room,
      this.httpOptions
    );
  }

  getSeries(roomId: RoomReferenceLike): Observable<string[]> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<string[]>(
      this.url(
        ["room", roomRef.toString(), "series"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions,
    );
  }

  setSeries(
    roomId: RoomReferenceLike,
    series: string,
    playerId: Player["id"],
  ): Observable<void> {
    const roomRef = new RoomReference(roomId);

    return this.http.put<void>(
      this.url(
        ["room", roomRef.toString(), "series"],
        this.queryParamsWithUserUuid({
          playerId: playerId
        })
      ),
      series,
      this.httpOptions,
    );
  }

  getAvatars(
    roomId: RoomReferenceLike,
  ): Observable<Array<{ avatar: string; available: boolean }>> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Array<{ avatar: string; available: boolean }>>(
      this.url(
        ["room", roomRef.toString(), "avatars"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions,
    );
  }

  getFeatures(): Observable<Array<string>> {
    return this.http
      .get<Array<string>>(this.url(["features"]), this.httpOptions)
      .pipe(catchError(() => of([])));
  }

  loginWithEmail(params: {
    email: string;
    password: string;
    userUuid?: string;
  }): Observable<{ uuid: string; token: SessionToken }> {
    const body: { email: string; password: string; userUuid?: string } = params;

    return this.http.post<{ uuid: string; token: SessionToken }>(
      this.url(["login"]),
      body,
      this.httpOptions,
    );
  }

  registerWithEmail(params: {
    email: string;
    password: string;
    userUuid?: string;
  }): Observable<{ uuid: string; token: SessionToken }> {
    let body: { email: string; password: string; userUuid?: string } = params;
    try {
      const uuid = this.storage.getUuid();

      if (!uuid) {
        throw Error('no user uuid defined')
      }

      body = { ...params, userUuid: uuid };
    } catch (error) {
      console.debug(error);
    }
    return this.http.post<{ uuid: string; token: SessionToken }>(
      this.url(["register"]),
      body,
      this.httpOptions,
    );
  }

  validateEmail(email: string): Observable<boolean> {
    return this.postWithValidation({
      path: ["email_validity_check"],
      query: { email },
      schema: z.boolean(),
    });
  }

  getUserProfile(userUuid: string): Observable<UserProfile> {
    return this.http.get<UserProfile>(
      this.url(["user", userUuid, "profile"]),
      this.httpOptions,
    );
  }

  getJoinedRooms(
    userUuid: string,
  ): Observable<Array<{ room: Room; player: Player; numPlayers: number }>> {
    return this.http.get<
      Array<{ room: Room; player: Player; numPlayers: number }>
    >(this.url(["user", userUuid, "joinedRooms"]), this.httpOptions);
  }

  async patchUser(userUuid: string, user: Partial<User>): Promise<User> {
    return firstValueFrom(
      this.http.patch<User>(
        this.url(["user", userUuid]),
        user,
        this.httpOptions,
      ),
    ).catch((e) => {
      console.error(`PATCH /user/${userUuid} failed`);
      throw e;
    });
  }

  getTasks(roomId: RoomReferenceLike): Observable<Array<Task>> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Array<Task>>(
      this.url(
        ["room", roomRef.toString(), "tasks"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  createTaskWithSubtasks(roomId: RoomReferenceLike, task: Task, subtasks: Array<Subtask>): Observable<void> {
    const roomRef = new RoomReference(roomId);

    return this.http.post<Task>(
      this.url(
        ["room", roomRef.toString(), "task"],
        this.queryParamsWithUserUuid(),
      ),
      task,
      this.httpOptions
    ).pipe(
      mergeMap((response: Task) => {
        const subtasksWithTaskId = subtasks.map((subtask) => ({ ...subtask, taskId: response.id }));
        return this.updateTaskWithSubtasks(roomId, response, subtasksWithTaskId);
      })
    )
  }

  batchUpdateTasks(roomId: RoomReferenceLike, batchOperations: Array<TaskOperation>): Observable<Array<Task>> {
    return this.patchWithValidation({
        path: ["room", roomId.toString(), "task-batch"],
        schema: z.array(Task),
        body: batchOperations,
        withUserUuid: true,
    });
  }


  batchUpdateSubtasks(roomId: RoomReferenceLike, batchOperations: Array<SubtaskOperation>): Observable<Array<Subtask>> {
    return this.patchWithValidation({
        path: ["room", roomId.toString(), "subtask-batch"],
        schema: z.array(Subtask),
        body: batchOperations,
        withUserUuid: true,
    });
  }

  updateTaskWithSubtasks(roomId: RoomReferenceLike, task: Task, subtasks: Array<Subtask>): Observable<void> {
    const roomRef = new RoomReference(roomId);

    const batchOperations: Array<SubtaskOperation> = [];

    for (const subtask of (subtasks ?? [])) {
      const deleted = subtask.deleted;
      let subtaskCopy = subtask;
      if (deleted !== undefined) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { deleted, ...rest } = subtask as Subtask & { deleted: boolean };
        subtaskCopy = rest;

      }

      if (deleted) {
        batchOperations.push({
          type: "delete",
          subtask: subtaskCopy,
        });
      } else {
        if (subtask.id) {
          batchOperations.push({
            type: "update",
            subtask: subtaskCopy,
          });
        } else {
          batchOperations.push({
            type: "create",
            subtask: subtaskCopy,
          });
        }
      }
    }

    const beforeTaskUpdate = batchOperations.length > 0 ? this.batchUpdateSubtasks(roomId, batchOperations).pipe(map(() => undefined)) : of(undefined);

    return beforeTaskUpdate.pipe(
      mergeMap(() => {
        return this.http.put<void>(
          this.url(
            ["room", roomRef.toString(), "task", task.id.toString()],
            this.queryParamsWithUserUuid(),
          ),
          task,
          this.httpOptions
        );
      }),
    );
  }

  deleteTask(roomId: RoomReferenceLike, taskId: Task["id"]): Observable<void> {
    const roomRef = new RoomReference(roomId);

    return this.http.delete<void>(
      this.url(
        ["room", roomRef.toString(), "task", taskId.toString()],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  setCurrentTask(roomId: RoomReferenceLike, taskId: Task["id"]): Observable<void> {
    return this.http.put<void>(
      this.url(
        ["room", roomId.toString(), "task", taskId.toString(), "makeCurrent"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  getSubtasks(roomId: RoomReferenceLike, taskId: Task["id"]): Observable<Array<Subtask>> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Array<Subtask>>(
      this.url(
        ["room", roomRef.toString(), "task", taskId.toString(), "subtasks"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  createSubtask(roomId: RoomReferenceLike, subtask: Subtask): Observable<Subtask> {
    const roomRef = new RoomReference(roomId);

    return this.http.post<Subtask>(
      this.url(
        ["room", roomRef.toString(), "subtask"],
        this.queryParamsWithUserUuid(),
      ),
      subtask,
      this.httpOptions
    );
  }

  updateSubtask(roomId: RoomReferenceLike, subtask: Subtask): Observable<Subtask> {
    if (!subtask.id) {
      throw new Error("subtask is missing an id");
    }

    const roomRef = new RoomReference(roomId);

    return this.http.put<Subtask>(
      this.url(
        ["room", roomRef.toString(), "subtask", subtask.id.toString()],
        this.queryParamsWithUserUuid(),
      ),
      subtask,
      this.httpOptions
    );
  }

  deleteSubtask(roomId: RoomReferenceLike, subtask: Subtask): Observable<void> {
    if (!subtask.id) {
      throw new Error("subtask is missing an id");
    }

    const roomRef = new RoomReference(roomId);

    return this.http.delete<void>(
      this.url(
        ["room", roomRef.toString(), "subtask", subtask.id.toString()],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  lockRoom(roomId: RoomReferenceLike): Observable<Room> {
    return this.setLockedRoom(roomId, true);
  }

  unlockRoom(roomId: RoomReferenceLike): Observable<Room> {
    return this.setLockedRoom(roomId, false);
  }

  setLockedRoom(roomId: RoomReferenceLike, locked: boolean): Observable<Room> {
    const roomRef = new RoomReference(roomId);

    return this.http.put<Room>(
      this.url(
        ["room", roomRef.toString(), "locked"],
        this.queryParamsWithUserUuid(),
      ),
      locked,
      this.httpOptions
    );
  }

  requestPasswordReset(args: { email: string }): Observable<void> {
    return this.http.post<void>(
      this.url(
        ["request_password_reset"],
        {
          email: args.email,
        }
      ),
      {},
      this.httpOptions)
  }

  resetPassword(args: { email: string, resetToken: string, newPassword: string}): Observable<void> {
    return this.http.post<void>(
      this.url(
        ["reset_password"],
        {
          email: args.email,
          token: args.resetToken.trim(),
          new_password: args.newPassword,
        }
      ),
      {},
      this.httpOptions)
  }

  jiraGenerateState(): Observable<string> {
    return this.postWithValidation({
      path: ["jira", "oauth", "state"],
      schema: z.string().min(1),
      withUserUuid: true,
    })
  }

  jiraSubmitAuthCode(code: string, state: string): Observable<void> {
    return this.getWithValidation({
      path: ["jira", "oauth", "submit_auth_code"],
      schema: z.any(),
      query: { code, state },
      withUserUuid: false,
    })
  }

  jiraResources(): Observable<Array<JiraResource>> {
    return this.getWithValidation({
      path: ["jira", "resources"],
      schema: z.array(JiraResource),
      withUserUuid: true,
    })
  }

  jiraProjects(resourceId: string): Observable<Array<JiraProject>> {
    return this.getWithValidation({
      path: ["jira", "projects"],
      schema: z.array(JiraProject),
      query: {
        resource_id: resourceId,
      },
      withUserUuid: true,
    })
  }

  jiraIssues(resourceId: string, projectKey: string): Observable<Array<JiraIssue>> {
    return this.getWithValidation({
      path: ["jira", "issues"],
      schema: z.array(JiraIssue),
      query: {
        resource_id: resourceId,
        project_key: projectKey,
      },
      withUserUuid: true,
    })
  }

  jiraBoards(resourceId: string): Observable<Array<JiraBoard>> {
    return this.getWithValidation({
      path: ["jira", "boards"],
      schema: z.array(JiraBoard),
      query: {
        resource_id: resourceId,
      },
      withUserUuid: true,
    })
  }

  jiraSprints(resourceId: string, projectId: string): Observable<Array<JiraSprint>> {
    return this.getWithValidation({
      path: ["jira", "sprints"],
      schema: z.array(JiraSprint),
      query: {
        resource_id: resourceId,
        project_id: projectId,
      },
      withUserUuid: true,
    })
  }


  jiraIssues2(resourceId: string, boardId: string, sprintId: string): Observable<Array<JiraIssue>> {
    return this.getWithValidation({
      path: ["jira", "issues2"],
      schema: z.array(JiraIssue),
      query: {
        resource_id: resourceId,
        board_id: boardId,
        sprint_id: sprintId,
      },
      withUserUuid: true,
    })
  }

  disconnectJira(): Observable<void> {
    return this.postWithValidation({
      path: ["jira", "disconnect"],
      schema: z.any(),
      withUserUuid: true,
    })
  }

  generateAiSubtasks(summary: string): Observable<Array<AiSubtaskEstimate>> {
    return this.postWithValidation({
      path: ["ai", "subtaskEstimate"],
      schema: z.array(AiSubtaskEstimate),
      query: {
        summary,
      },
      withUserUuid: true,
    })
  }

  getAiSubtasksQuota(): Observable<AiSubtaskEstimateQuota> {
    return this.getWithValidation({
      path: ["ai", "quota"],
      schema: AiSubtaskEstimateQuota,
      withUserUuid: true,
    })
  }

  getImportModalState(): Observable<unknown> {
    const uuid = this.storage.getUuid();

    if (!uuid) {
      throw new Error("no user uuid set");
    }

    return this.getWithValidation({
      path: ["user", uuid, "importModalState"],
      schema: z.any(),
      withUserUuid: true,
    })
  }

  putImportModalState(navdata: NavData): Observable<NavData> {
    const uuid = this.storage.getUuid();

    if (!uuid) {
      throw new Error("no user uuid");
    }

    return this.putWithValidation({
      path: ["user", uuid, "importModalState"],
      schema: NavData,
      withUserUuid: true,
      body: navdata,
    })
  }

  url(pathFragments: string[], queryParams?: Record<string, string | number>): string {
    return renderUrl(this.baseUrl, pathFragments, queryParams);
  }

  queryParamsWithUserUuid(params?: Record<string, string | number>): Record<string, string | number> {
    try {
      const uuid = this.storage.getUuid();
      if (!uuid) {
        throw new Error("no user uuid set");
      }
      return { ...(params ?? {}), user_uuid: uuid };
    } catch (error) {
      console.debug(error);
      return params ?? {};
    }
  }

  getWithValidation<SCHEMA extends ZodType>(params: {
    path: string[],
    query?: Record<string, string | number>,
    withUserUuid?: boolean,
    schema: SCHEMA,
  }): Observable<z.infer<SCHEMA>> {
    const query = params.query ?? {};
    return this.http.get<z.infer<SCHEMA>>(
      this.url(
        params.path,
        params.withUserUuid ? this.queryParamsWithUserUuid(query) : query,
      ),
      this.httpOptions).pipe(
        map((value) => {
          return params.schema.parse(value)
        })
      )
  }

  postWithValidation<SCHEMA extends ZodType, BODY>(params: {
    path: string[],
    query?: Record<string, string | number>,
    body?: BODY,
    withUserUuid?: boolean,
    schema: SCHEMA,
  }): Observable<z.infer<SCHEMA>> {
    const query = params.query ?? {};
    return this.http.post<z.infer<SCHEMA>>(
      this.url(
        params.path,
        params.withUserUuid ? this.queryParamsWithUserUuid(query) : query,
      ),
      params.body,
      this.httpOptions).pipe(
        map((value) => {
          return params.schema.parse(value)
        })
      )
  }

  patchWithValidation<SCHEMA extends ZodType, BODY>(params: {
    path: string[],
    query?: Record<string, string | number>,
    body?: BODY,
    withUserUuid?: boolean,
    schema: SCHEMA,
  }): Observable<z.infer<SCHEMA>> {
    const query = params.query ?? {};
    return this.http.patch<z.infer<SCHEMA>>(
      this.url(
        params.path,
        params.withUserUuid ? this.queryParamsWithUserUuid(query) : query,
      ),
      params.body,
      this.httpOptions).pipe(
        map((value) => {
          return params.schema.parse(value)
        })
      )
  }

  getGames(roomId: RoomReferenceLike): Observable<Array<Game>> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Array<Game>>(
      this.url(
        ["room", roomRef.toString(), "games"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  getActiveGame(roomId: RoomReferenceLike): Observable<Game> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Game>(
      this.url(
        ["room", roomRef.toString(), "games", "active"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  getArchivedGames(roomId: RoomReferenceLike): Observable<Array<Game>> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Array<Game>>(
      this.url(
        ["room", roomRef.toString(), "games", "archived"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  createGame(roomId: RoomReferenceLike): Observable<Game> {
    const roomRef = new RoomReference(roomId);

    return this.http.post<Game>(
      this.url(
        ["room", roomRef.toString(), "game"],
        this.queryParamsWithUserUuid(),
      ),
      {},
      this.httpOptions
    );
  }

  activateGame(roomId: RoomReferenceLike, gameId: number): Observable<Game> {
    const roomRef = new RoomReference(roomId);

    return this.http.patch<Game>(
      this.url(
        ["room", roomRef.toString(), "game", gameId.toString(), "activate"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions,
    );
  }

  getTasksForGame(roomId: RoomReferenceLike, gameId: number): Observable<Array<Task>> {
    const roomRef = new RoomReference(roomId);

    return this.http.get<Array<Task>>(
      this.url(
        ["room", roomRef.toString(), "game", gameId.toString(), "tasks"],
        this.queryParamsWithUserUuid(),
      ),
      this.httpOptions
    );
  }

  getGamesWithTasks(roomId: RoomReferenceLike): Observable<Array<GameWithTasks>> {
    const roomRef = new RoomReference(roomId);

    return this.getWithValidation({
      path: ["room", roomRef.toString(), "games-with-tasks-and-subtasks"],
      schema: z.array(GameWithTasks),
      withUserUuid: true,
    });

  }

  exportGame(roomId: RoomReferenceLike, gameId: Game["id"], resource_id: string, board_id: string, sprint_id: number): Observable<void> {
    const roomRef = new RoomReference(roomId);

    return this.postWithValidation({
      path: ["room", roomRef.toString(), "game", gameId.toString(), "jira", "export"],
      schema: z.any(),
      query: {
        resource_id,
        board_id,
        sprint_id,
      },
      withUserUuid: true,
    });
  }

  getIsJiraConnected(): Observable<{isConnected?: boolean}> {
    return this.getWithValidation({
      path: ["jira", "isConnected"],
      schema: z.object({isConnected: z.boolean()}),
      withUserUuid: true,
    });
  }

  getImportParameters(roomId: RoomReferenceLike, gameId: Game["id"]): Observable<ImportParameters | null | undefined> {
    const roomRef = new RoomReference(roomId);

    return this.getWithValidation({
      path: ["room", roomRef.toString(), "game", gameId.toString(), "importParameters"],
      schema: ImportParameters.nullable(),
      withUserUuid: true,
    });
  }

  putImportParameters(roomId: RoomReferenceLike, gameId: Game["id"], importParameters: ImportParameters): Observable<void> {
    const roomRef = new RoomReference(roomId);

    return this.putWithValidation({
      path: ["room", roomRef.toString(), "game", gameId.toString(), "importParameters"],
      schema: z.any(),
      withUserUuid: true,
      body: importParameters,
    });
  }

  putWithValidation<SCHEMA extends ZodType, BODY>(params: {
    path: string[],
    query?: Record<string, string | number>,
    body?: BODY,
    withUserUuid?: boolean,
    schema: SCHEMA,
  }): Observable<z.infer<SCHEMA>> {
    const query = params.query ?? {};
    return this.http.put<z.infer<SCHEMA>>(
      this.url(
        params.path,
        params.withUserUuid ? this.queryParamsWithUserUuid(query) : query,
      ),
      params.body,
      this.httpOptions).pipe(
      map((value) => {
        return params.schema.parse(value)
      })
    )
  }

  startRound(roomId: RoomReferenceLike):  Observable<Round> {
    const roomRef = new RoomReference(roomId);

    return this.http.post<Round>(
      this.url(
        ["room", roomRef.toString(), "round", "start"],
        this.queryParamsWithUserUuid(),
      ),
      {},
      this.httpOptions
    );
  }

  endRound(roomId: RoomReferenceLike, consensus: string): Observable<Round> {
    const roomRef = new RoomReference(roomId);

    return this.http.patch<Round>(
      this.url(
        ["room", roomRef.toString(), "round", "end"],
        this.queryParamsWithUserUuid(),
      ),
      {
        consensus: consensus
      },
      this.httpOptions
    );
  }
}
