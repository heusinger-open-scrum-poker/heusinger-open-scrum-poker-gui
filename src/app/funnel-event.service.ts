import { Injectable } from "@angular/core";
import { ActivatedRoute, Data, NavigationEnd, Router } from "@angular/router";
import { filter, map, mergeMap, tap } from "rxjs/operators";
import { Advertisement } from "./schemas/Advertisement";
import { SeoService } from "./seo.service";

const GOOGLE_ANALYTICS_ID = "AW-981673838";
const GOOGLE_CONVERSION_ID = "AW-981673838";
const GOOGLE_CONVERSION_LABEL = "C0wBCPyh89sBEO7OjNQD";
const GOOGLE_ADS_ID = "G-C8CXRGVRP8";

@Injectable({
  providedIn: "root",
})
export class FunnelEventService {
  constructor(
    private activatedRoute: ActivatedRoute,
    private metaService: SeoService,
  ) {
    window.dataLayer = window.dataLayer || Array.from({length: 0});

    this.gtag("js", new Date());
    this.gtag("config", GOOGLE_ANALYTICS_ID);
    this.gtag("config", GOOGLE_ADS_ID);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  gtag(...args: unknown[]) {
    // eslint-disable-next-line prefer-rest-params
    console.info("gtag with arguments", arguments);
    // eslint-disable-next-line prefer-rest-params
    window.dataLayer?.push(arguments);
  }

  public listenToRouterEvents(router: Router) {
    router.events
      .pipe(filter((event) => event instanceof NavigationEnd), map((event) => event as NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.gtag("config", GOOGLE_ANALYTICS_ID, {
          page_path: event.urlAfterRedirects,
        });

        const roomUrlRegex = /^\/room\/\d+/;

        if (roomUrlRegex.test(event.urlAfterRedirects)) {
          this.trackConversionEvent();
        }
      });


    router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter((route) => route.outlet === 'primary'),
        mergeMap((route) => route.data),
        tap(({ title, description, ogImage }: Data) => {
          this.metaService.updateTitle(title);
          this.metaService.updateDescription(description);
          this.metaService.updateOgImage(ogImage);
        })
      ).subscribe();
  }

  public trackConversionEvent() {
    this.gtag("event", "conversion", {
      send_to: `${GOOGLE_CONVERSION_ID}/${GOOGLE_CONVERSION_LABEL}`,
    });
  }

  public trackAdClickEvent(ad: Advertisement) {
    this.gtag("event", "adrotclick", {
      headline: ad.headline,
    });
  }

  public smokeTestEvent(feature: string, action: string) {
    this.gtag("event", "smoke-test: " + feature, {
      action: action,
    });
  }

  public splitTestEvent(feature: string, variant: string) {
    this.gtag("event", "split-test: " + feature, {
      variant: variant,
    });
  }
}
