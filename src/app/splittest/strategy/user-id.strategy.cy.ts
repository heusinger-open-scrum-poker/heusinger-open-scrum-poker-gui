import { HttpClientModule } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import LocalStorage from "../../shared/LocalStorage";
import { SplitTest } from "../../schemas/SplitTest";
import { UserIdStrategy } from "./user-id.strategy";

describe("UserIdStrategy", () => {
  let service: UserIdStrategy;
  let storage: LocalStorage;
  const splitTest: SplitTest = {
    feature: 'some feature',
    variants: ['A', 'B', 'C', 'D'],
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(UserIdStrategy);
    storage = TestBed.inject(LocalStorage);
  });

  it("should be created", () => {
    expect(service).to.be.ok;
  });

  it("should return variant with index userid mod #variants", () => {
    storage.setUuid('b0097806-8c1b-4b8e-bb76-8601cd52721e');
    const variant = service.getVariant(splitTest);
    expect(splitTest.variants.indexOf(variant)).to.eql(3);
  })
});
