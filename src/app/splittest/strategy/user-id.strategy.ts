import LocalStorage from "src/app/shared/LocalStorage";
import { SplitTest } from "../../schemas/SplitTest";
import { Injectable } from "@angular/core";
import { LocalStorageStrategy } from "./local-storage.strategy";
import { SplitTestStrategy } from ".";

@Injectable({
  providedIn: "root",
})
export class UserIdStrategy implements SplitTestStrategy {
  constructor(
    private storage: LocalStorage,
    private fallback: LocalStorageStrategy,
  ) {}

  getVariant(splitTest: SplitTest): string {
    const uuid = this.storage.getUuid();
    if (uuid) {
      const numberOfVariants = splitTest.variants.length;
      const hexId = uuid.replace('-', '');
      const index = parseInt(hexId, 16) % numberOfVariants;

      return splitTest.variants[index];
    } else {
      return this.fallback.getVariant(splitTest);
    }
  }
}
