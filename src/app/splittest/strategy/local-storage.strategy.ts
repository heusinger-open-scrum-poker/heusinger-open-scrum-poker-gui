import { SplitTest } from "../../schemas/SplitTest";
import { Injectable } from "@angular/core";
import LocalStorage from "../../shared/LocalStorage";
import { SplitTestStrategy } from ".";

@Injectable({
  providedIn: "root",
})
export class LocalStorageStrategy implements SplitTestStrategy {
  constructor(
    private storage: LocalStorage,
  ) {}

  getVariant(splitTest: SplitTest): string {
    let variant = this.storage.getSplitTestVariant(splitTest);

    if (!variant) {
      const index = Math.floor(Math.random() * splitTest.variants.length);
      variant = splitTest.variants[index];
      this.storage.setSplitTestVariant(splitTest, variant);
    }

    return variant;
  }
}
