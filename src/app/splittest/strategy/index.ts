import { SplitTest } from "src/app/schemas/SplitTest";

export interface SplitTestStrategy {
  getVariant(splitTest: SplitTest): string;
}
