import { HttpClientModule } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { LocalStorageStrategy } from "./local-storage.strategy";
import LocalStorage from "../../shared/LocalStorage";
import { SplitTest } from "../../schemas/SplitTest";

describe("LocalStorageStrategy", () => {
  let service: LocalStorageStrategy;
  let storage: LocalStorage;
  const splitTest: SplitTest = {
    feature: 'some feature',
    variants: ['variant A', 'variant B']
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(LocalStorageStrategy);
    storage = TestBed.inject(LocalStorage);
  });

  it("should be created", () => {
    expect(service).to.be.ok;
  });

  it("should be set up on first call", () => {
    expect(storage.getSplitTestVariant(splitTest)).to.be.null;
    const variant = service.getVariant(splitTest);
    expect(storage.getSplitTestVariant(splitTest)).to.eql(variant);
  })
});
