import { Injectable } from "@angular/core";
import { LocalStorageStrategy } from "./strategy/local-storage.strategy";
import { SplitTest } from "../schemas/SplitTest";
import { UserIdStrategy } from "./strategy/user-id.strategy";
import { FunnelEventService } from "../funnel-event.service";

export const SPLIT_TESTS: {
  _STARTPAGE: SplitTest,
  PRO: SplitTest,
} = {
  _STARTPAGE: {
    feature: "startpage",
    variants: ["old", "new"],
  },
  PRO: {
    feature: "pro",
    variants: ["even-id", "odd-id"],
  },
};

@Injectable({
  providedIn: "root",
})
export class SplitTestService {
  constructor(
    private localStorageStrategy: LocalStorageStrategy,
    private userIdStrategy: UserIdStrategy,
    private funnelEventService: FunnelEventService,
  ) {
  }

  getProVariant(): string {
    return this.userIdStrategy.getVariant(SPLIT_TESTS.PRO);
  }

  triggerFunnelEventPro(): void {
    const variant = this.userIdStrategy.getVariant(SPLIT_TESTS.PRO);

    this.funnelEventService.splitTestEvent(SPLIT_TESTS.PRO.feature, variant);
  }
}
