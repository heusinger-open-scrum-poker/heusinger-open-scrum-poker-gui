import { Component } from '@angular/core';
import { KwFeatureService } from '@kehrwasser-dev/features-angular';

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrl: './startpage.component.scss'
})
export class StartpageComponent {
  constructor(
    private featureService: KwFeatureService,
  ) {}

  get startpageVariant(): "new" | "old" {
    switch (this.featureService.getVariant("E204-startpage")?.()) {
      case "new":
        return "new"
      default:
        return "old"
    }
  }
}
