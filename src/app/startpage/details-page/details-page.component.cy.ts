import { MountConfig } from "cypress/angular";
import { DetailsPageComponent } from "./details-page.component";
import { ActivatedRoute } from "@angular/router";
import { of } from "rxjs";

const params = {
  content: "example"
}

describe("DetailsPageComponent", () => {
  const config: MountConfig<DetailsPageComponent> = {
    imports: [],
    declarations: [],
    providers: [
      {
        provide: ActivatedRoute,
        useValue: {
          queryParams: of(params),
        },
      },
    ],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(DetailsPageComponent, config);
  });
});
