import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-details-page",
  templateUrl: "./details-page.component.html",
  styleUrl: "./details-page.component.scss",
})
export class DetailsPageComponent implements OnInit {
  content: string | null = null;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    // Query-Parameter auslesen
    this.route.queryParams.subscribe((params) => {
      this.content = params["content"] || null;
      console.log("Mode:", this.content);
    });
  }
}
