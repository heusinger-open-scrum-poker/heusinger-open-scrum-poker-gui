import { MountConfig } from "cypress/angular";
import { BrandsComponent } from "./brands.component";

describe("BrandsComponent", () => {
  const config: MountConfig<BrandsComponent> = {
    imports: [],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(BrandsComponent, config);
  });
});
