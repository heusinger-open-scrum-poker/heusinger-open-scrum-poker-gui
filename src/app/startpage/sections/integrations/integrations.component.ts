import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-integrations",
  templateUrl: "./integrations.component.html",
  styleUrl: "./integrations.component.scss",
})
export class IntegrationsComponent {
  constructor(private router: Router) {}

  openDetails() {
    this.router.navigate(["/detailed-info"], {
      queryParams: { content: "integrations" },
    });
  }
}
