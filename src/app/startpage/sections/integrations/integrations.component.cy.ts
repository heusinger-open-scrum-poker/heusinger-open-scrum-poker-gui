import { MountConfig } from "cypress/angular";
import { IntegrationsComponent } from "./integrations.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";

describe("IntegrationsComponent", () => {
  const config: MountConfig<IntegrationsComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(IntegrationsComponent, config);
  });
});
