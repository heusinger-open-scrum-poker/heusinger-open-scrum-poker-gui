import { MountConfig } from "cypress/angular";
import { BusinessValueComponent } from "./business-value.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";

describe("BusinessValueComponent", () => {
  const config: MountConfig<BusinessValueComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(BusinessValueComponent, config);
  });
});
