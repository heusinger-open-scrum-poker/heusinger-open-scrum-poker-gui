import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-business-value",
  templateUrl: "./business-value.component.html",
  styleUrl: "./business-value.component.scss",
})
export class BusinessValueComponent {
  constructor(private router: Router) {}

  openDetails() {
    this.router.navigate(["/detailed-info"], {
      queryParams: { content: "business-value" },
    });
  }
}
