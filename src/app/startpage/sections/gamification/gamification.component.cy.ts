import { MountConfig } from "cypress/angular";
import { GamificationComponent } from "./gamification.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";

describe("GamificationComponent", () => {
  const config: MountConfig<GamificationComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(GamificationComponent, config);
  });
});
