import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-gamification",
  templateUrl: "./gamification.component.html",
  styleUrl: "./gamification.component.scss",
})
export class GamificationComponent {
  constructor(private router: Router) {}

  openDetails() {
    this.router.navigate(["/detailed-info"], {
      queryParams: { content: "gamification" },
    });
  }
}
