import { MountConfig } from "cypress/angular";
import { ProfessionalComponent } from "./professional.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";

describe("ProfessionalComponent", () => {
  const config: MountConfig<ProfessionalComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(ProfessionalComponent, config);
  });
});
