import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-professional",
  templateUrl: "./professional.component.html",
  styleUrl: "./professional.component.scss",
})
export class ProfessionalComponent {
  constructor(private router: Router) {}

  openDetails() {
    this.router.navigate(["/detailed-info"], {
      queryParams: { content: "professional" },
    });
  }
}
