import { MountConfig } from "cypress/angular";
import { FeaturesComponent } from "./features.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";

describe("FeaturesComponent", () => {
  const config: MountConfig<FeaturesComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(FeaturesComponent, config);
  });
});
