import { MountConfig } from "cypress/angular";
import { StartpageComponent } from "./startpage.component";

describe("HomepageComponent", () => {
  const config: MountConfig<StartpageComponent> = {
    imports: [],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(StartpageComponent, config);
  });
});
