import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RoomSelectComponent } from "./old-homepage/room-select/room-select.component";
import { CancelableButtonComponent } from "./old-homepage/cancelable-button/cancelable-button.component";
import { SigninBoxComponent } from "./old-homepage/signin-box/signin-box.component";
import { KwmcModule } from "../kwmc/kwmc.module";
import { FooterModule } from "../footer/footer.module";
import { TranslateForRootModule } from "../translate-for-root.module";
import { ModalModule } from "../modal/modal.module";
import { AuthModule } from "../auth/auth.module";
import { ModeSelectComponent } from "./old-homepage/mode-select/mode-select.component";
import { KehrwasserFeaturesModule } from "@kehrwasser-dev/features-angular";
import { NewHomepageComponent } from "./new-homepage/new-homepage.component";
import { FaqItemComponent } from "./new-homepage/faq-item/faq-item.component";
import { ScrollBackgroundDirective } from "./new-homepage/scroll-background.directive";
import { OldHomepageComponent } from "./old-homepage/old-homepage.component";
import { StartpageComponent } from "./startpage.component";
import { WrappedUserMenuComponent } from "./old-homepage/wrapped-user-menu/wrapped-user-menu.component";
import { HomepageFooterComponent } from "./new-homepage/homepage-footer/homepage-footer.component";
import { DetailsPageComponent } from "./details-page/details-page.component";
import { IntegrationsComponent } from "./sections/integrations/integrations.component";
import { ProfessionalComponent } from "./sections/professional/professional.component";
import { GamificationComponent } from "./sections/gamification/gamification.component";
import { FeaturesComponent } from "./sections/features/features.component";
import { BusinessValueComponent } from "./sections/business-value/business-value.component";
import { BrandsComponent } from "./sections/brands/brands.component";

@NgModule({
  declarations: [
    CancelableButtonComponent,
    SigninBoxComponent,
    RoomSelectComponent,
    ModeSelectComponent,
    StartpageComponent,
    OldHomepageComponent,
    NewHomepageComponent,
    FaqItemComponent,
    ScrollBackgroundDirective,
    WrappedUserMenuComponent,
    HomepageFooterComponent,
    DetailsPageComponent,
    IntegrationsComponent,
    ProfessionalComponent,
    GamificationComponent,
    FeaturesComponent,
    BusinessValueComponent,
    BrandsComponent,
  ],
  exports: [],
  imports: [
    CommonModule,
    FormsModule,
    KwmcModule,
    FooterModule,
    TranslateForRootModule,
    ModalModule,
    AuthModule,
    KehrwasserFeaturesModule,
  ],
  providers: [],
})
export class StartpageModule {}
