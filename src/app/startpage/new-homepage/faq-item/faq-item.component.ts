import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq-item',
  templateUrl: './faq-item.component.html',
  styleUrls: ['./faq-item.component.scss']
})
export class FaqItemComponent implements OnInit {
  @Input() question: string = '';
  @Input() answer: string = '';

  isOpen: boolean = true;  // Standardmäßig ausgeklappt
  isClosing: boolean = false; // Flag für die Closing-Animation

  ngOnInit(): void {
    // Standardzustand: Alle Items geöffnet
    this.isOpen = true;
  }

  toggle(): void {
    this.isOpen = ! this.isOpen
  }
}
