import { MountConfig } from "cypress/angular";
import { FaqItemComponent } from "./faq-item.component";

describe("FaqItemComponent", () => {
  const config: MountConfig<FaqItemComponent> = {
    imports: [],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(FaqItemComponent, config);
  });
});
