import { Directive, ElementRef, HostListener, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appScrollBackground]',
})
export class ScrollBackgroundDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(): void {
    const scrolled = window.scrollY;

    // Dynamische Anpassung der Hintergrundposition
    const backgroundPositionY = -(50 + scrolled * 0.4) + 'px';
    this.renderer.setStyle(this.el.nativeElement, 'background-position-y', backgroundPositionY);
  }

}
