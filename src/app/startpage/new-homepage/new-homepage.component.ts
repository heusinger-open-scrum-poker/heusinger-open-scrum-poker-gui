import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  selector: "app-new-homepage",
  templateUrl: "./new-homepage.component.html",
  styleUrl: "./new-homepage.component.scss",
})
export class NewHomepageComponent {
  constructor(private router: Router) {}

  openDetails(content: string) {
    this.router.navigate(["/detailed-info"], {
      queryParams: { content: content },
    });
  }
}
