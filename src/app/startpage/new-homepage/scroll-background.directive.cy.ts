import { MountConfig } from "cypress/angular";

describe("ScrollBackgroundDirective", () => {
  const config: MountConfig<unknown> = {
    imports: [],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount("<div appScrollBackground></div>", config);
  });
});
