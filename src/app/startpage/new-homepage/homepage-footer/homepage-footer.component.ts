import { Component } from "@angular/core";

@Component({
  selector: "app-homepage-footer",
  templateUrl: "./homepage-footer.component.html",
  styleUrl: "./homepage-footer.component.scss",
})
export class HomepageFooterComponent {}
