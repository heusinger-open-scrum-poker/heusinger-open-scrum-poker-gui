import { MountConfig } from "cypress/angular";
import { HomepageFooterComponent } from "./homepage-footer.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../../assets/i18n/en.json";

describe("HomepageFooterComponent", () => {
  const config: MountConfig<HomepageFooterComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
    declarations: [],
    componentProperties: {},
  };

  it("mounts", () => {
    cy.mount(HomepageFooterComponent, config);
  });
});
