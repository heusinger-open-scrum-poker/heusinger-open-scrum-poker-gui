import { MountConfig } from "cypress/angular";
import { ModeSelectComponent } from "./mode-select.component";
import en_translation from "../../../../assets/i18n/en.json";
import { TranslateTestingModule } from "ngx-translate-testing";

const config: MountConfig<ModeSelectComponent> = {
  imports: [
    TranslateTestingModule.withTranslations("en", en_translation),
  ],
}
describe("mode-select.component.cy.ts", () => {
  it("mounts", () => {
    cy.mount(ModeSelectComponent, config);
  });
});
