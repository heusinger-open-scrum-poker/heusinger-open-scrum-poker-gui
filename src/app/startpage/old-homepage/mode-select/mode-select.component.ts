import { Component, EventEmitter, Input, Output } from "@angular/core";
import { SigninMode } from "..";

@Component({
  selector: "app-mode-select",
  templateUrl: "./mode-select.component.html",
  styleUrls: ["./mode-select.component.scss"],
})
export class ModeSelectComponent {
  @Input() mode: SigninMode = "create";
  @Output("change") changeEmitter: EventEmitter<SigninMode> = new EventEmitter<SigninMode>();

  get isJoinMode(): boolean {
    return this.mode === "join";
  }

  get isCreateMode(): boolean {
    return this.mode === "create";
  }
}
