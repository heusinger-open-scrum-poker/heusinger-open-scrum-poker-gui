import {
  AfterViewInit,
  Component,
  ComponentRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { AgileCasinoService } from "src/app/agile-casino.service";
import { Player } from "src/app/schemas/Player";
import { Room, RoomReference } from "src/app/schemas/Room";
import LocalStorage from "src/app/shared/LocalStorage";
import { Settings } from "src/app/shared/Settings";
import { toObservable } from "src/app/shared/util";
import { environment } from "src/environments/environment";
import { emptyPlayer, SigninMode, EnterRoomOption } from "..";
import { CancelableButtonComponent } from "../cancelable-button/cancelable-button.component";
import { RoomOption } from "../room-select";
import { SigninState } from "../state";
import { HttpErrorResponse } from "@angular/common/http";
import { UserService } from "../../../user.service";
import { AlertModalComponent } from "src/app/modal/alert-modal/alert-modal.component";
import { InputComponent } from "src/app/kwmc/input/input.component";
import { KwFeatureService } from "@kehrwasser-dev/features-angular";

@Component({
  selector: "app-signin-box",
  templateUrl: "./signin-box.component.html",
  styleUrls: ["./signin-box.component.scss"],
})
export class SigninBoxComponent implements OnInit, AfterViewInit {
  ENABLE_NEW_MODE_SELECT = false;

  // Form fields
  playerName: string = "";
  roomIdInput: string = "";
  playerId: number | null = null;
  playerType: "estimator" | "inspector" = "estimator";
  cancellationPoints: Subscription[] = [];
  /** indicates whether the room list is being loaded */
  isLoading: boolean = false

  @ViewChild("alertModal") alertModal?: AlertModalComponent;
  @ViewChild("signInInput") signInInput?: InputComponent;
  @ViewChild("enterButton") enterButton?: ComponentRef<CancelableButtonComponent>;

  constructor(
    private agileCasinoService: AgileCasinoService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    protected state: SigninState,
    private storage: LocalStorage,
    private userService: UserService,
    private featureService: KwFeatureService,
  ) {}

  get settings() {
    return Settings;
  }

  get roomId() {
    return this.roomIdInput.trim();
  }

  get mode() {
    return this.state.signinMode;
  }

  get isJoinMode() {
    return this.state.signinMode === "join";
  }

  get isCreateMode() {
    return this.state.signinMode === "create";
  }

  setFocusOnSignInInput(): void {
    this.signInInput?.focus();
  }

  onRoomIdUpdated() {
    this.playerId = null;
    this.playerType = "estimator";

    if (!this.roomId) {
      return;
    }

    const roomRef = new RoomReference(this.roomId);

    const session = this.state.joinedRooms.find(
      ({ room }) => roomRef.matches(room.accessToken) || roomRef.matches(room.id)
    );

    if (session === undefined) {
      return;
    }

    if (session.player) {
      this.playerId = session.player.id;
      this.playerName = session.player.name;
      this.playerType = session.player.type;
    }
  }

  ngAfterViewInit(): void {
    this.setFocusOnSignInInput();
  }

  private goToRoom(room: Room, player: Player) {
    console.group("goToRoom");
    console.log("room: ", room);
    console.log("player: ", player);
    console.groupEnd();

    this.router.navigate(["/room/" + room.id]);
  }

  public selectJoinMode() {
    this.selectMode("join")
  }

  public selectCreateMode() {
    this.selectMode("create")
  }

  public selectMode(mode: SigninMode) {
    this.state.signinMode = mode;
    window.history.replaceState({}, "", `/${mode}`);

    if (mode === "join") {
      this.loadPreviousSession();
    }
  }

  private validatePlayer(): boolean {
    if (this.playerName.trim() === "") {
      this.alertModal?.showMessage(
        this.translate.instant("Alert.NoPlayerName"),
      );
      return false;
    }

    return true;
  }

  unsubscribeEntering() {
    this.cancellationPoints.forEach((s) => {
      s.unsubscribe();
    });
    this.cancellationPoints = this.cancellationPoints.filter((s) => !s.closed);
  }

  get isEntering() {
    return !!this.cancellationPoints.find((e) => !e.closed);
  }

  addCancellationPoint(x: Subscription) {
    this.cancellationPoints.push(x);
  }

  onSubmit(option: EnterRoomOption) {
    console.log("submit form");

    if (!this.validatePlayer()) {
      throw new Error("invalid player name");
    }

    this.unsubscribeEntering();

    this.addCancellationPoint(
      toObservable(this.userService.refreshUser(this.state.inviterHash)).subscribe({
        next: () => {
          this.enterRoom(option);
        },
        error: (e) => {
          this.unsubscribeEntering();
          console.error("unable to create user", e);
          this.enterRoom(option);
        },
      }),
    );
  }

  private enterRoom(option: EnterRoomOption) {
    console.log("signinMode:", this.state.signinMode);
    this.featureService.emitSplitTestEvent({
      splitTest: "E204-startpage",
      payload: {
        action: this.isCreateMode ? "create" : "join",
        spectate: option.spectate,
      },
    });

    if (this.isCreateMode) {
      return this.createRoom(option);
    } else if (this.isJoinMode) {
      return this.joinRoom(option);
    }
  }

  private createRoom(option: EnterRoomOption) {
    this.addCancellationPoint(
      this.agileCasinoService
        .createRoomAndPlayer({
          ...emptyPlayer(),
          name: this.playerName,
          type: option.spectate
            ? ("inspector" as const)
            : ("estimator" as const),
        })
        .subscribe({
          next: ([room, player]) => {
            if (room.id === null) {
              this.alertModal?.showMessage(
                this.translate.instant("Alert.FailCreateNewRoom"),
              );
              console.debug(
                "received room without id while openening new room",
              );
              return;
            }

            if (player.id === null) {
              this.alertModal?.showMessage(
                this.translate.instant("Alert.FailCreatNewRoom"),
              );
              console.debug(
                "received player without id while opening new room",
              );
              return;
            }

            this.agileCasinoService.startRound(room.id).subscribe({
              next: () => {},
              error: (error) => {
                console.log('Failed to start round with error:', error);
              },
            });

            this.goToRoom(room, player);
          },
          error: (e) => {
            this.unsubscribeEntering();

            console.error("unable to create room and player", e);
            this.alertModal?.showMessage(
              this.translate.instant("Alert.FailCreateRoom"),
            );
          },
        }),
    );
  }

  private joinRoom(option: EnterRoomOption) {
    const roomIdOrToken = this.roomId;

    if (!roomIdOrToken) {
      this.alertModal?.showMessage(
        this.translate.instant("Alert.RoomNumberNecessary")
      );
      this.unsubscribeEntering();
      return;
    }

    console.log("Room number: ", roomIdOrToken === undefined ? "none" : roomIdOrToken);

    const session = this.state.joinedRooms.find(
      ({ room }) => room.id.toString() === roomIdOrToken || room.accessToken === roomIdOrToken,
    );

    const newPlayerWithoutId = {
      ...emptyPlayer(),
      name: this.playerName,
      type:
        this.playerType === "inspector" || option.spectate
          ? ("inspector" as const)
          : ("estimator" as const),

    };
    const newPlayer: Player | Omit<Player, "id"> = this.playerId
      ? { id: this.playerId, ...newPlayerWithoutId  }
      : newPlayerWithoutId;

    const handleError = (e: HttpErrorResponse) => {
      if (e.status == 403) {
        if (e.error.message.match(/^unavailable player name:/)) {
          this.alertModal?.showMessage(
            this.translate.instant("Alert.NameDuplicate")
          );
        } else {
          this.alertModal?.showMessage(
            this.translate.instant("Alert.AccessDenied")
          );
        }
      } else if (e.status == 404) {
        this.alertModal?.showMessage(
          this.translate.instant("Alert.Room") +
            `${roomIdOrToken}` +
            this.translate.instant("Alert.Not"),
        );
      } else {

        console.error("failed to join a room", e);

        this.alertModal?.showMessage(
          this.translate.instant("Alert.ErrorJoin"),
        );
      }
    }

    this.addCancellationPoint(
      this.agileCasinoService.getPlayersOfRoom(roomIdOrToken).subscribe({
        next: (players: Player[]) => {
          if (players === null) {
            if (roomIdOrToken === undefined) {
              this.alertModal?.showMessage(
                this.translate.instant("Alert.RoomNumberNecessary"),
              );
              return;
            } else {
              this.alertModal?.showMessage(
                this.translate.instant("Alert.Room") +
                  `${roomIdOrToken}` +
                  this.translate.instant("AlertNot"),
              );
              return;
            }
          }

          const player = players.find((p) => p.id === session?.player.id);

          if (player === undefined) {
            players.forEach((player) => {
              if (player.name.toLowerCase() === this.playerName.toLowerCase()) {
                this.alertModal?.showMessage(
                  this.translate.instant("Alert.NameDuplicate"),
                );
                return;
              }
            });

            this.addCancellationPoint(
              this.agileCasinoService
                .joinRoomAndCreatePlayer(roomIdOrToken, newPlayer)
                .subscribe({
                  next: ([room, player]) => {
                    this.goToRoom(room, player);
                  },
                  error: (e: HttpErrorResponse) => {
                    this.unsubscribeEntering();

                    handleError(e);
                  },
                }),
            );
          } else {
            this.addCancellationPoint(
              this.agileCasinoService
                .joinRoomAndPutPlayer(roomIdOrToken, newPlayer as Player)
                .subscribe({
                  next: ([room, player]) => {
                    console.group("joined");
                    console.log("room", room);
                    console.log("player", player);

                    this.goToRoom(room, player);

                    console.groupEnd();
                  },
                  error: (e: HttpErrorResponse) => {
                    this.unsubscribeEntering();

                    handleError(e);
                  },
                }),
            );
          }
        },
        error: (e: HttpErrorResponse) => {
          this.unsubscribeEntering();

          handleError(e);
        },
      }),
    );
  }

  requestedSigninMode(): "join" | "create" {
    const routePath = this.route.snapshot.routeConfig?.path;

    if ((["join", "join/:id"] as Array<string|undefined>).includes(routePath)) {
      return "join";
    } else if (routePath === "create") {
      return "create";
    } else if (this.storage.hasSessions()) {
      return "join";
    } else {
      return "create";
    }
  }

  prefillJoinInfo() {
    const routePath = this.route.snapshot.routeConfig?.path;

    if (routePath === "join/:id") {
      const roomIdParam = this.route.snapshot.paramMap.get("id");
      if (roomIdParam) {
        this.roomIdInput = roomIdParam;
        this.onRoomIdUpdated();
      }
    } else {
      this.loadPreviousSession();
    }
  }

  loadPreviousSession() {
    const sessions = [...this.state.joinedRooms];
    sessions.sort((a, b) => b.room.id - a.room.id);
    const lastSession = sessions ? sessions[0] : undefined;
    if (lastSession) {
      this.roomIdInput = lastSession.room.id.toString();
      this.onRoomIdUpdated();
    }
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      console.log("Query Params", params);
      this.state.inviterHash = params["ref"];
    });

    const mode = this.requestedSigninMode();
    this.state.signinMode = mode;

    const userUuid = this.userService.getUserUuid();
    if (userUuid && mode === "join") {
      this.isLoading = true
      this.state
        .refreshJoinedRooms()
        .then(() => {
          this.prefillJoinInfo();
          this.isLoading = false;

          const joinedRoomsExist = this.state.joinedRooms.length > 0
          if (!mode && joinedRoomsExist) {
            this.state.signinMode = "join";
          }
        })
        .catch((error) => {
          this.prefillJoinInfo();
          this.isLoading = false;

          console.error("unable to load joined rooms list");
          console.error(error);
        });
    } else if (userUuid && mode === "create") {
      this.isLoading = true
      this.state
        .refreshJoinedRooms()
        .then(() => {
          this.isLoading = false;
        })
        .catch(() => {
          this.isLoading = false;
        });
    }

    console.log("production version: ", environment.production);
  }

  isKnownRoomId(): boolean {
    if (!this.roomId) {
      return false;
    }
    const ref = new RoomReference(this.roomId);
    const foundRoom = this.state.joinedRooms.find(({ room }) => ref.matches(room.accessToken) || ref.matches(room.id));
    return foundRoom !== undefined;
  }

  onRoomEdited(newRoomId: string) {
    this.roomIdInput = newRoomId;
    this.onRoomIdUpdated();
  }

  onRoomSelected(selection: string | null) {
    this.roomIdInput = selection ?? "";
    this.onRoomIdUpdated();
  }

  onPlayerNameEdited(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.playerName = inputElement.value ?? "";
  }

  onSessionSelected(event: RoomOption | string | null): void {
    if (!event) {
      this.roomIdInput = "";
    } else if (typeof event === "string") {
      this.roomIdInput = event;
    } else {
      this.roomIdInput = event?.roomId ?? "";
      this.signInInput?.setInput(event?.playerName ?? "");
      this.playerName = event?.playerName ?? "";
    }

    this.onRoomIdUpdated();
  }

  get showInlineLoading(): boolean {
    return this.isJoinMode && this.isLoading;
  }

  get showNameInput(): boolean {
    return !this.isLoading || this.isCreateMode;
  }

  get showRoomInput(): boolean {
    return this.isJoinMode && !this.isLoading;
  }

  get joinButtonIsBusy(): boolean {
    return (this.isLoading && this.isJoinMode) || this.isEntering;
  }
}
