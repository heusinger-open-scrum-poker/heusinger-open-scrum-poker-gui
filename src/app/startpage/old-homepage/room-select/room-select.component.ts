import {
  Component,
  ElementRef,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  HostListener,
  booleanAttribute,
} from "@angular/core";
import {
  autoUpdate,
  computePosition,
  flip,
  offset,
  size,
} from "@floating-ui/dom";
import { Player } from "src/app/schemas/Player";
import { Room } from "src/app/schemas/Room";
import { RoomOption } from ".";

@Component({
  selector: "app-room-select",
  templateUrl: "./room-select.component.html",
  styleUrls: ["./room-select.component.scss"],
})
export class RoomSelectComponent implements AfterViewInit, OnDestroy {
  @ViewChild("reference") reference!: ElementRef;
  @ViewChild("floating") floating!: ElementRef;

  @Input() placeholder: string = "";
  @Input() roomId: string = "";
  @Input() joinedRooms!: Array<{
    room: Room;
    player: Player;
    numPlayers: number;
  }>;
  @Input({transform: booleanAttribute}) busy: boolean = false;
  @Input({transform: booleanAttribute}) disabled: boolean = false;


  @Output() valueChange: EventEmitter<RoomOption | string | null> =
    new EventEmitter();

  protected showMenu: boolean = false;
  private cleanupFloatingUi: () => void = () => {};
  private update?: () => void;

  get roomsList(): Array<RoomOption> {
    const joinedRooms = [...this.joinedRooms];
    joinedRooms.sort((a, b) => b.room.id - a.room.id);
    return joinedRooms.map((it) => ({
      roomId: it.room.id.toString(),
      playerName: it.player.name,
      createdAt: it.room.created_at,
      numPlayers: it.numPlayers,
    }));
  }

  constructor(private self: ElementRef) {}

  ngAfterViewInit(): void {
    this.setupFloating();
  }

  ngOnDestroy(): void {
    this.cleanupFloatingUi();
  }

  setupFloating(): void {
    const floating = this.floating.nativeElement;
    const reference = this.reference.nativeElement;

    const update = () => {
      computePosition(reference, floating, {
        placement: "bottom-start",
        middleware: [
          flip(),
          offset(-1),
          size({
            apply({ rects, availableWidth, availableHeight }) {
              Object.assign(floating.style, {
                width: `${rects.reference.width - 2}px`,
                maxWidth: `${availableWidth}px`,
                maxHeight: `${availableHeight}px`,
              });
            },
          }),
        ],
      })
        .then(({ x, y }) => {
          Object.assign(floating.style, {
            left: `${x + 1}px`,
            top: `${y}px`,
          });
        })
        .catch((e) => {
          console.debug(e);
        });
    };

    this.update = update;

    this.cleanupFloatingUi = autoUpdate(
      this.reference.nativeElement,
      this.floating.nativeElement,
      update,
      {
        elementResize: false,
      },
    );
  }

  handleClick(): void {
    if (this.busy || this.disabled) {
      return;
    }
    this.showMenu = !this.showMenu;
    setTimeout(() => this.update?.());
  }

  getOptions(): Array<RoomOption> {
    return this.roomsList ?? [];
  }

  select(option: RoomOption): void {
    this.showMenu = false;
    this.roomId = option.roomId;
    this.valueChange.emit(option);
  }

  onRoomChanged(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    this.roomId = inputElement.value;
    this.valueChange.emit(this.roomId);
  }

  numRooms(): number {
    return this.roomsList.length;
  }

  formatDateTime(date: Date | number): string {
    return new Intl.DateTimeFormat("en-US", { dateStyle: "medium" }).format(
      new Date(date),
    );
  }

  @HostListener("document:click", ["$event"])
  onClickOutside(event: Event): void {
    const clickedInside = this.self.nativeElement.contains(event.target);
    if (!clickedInside) {
      this.showMenu = false;
    }
  }

  identifyOption(_index: number, option: RoomOption) {
    return option.roomId;
  }
}
