export type RoomOption = {
  roomId: string;
  playerName: string;
  createdAt: number;
  numPlayers: number;
};
