import { AfterViewInit, Component, Input, ViewChild } from "@angular/core";
import { SigninState } from "../state";
import { UserService } from "src/app/user.service";
import { ActivatedRoute, Router } from "@angular/router";
import { PasswordResetModalComponent } from "src/app/auth/password-reset-modal/password-reset-modal.component";
import { ConfirmationBoxComponent } from "src/app/modal/confirmation-box/confirmation-box.component";
import { ConfirmationResult } from "src/app/modal/confirmation-box";

@Component({
  selector: "app-wrapped-user-menu",
  templateUrl: "./wrapped-user-menu.component.html",
  styleUrls: ["./wrapped-user-menu.component.scss"],
})
export class WrappedUserMenuComponent implements AfterViewInit {
  @Input() variant: "old" | "new" = "old";

  @ViewChild("resetModal") resetModal?: PasswordResetModalComponent;
  @ViewChild("signOutModal") signOutModal?: ConfirmationBoxComponent;

  constructor(
    private state: SigninState,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  get userFeatureEnabled(): boolean {
    return this.userService.userFeatureEnabled();
  }

  ngAfterViewInit(): void {
    this.handlePasswordReset();
  }

  handlePasswordReset(): void {
    this.route.queryParams.subscribe(
      (queryParams) => {
        if (queryParams['email'] && queryParams['token']) {
          this.resetModal?.showModal({
            token: queryParams['token'],
            email: queryParams['email'],
          });
          // clear query params
          this.router.navigate(["create"]);
        }
      }
    );
  }

  handleLogin() {
    this.state.refreshJoinedRooms()
  }

  handleLogout() {
    this.signOutModal?.showModal();
    const handle = this.signOutModal?.result.subscribe(
      (result: ConfirmationResult) => {
        handle?.unsubscribe();
        switch (result) {
          case "cancel": {
            this.signOutModal?.close();
          } break;
          case "confirm": {
            this.userService.logout()
            this.state.refreshJoinedRooms()
            this.signOutModal?.close();
          } break;
        }
      }
    );
  }
}
