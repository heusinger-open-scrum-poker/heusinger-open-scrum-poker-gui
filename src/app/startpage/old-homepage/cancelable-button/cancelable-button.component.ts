import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  booleanAttribute,
} from "@angular/core";
import { EnterRoomOption } from "..";

export type Phase = "neutral" | "waiting" | "canceling";

@Component({
  selector: "app-cancelable-button",
  templateUrl: "./cancelable-button.component.html",
  styleUrls: ["./cancelable-button.component.scss"],
})
export class CancelableButtonComponent {
  @Input({transform: booleanAttribute}) isSubscribed: boolean = false;
  @Input() timeoutMs: number = 10 * 1000;
  @Input() cancelText: string = "Cancel action?";
  @Input({transform: booleanAttribute}) disabled: boolean = false;

  @Output("unsubscribe") onUnsubscribe: EventEmitter<void> =
    new EventEmitter<void>();
  @Output("timeout") onTimeout: EventEmitter<void> = new EventEmitter<void>();
  @Output("enter") onClick: EventEmitter<EnterRoomOption> =
    new EventEmitter<EnterRoomOption>();

  timeoutHandle: null | ReturnType<typeof setTimeout> = null;
  canceling: boolean = false;

  showDropdown: boolean = false;

  constructor(private self: ElementRef) {}

  public getPhase(): Phase {
    if (!this.isSubscribed) {
      return "neutral";
    } else if (this.canceling) {
      return "canceling";
    } else {
      return "waiting";
    }
  }

  public handleClick(event: Event, options?: EnterRoomOption) {
    event.stopPropagation();
    event.preventDefault();

    this.showDropdown = false;

    switch (this.getPhase()) {
      case "neutral":
        this.onClick.emit(options ?? {});
        this.enterWaitingPhase();
        break;
      case "waiting":
        this.enterCancelingPhase();
        break;
      case "canceling":
        this.returnToNeutralPhase();
        break;
    }
  }

  enterWaitingPhase() {
    if (typeof this.timeoutHandle === "number") {
      clearTimeout(this.timeoutHandle);
      this.timeoutHandle = null;
    }

    this.canceling = false;

    this.timeoutHandle = setTimeout(() => {
      if (this.getPhase() === "waiting") {
        this.onTimeout.emit();
        this.enterCancelingPhase();
      }
    }, this.timeoutMs);
  }

  enterCancelingPhase() {
    this.canceling = true;
  }

  returnToNeutralPhase() {
    this.canceling = false;

    if (this.isSubscribed) {
      this.onUnsubscribe.emit();
    }

    if (typeof this.timeoutHandle === "number") {
      clearTimeout(this.timeoutHandle);
      this.timeoutHandle = null;
    }
  }

  setShowDropdown(value: boolean, event?: Event) {
    event?.stopPropagation();
    event?.preventDefault();
    this.showDropdown = value;
  }

  toggleShowDropdown(event?: Event) {
    this.setShowDropdown(!this.showDropdown, event);
  }

  @HostListener("document:click", ["$event"])
  onClickOutside(event: Event): void {
    const clickedInside = this.self.nativeElement.contains(event.target);
    if (!clickedInside) {
      this.setShowDropdown(false);
    }
  }
}
