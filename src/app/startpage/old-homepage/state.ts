import { Injectable } from "@angular/core";
import { firstValueFrom } from "rxjs";
import { SigninMode } from ".";
import { AgileCasinoService } from "../../agile-casino.service";
import { Player } from "../../schemas/Player";
import { Room } from "../../schemas/Room";
import LocalStorage from "../../shared/LocalStorage";
import { Settings } from "../../shared/Settings";

@Injectable({
  providedIn: "root",
})
export class SigninState {
  private _settings = Settings;
  private _inviterHash: string = "";
  private _joinedRooms: Array<{
    room: Room;
    player: Player;
    numPlayers: number;
  }> = [];
  private _signinMode: SigninMode = "create";

  constructor(
    private storage: LocalStorage,
    private agileCasinoService: AgileCasinoService,
  ) {}

  get settings() {
    return this._settings;
  }

  get userUuid() {
    return this.storage.getUuid();
  }

  get inviterHash() {
    return this._inviterHash;
  }

  set inviterHash(hash: string) {
    this._inviterHash = hash;
  }

  get joinedRooms() {
    return this._joinedRooms;
  }

  get signinMode() {
    return this._signinMode;
  }

  set signinMode(mode: SigninMode) {
    this._signinMode = mode;
  }

  async refresh(): Promise<void> {
    await this.refreshJoinedRooms();
  }

  async refreshJoinedRooms(): Promise<void> {
    if (!this.userUuid) {
      this._joinedRooms = []
      return;
    }

    this._joinedRooms = await firstValueFrom(
      this.agileCasinoService.getJoinedRooms(this.userUuid),
    );

    this.storage.setHasSessions(this.joinedRooms.length > 0);
  }
}
