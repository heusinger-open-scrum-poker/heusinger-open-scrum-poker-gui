import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ActivatedRoute, convertToParamMap } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { MountConfig } from "cypress/angular";
import { of } from "rxjs";
import { FooterComponent } from "../../footer/footer.component";
import { CancelableButtonComponent } from "./cancelable-button/cancelable-button.component";
import { RoomSelectComponent } from "./room-select/room-select.component";
import { RoomComponent } from "../../room/room.component";
import { OldHomepageComponent } from "./old-homepage.component";
import { TranslateTestingModule } from "ngx-translate-testing";
import en_translation from "../../../assets/i18n/en.json";
import { SnackbarComponent } from "../../kwmc/snackbar/snackbar.component";
import { SigninBoxComponent } from "./signin-box/signin-box.component";
import { ModalModule } from "../../modal/modal.module";
import { AuthModule } from "../../auth/auth.module";
import { KwmcModule } from "../../kwmc/kwmc.module";
import { FooterModule } from "../../footer/footer.module";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";

const config_with_url = (path: string): MountConfig<OldHomepageComponent> => ({
  imports: [
    HttpClientModule,
    FormsModule,
    RouterTestingModule.withRoutes([
      { path: "create", component: OldHomepageComponent },
      { path: "join", component: OldHomepageComponent },
      { path: "room/:id", component: RoomComponent },
      { path: "join/:id", component: OldHomepageComponent },
      { path: "", redirectTo: "/create", pathMatch: "full" },
    ]),
    TranslateTestingModule.withTranslations("en", en_translation),
    ModalModule,
    AuthModule,
    KwmcModule,
    FooterModule,
  ],
  declarations: [
    FooterComponent,
    RoomSelectComponent,
    CancelableButtonComponent,
    SnackbarComponent,
    SigninBoxComponent,
  ],
  providers: [
    {
      provide: ActivatedRoute,
      useValue: {
        queryParams: of(convertToParamMap({})),
        snapshot: {
          paramMap: of(convertToParamMap({})),
          routeConfig: {
            path,
          },
        },
      },
    },
  ],
});

describe("signin.component.cy.ts", () => {
  it("creates a room", () => {
    cy.intercept("POST", "/room*", ({ reply }) => {
      reply({
        statusCode: 200,
        body: {
          id: 4,
          status: "estimating",
          maxPlayers: 0,
          roundsPlayed: 0,
          created_at: "2023-01-04",
        },
      });
    }).as("createRoom");

    cy.intercept("POST", "/room/4/players*", (req) => {
      req.reply({
        statusCode: 200,
        body: {
          id: 2,
          name: "meep",
          avatar: "orange",
          cardValue: undefined,
          type: "estimator",
        },
      });
    }).as("createPlayer");

    cy.intercept("GET", "/user/*", {}).as("getUser");

    cy.mount(OldHomepageComponent, config_with_url("create"));
    cy.get('.mode-select').within(() => {
      cy.contains("Create").click();
    });
    cy.get('input[placeholder="Name of player"]')
      .should("not.be.disabled")
      .type("Joe Dart");
    cy.get(".buttons").contains("Create").click();

    cy.wait("@createRoom");
  });
});

describe("signin.component.cy.ts", () => {
  it("joins as former player, when in local storage", () => {
    const sessions = [
      {
        roomId: 1,
        playerId: 3,
      },
    ];
    localStorage.setItem("uuid", "example-uuid");
    localStorage.setItem("sessions", JSON.stringify(sessions));

    const body: Array<{room: Room, player: Player, numPlayers: number}> = [
      {
        room: {
          id: 1, created_at: new Date().getTime(),
          status: "estimating",
          maxPlayers: 0,
          roundsPlayed: 0,
          series: ""
        },
        player: {
          id: 3,
          name: "Joe",
          type: "estimator"
        },
        numPlayers: 1,
      },
    ];

    cy.intercept("GET", `/user/example-uuid/joinedRooms*`, (req) => {
      req.reply({
        statusCode: 200,
        body,
      });
    }).as("getJoinedRooms");

    cy.intercept("GET", `/user/example-uuid*`, {
      statusCode: 200,
      body: { uuid: "example-uuid" },
    }).as("getUser");
    cy.intercept("PUT", `/players/3*`, { statusCode: 200, body: { id: 3 } }).as(
      "putPlayer",
    );
    cy.intercept("GET", `/room/1*`, { statusCode: 200, body: { id: 1 } }).as(
      "getRoom",
    );
    cy.intercept("GET", `/room/1/players*`, {
      statusCode: 200,
      body: [{ id: 3, name: "Joe" }],
    }).as("getPlayers");
    cy.intercept("POST", `/room/1/players*`, { statusCode: 200, body: {} }).as(
      "postPlayers",
    );

    expect(localStorage.getItem("sessions")).to.equal(JSON.stringify(sessions));

    cy.mount(OldHomepageComponent, config_with_url("join"));
    cy.get(".dropdown-button").click();
    cy.get(".option").click();
    cy.wait("@getJoinedRooms");
    cy.get('[name="player-name"] input').should("have.value", "Joe");
    cy.get('[name="room-name"] input').should("have.value", "1");
    cy.get(".buttons").contains("Join").click();
    cy.wait("@getUser");
    cy.wait("@putPlayer");
    cy.wait("@getRoom");
  });
});

describe("signin.component.cy.ts", () => {
  it("lists all recent games from latest to oldest", () => {
    const sessions = [
      { roomId: 2, playerId: 20 },
      { roomId: 1, playerId: 10 },
      { roomId: 3, playerId: 30 },
    ];
    localStorage.setItem("uuid", "example-uuid");
    localStorage.setItem("sessions", JSON.stringify(sessions));

    const body: Array<{room: Room, player: Player, numPlayers: number}> = [
      {
        room: {
            id: 1, created_at: new Date("2022-12-11").getTime(),
            status: "estimating",
            maxPlayers: 0,
            roundsPlayed: 0,
            series: ""
        },
        player: {
            id: 10,
            name: "",
            type: "estimator"
        },
        numPlayers: 1,
      },
      {
        room: {
            id: 2, created_at: new Date("2022-12-11").getTime(),
            status: "estimating",
            maxPlayers: 0,
            roundsPlayed: 0,
            series: ""
        },
        player: {
            id: 20,
            name: "",
            type: "estimator"
        },
        numPlayers: 1,
      },
      {
        room: {
            id: 3, created_at: new Date("2022-12-11").getTime(),
            status: "estimating",
            maxPlayers: 0,
            roundsPlayed: 0,
            series: ""
        },
        player: {
            id: 30,
            name: "",
            type: "estimator"
        },
        numPlayers: 1,
      },
    ];

    cy.intercept("GET", `/user/example-uuid/joinedRooms*`, (req) => {
      req.reply({
        statusCode: 200,
        body,
      });
    }).as("getJoinedRooms");

    cy.mount(OldHomepageComponent, config_with_url("join"));
    cy.wait(2000);
    cy.get(".dropdown-button");
    cy.get(".dropdown-button").click();
    cy.get('.options :nth-child(1) [name="room-id"]').contains("3");
    cy.get('.options :nth-child(2) [name="room-id"]').contains("2");
    cy.get('.options :nth-child(3) [name="room-id"]').contains("1");
  });
});
