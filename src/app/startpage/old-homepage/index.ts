import { Player } from "../../schemas/Player";

export type SigninMode = "create" | "join";

export function emptyPlayer(): Omit<Player, "id" | "name"> {
  return {
    avatar: undefined,
    cardValue: undefined,
    type: "estimator",
  };
}

export type EnterRoomOption = {
  spectate?: boolean;
};
