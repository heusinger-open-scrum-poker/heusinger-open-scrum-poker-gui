import { describe, it, expect } from 'vitest';
import { Observable, of } from "rxjs";
import { SigninState } from "./state";
import LocalStorage from "../../shared/LocalStorage";
import { AgileCasinoService } from "../../agile-casino.service";
import { Room } from "../../schemas/Room";
import { Player } from "../../schemas/Player";

describe("state.ts", () => {
  it("refreshes", async () => {
    const storage = {
      getUuid() {
        return "some-bogus-uuid";
      },
      setHasSessions(_: boolean) {},
    } as LocalStorage;
    const exampleJoinedRooms = [
      { room: {} as Room, player: {} as Player, numPlayers: 1 },
    ];
    const acService = {
      getJoinedRooms(
        userUuid: string,
      ): Observable<Array<{ room: Room; player: Player; numPlayers: number }>> {
        if (userUuid) {
          return of(exampleJoinedRooms);
        } else {
          throw new Error("mock > getJoinedRooms");
        }
      },
    } as AgileCasinoService;
    const state = new SigninState(storage, acService);

    expect(state.joinedRooms.length).to.be.eq(0);
    await state
      .refresh()
      .then(() => expect(state.joinedRooms.length).to.be.eq(1));
  });
});
