import { Component, Input } from '@angular/core';
import sanitizeHtml from 'sanitize-html';

@Component({
  selector: 'app-rich-text-view',
  templateUrl: './rich-text-view.component.html',
  styleUrl: './rich-text-view.component.scss'
})
export class RichTextViewComponent {
  @Input() content: string = "";

  safe(text: string): string {
    return sanitizeHtml(text);
  }
}
