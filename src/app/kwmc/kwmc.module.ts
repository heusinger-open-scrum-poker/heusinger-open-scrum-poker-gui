import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ConnectionStatusComponent } from "./connection-status/connection-status.component";
import { TranslateForRootModule } from "../translate-for-root.module";
import { AdRotationComponent } from "./ad-rotation/ad-rotation.component";
import { CardComponent } from "./card/card.component";
import { CheckboxComponent } from "./checkbox/checkbox.component";
import { DisclaimerComponent } from "./disclaimer/disclaimer.component";
import { DropdownComponent } from "./dropdown/dropdown.component";
import { NotificationComponent } from "./notification/notification.component";
import { SettingsMenuComponent } from "./settings-menu/settings-menu.component";
import { SmallDeviceDisclaimerComponent } from "./small-device-disclaimer/small-device-disclaimer.component";
import { SnackbarComponent } from "./snackbar/snackbar.component";
import { TabBarComponent } from "./tab-bar/tab-bar.component";
import { IconComponent } from "./icon/icon.component";
import { PillComponent } from "./pill/pill.component";
import { ContextMenuComponent } from "./context-menu/context-menu.component";
import { ContextMenuButtonComponent } from "./context-menu-button/context-menu-button.component";
import { ContextMenuPopupComponent } from "./context-menu-popup/context-menu-popup.component";
import { RichTextViewComponent } from "./rich-text-view/rich-text-view.component";
import { NewBadgeComponent } from "./new-badge/new-badge.component";
import { IndeterminateProgressComponent } from "./indeterminate-progress/indeterminate-progress.component";
import { ButtonComponent } from "./button/button.component";
import { InputComponent } from "./input/input.component";
import { FeatureLockComponent } from "./feature-lock.component";
import { InlineLoadingComponent } from "./inline-loading.component";
import { InlineAlert2Component } from "./inline-alert2.component";
import { InlineAlertComponent } from "./inline-alert.component";

@NgModule({
  declarations: [
    AdRotationComponent,
    ButtonComponent,
    CardComponent,
    CheckboxComponent,
    ConnectionStatusComponent,
    ContextMenuComponent,
    ContextMenuButtonComponent,
    ContextMenuPopupComponent,
    DisclaimerComponent,
    DropdownComponent,
    FeatureLockComponent,
    IconComponent,
    IndeterminateProgressComponent,
    InlineAlertComponent,
    InlineAlert2Component,
    InlineLoadingComponent,
    InputComponent,
    NewBadgeComponent,
    NotificationComponent,
    SettingsMenuComponent,
    SmallDeviceDisclaimerComponent,
    SnackbarComponent,
    TabBarComponent,
    PillComponent,
    RichTextViewComponent,
  ],
  exports: [
    AdRotationComponent,
    ButtonComponent,
    CardComponent,
    CheckboxComponent,
    ConnectionStatusComponent,
    ContextMenuComponent,
    ContextMenuButtonComponent,
    ContextMenuPopupComponent,
    DisclaimerComponent,
    DropdownComponent,
    FeatureLockComponent,
    IconComponent,
    IndeterminateProgressComponent,
    InlineAlertComponent,
    InlineAlert2Component,
    InlineLoadingComponent,
    InputComponent,
    NewBadgeComponent,
    NotificationComponent,
    SettingsMenuComponent,
    SmallDeviceDisclaimerComponent,
    SnackbarComponent,
    TabBarComponent,
    PillComponent,
    RichTextViewComponent,
  ],
  imports: [CommonModule, FormsModule, TranslateForRootModule, ReactiveFormsModule],
  providers: [],
})
export class KwmcModule {}
