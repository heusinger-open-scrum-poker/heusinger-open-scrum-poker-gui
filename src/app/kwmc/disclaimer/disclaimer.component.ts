import { Component, Input } from "@angular/core";

@Component({
  selector: "app-disclaimer",
  templateUrl: "./disclaimer.component.html",
  styleUrls: ["./disclaimer.component.scss"],
})
export class DisclaimerComponent {
  @Input() icon!: string;
  @Input() viewBox: string = '0 0 24 24';
  @Input() size: "small" | "medium" = "small";
  @Input() textAlign: "left" | "center" = "left";
}
