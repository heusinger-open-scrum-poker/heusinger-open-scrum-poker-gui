import { Component, Input } from "@angular/core";
import { ButtonFlavor, ButtonGap, ButtonHorizontalPadding, ButtonVerticalPadding, ButtonWidth } from ".";

@Component({
  selector: "app-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"],
  host: {
    '[class]': 'hostClass',
  },
})
export class ButtonComponent {
  @Input() busy: boolean = false;
  @Input() disabled: boolean = false;
  @Input() flavor: ButtonFlavor = "default";
  @Input() gap?: ButtonGap;
  @Input() px?: ButtonHorizontalPadding;
  @Input() py?: ButtonVerticalPadding;
  @Input() width: ButtonWidth = "default";
  @Input() tooltipContainer: boolean = false;

  get buttonClass(): string {
    return `
    base
    flavor-${this.flavor ?? "default"}
    gap-${this.gap ?? this.defaultGap}
    px-${this.px ?? this.defaultPx}
    py-${this.py ?? this.defaultPy}
    ${this.tooltipContainer ? "tooltip-container" : ""}
    `
  }

  get hostClass(): string {
    return `
    host-flavor-${this.flavor ?? "default"}
    host-width-${this.width ?? "default"}
    `
  }

  get defaultGap(): ButtonGap {
    switch (this.flavor) {
      case "control-small": {
        return "0_5";
      }
      case "primary-control": {
        return "1";
      }
      default: {
        return "2";
      }
    }
  }

  get defaultPx(): ButtonHorizontalPadding {
    switch (this.flavor) {
      case "default":
      case "primary":
      case "secondary":
      case "tertiary":
      case "high-impact": {
        return "4";
      }
      case "primary-control": {
        return "1";
      }
      default: {
        return "2";
      }
    }
  }

  get defaultPy(): ButtonVerticalPadding {
    switch (this.flavor) {
      case "default":
      case "primary":
      case "secondary":
      case "tertiary":
      case "high-impact": {
        return "4";
      }
      case "project-management": {
        return "1";
      }
      case "control": {
        return "2";
      }
      case "primary-control": {
        return "1";
      }
      default: {
        return "2";
      }
    }
  }
}
