export type ButtonFlavor = "default" | "primary" | "secondary" | "tertiary" | "high-impact" | "control" | "control-small" | "control-small-faded" | "project-management" | "primary-control"
export type ButtonWidth = "default" | "smaller" | "medium"
export type ButtonHorizontalPadding = "0" | "0_5" | "1" | "2" | "3" | "4" | "12";
export type ButtonVerticalPadding = "0" | "0_5" | "1" | "2" | "3" | "4";
export type ButtonGap = "0" | "0_5" | "1" | "2" | "3" | "4";

