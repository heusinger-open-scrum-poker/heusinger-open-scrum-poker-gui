import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { InputFlavor, InputHorizontalPadding } from ".";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-input",
  templateUrl: "./input.component.html",
  styleUrls: ["./input.component.scss"],
  host: {
    "[class]": "hostClass",
  },
})
export class InputComponent {
    @ViewChild("input") input?: ElementRef<HTMLInputElement>;
    @Input() type: "text" | "password" = "text";
    @Input() px?: InputHorizontalPadding;
    @Input() flavor?: InputFlavor;
    @Input() value: string = "";
    @Input() disabled: boolean = false;
    @Input() isLoading: boolean = false;
    @Input() placeholder: string = "";
    @Input() maxlength?: number = undefined;
    @Input() autofocus?: boolean = false;
    @Input() busy: boolean = false;
    @Input() control?: FormControl;
    @Input() textAlign?: "center" | "left" | "right";
    @Output() selected: EventEmitter<string> = new EventEmitter<string>();


    get inputClass() {
      return `
      base
      flavor-${this.flavor ?? "default"}
      px-${this.px ?? this.defaultPx}
      textAlign-${this.textAlign ?? "left"}
      `
    }

    get hostClass() {
      return `
      host-flavor-${this.flavor ??  "default"}
      `
    }

    get defaultPx() {
      return "3";
    }

    focus() {
      this.input?.nativeElement.focus();
    }

    setInput(value: string) {
      this.value = value;
    }
}
