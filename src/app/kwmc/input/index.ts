export type InputFlavor = "default" | "task"

export type InputHorizontalPadding = "0" | "1" | "2" | "3";
