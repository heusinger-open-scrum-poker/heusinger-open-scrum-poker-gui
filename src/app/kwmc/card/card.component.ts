import { Component, Input, OnInit } from "@angular/core";
import { Player } from "../../schemas/Player";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"],
})
export class CardComponent implements OnInit {
  @Input() player!: Player;
  @Input() revealed!: boolean;

  constructor() {}

  ngOnInit(): void {}
}
