import { MountConfig } from "cypress/angular";
import { Player } from "../../schemas/Player";
import { CardComponent } from "./card.component";

describe("CardComponent", () => {
  const player: Player = {
    id: 0,
    name: "test",
    avatar: "darkblue",
    cardValue: "5",
    type: "estimator",
  };
  const revealed = false;
  const config: MountConfig<CardComponent> = {
    componentProperties: {
      player,
      revealed,
    },
  };

  it("mounts", () => {
    cy.mount(CardComponent, config);
  });
});
