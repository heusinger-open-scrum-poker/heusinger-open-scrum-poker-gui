import { Component, Input } from "@angular/core";

@Component({
  selector: "app-inline-loading",
  templateUrl: "./inline-loading.component.html",
  styleUrls: ["./inline-loading.component.scss"],
})
export class InlineLoadingComponent {
    @Input() statusText?: string;
}
