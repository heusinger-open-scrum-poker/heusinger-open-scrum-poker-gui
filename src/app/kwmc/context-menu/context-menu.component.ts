import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  AfterViewInit,
  HostListener,
  ContentChild,
} from "@angular/core";
import { autoUpdate, computePosition, flip, size } from "@floating-ui/dom";
import { ContextMenuPopupComponent } from "../context-menu-popup/context-menu-popup.component";
import { ContextMenuButtonComponent } from "../context-menu-button/context-menu-button.component";
@Component({
  selector: "app-context-menu",
  templateUrl: "./context-menu.component.html",
  styleUrls: ["./context-menu.component.scss"],
})
export class ContextMenuComponent implements OnDestroy, AfterViewInit {
  @ContentChild(ContextMenuButtonComponent) reference?: ContextMenuButtonComponent;
  @ContentChild(ContextMenuPopupComponent) floating?: ContextMenuPopupComponent;

  @Input() placement: "bottom-start" | "bottom-end" = "bottom-start";

  get isOpen(): boolean {
    return this.floating?.isOpen ?? false;
  }

  set isOpen(isOpen: boolean) {
    if (this.floating) {
      this.floating.isOpen = isOpen;
    }
  }

  private cleanupFloatingUi: () => void = () => {};

  constructor(private self: ElementRef) {}

  open(): void {
    this.updateFloating();
    this.isOpen = true;
  }

  close(): void {
    this.isOpen = false;
  }

  @HostListener("document:click", ["$event"])
  onClickOutside(event: Event): void {
    if (!this.isOpen) {
      return;
    }

    const clickedInside = this.self.nativeElement.contains(event.target);
    if (!clickedInside) {
      this.close();
    }
  }

  ngAfterViewInit(): void {
    if (!this.reference?.self || !this.floating?.self) {
      setTimeout(() => this.ngAfterViewInit(), 500);
      return;
    }

    this.reference.toggle.subscribe(() => {
      if (this.isOpen) {
        this.close();
      } else {
        this.open();
      }
    });

    this.cleanupFloatingUi = autoUpdate(
      this.reference.self.nativeElement,
      this.floating.self.nativeElement,
      () => this.updateFloating(),
    );
  }

  updateFloating(): void {
    const floating = this.floating?.self.nativeElement;
    const reference = this.reference?.self.nativeElement;

    if (!floating) {
      throw new Error("floating element not found");
    }

    if (!reference) {
      throw new Error("reference element not found");
    }

    computePosition(reference, floating, {
      placement: this.placement,
      middleware: [
        flip(),
        size({
          apply({ availableWidth, availableHeight }) {
            Object.assign(floating.style, {
              maxWidth: `${availableWidth}px`,
              maxHeight: `${availableHeight}px`,
            });
          },
        }),
      ],
    }).then(({ x, y }) => {
      Object.assign(floating.style, {
        left: `${x}px`,
        top: `${y}px`,
      });
    });
  }

  ngOnDestroy(): void {
    this.cleanupFloatingUi();
  }
}
