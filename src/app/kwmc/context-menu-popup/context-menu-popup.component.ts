import {
  Component,
  ElementRef,
} from "@angular/core";

@Component({
  selector: "app-context-menu-popup",
  templateUrl: "./context-menu-popup.component.html",
  styleUrls: ["./context-menu-popup.component.scss"],
  host: {
    "[class.hidden]": "!isOpen",
  },
})
export class ContextMenuPopupComponent {

  public isOpen: boolean = false;

  constructor(
    public self: ElementRef,
  ) {}

  public setOpen(isOpen: boolean): void {
    this.isOpen = isOpen;
    console.log({isOpen});
  }
}
