export type TabSpec = {
  label: string;
  icon?: string;
};

export type TabBarState = {
  selectedIndex: number;
};
