import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TabBarState, TabSpec } from ".";

export type TabChangeEvent = { spec: TabSpec, prevIndex: number, index: number };

@Component({
  selector: "app-tab-bar",
  templateUrl: "./tab-bar.component.html",
  styleUrls: ["./tab-bar.component.scss"],
})
export class TabBarComponent {
  @Input() tabs: Array<TabSpec> = [];
  @Input() state: TabBarState = {
    selectedIndex: 0,
  };

  @Output("tabChange") onTabChange: EventEmitter<TabChangeEvent> =
    new EventEmitter<TabChangeEvent>();

  setSelectedIndex(index: number) {
    const prevIndex = this.state.selectedIndex;
    this.state.selectedIndex = index;
    if (this.tabs[index]) {
      this.onTabChange.emit({
        index,
        prevIndex,
        spec: this.tabs[index]
      });
    }
  }
}
