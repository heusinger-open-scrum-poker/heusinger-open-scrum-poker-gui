import {
  Component,
  ElementRef,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { autoUpdate, computePosition, flip, size } from "@floating-ui/dom";
import { Option } from "./index";

@Component({
  selector: "app-dropdown",
  templateUrl: "./dropdown.component.html",
  styleUrls: ["./dropdown.component.scss"],
})
export class DropdownComponent<T> implements AfterViewInit, OnDestroy {
  @ViewChild("reference") reference!: ElementRef;
  @ViewChild("floating") floating!: ElementRef;

  @Input() options: Option<T>[] = [];
  @Input() placeholder: string = "";
  @Input() value: Option<T> | null = null;
  @Output() valueChange: EventEmitter<Option<T> | null> = new EventEmitter();

  protected showMenu: boolean = false;
  private cleanupFloatingUi: () => void = () => {};

  ngAfterViewInit(): void {
    const floating = this.floating.nativeElement;
    const reference = this.reference.nativeElement;

    const update = () => {
      computePosition(reference, floating, {
        placement: "bottom-start",
        middleware: [
          flip(),
          size({
            apply({ rects, availableWidth, availableHeight }) {
              Object.assign(floating.style, {
                width: `${rects.reference.width}px`,
                maxWidth: `${availableWidth}px`,
                maxHeight: `${availableHeight}px`,
              });
            },
          }),
        ],
      }).then(({ x, y }) => {
        Object.assign(floating.style, {
          left: `${x}px`,
          top: `${y}px`,
        });
      });
    };

    this.cleanupFloatingUi = autoUpdate(
      this.reference.nativeElement,
      this.floating.nativeElement,
      update,
    );
  }

  ngOnDestroy(): void {
    this.cleanupFloatingUi();
  }

  onClick(): void {
    this.showMenu = !this.showMenu;
  }

  select(option: Option<T>): void {
    this.showMenu = false;
    this.value = option;
    this.valueChange.emit(this.value);
  }
}
