import { Component } from "@angular/core";
import { NotificationsService } from "src/app/notifications.service";
import { Notification } from "src/app/shared/Notification.class";
import {animate, style, transition, trigger} from "@angular/animations";

export type NotificationItem = { notification: Notification, handle: string };

@Component({
  selector: "app-snackbar",
  templateUrl: "./snackbar.component.html",
  styleUrls: ["./snackbar.component.scss"],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ position: "relative", opacity: 0 }),
            animate('500ms ease-out',
              style({position: "relative",  opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({position: "relative",  opacity: 1 }),
            animate('500ms ease-in',
              style({position: "relative",  opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class SnackbarComponent {
  public notifications: Array<NotificationItem> = [];
  public timeout = 2500;
  public timeoutHandle?: ReturnType<typeof setTimeout>;

  constructor(private notificationsService: NotificationsService) {}

  setNotification(notification: Notification) {
    const handle = this.randomHandle();

    if (notification.type === "pending") {
      this.notifications.push({notification, handle});
      notification.pending?.then((followup) => {
        this.replaceNotification(handle, followup);
        setTimeout(() => {
          this.removeNotification(handle);
        }, this.timeout);
      });
    } else {
      this.notifications.push({notification, handle});

      setTimeout(() => {
        this.removeNotification(handle);
      }, this.timeout);
    }
  }

  removeNotification(removeHandle: string) {
    this.notifications = this.notifications.filter(({handle}) => handle !== removeHandle);
  }

  replaceNotification(replaceHandle: string, replacement: Notification) {
    this.notifications = this.notifications.map(({handle, notification}) => handle !== replaceHandle ? {handle, notification} : {handle, notification: replacement});
  }

  public ngOnInit() {
    this.notificationsService.subject$.subscribe((notification) =>
      this.setNotification(notification),
    );
  }

  randomHandle(): string {
    const date = new Date();
    const random = Math.floor(Math.random() * 1e9);
    const handle = `${date.getTime()}-${random}`;
    return handle;
  }

  trackByHandle(index: number, item: NotificationItem): string {
    return item.handle;
  }
}
