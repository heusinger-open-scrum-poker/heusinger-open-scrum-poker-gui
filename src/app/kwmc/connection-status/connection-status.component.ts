import { Component, OnInit } from "@angular/core";

enum Status {
  Offline = "offline",
  Online = "online",
}

@Component({
  selector: "app-connection-status",
  templateUrl: "./connection-status.component.html",
  styleUrls: ["./connection-status.component.scss"],
})
export class ConnectionStatusComponent implements OnInit {
  private _connectionStatus: Status;

  constructor() {
    this._connectionStatus = window.navigator.onLine ? Status.Online : Status.Offline;
  }

  ngOnInit(): void {
    window.addEventListener("online", () => this.setOnline());
    window.addEventListener("offline", () => this.setOffline());
  }

  setOffline(): void {
    console.log("fooo");
    this._connectionStatus = Status.Offline;
  }

  setOnline(): void {
    console.log("bar");
    this._connectionStatus = Status.Online;
  }

  set connectionStatus(status: Status) {
    this._connectionStatus = status;
  }

  get connectionStatus() {
    return this._connectionStatus;
  }
}
