import { MountConfig } from "cypress/angular";
import { ConnectionStatusComponent } from "./connection-status.component";

describe("ConnectionStatusComponent", () => {
  const config: MountConfig<ConnectionStatusComponent> = {};

  it("mounts", () => {
    cy.mount(ConnectionStatusComponent, config);
  });
});
