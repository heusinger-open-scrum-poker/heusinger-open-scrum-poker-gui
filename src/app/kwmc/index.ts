export type InlineAlertType = "info" | "success" | "warning" | "danger" | "critical"
