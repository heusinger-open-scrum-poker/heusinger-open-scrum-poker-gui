import { Component, Input } from "@angular/core";
import { InlineAlertType } from ".";

export const ICON_MAP = {
  critical: "settings-6-fill",
  success: "checkbox-circle-fill",
  warning: "error-warning-fill",
  danger: "alert-fill",
  info: "information-2-fill",
} as const;

@Component({
  selector: "app-inline-alert",
  templateUrl: "./inline-alert.component.html",
  styleUrls: ["./inline-alert.component.scss"],
  host: {
    "[class.info]": "type === 'info'",
    "[class.success]": "type === 'success'",
    "[class.warning]": "type === 'warning'",
    "[class.danger]": "type === 'danger'",
    "[class.critical]": "type === 'critical'",
  },
})
export class InlineAlertComponent {
  @Input() type: InlineAlertType = "info";
  @Input() title: string = "";
  @Input() description: string = "";


  get iconName(): (typeof ICON_MAP)[InlineAlertType] {
    return ICON_MAP[this.type];
  }
}
