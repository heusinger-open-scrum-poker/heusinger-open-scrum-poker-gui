import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-small-device-disclaimer",
  templateUrl: "./small-device-disclaimer.component.html",
  styleUrls: ["./small-device-disclaimer.component.scss"],
})
export class SmallDeviceDisclaimerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
