import { MountConfig } from "cypress/angular";
import { TranslateTestingModule } from "ngx-translate-testing";
import { SmallDeviceDisclaimerComponent } from "./small-device-disclaimer.component";
import en_translation from "../../../assets/i18n/en.json";

describe("SmallDeviceDisclaimerComponent", () => {
  const config: MountConfig<SmallDeviceDisclaimerComponent> = {
    imports: [TranslateTestingModule.withTranslations("en", en_translation)],
  };

  it("mounts", () => {
    cy.mount(SmallDeviceDisclaimerComponent, config);
  });
});
