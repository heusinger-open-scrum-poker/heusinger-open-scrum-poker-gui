import { Component, Input } from "@angular/core";

@Component({
  selector: "app-new-badge",
  styleUrl: "./new-badge.component.scss",
  templateUrl: "./new-badge.component.html",
})
export class NewBadgeComponent {
  @Input("label") label: string = "";
}
