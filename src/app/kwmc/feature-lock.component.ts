import { Component, Input, booleanAttribute } from "@angular/core";

@Component({
  selector: "app-feature-lock",
  templateUrl: "./feature-lock.component.html",
  styleUrls: ["./feature-lock.component.scss"],
  host: {
    "[class.locked]": "!unlocked",
    "[class.unlocked]": "unlocked",
  },
})
export class FeatureLockComponent {
    @Input({ transform: booleanAttribute }) unlocked: boolean = false;
    @Input({ transform: booleanAttribute }) noIcon: boolean = false;
    @Input() text?: string;
}
