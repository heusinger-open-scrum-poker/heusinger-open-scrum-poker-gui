import { Component, Input } from "@angular/core";

@Component({
  selector: "app-notification",
  templateUrl: "./notification.component.html",
  styleUrls: ["./notification.component.scss"],
})
export class NotificationComponent {
  @Input() type?: "ok" | "info" | "error" | "warning" | "pending" = "info";
  @Input() title?: string = "";
  @Input() message: string = "";
}
