import { MountConfig } from "cypress/angular";
import { NotificationComponent } from "./notification.component";

describe("NotificationComponent", () => {
  let component: NotificationComponent;

  it("mounts", () => {
    cy.mount(NotificationComponent, {});
  });

  it("shows the content", () => {
    cy.mount(NotificationComponent, {
      componentProperties: {
        title: "some title",
        message: "some message",
      }
    });

    cy.contains("some title");
    cy.contains("some message");
  });

  it("is of type success", () => {
    cy.mount(NotificationComponent, {
      componentProperties: {
        type: "ok"
      }
    });

    cy.get('.notification')
      .should('have.class', 'type-ok')
      .should('not.have.class', 'type-info')
      .should('not.have.class', 'type-warning')
      .should('not.have.class', 'type-error');

    cy.get('app-icon')
      .should('have.class', 'type-ok')
      .should('not.have.class', 'type-info')
      .should('not.have.class', 'type-warning')
      .should('not.have.class', 'type-error')
  });
});

