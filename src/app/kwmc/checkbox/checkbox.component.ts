import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "app-checkbox",
  templateUrl: "./checkbox.component.html",
  styleUrls: ["./checkbox.component.scss"],
  host: {
    "(click)": "toggle()",
  },
})
export class CheckboxComponent {
  @Input() label: string = "Example Label";

  @Input() checked: boolean = false;
  @Output() checkedChange: EventEmitter<boolean> = new EventEmitter();

  protected toggle() {
    this.checked = !this.checked;
    this.checkedChange.emit(this.checked);
  }
}
