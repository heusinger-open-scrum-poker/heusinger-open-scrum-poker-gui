import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import {
  Advertisement,
} from "src/app/schemas/Advertisement";
import ADVERTISEMENTS from "src/assets/ads.json";
import { FunnelEventService } from "../../funnel-event.service";

@Component({
  selector: "app-ad-rotation",
  templateUrl: "./ad-rotation.component.html",
  styleUrls: ["./ad-rotation.component.scss"],
})
export class AdRotationComponent implements OnDestroy, OnInit {
  @Input() changeAd!: Observable<unknown>;

  ad: Advertisement;

  private subscription: Subscription | undefined;

  constructor(private funnelEventService: FunnelEventService) {
    this.ad = this.getRandomAdvertisement();
  }

  ngOnInit(): void {
    this.subscription = this.changeAd.subscribe(
      () => (this.ad = this.getRandomAdvertisement()),
    );
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  private getRandomAdvertisement(): Advertisement {
    const jsonAd =
      ADVERTISEMENTS[Math.floor(ADVERTISEMENTS.length * Math.random())];
    const ad = Advertisement.safeParse(jsonAd);

    if (ad.success) {
      return ad.data;
    } else {
      throw new Error("Ad did not conform to schema: " + JSON.stringify(jsonAd));
    }
  }

  protected handleClick() {
    this.funnelEventService.trackAdClickEvent(this.ad);
  }
}
