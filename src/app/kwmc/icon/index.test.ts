import { describe, it, expect } from 'vitest';
import { SIZE_REGEX } from '.';

describe("size regex", () => {
  it("matches various size specifications correctly", () => {
    function testRegex(input: string) {
      const match = SIZE_REGEX.exec(input);
      return { input, value: match?.[1], unit: match?.[3] };
    }

    expect([
      "0px",
      "1px",
      "12 px",
      " 12.8 rem ",
      " 0.8em ",
      "0",
      "17",
      ".8 rem",
      "1. rem",
    ].map(testRegex))
    .toMatchSnapshot()
  })
})
