import { Component, Input } from "@angular/core";
import { SIZE_REGEX } from ".";

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent {
  @Input() name: string = "alert-circle";
  @Input() viewBox: string = "0 0 24 24";
  @Input() size: string = "12px";
  @Input() aspectRatio: number = 1;

  get width() {
    const match = SIZE_REGEX.exec(this.size);

    if (match) {
      const [_match, value, _decimals, unit] = match
      return `${+value * this.aspectRatio}${unit ?? ""}`;
    }

    return "";
  }

  get height() {
    return this.size;
  }
}
