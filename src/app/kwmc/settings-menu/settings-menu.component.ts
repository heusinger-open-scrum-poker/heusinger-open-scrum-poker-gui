import {
  Component,
  ElementRef,
  Input,
  ViewChild,
  OnDestroy,
  AfterViewInit,
  HostListener,
} from "@angular/core";
import { autoUpdate, computePosition, flip, size } from "@floating-ui/dom";
@Component({
  selector: "app-settings-menu",
  templateUrl: "./settings-menu.component.html",
  styleUrls: ["./settings-menu.component.scss"],
})
export class SettingsMenuComponent implements OnDestroy, AfterViewInit {
  @ViewChild("reference") reference!: ElementRef;
  @ViewChild("floating") floating!: ElementRef;

  @Input() icon: string = "";
  @Input() size: string = "";
  @Input() disabled: boolean = false;

  protected isOpen = false;

  private cleanupFloatingUi: () => void = () => {};

  constructor(private self: ElementRef) {}

  public toggleShowOverflowItems() {
    if (this.disabled) {
      this.isOpen = false;
    } else {
      this.isOpen = !this.isOpen;
    }
  }

  @HostListener("document:click", ["$event"])
  onClickOutside(event: Event): void {
    if (!this.isOpen) {
      return;
    }

    const clickedInside = this.self.nativeElement.contains(event.target);
    if (!clickedInside) {
      this.close();
    }
  }

  ngAfterViewInit(): void {
    const floating = this.floating.nativeElement;
    const reference = this.reference.nativeElement;

    const update = () => {
      computePosition(reference, floating, {
        placement: "bottom-start",
        middleware: [
          flip(),
          size({
            apply({ availableWidth, availableHeight }) {
              Object.assign(floating.style, {
                maxWidth: `${availableWidth}px`,
                maxHeight: `${availableHeight}px`,
              });
            },
          }),
        ],
      }).then(({ x, y }) => {
        Object.assign(floating.style, {
          left: `${x}px`,
          top: `${y}px`,
        });
      });
    };

    this.cleanupFloatingUi = autoUpdate(
      this.reference.nativeElement,
      this.floating.nativeElement,
      update,
    );
  }

  ngOnDestroy(): void {
    this.cleanupFloatingUi();
  }

  public close(): void {
    this.isOpen = false;
  }
}
