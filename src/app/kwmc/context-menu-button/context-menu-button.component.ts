import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
} from "@angular/core";

@Component({
  selector: "app-context-menu-button",
  templateUrl: "./context-menu-button.component.html",
  styleUrls: ["./context-menu-button.component.scss"],
  host: {
    "[class.disabled]": "disabled",
  },
})
export class ContextMenuButtonComponent {

  @Input("disabled") disabled: boolean = false;

  @Output("toggle") toggle: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    public self: ElementRef,
  ) {}

  @HostListener('click')
  handleClick(): void {
    if (this.disabled) {
      return;
    }

    this.toggle.emit();
  }

}
