import { Component, EventEmitter, Input, Output } from "@angular/core";
import { InlineAlertType } from ".";

@Component({
  selector: "app-inline-alert2",
  templateUrl: "./inline-alert2.component.html",
  styleUrls: ["./inline-alert2.component.scss"],
  host: {
    "[class.info]": "type === 'info'",
    "[class.success]": "type === 'success'",
    "[class.warning]": "type === 'warning'",
    "[class.danger]": "type === 'danger'",
    "[class.critical]": "type === 'critical'",
  },
})
export class InlineAlert2Component {
  @Input() type: InlineAlertType = "info";
  @Input() title: string = "";
  @Input() description: string = "";
  @Input() actionLabel: string = "";
  @Input() closable: boolean = true;
  @Output("close") closeEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output("action") actionEmitter: EventEmitter<void> = new EventEmitter<void>();
}
