import { EventHandler, createFirebaseEvents } from "@kehrwasser-dev/features";
import { initializeApp } from "firebase/app";
import { collection, getFirestore } from "firebase/firestore";
import { UserService } from "./app/user.service";
import { environment } from "./environments/environment";

const firebaseConfig = {
  apiKey: "AIzaSyCYLxbsVK5Nb-POkEAiWIVJkCY7KKaGRGI",
  authDomain: "leads-c175e.firebaseapp.com",
  projectId: "leads-c175e",
  storageBucket: "leads-c175e.appspot.com",
  messagingSenderId: "118305514017",
  appId: "1:118305514017:web:90f06dc6a4ebbf4d94b960",
  measurementId: "G-2Z9JWHNKZ9"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const NULL_EVENT_HANDLER: EventHandler = {
    handleSplitTestEvent: async function(): Promise<void> {
      return;
    },
    handleSmokeTestEvent: async function(): Promise<void> {
      return;
    },
}


if (!environment.firebaseDocument) {
  console.error("No Firebase document defined, disabling Firebase analytics events...");
}

export function FIREBASE_EVENTS(userService: UserService): EventHandler {

  if (!environment.firebaseDocument) {
    return NULL_EVENT_HANDLER;
  }

  const baseCollection = collection(db, environment.firebaseDocument);

  return createFirebaseEvents(baseCollection, {
    makeSplitTestPayload: function(feature: string, variant: string, payload: object): object {
      const user = userService.getUser();

      return {
        ...payload,
        feature,
        variant,
        user: user?.uuid,
        timestamp: new Date(),
        host: location.host,
        userAgent: navigator.userAgent,
        viewportHeight: visualViewport?.height,
        viewportWidth: visualViewport?.width,
      };
    },
    makeSmokeTestPayload: function(feature: string, payload: object): object {
      const user = userService.getUser();
      return {
        ...payload,
        feature,
        user: user?.uuid,
        timestamp: new Date(),
        host: location.host,
        userAgent: navigator.userAgent,
        viewportHeight: visualViewport?.height,
        viewportWidth: visualViewport?.width,
      };
    },
  });
};
