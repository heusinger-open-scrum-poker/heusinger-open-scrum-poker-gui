import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";
import { WindowEnv } from "./environments/types";

declare global {
  interface Window {
    adsbygoogle?: { loaded: boolean; push: (...args: unknown[]) => void };
    dataLayer?: { push: (...args: unknown[]) => void };
    gtag?: (...args: unknown[]) => void;
    env: WindowEnv;
  }
}

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));
