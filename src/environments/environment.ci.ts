import { Environment, WindowEnv } from "./types";

export const environment: Environment = {
  ...WindowEnv.parse(window.env),
  production: true,
};
