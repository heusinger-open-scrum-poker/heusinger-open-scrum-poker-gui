// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { Environment } from "./types";

export const environment: Environment = {
  production: false,
  guiBaseUrl: "http://localhost:4200",
  apiBaseUrl: "http://localhost:9999",
  brokerBaseUrl: "ws://localhost:9999",
  jiraClientId: "90H1CffSLuvIKzKflw1h2fcJOJv9L5h9",
  footerVisible: true,
  directCardSelection: false,
  loginFeature: true,
  taskFeature: true,
  jiraFeature: true,
  aiFeature: true,
  newStatistics: true,
  newTaskModal: true,
  newSidebar: true,
  gameFeature: true,
  taskExportFeature: true,
  showAverage: true,
  firebaseDocument: "test",
  statisticsFeature: true,
  experiment203: false,
  experiment204: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
