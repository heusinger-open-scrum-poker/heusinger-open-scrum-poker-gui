import { z } from "zod";

const FeatureFlag = z.string().optional().transform((flag) => flag === "true");

export type WindowEnv = z.input<typeof WindowEnv>;
export const WindowEnv = z.object({
  apiBaseUrl: z.string(),
  guiBaseUrl: z.string(),
  brokerBaseUrl: z.string(),
  jiraClientId: z.string(),
  footerVisible: FeatureFlag,
  directCardSelection: FeatureFlag,
  loginFeature: FeatureFlag,
  taskFeature: FeatureFlag,
  jiraFeature: FeatureFlag,
  aiFeature: FeatureFlag,
  newStatistics: FeatureFlag,
  newTaskModal: FeatureFlag,
  newSidebar: FeatureFlag,
  gameFeature: FeatureFlag,
  taskExportFeature: FeatureFlag,
  showAverage: FeatureFlag,
  firebaseDocument: z.string(),
  statisticsFeature: FeatureFlag,
  experiment203: FeatureFlag,
  experiment204: FeatureFlag,
});

export type Environment = z.infer<typeof Environment>;
export const Environment = WindowEnv.extend({
  production: z.boolean(),
});
