# AgileCasino

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.7.

## Environment Variables

```
GUI_BASE_URL=<frontend url>                                   (required)
API_BASE_URL=<backend url>                                    (required)
BROKER_BASE_URL=<backend websocket url>                       (required)
JIRA_CLIENT_ID=<oauth client id for your jira app>            (required)
FOOTER_VISIBLE=<'false' or 'true'>                            (optional)
DIRECT_CARD_SELECTION=<'false' or 'true'>                     (optional)
LOGIN_FEATURE=<'false' or 'true'>                             (optional)
TASK_FEATURE=<'false' or 'true'>                              (optional)
JIRA_FEATURE=<'false' or 'true'>                              (optional)
AI_FEATURE=<'false' or 'true'>                                (optional)
NEW_STATISTICS=<'false' or 'true'>                            (optional)
NEW_TASK_MODAL=<'false' or 'true'>                            (optional)
NEW_SIDEBAR=<'false' or 'true'>                               (optional)
TASK_EXPORT_FEATURE=<'false' or 'true'>                       (optional)
SHOW_AVERAGE=<'false' or 'true'>                              (optional)
FIREBASE_DOCUMENT=<firebase document name>                    (required)
STATISTICS_FEATURE=<'false' or 'true'>                        (defaults to 'false')
EXPERIMENT_203=<'false' or 'true'>                            (defaults to 'false')
EXPERIMENT_204=<'false' or 'true'>                            (defaults to 'false')
```


### How to add a feature flag?

1. Extend the `WindowEnv` schema in `src/environments/types.ts`.
2. The type checker should now require you to add a default value in `src/environment/environment.ts`.
3. Extend `src/assets/env.js` and `src/assets/env.template.js`
4. Add the environment variable to the cypress e2e test in `.gitlab-ci.yml`, by adding a `--env` flag to with appropriate argument to the `envsub` command.
5. Document the environment variable used in `env.template.js` in the README, under [Environment Variables](#environment-variables)


## Continuous Deployment Guide

"Schnell scheitern. Schnell lernen."

> The goal of continuous deployment is to release applications (and business value) to end users faster and more cost-effectively by managing small, incremental software changes.

1. Make sure useful progress outputs often
2. Not work faster, but in smaller batches
3. Small is beautiful: It can be debugged faster and value arrives faster at the user
4. If something small is useful and production ready, push it to `master`
5. Only push finished, potentially releasable stuff to master
6. Break down features in daily ready tasks
7. Automatic testing happens on each commit on `master`
8. Passing code is considered as an valid increment and thus a release candidate
9. It is automatically deployed to the staging environment (dockerized as `:unstable` and deployed on Digital Ocean)
10. For a feature to be considered as done, it must be successfully reviewed on staging
11. On staging validated commits get a tag
12. Tags are deployed to production - a simple confirmation is needed (dockerized as the tag and as `:latest` and deployed on Digital Ocean)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

To build the project, first install the NPM dependencies

```sh
npm ci
```

Then run the build script

```sh
npm run build
```

On Linux systems you might need to pass an option for node

```sh
env NODE_OPTIONS=--openssl-legacy-provider npm run build
```

For a production build you can pass the `--prod` flag

```sh
npm run build -- --prod
```

The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm test` to execute the unit tests via [Vitest](https://vitest.dev/).

## Running component tests

Run `env LANG="en_EN.UTF-8" npx cypress run --browser chrome --component` to execute the component tests via [Cypress](https://www.cypress.io/).

## Running end-to-end tests

Run `env LANG="en_EN.UTF-8" npx cypress run --browser chrome --e2e` to execute the end-to-end tests via [Cypress](https://www.cypress.io/).
An instance of Agile Casino must run, with the frontend accessible via `http://localhost:4200`.
For example, this can be accomplished by starting up backend and database with `docker-compose up -d backend dbmysql` and running the frontend via `npx ng run`.

## Languages

Covering german and english. Using ngx-translate. Translation files can be found src/assets/i18n/{de,en}.json.
Instructions can be seen here [Code And Web] (https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-angular-app-with-ngx-translate). Extraction can be done via the therein mentioned ngx-translate-extract. Due to issues with the version of @biesbjerg/ngx-translate-extract, switched to @bartholomej/ngx-translate-extract.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Gameplay

The user can choose between either creating a new room or joining an existing room by entering an existing room number.
Once a room is entered, the user gets his card presented.
By clicking on a card, the card is highlighted. The cardValueTemp will be set to the value of the chosen card.
The user can now place the card ("Place Card" - Button) to the table, which will make his card of choice (set the cardValue equal to the cardValueTemp).
As soon as one player made a confirmed choice the round can be revealed. In case the user wants to switch cards, he can take it back ("Take Back" - Button) and choose an alternative one and confirm this new one, but only unless the reveal has not been triggered by any player.
In the revealed state the card values of every player can be seen.

Pressing "New Round" starts a new round for the whole room.
Every member starts to choose again...
