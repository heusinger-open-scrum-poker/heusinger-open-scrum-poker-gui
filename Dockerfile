FROM httpd:2.4-alpine


RUN apk add --no-cache gettext libcap procps \
  && setcap 'cap_net_bind_service=+ep' /usr/local/apache2/bin/httpd

RUN adduser -D -u 1500 nonroot
COPY ./dist/agile-casino/ /home/nonroot/htdocs/
# URL Rewriting
COPY ./deployment/httpd.custom.conf /usr/local/apache2/conf/httpd.conf
COPY ./deployment/.htaccess /home/nonroot/htdocs/.htaccess
RUN chown -R nonroot:nonroot /home/nonroot/htdocs/ \
    && chown -R nonroot:nonroot /usr/local/apache2

# non-root user privilege
USER nonroot

# When the container starts, replace the env.js with values from environment variables
ENTRYPOINT ["/bin/sh",  "-c",  "envsubst < /home/nonroot/htdocs/assets/env.template.js > /home/nonroot/htdocs/assets/env.js && apachectl -D FOREGROUND"]
